package com.xiledsystems.aal.ads;

import android.app.Activity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.xiledsystems.aal.ads.google.BuildConfig;
import com.xiledsystems.aal.ads.base.BaseFullScreenAd;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;

@State(DevelopmentState.PRODUCTION)
public class FullScreenAd extends BaseFullScreenAd {

    private InterstitialAd ad;
    private boolean adLoaded = false;
    private AdRequest.Builder requestBuilder;


    public FullScreenAd(Activity context, String appKey, com.xiledsystems.aal.ads.AdInterface adHandler) {
        super(context, appKey, adHandler);
    }

    @Override
    public void onCreate(Activity context, String appKey) {
        ad = new InterstitialAd(context);
        ad.setAdListener(new GAdListener());
        ad.setAdUnitId(appKey);
        requestBuilder = new AdRequest.Builder();
    }

    @Override
    public void loadAd() {
        ad.loadAd(requestBuilder.build());
    }

    @Override
    public void showAd() {
        if (adLoaded) {
            try {
                ad.show();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
                if (getAdHandler() != null) {
                    getAdHandler().adError("General Exception caught: " + e.getMessage());
                }
            }
        } else {
            if (getAdHandler() != null) {
                getAdHandler().adError("Ad is not loaded yet.");
            }
        }
    }

    @Override
    public boolean isAdLoaded() {
        return adLoaded;
    }

    @Override
    public void onDestroy() {
        ad = null;
    }

    @Override
    public void addTestDevice(String device) {
        if (requestBuilder == null) {
            requestBuilder = new AdRequest.Builder();
        }
        requestBuilder.addTestDevice(device);
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    private class GAdListener extends AdListener {
        @Override
        public void onAdFailedToLoad(int errorCode) {
            if (getAdHandler() != null) {
                getAdHandler().adLoadingError("Failed to load, Admob Error Code: " + errorCode);
            }
        }

        @Override
        public void onAdClosed() {
            super.onAdClosed();
            if (getAdHandler() != null) {
                getAdHandler().adClosed();
            }
        }

        @Override
        public void onAdLoaded() {
            super.onAdLoaded();
            adLoaded = true;
            if (getAdHandler() != null) {
                getAdHandler().adLoaded();
            }
        }
    }

}