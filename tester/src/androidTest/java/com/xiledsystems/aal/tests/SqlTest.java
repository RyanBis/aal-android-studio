package com.xiledsystems.aal.tests;


import android.content.ContentValues;
import android.database.Cursor;

import com.xiledsystems.aal.sql.Column;
import com.xiledsystems.aal.sql.DBConfig;
import com.xiledsystems.aal.sql.DBObject;
import com.xiledsystems.aal.sql.DBObjectFactory;
import com.xiledsystems.aal.sql.DataType;
import com.xiledsystems.aal.sql.Query;
import com.xiledsystems.aal.sql.ResultList;
import com.xiledsystems.aal.sql.SqlDb;
import com.xiledsystems.aal.sql.Table;
import com.xiledsystems.aal.util.Rand;
import com.xiledsystems.aal.util.StopWatch;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;


public class SqlTest extends MainTest {

    @Test
    public void addColumnTest() throws Exception {
        DBConfig config = new DBConfig();
        config.addTable(new Table("bands").addColumn("name"));
        SqlDb db = new SqlDb(activity, config);
        db.initialize();

        db.startBatch();
        for (int i = 0; i < 200; i++) {
            db.insert("bands", Rand.getRandomString(25));
        }

        db.endBatch();

        db.addColumnToExistingTable("bands", new Column("albums", DataType.INTEGER), "2");

        ArrayList<String> names = db.getColumnNames("bands");
        assertTrue(names.contains("albums"));

        Query q = new Query().select().from("bands");
        ResultList list = q.execute(db);
        assertFalse(list.isEmpty());
        assertTrue(list.get(0).getInt("albums") == 2);
        db.closeDb();
    }

    @Test
    public void foreignKeyTest() throws Exception {
        DBConfig config = new DBConfig();
        Table customers = new Table("customer").addColumn("name");
        config.addTable(customers);
        Table reps = new Table("reps").addColumn("name");
        config.addTable(reps);
        Table orders = new Table("orders").addColumn(new Column("customer_id", DataType.INTEGER).setForeignKey(customers));
        orders.addColumn(new Column("rep_id", DataType.INTEGER).setForeignKey(reps));
        config.addTable(orders);
        SqlDb db = new SqlDb(activity, config);
        db.initialize();

        long cust1Id;
        long rep2Id;

        ContentValues val = new ContentValues();
        val.put("name", "joe customer 1");
        cust1Id = db.insert(customers.getName(), val);
        val.clear();
        val.put("name", "bob customer 2");
        db.insert(customers.getName(), val);

        val.clear();
        val.put("name", "Jack Rep 1");
        db.insert(reps.getName(), val);
        val.clear();
        val.put("name", "Will Rep 2");
        rep2Id = db.insert(reps.getName(), val);

        val.clear();
        val.put("customer_id", cust1Id);
        val.put("rep_id", rep2Id);
        db.insert(orders.getName(), val);
        db.closeDb();
    }

    @Test
    public void tableNameTest() throws Exception {
        DBConfig config = new DBConfig();
        config.addTable(new Table("first").addColumn("name").addColumn("age", DataType.INTEGER).addColumn("weight", DataType.FLOAT).
                            addColumn("data", DataType.BLOB).addColumn(new Column("uniqueNumber").setIsUnique(true)));
        SqlDb db = new SqlDb(activity, config);
        db.initialize();
        String path = db.getDBPath();
        db.closeDb();
        db = new SqlDb(activity);
        db.openDBFile(path);
        DBConfig genConfig = db.getDbConfig();
        assertTrue(genConfig.equals(config));
        db.closeDb();
    }

    @Test
    public void dbConfigAddTableTest() throws Exception {
        DBConfig config = new DBConfig();
        Table first = new Table("first");
        first.addColumns("name", "address");
        config.addTable(first);
        SqlDb db = new SqlDb(activity, config);
        db.initialize();
        ArrayList<String> tableNames = db.getTableNames();
        assertTrue(tableNames.contains("first"));
        Table second = new Table("second").addColumn("email");
        config.addTable(second);
        db.setDBConfig(config);
        tableNames = db.getTableNames();
        assertTrue(tableNames.contains("second"));
        db.closeDb();
    }

    @Test
    public void simpleDbObjectTest() throws Exception {
        DBConfig config = new DBConfig();
        Table test = new Table(SqlDbTestObject.TABLE_NAME);
        test.addColumns(SqlDbTestObject.NAME, SqlDbTestObject.AGE, SqlDbTestObject.CREATED);
        test.addColumn(SqlDbTestObject.DATA, DataType.BLOB);
        config.addTable(test);
        SqlDb db = new SqlDb(activity, config);
        SqlDbTestObject ob1 = new SqlDbTestObject();
        ob1.timeCreated = System.currentTimeMillis();
        ob1.name = "Joe Shmoe";
        ob1.age = 50;
        ob1.data = new byte[] { 87, 71, 81, 58, 30 };
        ob1.insert(db);
        Query query = new Query();
        query.select().from(SqlDbTestObject.TABLE_NAME);
        ArrayList<SqlDbTestObject> list = query.getObjectList(db, SqlDbTestObject.factory);
        assertTrue(list.size() > 0);
        SqlDbTestObject ob2 = list.get(0);
        assertTrue(ob1.equals(ob2));
        db.closeDb();
    }

    @Test
    public void objectSingleTimeTest() throws Exception {
        DBConfig config = new DBConfig();
        config.addTable("some_table", new String[] { "name", "age", "arrays" } );
        SqlDb db = new SqlDb(activity, config);
        db.initialize();
        Thing2 t = new Thing2();
        t.name = "John";
        t.age = 20;
        t.array = new byte[128];
        t.addData("name", t.name);
        t.addData("age", t.age);
        t.addData("arrays", t.array);
        StopWatch timer = new StopWatch();
        timer.start();
        t.insertWithStatement(db);
        long insertTime = timer.stop();
        timer.start();
        Query q = new Query().select().from("some_table");
        List<Thing2> list = q.getObjectList(db, new DBObjectFactory<Thing2>() {
            @Override
            public Thing2 newObject(Cursor cursor) {
                return new Thing2(cursor);
            }
        });
        long loadTime = timer.stop();
        int i = 0;
        i++;
    }

    @Test
    public void objectMultipleTimeTest() throws Exception {
        DBConfig config = new DBConfig();
        Table table = new Table("some_table");
        table.addColumns("name", "age", "dub", "floats", "longs", "shorts", "bytes", "bools", "arrays");
        config.addTable(table);
        SqlDb db = new SqlDb(activity, config);
        db.initialize();
        db.startBatch();
        StopWatch timer = new StopWatch();
        timer.start();
        byte[] bytes = Rand.randomBytes(6);
        for (int i = 0; i < 5000; i++) {
            Thing2 t = new Thing2();
            t.name = "John";
            t.age = 20;
            t.array = bytes.clone();
            t.bool = true;
            t.byt = 0x58;
            t.dub = 3.14;
            t.flt = 1.1f;
            t.lng = 80239895984L;
            t.sht = 4000;
            t.addData("name", t.name);
            t.addData("age", t.age);
            t.addData("dub", t.dub);
            t.addData("floats", t.flt);
            t.addData("longs", t.lng);
            t.addData("shorts", t.sht);
            t.addData("bytes", t.byt);
            t.addData("bools", t.bool);
            t.addData("arrays", t.array);
            t.insertWithStatement(db);
        }
        db.endBatch();
        long insertTime = timer.stop();
        timer.start();
        Query q = new Query().select().from("some_table");
        List<Thing2> list = q.getObjectList(db, new DBObjectFactory<Thing2>() {
            @Override
            public Thing2 newObject(Cursor cursor) {
                return new Thing2(cursor);
            }
        });
        long loadTime = timer.stop();
        int i = 0;
        i++;
    }

    private class Thing2 extends DBObject {

        private String name;
        private int age;
        private double dub;
        private float flt;
        private Long lng;
        private short sht;
        private Byte byt;
        private boolean bool;
        private byte[] array;


        Thing2() {
            super();
        }

        Thing2(Cursor c) {
            super(c);
        }

        @Override
        public void loadFromCursor(Cursor c) {
            name = getCursorString("name", c);
            age = getCursorInt("age", c);
            dub = getCDouble("dub", c);
            flt = getCFloat("floats", c);
            lng = getCursorLong("longs", c);
            sht = getCShort("shorts", c);
            byt = (byte) getCursorInt("bytes", c);
            bool = getCBoolean("bools", c);
            array = getCursorBlob("arrays", c);
        }

        private double getCDouble(String column, Cursor c) {
            int id = c.getColumnIndex(column);
            if (id != -1) {
                return c.getDouble(id);
            }
            return 0.0;
        }

        private short getCShort(String column, Cursor c) {
            int id = c.getColumnIndex(column);
            if (id != -1) {
                return c.getShort(id);
            }
            return 0;
        }

        private boolean getCBoolean(String column, Cursor c) {
            int id = c.getColumnIndex(column);
            if (id != -1) {
                return c.getInt(id) == 1;
            }
            return false;
        }

        private float getCFloat(String column, Cursor c) {
            int id = c.getColumnIndex(column);
            if (id != -1) {
                return c.getFloat(id);
            }
            return 0.0f;
        }

        @Override
        public String getTableName() {
            return "some_table";
        }

        @Override
        public ContentValues getCreateValues() {
            ContentValues vals = new ContentValues();
            vals.put("name", name);
            vals.put("age", age);
            vals.put("dub", dub);
            vals.put("floats", flt);
            vals.put("longs", lng);
            vals.put("shorts", sht);
            vals.put("bytes", byt);
            vals.put("bools", bool);
            vals.put("arrays", array);
            return vals;
        }
    }

}
