package com.xiledsystems.aal.tests;


import android.util.Log;

import com.xiledsystems.aal.util.StopWatch;
import com.xiledsystems.aal.util.ThreadManager;

import org.junit.Test;

import java.util.concurrent.Semaphore;

public final class ThreadTest extends MainTest {

    private StopWatch timer;
    private long newThreadTime;
    private long threadPoolTime;
    private long asyncTime;


    @Test
    public final void doThreadTests() throws Exception {
        Semaphore s = new Semaphore(0);
        timer = new StopWatch();
        timer.start();
        doNewThreadTest(s);
        s.acquire();

        timer.start();
        doThreadPoolTest(s);
        s.acquire();

        Log.e("ThreadTest", "New Thread Time: " + newThreadTime + "\nThread Pool Time: " + threadPoolTime + "\nAsync Time: " + asyncTime);
    }

    private void doNewThreadTest(final Semaphore s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                newThreadTime = timer.stop();
                s.release();
            }
        }).start();
    }

    private void doAsyncTest(final Semaphore s) {
        ThreadManager.async(new Runnable() {
            @Override
            public void run() {
                asyncTime = timer.stop();
                s.release();
            }
        });
    }

    private void doThreadPoolTest(final Semaphore s) {
        ThreadManager.getInstance(true).execute(new Runnable() {
            @Override
            public void run() {
                threadPoolTime = timer.stop();
                s.release();
            }
        });
    }

}
