package com.xiledsystems.aal.tests;


import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import com.xiledsystems.aal.util.DataStore;
import com.xiledsystems.aal.util.Rand;
import com.xiledsystems.aal.util.ThreadManager;
import org.junit.Test;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


public class DataStoreTest extends MainTest {


    public static final String TEXT_ALLOW = "Allow";
    public static final String TEXT_DENY = "Deny";
    public static final String TEXT_NEVER_ASK_AGAIN = "Never ask again";

    private UiDevice device;

    @Override
    public void setup() {
        super.setup();
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    public static void assertViewWithTextIsVisible(UiDevice device, String text) {
        UiObject allowButton = device.findObject(new UiSelector().text(text));
        if (!allowButton.exists()) {
            throw new AssertionError("View with text <" + text + "> not found!");
        }
    }

    public static void allowCurrentPermission(UiDevice device) throws UiObjectNotFoundException {
        UiObject allowButton = device.findObject(new UiSelector().text(TEXT_ALLOW));
        allowButton.click();
    }

    public static void denyCurrentPermission(UiDevice device) throws UiObjectNotFoundException {
        UiObject denyButton = device.findObject(new UiSelector().text(TEXT_DENY));
        denyButton.click();
    }

    public static void denyCurrentPermissionPermanently(UiDevice device) throws UiObjectNotFoundException {
        UiObject neverAskAgainCheckbox = device.findObject(new UiSelector().text(TEXT_NEVER_ASK_AGAIN));
        neverAskAgainCheckbox.click();
        denyCurrentPermission(device);
    }

    public static void grantPermission(UiDevice device, String permissionTitle) throws UiObjectNotFoundException {
        UiObject permissionEntry = device.findObject(new UiSelector().text(permissionTitle));
        permissionEntry.click();
    }

    @Test
    public void putAndGetListTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        ThreadManager.getInstance(true).execute(new Runnable() {
            @Override
            public void run() {
                final DataStore store = new DataStore(activity);
                if (!store.hasPermissions()) {
                    store.requestPermission(new DataStore.PermissionListener() {
                        @Override
                        public void permissionsResult(boolean gotPermissions) {
                            assertTrue(gotPermissions);
                            doPutAndGetTests(store);
                            release(s);
                        }
                    });
                    ThreadManager.getInstance(true).execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(500);
                                assertViewWithTextIsVisible(device, TEXT_ALLOW);
                                assertViewWithTextIsVisible(device, TEXT_DENY);
                                allowCurrentPermission(device);
                            } catch (Exception e) {
                            }

                        }
                    });
                } else {
                    doPutAndGetTests(store);
                    release(s);
                }
            }
        });
        s.acquire();
    }

    private void doPutAndGetTests(DataStore store) {
        doInternalTest(store);
        doCacheTest(store);
        doExternalTest(store);
    }

    private void doInternalTest(DataStore store) {
        ArrayList<String> list = getRandomList();
        store.put("stringlist", list);

        ArrayList<String> savedList = store.get("stringlist");
        assertTrue(list.equals(savedList));

        ArrayList<String> list2 = getRandomList();
        DataStore.put(activity, "stringlist2", list2);
        ArrayList<String> savedList2 = DataStore.get(activity, "stringlist2");
        assertTrue(list2.equals(savedList2));

        assertFalse(list.equals(list2));
        assertFalse(savedList.equals(savedList2));
    }

    private void doCacheTest(DataStore store) {
        ArrayList<String> list = getRandomList();
        store.putToCache("stringlist", list);

        ArrayList<String> savedList = store.getFromCache("stringlist");
        assertTrue(list.equals(savedList));

        ArrayList<String> list2 = getRandomList();
        DataStore.putToCache(activity, "stringlist2", list2);
        ArrayList<String> savedList2 = DataStore.getFromCache(activity, "stringlist2");
        assertTrue(list2.equals(savedList2));

        assertFalse(list.equals(list2));
        assertFalse(savedList.equals(savedList2));
    }

    private void doExternalTest(DataStore store) {
        ArrayList<String> list = getRandomList();
        store.putToExternal("stringlist", list);

        ArrayList<String> savedList = store.getFromExternal("stringlist");
        assertTrue(list.equals(savedList));
    }

    private static ArrayList<String> getRandomList() {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < Rand.rndInt(10, 25); i++) {
            strings.add(Rand.getRandomString(Rand.rndInt(24,64)));
        }
        return strings;
    }

}
