package com.xiledsystems.aal.tests;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.tester.EventActivity;
import com.xiledsystems.aal.util.DeviceUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import java.util.concurrent.Semaphore;


@RunWith(AndroidJUnit4.class)
@LargeTest
public abstract class MainTest {

    @Rule
    public ActivityTestRule<EventActivity> mRule = new ActivityTestRule<>(EventActivity.class);

    EventActivity activity;
    EventManager eventManager;


    @Before
    public void setup() {
        activity = mRule.getActivity();
    }

    public void release(Semaphore s) {
        if (eventManager != null) {
            eventManager.shutdown();
        }
        s.release();
    }

    @After
    public void cleanUp() {
        DeviceUtil.clearAppData(activity, false);
    }
}
