package com.xiledsystems.aal.tests;


import com.xiledsystems.aal.communication.HttpRequest;
import com.xiledsystems.aal.communication.RequestListener;
import com.xiledsystems.aal.communication.RequestType;
import org.json.JSONObject;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.concurrent.Semaphore;



public class HttpTest extends MainTest {

    @Test
    public void getTest() throws  Exception {
        final Semaphore s = new Semaphore(0);
        HttpRequest post = new HttpRequest(activity, RequestType.GET);
        post.setLoggingEnabled(true);
        post.setUrl("http://jsonplaceholder.typicode.com/posts/1");
        post.execute(new RequestListener() {
            @Override
            public void onResponse(Response response) {
                assertTrue(response.response(), response.wasSuccess());
                release(s);
            }
        });
        s.acquire();
    }

    @Test
    public void getTest2() throws  Exception {
        final Semaphore s = new Semaphore(0);
        HttpRequest post = new HttpRequest(activity, RequestType.GET);
        post.setLoggingEnabled(true);
        post.addUrlParameter("userId", "1").addUrlParameter("id", "2");
        post.setUrl("http://jsonplaceholder.typicode.com/posts");
        post.execute(new RequestListener() {
            @Override
            public void onResponse(Response response) {
                assertTrue(response.response(), response.wasSuccess());
                release(s);
            }
        });
        s.acquire();
    }

    @Test
    public void postTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        HttpRequest post = new HttpRequest(activity, RequestType.POST);
        post.setLoggingEnabled(true);
        JSONObject user = new JSONObject();
        user.put("userId", 11);
        user.put("id", 666);
        user.put("title", "Blah!");
        user.put("body", "This is the body.");
        post.setData(user);
        post.setUrl("http://jsonplaceholder.typicode.com/posts");
        post.execute(new RequestListener() {
            @Override
            public void onResponse(Response response) {
                assertTrue(response.responseCode() < 400);
                release(s);
            }
        });
        s.acquire();
    }

    @Test
    public void deleteTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        HttpRequest post = new HttpRequest(activity, RequestType.DELETE);
        post.setLoggingEnabled(true);
        post.setUrl("http://jsonplaceholder.typicode.com/posts/1");
        post.execute(new RequestListener() {
            @Override
            public void onResponse(Response response) {
                assertTrue(response.wasSuccess());
                release(s);
            }
        });
        s.acquire();
    }

}
