package com.xiledsystems.aal.tests;


import com.xiledsystems.aal.communication.tcp.DiscoveryListener;
import com.xiledsystems.aal.communication.tcp.FoundType;
import com.xiledsystems.aal.communication.tcp.ServerReceiver;
import com.xiledsystems.aal.communication.tcp.ServiceType;
import com.xiledsystems.aal.communication.tcp.TCPConfig;
import com.xiledsystems.aal.communication.tcp.TCPManager;
import com.xiledsystems.aal.communication.tcp.TCPPacket;
import com.xiledsystems.aal.communication.tcp.TCPPacketConverter;
import com.xiledsystems.aal.communication.tcp.TCPTarget;
import com.xiledsystems.aal.communication.tcp.TCPTargetState;
import com.xiledsystems.aal.communication.tcp.TLVConverter;
import com.xiledsystems.aal.communication.tcp.TLVLengthType;
import com.xiledsystems.aal.communication.tcp.TLVPacket;
import com.xiledsystems.aal.communication.tcp.TargetStateListener;
import com.xiledsystems.aal.util.ByteBuff;
import com.xiledsystems.aal.util.Rand;
import org.junit.Test;
import static junit.framework.Assert.assertTrue;

import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;


public class TCPSystemTest extends MainTest {


    private TCPPacketConverter converter = new TLVConverter(TLVLengthType.INT);
    private byte[] mBuffer;

    private TCPTarget mTarget;

    @Test
    public void serverTargetTest() throws Exception {

        final Semaphore s = new Semaphore(0);

        final AtomicInteger counter = new AtomicInteger(1);

        mBuffer = newDoubleTLVPacket();
        final TCPConfig config = new TCPConfig();
        config.loggingEnabled = true;
        config.serviceName = "MyTCPService";
        config.serviceType = ServiceType.custom("chat");
        config.defaultPacketConverter = converter;
        config.receiveBufferSize = 256;

        final TCPManager manager = TCPManager.get(config);
        manager.startServer(activity);
        manager.setTargetStateListener(new TargetStateListener() {
            @Override
            public void onStateChanged(StateChangeEvent event) {
                if (event.is(TCPTargetState.CONNECTED)) {
                    final TCPTarget target = event.target();
                    target.send(new TCPPacket(mBuffer));
                }
            }
        });

        manager.setDiscoveryListener(new DiscoveryListener() {
            @Override
            public void onDiscovered(TCPTarget target, FoundType foundType) {
                if (foundType == FoundType.DISCOVERED) {
                    manager.stopScan();
                    mTarget = target;
                    mTarget.connect();
                }
            }
        });

        manager.setServerPacketConverter(converter);
        manager.setServerReceiver(new ServerReceiver() {
            @Override
            public void onDataReceived(TCPTarget tcpObject, TCPPacket packet) {
                byte[] bytes = packet.bytes();
                assertTrue(Arrays.equals(bytes, mBuffer));
                if (counter.get() >= 3) {
                    s.release();
                } else {
                    counter.incrementAndGet();
                    mBuffer = newDoubleTLVPacket();
                    mTarget.send(new TCPPacket(mBuffer));
                }
            }
        });

        manager.startScan(activity, config.serviceType);
        s.acquire();
    }

    private static byte[] newDoubleTLVPacket() {
        int p1Size = Rand.rndInt(25, 1500);
        int p2Size = Rand.rndInt(500, 2500);
        TLVPacket packet1 = new TLVPacket(69, p1Size, Rand.randomBytes(1500));
        TLVPacket packet2 = new TLVPacket(44, p2Size, Rand.randomBytes(2500));
        ByteBuff buff = new ByteBuff();
        buff.append(packet1.getBytes()).append(packet2.getBytes());
        return buff.pollBytes();
    }

}
