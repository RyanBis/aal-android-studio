package com.xiledsystems.aal.tests;


import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.events.EventManagerConfig;
import com.xiledsystems.aal.util.ByteUtil;
import com.xiledsystems.aal.util.TextUtil;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


public class EventTest extends MainTest {

    private enum TestEvent {
        ONE,
        TWO,
        THREE
    }

    @Test
    public void allEventListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = activity.getEventManager();

        eventManager.registerAllEventListener(new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                TestEvent tEvent = event.getCategory(TestEvent.class);
                if (tEvent != null) {
                    switch (tEvent) {
                        case ONE:
                            eventManager.registerAllEventListener(new EventListener() {
                                @Override
                                public boolean onEvent(Event event) {
                                    return false;
                                }
                            });
                            eventManager.postEvent(TestEvent.TWO);
                            return true;
                        case TWO:
                            eventManager.postEvent(TestEvent.THREE);
                            eventManager.registerAllEventListener(new EventListener() {
                                @Override
                                public boolean onEvent(Event event) {
                                    return false;
                                }
                            });
                            return true;
                        case THREE:
                            release(s);
                            return true;
                    }
                }
                return false;
            }
        });
        for (int i = 0; i < 1000; i++) {
            eventManager.registerAllEventListener(new EventListener() {
                @Override
                public boolean onEvent(Event event) {
                    return false;
                }
            });
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 1000);
        s.acquire();
    }

    @Test
    public void categoryListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = activity.getEventManager();

        eventManager.registerCategoryListener(TestEvent.class, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                TestEvent tEvent = event.getCategory(TestEvent.class);
                if (tEvent != null) {
                    switch (tEvent) {
                        case ONE:
                            eventManager.registerCategoryListener(TestEvent.class, new EventListener() {
                                @Override
                                public boolean onEvent(Event event) {
                                    List<byte[]> byteList = new ArrayList<>();
                                    for (int i = 0; i < 50000; i++) {
                                        byteList.add(ByteUtil.random(1024));
                                    }
                                    return false;
                                }
                            });
                            eventManager.postEvent(TestEvent.TWO);
                            return true;
                        case TWO:
                            eventManager.postEvent(TestEvent.THREE);
                            eventManager.registerCategoryListener(TestEvent.class, new EventListener() {
                                @Override
                                public boolean onEvent(Event event) {
                                    return false;
                                }
                            });
                            return true;
                        case THREE:
                            release(s);
                            return true;
                    }
                }
                return false;
            }
        });
        for (int i = 0; i < 5000; i++) {
            eventManager.registerCategoryListener(TestEvent.class, new EventListener() {
                @Override
                public boolean onEvent(Event event) {
                    return false;
                }
            });
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 5000);
        s.acquire();
    }

    @Test
    public void eventListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = activity.getEventManager();
        eventManager.registerListener(TestEvent.ONE, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                eventManager.registerListener(TestEvent.TWO, new EventListener() {
                    @Override
                    public boolean onEvent(Event event) {
                        List<byte[]> byteList = new ArrayList<>();
                        for (int i = 0; i < 50000; i++) {
                            byteList.add(ByteUtil.random(1024));
                        }
                        return false;
                    }
                });
                eventManager.postEvent(TestEvent.TWO);
                return true;
            }
        });
        eventManager.registerListener(TestEvent.TWO, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                eventManager.registerListener(TestEvent.THREE, new EventListener() {
                    @Override
                    public boolean onEvent(Event event) {
                        List<byte[]> byteList = new ArrayList<>();
                        for (int i = 0; i < 50000; i++) {
                            byteList.add(ByteUtil.random(1024));
                        }
                        return false;
                    }
                });
                eventManager.postEvent(TestEvent.THREE);
                return true;
            }
        });
        eventManager.registerListener(TestEvent.THREE, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                eventManager.registerListener(TestEvent.ONE, new EventListener() {
                    @Override
                    public boolean onEvent(Event event) {
                        return false;
                    }
                });
                release(s);
                return true;
            }
        });
        for (int i = 0; i < 5000; i++) {
            eventManager.registerListener(TestEvent.ONE, new EventListener() {
                @Override
                public boolean onEvent(Event event) {
                    return true;
                }
            });
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 5000);
        s.acquire();
    }

    @Test
    public void delayedEventTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        final AtomicInteger delayLength = new AtomicInteger(100);
        eventManager = activity.getEventManager();
        final AtomicLong start = new AtomicLong(0);
        eventManager.registerCategoryListener(TestEvent.class, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                long diff = System.currentTimeMillis() - start.get();
                assertTrue(diff > delayLength.get());
                // Allow for as much as one cycle + once cycle - 1 ms of buffer, as it depends on when the event is posted
                // relative to the event cycle
                int max = delayLength.get() + (((1000 / eventManager.getCurrentEPS()) * 2) - 1);
                assertTrue(TextUtil.string("Diff: ", diff, " Max: ", max), diff < max);
                if (delayLength.get() < 600) {
                    delayLength.set(delayLength.get() + 100);
                    start.set(System.currentTimeMillis());
                    eventManager.postDelayedEvent(TestEvent.ONE, delayLength.get());
                } else {
                    release(s);
                }
                return true;
            }
        });
        start.set(System.currentTimeMillis());
        eventManager.postDelayedEvent(TestEvent.ONE, delayLength.get());
        s.acquire();
    }

    @Test
    public void delayedEventConfigTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        final EventManagerConfig config = new EventManagerConfig();
        config.logging = true;
        config.defaultDelay = 100L;
        eventManager = activity.getEventManager();
        eventManager.setConfig(config);
        final AtomicLong start = new AtomicLong(0);
        eventManager.registerCategoryListener(TestEvent.class, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                long diff = System.currentTimeMillis() - start.get();
                assertTrue(diff > config.defaultDelay);
                // Allow for as much as one cycle + once cycle - 1 ms of buffer, as it depends on when the event is posted
                // relative to the event cycle
                int max = (int) (config.defaultDelay + (((1000 / eventManager.getCurrentEPS()) * 2) - 1));
                assertTrue(TextUtil.string("Diff: ", diff, " Max: ", max), diff < max);
                if (config.defaultDelay < 600) {
                    config.defaultDelay = config.defaultDelay + 100;
                    start.set(System.currentTimeMillis());
                    eventManager.postEvent(TestEvent.ONE);
                } else {
                    release(s);
                }
                return true;
            }
        });
        start.set(System.currentTimeMillis());
        eventManager.postEvent(TestEvent.ONE);
        s.acquire();
    }

    @Test
    public void maxExecutionTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = activity.getEventManager();
        final int max = 3;
        eventManager.registerListener(TestEvent.ONE, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                int i = 0;
                i++;
                return true;
            }
        });
        eventManager.registerListener(TestEvent.ONE, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                return true;
            }
        });
        eventManager.registerListener(TestEvent.ONE, new EventListener() {
            @Override
            public boolean onEvent(Event event) {
                assertTrue(event.currentExecutions() + 1 == max);
                release(s);
                return true;
            }
        });
        eventManager.postEventMax(max, TestEvent.ONE);
        s.acquire();
    }

}
