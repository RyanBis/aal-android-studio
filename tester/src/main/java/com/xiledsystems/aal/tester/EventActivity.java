package com.xiledsystems.aal.tester;


import android.os.Bundle;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.events.EventManagerConfig;
import com.xiledsystems.aal.events.PostType;


public class EventActivity extends AalActivity {


    private EventManager eventManager;


    @Override
    public void onCreateActivity(Bundle savedInstanceState) {
        EventManagerConfig config = new EventManagerConfig();
        config.logging = true;
        eventManager = EventManager.getInstance(config);
        eventManager.init();
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    @Override
    protected void onDestroy() {
        eventManager.shutdown();
        super.onDestroy();
    }
}
