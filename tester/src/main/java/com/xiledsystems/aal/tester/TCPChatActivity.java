package com.xiledsystems.aal.tester;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.xiledsystems.aal.communication.tcp.DiscoveryListener;
import com.xiledsystems.aal.communication.tcp.FoundType;
import com.xiledsystems.aal.communication.tcp.ServerReceiver;
import com.xiledsystems.aal.communication.tcp.ServiceType;
import com.xiledsystems.aal.communication.tcp.StringConverter;
import com.xiledsystems.aal.communication.tcp.TCPConfig;
import com.xiledsystems.aal.communication.tcp.TCPManager;
import com.xiledsystems.aal.communication.tcp.TCPPacket;
import com.xiledsystems.aal.communication.tcp.TCPTarget;
import com.xiledsystems.aal.communication.tcp.TCPTargetState;
import com.xiledsystems.aal.communication.tcp.TargetReceiver;
import com.xiledsystems.aal.communication.tcp.TargetStateListener;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.util.TextUtil;
import com.xiledsystems.aal.util.ThreadManager;


public class TCPChatActivity extends AalActivity {

    private final static String NL = "\n";

    private EditText message;
    private TextView incoming;
    private TextView outgoing;
    private Switch host;
    private Button send;

    private TCPManager manager;

    private TCPTarget target;
    private boolean server = false;
    private StringConverter converter = new StringConverter();
    private StringBuilder incomingCache = new StringBuilder();
    private StringBuilder outgoingCache = new StringBuilder();
    private boolean connected = false;

    private TargetReceiver targetReceiver = new TargetReceiver() {
        @Override
        public void onDataReceived(TCPTarget target, TCPPacket packet) {
            StringConverter con = packet.getConverter();
            setIncomingText(con.getContent());
        }
    };

    private ServerReceiver connectionReceiver = new ServerReceiver() {

        @Override
        public void onDataReceived(TCPTarget connection, TCPPacket packet) {
            StringConverter con = packet.getConverter();
            setIncomingText(con.getContent());
        }
    };


    @Override
    public void onCreateActivity(Bundle savedInstanceState) {
        setContentView(R.layout.tcp_chat_activity);

        TCPConfig config = new TCPConfig();
        config.serviceName = "Chatter";
        config.serviceType = ServiceType.custom("chat");
        config.defaultPacketConverter = converter;
        config.loggingEnabled = true;

        manager = TCPManager.get(config);
        manager.setServerPacketConverter(converter);
        manager.setServerReceiver(connectionReceiver);

        message = find(R.id.message);

        incoming = find(R.id.incoming);
        outgoing = find(R.id.outgoing);

        send = find(R.id.send);
        send.setEnabled(false);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = message.getText().toString();
                outgoingCache.append(msg).append(NL);
                outgoing.setText(outgoingCache.toString());
                message.setText("");
                if (TextUtil.notEmpty(msg)) {
                    TCPPacket packet = converter.newPacket(msg);
                    target.send(packet);
                }
            }
        });

        host = find(R.id.host);
        host.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                server = isChecked;
                if (isChecked) {
                    manager.disconnectAllTargets();
                    manager.stopScan();
                    if (!manager.startServer(TCPChatActivity.this)) {
                        send.setEnabled(false);
                    }
                } else {
                    manager.stopServer();
                    send.setEnabled(false);
                    scan();
                }
            }
        });
        scan();
    }

    private void scan() {
        manager.setDiscoveryListener(new DiscoveryListener() {
            @Override
            public void onDiscovered(TCPTarget targ, FoundType foundType) {
                manager.stopScan();
                target = targ;
                target.setReceiver(targetReceiver);
                target.setStateListener(new TargetStateListener() {
                    @Override
                    public void onStateChanged(StateChangeEvent event) {
                        if (event.is(TCPTargetState.CONNECTED)) {
                            connected = true;
                            setSendEnabled(true);
                        } else {
                            if (send.isEnabled()) {
                                connected = false;
                                setSendEnabled(false);
                            }
                        }
                    }
                });
                if (foundType == FoundType.REQUESTED) {
                    connected = true;
                    setSendEnabled(true);
                } else {
                    target.connect();
                }
            }
        });
        manager.startScan(this, ServiceType.custom("chat"));
    }

    private void setIncomingText(final String text) {
        ThreadManager.getHandler().post(new Runnable() {
            @Override
            public void run() {
                incomingCache.append(text).append(NL);
                incoming.setText(incomingCache.toString());
            }
        });
    }

    private void setSendEnabled(final boolean enabled) {
        ThreadManager.getHandler().post(new Runnable() {
            @Override
            public void run() {
                send.setEnabled(enabled);
            }
        });
    }
}
