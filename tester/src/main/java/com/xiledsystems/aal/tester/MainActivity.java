package com.xiledsystems.aal.tester;

import android.os.Bundle;
import com.xiledsystems.aal.context.AalActivity;


public class MainActivity extends AalActivity {

    @Override
    public void onCreateActivity(Bundle savedInstanceState) {
        setContentView(R.layout.main_activity);
    }
}
