package com.xiledsystems.aal.communication.tcp;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ByteBuff;
import com.xiledsystems.aal.util.ByteUtil;

import java.util.Arrays;


@State(DevelopmentState.ALPHA)
public final class TLVPacket {


    long type;
    final TLVLengthType typeSize;
    private TLVLengthType lengthType;
    long length;
    byte[] data;
    boolean reverse;


    public TLVPacket(TLVLengthType typeSize, long type, TLVLengthType lengthType, long length, byte[] content, boolean reverseBytes) {
        this(typeSize);
        this.reverse = reverseBytes;
        this.type = type;
        this.length = length;
        data = content;
        setLength(lengthType);
    }

    public TLVPacket(byte type, byte length, byte[] content, boolean reverseBytes) {
        this(TLVLengthType.BYTE, type, TLVLengthType.BYTE, length, content, reverseBytes);
    }

    public TLVPacket(byte type, byte length, byte[] content) {
        this(type, length, content, false);
    }

    public TLVPacket(short type, short length, byte[] content, boolean reverseBytes) {
        this(TLVLengthType.SHORT, type, TLVLengthType.SHORT, length, content, reverseBytes);
    }

    public TLVPacket(short type, short length, byte[] content) {
        this(type, length, content, false);
    }

    public TLVPacket(int type, int length, byte[] content, boolean reverseBytes) {
        this(TLVLengthType.INT, type, TLVLengthType.INT, length, content, reverseBytes);
    }

    public TLVPacket(int type, int length, byte[] content) {
        this(type, length, content, false);
    }

    public TLVPacket(long type, long length, byte[] content, boolean reverseBytes) {
        this(TLVLengthType.LONG, type, TLVLengthType.LONG, length, content, reverseBytes);
    }

    public TLVPacket(long type, long length, byte[] content) {
        this(type, length, content, false);
    }

    TLVPacket(TLVLengthType typeSize) {
        this.typeSize = typeSize;
    }



    public final int getPacketLength() {
        return typeSize.size() + (int) length + (data == null ? 0 : data.length);
    }

    public final byte[] getBytes() {
        return getBytes_private(typeSize, type, lengthType, length, data);
    }



    final void setLength(TLVLengthType lengthType) {
        this.lengthType = lengthType;
        if (data == null) {
            data = new byte[(int) length];
        } else {
            // If there's data passed iu that's larger than the given length,
            // the ending bytes will be trimmed off.
            if (data.length > length) {
                byte[] tmp = data.clone();
                data = Arrays.copyOf(tmp, (int) length);
            }
        }
    }

    final void setManualLength(TLVLengthType lengthType, long length) {
        this.length = length;
        setLength(lengthType);
    }


    private static byte[] getBytes_private(TLVLengthType typeSize, long type, TLVLengthType lengthType, long length, byte[] data) {
        int size = typeSize.size() + lengthType.size() + (data == null ? 0 : data.length);
        byte[] typeBytes;
        byte[] sizeBytes;
        switch (typeSize) {
            case SHORT:
                typeBytes = ByteUtil.shortToBytes((short) type);
                break;
            case INT:
                typeBytes = ByteUtil.intToBytes((int) type);
                break;
            case LONG:
                typeBytes = ByteUtil.longToBytes(type);
                break;
            default:
                typeBytes = new byte[] { (byte) type };
        }
        switch (lengthType) {
            case SHORT:
                sizeBytes = ByteUtil.shortToBytes((short) length);
                break;
            case INT:
                sizeBytes = ByteUtil.intToBytes((int) length);
                break;
            case LONG:
                sizeBytes = ByteUtil.longToBytes(length);
                break;
            default:
                sizeBytes = new byte[] { (byte) length };
        }
        final ByteBuff buff = new ByteBuff(size);
        buff.append(typeBytes);
        buff.append(sizeBytes);
        if (data != null && data.length > 0) {
            buff.append(data);
        }
        return buff.pollBytes();
    }

}
