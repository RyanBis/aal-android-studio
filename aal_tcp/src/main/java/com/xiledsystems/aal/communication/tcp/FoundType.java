package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public enum FoundType {

    /**
     * {@link TCPTarget} was found by explicit generation.
     */
    EXPLICIT,

    /**
     * {@link TCPTarget} was found by running one of the startScan() methods in {@link TCPManager}. This gets fired the first
     * time a {@link TCPTarget} is found, all other times in an app session, it will be {@link #REDISCOVERED}.
     */
    DISCOVERED,

    /**
     * {@link TCPTarget} was found again from one of the startScan() methods in {@link TCPManager}.
     */
    REDISCOVERED,

    /**
     * {@link TCPTarget} was found by it connecting (so the phone is running as a server, and some other
     * client connected to it).
     */
    REQUESTED
}
