package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ByteBuff;
import com.xiledsystems.aal.util.ByteUtil;
import java.util.ArrayList;


@State(DevelopmentState.ALPHA)
public class TLVConverter implements TCPPacketConverter {

    private ArrayList<TLVPacket> packets;
    private TLVLengthType typeSize;
    private TLVLengthType lengthType;
    private boolean reverseBytes;


    public TLVConverter(TLVLengthType typeSize, TLVLengthType lengthType, boolean reverseBytes) {
        this.typeSize = typeSize;
        this.reverseBytes = reverseBytes;
        this.lengthType = lengthType;
        packets = new ArrayList<>(1);
    }

    public TLVConverter(TLVLengthType size, boolean reverseBytes) {
        this(size, size, reverseBytes);
    }

    public TLVConverter(TLVLengthType size) {
        this(size, false);
    }

    @Override
    public void reset() {
        packets.clear();
    }

    @Override
    public void onInput(byte[] data) {
        if (data != null && data.length >= (typeSize.size() + lengthType.size())) {
            int curIndex = 0;
            while ((curIndex + typeSize.size() + lengthType.size()) < data.length) {
                TLVPacket p = new TLVPacket(typeSize);
                byte[] t = new byte[typeSize.size()];
                System.arraycopy(data, curIndex, t, 0, t.length);
                p.type = ByteUtil.doBytesToLong(t.length, t, p.reverse);
                curIndex += typeSize.size();
                byte[] size = new byte[lengthType.size()];
                System.arraycopy(data, curIndex, size, 0, size.length);
                curIndex += size.length;
                p.setManualLength(lengthType, ByteUtil.doBytesToLong(lengthType.size(), size, p.reverse));
                if (curIndex + p.length <= data.length) {
                    System.arraycopy(data, curIndex, p.data, 0, (int) p.length);
                    curIndex += p.length;
                }
                packets.add(p);
            }
        }
    }

    @Override
    public byte[] getBytes() {
        int size = packets.size();
        if (size > 0) {
            ByteBuff buff = new ByteBuff(packets.get(0).getPacketLength());
            for (int i = 0; i < size; i++) {
                buff.append(packets.get(i).getBytes());
            }
            return buff.pollBytes();
        }
        return new byte[0];
    }

    public ArrayList<TLVPacket> getPackets() {
        return packets;
    }

    @Override
    public int length() {
        return packets == null ? 0 : packets.size();
    }

    @Override
    public TLVConverter newInstance() {
        return newInstance_private(typeSize, lengthType, reverseBytes);
    }

    private static TLVConverter newInstance_private(TLVLengthType type, TLVLengthType size, boolean reverseBytes) {
        return new TLVConverter(type, size, reverseBytes);
    }

}
