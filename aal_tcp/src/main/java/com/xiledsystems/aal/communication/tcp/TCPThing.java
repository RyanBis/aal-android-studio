package com.xiledsystems.aal.communication.tcp;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ByteBuff;
import com.xiledsystems.aal.util.TextUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicBoolean;


@State(DevelopmentState.ALPHA)
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
abstract class TCPThing {


    String serviceType;
    String serviceName;

    BufferedInputStream input;
    BufferedOutputStream output;
    Socket socket;
    TCPPacketConverter packetConverter;
    TargetReceiver targetReceiver;
    ServerReceiver serverReceiver;
    InetAddress ipAddress;
    private Handler listenerHandler;
    private ByteBuff buffer;
    final AtomicBoolean keepListening;
    private final LinkedList<TCPPacket> sendQueue;
    int port;
    final TCPManager manager;


    protected TCPThing(TCPManager manager, TCPPacketConverter converter) {
        this.manager = manager;
        sendQueue = new LinkedList<>();
        packetConverter = converter.newInstance();
        keepListening = new AtomicBoolean(false);
    }

    public final void setPacketConverter(TCPPacketConverter converter) {
        packetConverter = converter;
    }

    public void send(final TCPPacket packet) {
        manager.postToUpdateThread(new Runnable() {
            @Override
            public void run() {
                sendQueue.add(packet);
            }
        });
    }

    abstract void update(long curTimeMs);
    abstract void disconnect();
    protected abstract boolean canSend();

    final void onUpdate(long curTimeMs) {
        if (hasWaitingPackets()) {
            processNextSend();
        }
        update(curTimeMs);
    }

    final void dispatchReadEvent(byte[] bytes) {
        manager.logD(TextUtil.string(serviceName, " received ", bytes.length, " bytes from ", ipAddress, "."));
        TCPPacket packet = new TCPPacket(packetConverter, bytes);
        if (targetReceiver != null) {
            manager.dispatchTargetReadEvent((TCPTarget) this, packet, targetReceiver);
        } else if (serverReceiver != null) {
            manager.dispatchServerReadEvent((TCPTarget) this, packet);
        }
        packetConverter.reset();
    }

    final void initStreams() {
        try {
            input = new BufferedInputStream(socket.getInputStream());
            output = new BufferedOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    final void startListening() {
        if (listenerHandler != null && keepListening.get()) {
            keepListening.set(false);
        }
        buffer = new ByteBuff(manager.config.receiveBufferSize);
        keepListening.set(true);
        final Runnable listenerRunnable = new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                listenerHandler = new Handler(Looper.myLooper());
                byte[] buff = new byte[manager.config.receiveBufferSize];
                while (keepListening.get()) {
                    if (input != null) {
                        try {
                            int read;
                            while ((read = input.read(buff, 0, buff.length)) > 0) {
                                buffer.append(Arrays.copyOf(buff, read));
                                if (read < buff.length) break;
                            }
                            if (buffer.length() > 0) {
                                dispatchReadEvent(buffer.pollBytes());
                            }
                        } catch (Exception e) {
                            // TODO - Handle this error. It's most likely due to losing the connection
                            e.printStackTrace();
                        }
                    } else {
                        // TODO Log error somewhere, we're disconnecting now
                        disconnect();
                        return;
                    }
                }
                Looper.loop();
            }
        };
        manager.runListenerThread(listenerRunnable);
    }

    void close() {
        keepListening.set(false);
        if (input != null) {
            try {
                input.close();
            } catch (Exception e) {}
            input = null;
        }
        if (output != null) {
            try {
                output.close();
            } catch (Exception e) {}
            output = null;
        }
        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    private void processNextSend() {
        final TCPPacket packet = sendQueue.poll();
        final byte[] bytes = packet.bytes();
        if (packet.length() > 0) {
            try {
                output.write(bytes);
                output.flush();
                packet.clear();
                manager.logD(TextUtil.string(serviceName, " sent ", bytes.length, " bytes to ", ipAddress, "."));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private boolean hasWaitingPackets() {
        return sendQueue.size() > 0 && canSend();
    }

}
