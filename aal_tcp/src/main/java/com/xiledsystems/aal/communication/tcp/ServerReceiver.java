package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public interface ServerReceiver extends TCPThingReceiver<TCPTarget> {
    void onDataReceived(TCPTarget connection, TCPPacket packet);
}
