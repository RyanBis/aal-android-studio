package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public enum TCPTargetState {
    DISCONNECTED(1 << 0),
    FOUND(1 << 1),
    RESOLVING(1 << 2),
    RESOLVED(1 << 3),
    CONNECTING(1 << 4),
    CONNECTED(1 << 5);


    private static TCPTargetState[] VALUES;
    private final int bit;

    TCPTargetState(int bit) {
        this.bit = bit;
    }

    public final int bit() {
        return bit;
    }

    public final int or(TCPTargetState state) {
        return bit | state.bit();
    }

    public final int or(int mask) {
        return bit | mask;
    }

    public final int remove(int mask) {
        return mask & ~bit;
    }

    public static boolean is(TCPTargetState state, int mask) {
        return ((mask & state.bit()) != 0);
    }

    public static boolean isAny(int mask, TCPTargetState... states) {
        if (states != null && states.length > 0) {
            for (TCPTargetState state : states) {
                if (is(state, mask)) {
                    return true;
                }
            }
            return false;
        } else {
            return true;
        }
    }

    public static int remove(int mask, TCPTargetState state) {
        return (mask & ~state.bit());
    }

    public static int removeAll(int mask, TCPTargetState... states) {
        if (states != null && states.length > 0) {
            for (TCPTargetState s : states) {
                mask = remove(mask, s);
            }
            return mask;
        } else {
            return mask;
        }
    }

    public static TCPTargetState[] VALUES() {
        if (VALUES == null) {
            VALUES = TCPTargetState.values();
        }
        return VALUES;
    }
}
