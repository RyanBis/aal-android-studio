package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
interface TCPThingReceiver<T extends TCPThing> {
    void onDataReceived(T tcpObject, TCPPacket packet);
}
