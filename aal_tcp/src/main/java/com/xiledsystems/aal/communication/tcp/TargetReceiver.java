package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public interface TargetReceiver extends TCPThingReceiver<TCPTarget> {
    void onDataReceived(TCPTarget target, TCPPacket packet);
}
