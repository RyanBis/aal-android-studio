package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public enum ServiceType {

    FTP("ftp"),
    SSH("ssh"),
    TELNET("telnet"),
    SMTP("smtp"),
    TIME("time"),
    DOMAIN("domain"),
    TFTP("tftp"),
    HTTP("http"),
    WWW("www"),
    WWW_HTTP("www-http"),
    NPP("npp"),
    SFTP("sftp"),
    NTP("ntp");


    private final static String TYPE_TEMPLATE = "_%s._tcp.";


    private final String type;

    ServiceType(String type) {
        this.type = String.format(TYPE_TEMPLATE, type);
    }

    public String type() {
        return type;
    }

    public static String custom(String type) {
        return String.format(TYPE_TEMPLATE, type);
    }

}
