package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public final class ByteConverter implements TCPPacketConverter {

    private byte[] bytes;

    @Override
    public final void onInput(byte[] data) {
        bytes = data;
    }

    @Override
    public final byte[] getBytes() {
        return bytes;
    }

    @Override
    public final int length() {
        return bytes == null ? 0 : bytes.length;
    }

    @Override
    public final void reset() {
        bytes = new byte[0];
    }

    @Override
    public final ByteConverter newInstance() {
        return newInstance_private();
    }

    private static ByteConverter newInstance_private()
    {
        return new ByteConverter();
    }
}
