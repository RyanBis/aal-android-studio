package com.xiledsystems.aal.communication.tcp;


import android.annotation.TargetApi;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.Loop;
import com.xiledsystems.aal.util.TextUtil;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * <b>NOTE: </b> The TCP classes will ONLY work on devices running {@link android.os.Build.VERSION_CODES#JELLY_BEAN} or higher.
 *
 * This is the main entry point into the TCP System. Use this class to start/stop scans for TCP targets in the local network. You can also
 * setup a server using this class. See {@link TCPConfig} as well for configuration options.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@State(DevelopmentState.ALPHA)
public final class TCPManager {

    private static TCPManager instance;


    public static TCPManager get(TCPConfig config) {
        if (instance == null) {
            instance = new TCPManager(config);
        } else {
            instance.setConfig(config);
        }
        return instance;
    }

    public static TCPManager get() {
        if (instance == null) {
            instance = new TCPManager();
        }
        return instance;
    }

    private static final String TAG = "TCPSystem";
    private static long sNow;
    private static NsdManager nsdManager;
    TCPConfig config;
    private final Loop loop;
    private final UpdateLoop loopRunnable;
    private final HashSet<TCPTarget> targets;
    private DiscoveryListener discoveryListener;
    TargetStateListener defaultTargetStateListener;
    TargetReceiver defaultTargetReceiver;
    private NativeDiscoveryListener nativeDiscoveryListener;
    private NativeResolveListener nativeResolveListener;
    static TCPServer server;
    private String scanServiceName;
    private String scanServiceType = "";
    private SocketConnectThread connectThread;
    private final ThreadPoolExecutor listenerExecutor;
    final AtomicInteger targetsConnected;
    private boolean scanStarted = false;


    private TCPManager() {
        this(new TCPConfig.DefaultTCPConfig());
    }

    private TCPManager(TCPConfig config) {
        this.config = config != null ? config.clone() : new TCPConfig.DefaultTCPConfig();
        targetsConnected = new AtomicInteger(0);
        targets = new HashSet<>();
        nativeDiscoveryListener = new NativeDiscoveryListener();
        nativeResolveListener = new NativeResolveListener();
        connectThread = new SocketConnectThread();
        connectThread.start();
        connectThread.prepareHandler();
        loopRunnable = new UpdateLoop();
        loop = new Loop(loopRunnable);
        listenerExecutor = new ThreadPoolExecutor(config.maxTotalConnections / 2, config.maxTotalConnections, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        initConfig();
    }

    private void initConfig() {
        loop.setInterval(config.updateRate);
        if (!loop.isRunning()) {
            loop.start();
        }
        if (listenerExecutor.getCorePoolSize() != config.maxTargetConnections / 2) {
            listenerExecutor.setCorePoolSize(config.maxTotalConnections / 2);
        }
        if (listenerExecutor.getMaximumPoolSize() != config.maxTotalConnections) {
            listenerExecutor.setMaximumPoolSize(config.maxTotalConnections);
        }
        listenerExecutor.prestartAllCoreThreads();
    }


    /**
     * Set the default {@link TargetStateListener}. This will get called for every state change in every
     * known {@link TCPTarget}.
     *
     * @param listener
     */
    public final void setTargetStateListener(TargetStateListener listener) {
        defaultTargetStateListener = listener;
    }

    /**
     * @return whether the server can be started or not.
     */
    public final boolean serverStartPossible() {
        return TextUtil.notEmpty(config.serviceName) && TextUtil.notEmpty(config.serviceType);
    }

    /**
     * Start up a server to allow {@link TCPTarget}s to connect. This calls {@link #serverStartPossible()} before
     * trying to start the server to ensure the necessary configuration options are set.
     *
     * @param context
     * @return whether or not the server successfully started.
     */
    public final boolean startServer(Context context) {
        return serverStartPossible() && initServer(context);
    }

    /**
     * Stop the server from advertising itself, and don't allow anymore {@link TCPTarget}s to connect, as well
     * as disconnecting any connected ones.
     */
    public final void stopServer() {
        server.stopServer();
    }

    /**
     * Set the {@link TCPConfig} that this class will use for different configuration settings.
     * If you pass <code>null</code> here, an instance of {@link com.xiledsystems.aal.communication.tcp.TCPConfig.DefaultTCPConfig} will
     * be set to the manager (overriding any previous {@link TCPConfig}).
     */
    public final void setConfig(TCPConfig config) {
        this.config = config != null ? config.clone() : new TCPConfig.DefaultTCPConfig();
        initConfig();
    }

    /**
     * Set the {@link DiscoveryListener} to listen for when {@link TCPTarget}s are discovered from a scan. This will also
     * get called when a {@link TCPTarget} connects to the server, if it is running. Check {@link FoundType} to tell how it
     * was discovered.
     */
    public final void setDiscoveryListener(DiscoveryListener listener) {
        discoveryListener = listener;
    }

    /**
     * Set a default {@link TargetReceiver} to listen for all data received for all connected {@link TCPTarget}s. Use this receiver
     * for {@link TCPTarget}s that you explicitly connect to.
     * @param receiver
     */
    public final void setTargetReceiver(TargetReceiver receiver) {
        defaultTargetReceiver = receiver;
    }

    /**
     * This simply calls {@link #setServerReceiver(ServerReceiver)}, and {@link #setServerPacketConverter(TCPPacketConverter)}.
     *
     * @param receiver
     * @param packetConverter
     */
    public final void setServerCallbacks(ServerReceiver receiver, TCPPacketConverter packetConverter) {
        setServerReceiver(receiver);
        setServerPacketConverter(packetConverter);
    }

    /**
     * Set a default {@link ServerReceiver} to listen for all data received by all {@link TCPTarget}s that have connected to the
     * server.
     * @param receiver
     */
    public final void setServerReceiver(ServerReceiver receiver) {
        if (server == null) {
            server = new TCPServer(this);
        }
        server.setServerReceiver(receiver);
    }

    /**
     * Set the {@link TCPPacketConverter} that the server will use to encode/decode transmissions.
     *
     * @param converter
     */
    public final void setServerPacketConverter(TCPPacketConverter converter) {
        if (server == null) {
            server = new TCPServer(this);
        }
        server.packetConverter = converter.newInstance();
    }

    /**
     * Start scanning for {@link TCPTarget}s within the local network. Discovered {@link TCPTarget}s will be dispatched
     * to the {@link DiscoveryListener}, with the {@link FoundType} of {@link FoundType#DISCOVERED}.
     *
     * @param context
     * @param serviceType - The service type to search for. See {@link ServiceType}.
     */
    public final void startScan(Context context, String serviceType) {
        scanServiceType = serviceType;
        if (nsdManager == null) {
            nsdManager = (NsdManager) context.getApplicationContext().getSystemService(Context.NSD_SERVICE);
        }
        nsdManager.discoverServices(scanServiceType, NsdManager.PROTOCOL_DNS_SD, nativeDiscoveryListener);
        scanStarted = true;
    }

    /**
     * Start scanning for {@link TCPTarget}s within the local network. Discovered {@link TCPTarget}s will be dispatched
     * to the {@link DiscoveryListener}, with the {@link FoundType} of {@link FoundType#DISCOVERED}.
     *
     * @param context
     * @param serviceType - The service type to search for. See {@link ServiceType}.
     * @param serviceName - The name of the {@link TCPTarget} to look for.
     */
    public final void startScan(Context context, String serviceType, String serviceName) {
        scanServiceType = serviceType;
        scanServiceName = serviceName;
        if (nsdManager == null) {
            nsdManager = (NsdManager) context.getApplicationContext().getSystemService(Context.NSD_SERVICE);
        }
        nsdManager.discoverServices(scanServiceType, NsdManager.PROTOCOL_DNS_SD, nativeDiscoveryListener);
        scanStarted = true;
    }

    /**
     * Stop scanning for {@link TCPTarget}s.
     */
    public final void stopScan() {
        if (scanStarted) {
            nsdManager.stopServiceDiscovery(nativeDiscoveryListener);
            scanStarted = false;
        }
    }

    /**
     * Create a new {@link TCPTarget} with the given address, port, name, and service type.
     *
     * @param name
     * @param serviceType
     * @param address
     * @param port
     * @return
     */
    public TCPTarget newTarget(String name, String serviceType, InetAddress address, int port) {
        final TCPTarget target = new TCPTarget(this, address, port, serviceType, name);
        targets.add(target);
        return target;
    }

    /**
     * You should be sure to call this method when you are done using the TCP classes. This will shutdown any threads
     * the manager is using, and clean up any resources.
     */
    public final void shutdown() {
        listenerExecutor.shutdown();
        connectThread.quit();
        if (loop != null) {
            loop.dispose();
        }
        if (server != null && server.isRegistered()) {
            server.stopServer();
        }
        server = null;
        nsdManager = null;
    }

    /**
     * Disconnect all {@link TCPTarget}s you've connected to by using {@link TCPTarget#connect()} .
     */
    public final void disconnectAllTargets() {
        postToUpdateThread(new Runnable() {
            @Override
            public void run() {
                for (TCPTarget t : targets) {
                    t.disconnect();
                }
            }
        });
    }





    final void postTargetStateChange(TCPTarget target, int stateMask) {
        if (defaultTargetStateListener != null) {
            defaultTargetStateListener.onStateChanged(new TargetStateListener.StateChangeEvent(target, stateMask));
        }
    }

    final void dispatchTargetReadEvent(final TCPTarget target, final TCPPacket packet, final TargetReceiver receiver) {
        postToUpdateThread(new Runnable() {
            @Override
            public void run() {
                if (receiver != null) {
                    receiver.onDataReceived(target, packet);
                }
                if (defaultTargetReceiver != null) {
                    defaultTargetReceiver.onDataReceived(target, packet);
                }
            }
        });
    }

    final void dispatchServerReadEvent(final TCPTarget connection, final TCPPacket packet) {
        postToUpdateThread(new Runnable() {
            @Override
            public void run() {
                if (server.receiver != null) {
                    server.receiver.onDataReceived(connection, packet);
                }
            }
        });
    }

    final void runListenerThread(Runnable listenerRunner) {
        listenerExecutor.execute(listenerRunner);
    }

    final void clearServerTarget(TCPTarget target) {
        if (server != null) {
            server.remove(target);
        }
    }

    final void resolveService(TCPTarget target) {
        logI(TextUtil.string("Resolving service '", target.getServiceInfo().getServiceName(), "'..."));
        nsdManager.resolveService(target.getServiceInfo(), nativeResolveListener);
    }

    final void postConnect(Runnable action) {
        connectThread.handler.post(action);
    }

    final void postToUpdateThread(Runnable action) {
        loop.postAction(action);
    }

    final void logE(String msg) {
        if (config.loggingEnabled) {
            Log.e(TAG, msg);
        }
    }

    final void logW(String msg) {
        if (config.loggingEnabled) {
            Log.w(TAG, msg);
        }
    }

    final void logD(String msg) {
        if (config.loggingEnabled) {
            Log.d(TAG, msg);
        }
    }

    final void logI(String msg) {
        if (config.loggingEnabled) {
            Log.i(TAG, msg);
        }
    }

    final void onServerDiscovered(TCPTarget target) {
        targets.add(target);
        target.onDiscovered();
        if (discoveryListener != null) {
            discoveryListener.onDiscovered(target, FoundType.REQUESTED);
        }
    }



    private void onDeviceRediscovered(TCPTarget target) {
        if (discoveryListener != null) {
            discoveryListener.onDiscovered(target, FoundType.REDISCOVERED);
        }
    }

    private void onDeviceDiscovered(TCPTarget target) {
        targets.add(target);
        target.onDiscovered();
        if (discoveryListener != null) {
            discoveryListener.onDiscovered(target, FoundType.DISCOVERED);
        }
    }

    private void updateTargets(long sNow) {
        int size = targets.size();
        if (size > 0) {
            for (TCPTarget t : targets) {
                t.onUpdate(sNow);
            }
        }
    }

    private boolean initServer(Context context) {
        if (nsdManager == null) {
            nsdManager = (NsdManager) context.getApplicationContext().getSystemService(Context.NSD_SERVICE);
        }
        if (server == null) {
            server = new TCPServer(this);
        }
        return server.startServer(nsdManager);
    }

    private TCPTarget getTarget(NsdServiceInfo info) {
        if (targets.size() > 0) {
            for (TCPTarget t : targets) {
                String tType = t.getServiceType().replace(".", "");
                String iType = info.getServiceType().replace(".", "");
                String tName = t.getServiceName();
                if (tType.equalsIgnoreCase(iType) && tName.equalsIgnoreCase(info.getServiceName())) {
                    t.setPort(info.getPort());
                    t.ipAddress = info.getHost();
                    return t;
                }
            }
        }
        return null;
    }




    private final class UpdateLoop implements Runnable {
        @Override
        public void run() {
            sNow = System.currentTimeMillis();
            updateTargets(sNow);
            if (server != null) {
                server.onUpdate(sNow);
            }
        }
    }

    private final class NativeDiscoveryListener implements NsdManager.DiscoveryListener {
        @Override
        public final void onStartDiscoveryFailed(String serviceType, int errorCode) {
            logE(TextUtil.string("Failed to start service discovery! Errorcode: ", errorCode));
        }

        @Override
        public final void onStopDiscoveryFailed(String serviceType, int errorCode) {
            logE(TextUtil.string("Failed to stop service discovery! Errorcde: ", errorCode));
        }

        @Override
        public final void onDiscoveryStarted(String serviceType) {
            logD(TextUtil.string("Service discovery started. Looking for targets using the service type ", serviceType, "."));
        }

        @Override
        public final void onDiscoveryStopped(String serviceType) {
            logD(TextUtil.string("Service discovery for service type ", serviceType, " has stopped."));
        }

        @Override
        public final void onServiceFound(NsdServiceInfo serviceInfo) {
            if (TextUtil.notEmpty(scanServiceType)) {
                if (!serviceInfo.getServiceType().equalsIgnoreCase(scanServiceType)) {
                    return;
                }
            }
            if (TextUtil.notEmpty(scanServiceName)) {
                if (!serviceInfo.getServiceName().equalsIgnoreCase(scanServiceName)) {
                    return;
                }
            }
            final TCPTarget target = new TCPTarget(TCPManager.this, serviceInfo);
            final FoundType fType;
            if (targets.contains(target)) {
                logI(TextUtil.string("Re-found service '", serviceInfo.getServiceName(), "' using service type ", serviceInfo.getServiceType(), "."));
                onDeviceRediscovered(target);
            } else {
                logI(TextUtil.string("Found service '", serviceInfo.getServiceName(), "' using service type ", serviceInfo.getServiceType(), "."));
                onDeviceDiscovered(target);
            }
        }

        @Override
        public final void onServiceLost(NsdServiceInfo serviceInfo) {
            logW(TextUtil.string("Service '", serviceInfo.getServiceName(), "' has been lost."));
        }
    }

    private final class SocketConnectThread extends HandlerThread {

        private Handler handler;

        public SocketConnectThread() {
            super("TCPSockConThrd");
        }

        public final void prepareHandler() {
            handler = new Handler(getLooper());
        }
    }

    private final class NativeResolveListener implements NsdManager.ResolveListener {
        @Override
        public final void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {
            logE(TextUtil.string("Failed to resolve service '", serviceInfo.getServiceName(), "' with errorcode: ", errorCode));
        }

        @Override
        public final void onServiceResolved(NsdServiceInfo serviceInfo) {
            logD(TextUtil.string("Resolved service '", serviceInfo.getServiceName(), "' successfully."));
            TCPTarget t = getTarget(serviceInfo);
            if (t != null) {
                t.onTargetResolved();
            }
        }
    }
}
