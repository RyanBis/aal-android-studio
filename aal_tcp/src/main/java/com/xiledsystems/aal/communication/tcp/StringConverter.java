package com.xiledsystems.aal.communication.tcp;


import android.annotation.TargetApi;
import android.os.Build;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;
import java.nio.charset.Charset;


@State(DevelopmentState.ALPHA)
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class StringConverter implements TCPPacketConverter {

    private String content;
    private Charset charset = Charset.forName("UTF-8");


    public String getContent() {
        return content;
    }

    @Override
    public void onInput(byte[] data) {
        content = new String(data, 0, data.length, charset);
    }

    @Override
    public byte[] getBytes() {
        return content.getBytes(charset);
    }

    @Override
    public int length() {
        return TextUtil.isEmpty(content) ? 0 : content.length();
    }

    @Override
    public void reset() {
        content = "";
    }

    public TCPPacket newPacket(String data) {
        return new TCPPacket(data.getBytes(charset));
    }

    @Override
    public StringConverter newInstance() {
        return newInstance_private();
    }

    private static StringConverter newInstance_private()
    {
        return new StringConverter();
    }
}
