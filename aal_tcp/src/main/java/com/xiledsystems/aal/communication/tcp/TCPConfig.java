package com.xiledsystems.aal.communication.tcp;


import android.annotation.TargetApi;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


/**
 * <b>NOTE: </b> The TCP classes will ONLY work on devices running {@link android.os.Build.VERSION_CODES#JELLY_BEAN} or higher.
 */
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@State(DevelopmentState.ALPHA)
public class TCPConfig implements Cloneable {


    public final static int DEFAULT_UPDATE_RATE = 25;

    public final static int DEFAULT_RCV_BUFFER = 1500;

    public final static int DEFAULT_MAX_SERVER_SOCKETS = 1;

    public final static int DEFAULT_MAX_TARGET_SOCKETS = 10;


    /**
     * The rate at which the {@link TCPManager}s update loop will
     * run at. Default is {@link #DEFAULT_UPDATE_RATE}.
     */
    public int updateRate                               = DEFAULT_UPDATE_RATE;

    /**
     * This sets the size of the receive buffer. The default is {@link #DEFAULT_RCV_BUFFER}, which is
     * the maximum MTU size for ethernet. You can push this higher if you want, but beyond 1500, you
     * will get diminishing results (not to mention it may end up causing issues the higher you push it).
     */
    public int receiveBufferSize                        = DEFAULT_RCV_BUFFER;

    /**
     * Set this if you wish to hardcode the server to a particular port number. You wlil have
     * to worry about collisions with other services running on the calling device.
     * Default is <code>null</code>, which uses the next available port.
     */
    public Integer serverPort                           = null;

    /**
     * The service name, or the name other devices see when they find your server. If either this,
     * or {@link #serviceType} are null, or empty, you will not be able to start a server.
     */
    public String serviceName                           = null;

    /**
     * The service type to broadcast your server to. The default is {@link ServiceType#HTTP}.
     */
    public String serviceType                           = ServiceType.HTTP.type();

    /**
     * Turn on logging, to get debugging/error information. Default is <code>false</code>
     */
    public boolean loggingEnabled                       = false;

    /**
     * Defines the maximum number of server connections to allow. Default is {@link #DEFAULT_MAX_SERVER_SOCKETS}. Note that
     * for each server connection, you will take up another thread. It's best to keep this as low as possible, unless
     * you absolutely need to server that many connections at once.
     */
    public int maxServerConnections                     = DEFAULT_MAX_SERVER_SOCKETS;

    /**
     * Defines how many targets you can have connected at one time. Default is {@link #DEFAULT_MAX_TARGET_SOCKETS}. For each target
     * connected, you will take up another thread.
     */
    public int maxTargetConnections                     = DEFAULT_MAX_TARGET_SOCKETS;

    /**
     * Defines the total maximum connections. Default is ({@link #DEFAULT_MAX_SERVER_SOCKETS} + {@link #DEFAULT_MAX_TARGET_SOCKETS}) + 1.
     */
    public int maxTotalConnections                      = DEFAULT_MAX_SERVER_SOCKETS + DEFAULT_MAX_TARGET_SOCKETS;

    /**
     * This allows you to automatically format every incoming/outgoing packet you receive,
     * or send. The default is {@link ByteConverter}, which is essentially a pass-through. There
     * are other options such as {@link StringConverter}, or {@link TLVConverter}, or you can
     * implement your own by extending one of the pre-made ones, or implementing the
     * {@link TCPPacketConverter} interface.
     */
    public TCPPacketConverter defaultPacketConverter    = new ByteConverter();


    @Override
    protected TCPConfig clone() {
        try {
            return (TCPConfig) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    final NsdServiceInfo getServiceInfo() {
        NsdServiceInfo info = new NsdServiceInfo();
        info.setServiceName(serviceName);
        info.setPort(serverPort);
        info.setServiceType(serviceType);
        return info;
    }

    public static class DefaultTCPConfig extends TCPConfig {

    }

}
