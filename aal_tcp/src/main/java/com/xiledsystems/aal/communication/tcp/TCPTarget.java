package com.xiledsystems.aal.communication.tcp;


import android.annotation.TargetApi;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;

import java.net.InetAddress;
import java.net.Socket;

import static com.xiledsystems.aal.communication.tcp.TCPTargetState.*;

import java.util.concurrent.atomic.AtomicInteger;


@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@State(DevelopmentState.ALPHA)
public class TCPTarget extends TCPThing {

    private final static int CONNECT_DELAY_TIME = 500;

    private final AtomicInteger stateMask;
    private TargetStateListener stateListener;
    private final FoundType foundType;


    TCPTarget(TCPManager manager, FoundType foundType, InetAddress address, int port, String serviceType, String serviceName, TCPPacketConverter packetConverter) {
        super(manager, packetConverter);
        this.foundType = foundType;
        if (address == null) {
            try {
                ipAddress = InetAddress.getByName(null);
            } catch (Exception e) {
            }
        } else {
            this.ipAddress = address;
        }
        this.port = port;
        this.serviceType = serviceType;
        this.serviceName = serviceName;
        stateMask = new AtomicInteger(TCPTargetState.DISCONNECTED.bit() | TCPTargetState.FOUND.bit());
    }

    TCPTarget(TCPManager manager, InetAddress address, int port, String serviceType, String serviceName) {
        this(manager, FoundType.EXPLICIT, address, port, serviceType, serviceName, manager.config.defaultPacketConverter);
    }

    TCPTarget(TCPManager manager, TCPServer server, Socket socket, TCPPacketConverter packetConverter) {
        this(manager, FoundType.REQUESTED, socket.getInetAddress(), socket.getPort(),
                server.serviceInfo.getServiceType(), server.serviceInfo.getServiceName(), packetConverter);
        this.socket = socket;
        serverReceiver = server.receiver;
        initStreams();
        startListening();
        onTargetConnected(false);
    }

    TCPTarget(TCPManager manager, NsdServiceInfo serviceInfo) {
        this(manager, FoundType.DISCOVERED, null, -1, serviceInfo.getServiceType(), serviceInfo.getServiceName(), manager.config.defaultPacketConverter);
    }

    public final InetAddress getAddress() {
        return ipAddress;
    }

    public final int getPort() {
        return port;
    }

    public final String getServiceType() {
        return serviceType;
    }

    public final String getServiceName() {
        return serviceName;
    }

    public final void setStateListener(TargetStateListener listener) {
        stateListener = listener;
    }

    public final void setReceiver(TargetReceiver receiver) {
        targetReceiver = receiver;
    }

    public boolean isConnected() {
        return TCPTargetState.is(TCPTargetState.CONNECTED, stateMask.get());
    }

    public void connect() {
        if (TCPTargetState.isAny(stateMask.get(), TCPTargetState.CONNECTED, TCPTargetState.CONNECTING)) {
            // TODO - Log the early out here
            return;
        }
        if (manager.targetsConnected.get() >= manager.config.maxTargetConnections) {
            // TODO - Log the early out here
            return;
        }
        boolean resolved = TCPTargetState.is(TCPTargetState.RESOLVED, stateMask.get());
        onTargetConnecting(resolved);
        if (resolved) {
            manager.logD(TextUtil.string("Connecting to target '", serviceName, "' at ", ipAddress, ":", port, "..."));
            doConnect();
        } else {
            manager.resolveService(this);
        }
    }

    public void disconnect() {
        onTargetDisconnected();
        close();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj instanceof TCPTarget) {
            TCPTarget o = (TCPTarget) obj;
            boolean type = o.getServiceType().equals(serviceType);
            boolean name = o.getServiceName().equals(serviceName);
            boolean con = o.packetConverter.getClass() == packetConverter.getClass();
            return type && name && con;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = 71;
        result += serviceType.hashCode();
        result += serviceName.hashCode();
        result += packetConverter.getClass().hashCode();
        return 13 * result;
    }

    @Override
    protected boolean canSend() {
        return true;
    }

    @Override
    void update(long curTimeMs) {
    }

    void onDiscovered() {
        int tmp = stateMask.get();
        tmp = FOUND.or(tmp);
        stateMask.set(tmp);
        postStateChange();
    }

    final void setPort(int port) {
        this.port = port;
    }

    @Override
    void close() {
        super.close();
        if (foundType == FoundType.REQUESTED) {
            manager.clearServerTarget(this);
        }
    }

    final void onTargetDisconnected() {
        int tmp = removeAll(stateMask.get(), CONNECTED, CONNECTING);
        tmp = DISCONNECTED.or(tmp);
        stateMask.set(tmp);
        postStateChange();
    }

    final void onTargetConnected(boolean postConnectedEvent) {
        manager.logD(TextUtil.string("Connected to '", serviceName, "' at ", ipAddress, "."));
        int tmp = remove(stateMask.get(), CONNECTING);
        tmp = CONNECTED.or(tmp);
        stateMask.set(tmp);
        manager.targetsConnected.incrementAndGet();
        if (postConnectedEvent) {
            postStateChange();
        }
    }

    final void onTargetConnecting(boolean resolved) {
        int tmp = DISCONNECTED.remove(stateMask.get());
        tmp = CONNECTING.or(tmp);
        if (!resolved) {
            tmp = RESOLVING.or(tmp);
        }
        stateMask.set(tmp);
        postStateChange();
    }

    final void onTargetResolved() {
        int tmp = RESOLVING.remove(stateMask.get());
        tmp = RESOLVED.or(tmp);
        stateMask.set(tmp);
        postStateChange();
        doConnect();
    }

    final void onTargetLost() {
        stateMask.set(DISCONNECTED.bit());
        postStateChange();
    }

    final NsdServiceInfo getServiceInfo() {
        NsdServiceInfo info = new NsdServiceInfo();
        info.setPort(port);
        info.setServiceName(serviceName);
        info.setServiceType(serviceType);
        return info;
    }


    private void doConnect() {
        manager.postConnect(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket(ipAddress, port);
                    initStreams();
                    startListening();
                    onTargetConnected(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    onTargetDisconnected();
                }
            }
        });
    }

    private void postStateChange() {
        if (stateListener != null) {
            stateListener.onStateChanged(new TargetStateListener.StateChangeEvent(this, stateMask.get()));
        }
        manager.postTargetStateChange(this, stateMask.get());
    }
}
