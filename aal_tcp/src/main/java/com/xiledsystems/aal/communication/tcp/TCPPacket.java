package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.ALPHA)
public final class TCPPacket {

    private TCPPacketConverter packetConverter;

    public TCPPacket(byte[] bytes) {
        this(TCPManager.get().config.defaultPacketConverter.newInstance(), bytes);
    }

    public TCPPacket(TCPPacketConverter converter, byte[] bytes) {
        packetConverter = converter == null ? TCPManager.get().config.defaultPacketConverter.newInstance() : converter.newInstance();
        packetConverter.onInput(bytes);
    }

    public final byte[] bytes() {
        return packetConverter.getBytes();
    }

    public final int length() {
        return packetConverter.length();
    }

    public final <T extends TCPPacketConverter> T getConverter() {
        return (T) packetConverter;
    }

    public final void clear() {
//        packetConverter.reset();
    }

}
