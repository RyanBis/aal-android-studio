package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.ALPHA)
public interface TCPPacketConverter {
    void onInput(byte[] data);
    byte[] getBytes();
    int length();
    void reset();
    <T extends TCPPacketConverter> T newInstance();
}
