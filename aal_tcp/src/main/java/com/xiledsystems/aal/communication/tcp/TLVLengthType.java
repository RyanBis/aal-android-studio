package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public enum TLVLengthType {
    BYTE,
    SHORT,
    INT,
    LONG;

    public int size() {
        switch (this) {
            case SHORT:
                return 2;
            case INT:
                return 4;
            case LONG:
                return 8;
            default:
                return 1;
        }
    }
}
