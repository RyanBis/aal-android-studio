package com.xiledsystems.aal.communication.tcp;


import android.annotation.TargetApi;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;


@State(DevelopmentState.ALPHA)
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public final class TCPServer {

    private final static String TAG = "TCPServer";


    final TCPManager manager;
    private static NsdManager nsdManager;
    NsdServiceInfo serviceInfo;
    private ServerRegistrationListener serverRegListener;
    private boolean autoPortSelection;
    private ServerSocket serverSocket;
    private boolean isRegistered = false;
    private boolean acceptingConnections = false;
    private final LinkedList<TCPTarget> connections;
    TCPPacketConverter packetConverter;
    ServerReceiver receiver;


    TCPServer(TCPManager manager) {
        this.manager = manager;
        connections = new LinkedList<>();
        resetInfoFromConfig(manager.config);
    }

    final void resetInfoFromConfig(TCPConfig config) {
        serviceInfo = new NsdServiceInfo();
        serviceInfo.setServiceName(config.serviceName);
        serviceInfo.setServiceType(config.serviceType);
        autoPortSelection = config.serverPort == null;
        if (!autoPortSelection) {
            serviceInfo.setPort(config.serverPort);
        }
    }

    final void setServerReceiver(ServerReceiver receiver) {
        this.receiver = receiver;
    }

    boolean startServer(NsdManager nsdManager) {
        if (this.nsdManager == null) {
            this.nsdManager = nsdManager;
        }
        try {
            serverSocket = new ServerSocket(autoPortSelection ? 0 : serviceInfo.getPort());
            serviceInfo.setPort(serverSocket.getLocalPort());
            serverRegListener = new ServerRegistrationListener();
            nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, serverRegListener);
            acceptingConnections = true;
            startListening();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    final void onUpdate(long curTimeMs) {
        if (isRegistered) {
            if (connections != null && connections.size() > 0) {
                for (TCPTarget sc : connections) {
                    sc.onUpdate(curTimeMs);
                }
            }
        }
    }

    final boolean isRegistered() {
        return isRegistered;
    }

    final void remove(TCPTarget connection) {
        connections.remove(connection);
    }

    final void stopServer() {
        // TODO Close all connections, shutdown associated threads
        acceptingConnections = false;
        for (TCPTarget sc : connections) {
            sc.close();
        }
        connections.clear();
        if (isRegistered) {
            if (nsdManager != null) {
                nsdManager.unregisterService(serverRegListener);
            }
        }
        if (serverSocket != null && !serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        nsdManager = null;
    }

    private void startListening() {
        manager.runListenerThread(new Runnable() {
            @Override
            public void run() {
                while (acceptingConnections) {
                    try {
                        if (connections.size() < manager.config.maxServerConnections) {
                            final Socket s = serverSocket.accept();
                            final TCPTarget conn = new TCPTarget(manager, TCPServer.this, s, packetConverter.newInstance());
                            connections.add(conn);
                            manager.onServerDiscovered(conn);
                        }
                        Thread.sleep(manager.config.updateRate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private class ServerRegistrationListener implements NsdManager.RegistrationListener {

        @Override
        public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            isRegistered = false;
            manager.logE(TextUtil.string("Server '", serviceInfo.getServiceName(), "' registration failed with errorcode: ", errorCode));
        }

        @Override
        public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
            manager.logE(TextUtil.string("Server '", serviceInfo.getServiceName(), "' failed to unregister with errorcode: ", errorCode));
        }

        @Override
        public void onServiceRegistered(NsdServiceInfo mServiceInfo) {
            isRegistered = true;
            if (serviceInfo == null) {
                serviceInfo = mServiceInfo;
            } else {
                serviceInfo.setServiceName(mServiceInfo.getServiceName());
            }
            manager.logD(TextUtil.string("Server '", serviceInfo.getServiceName(), "' successfully registered with service type ", serviceInfo.getServiceType(),
                    " on port ", serviceInfo.getPort()));
        }

        @Override
        public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
            isRegistered = false;
            manager.logD(TextUtil.string("Server '", serviceInfo.getServiceName(), "' successfully unregistered."));
        }
    }

}
