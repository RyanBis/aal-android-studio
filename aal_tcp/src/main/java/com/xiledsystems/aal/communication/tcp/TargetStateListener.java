package com.xiledsystems.aal.communication.tcp;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public interface TargetStateListener {

    void onStateChanged(StateChangeEvent stateChangeEvent);

    final class StateChangeEvent {
        private final TCPTarget target;
        private final int newStateMask;

        StateChangeEvent(TCPTarget target, int mask) {
            this.target = target;
            newStateMask = mask;
        }

        public TCPTarget target() {
            return target;
        }

        public int mask() {
            return newStateMask;
        }

        public boolean is (TCPTargetState state) {
            return TCPTargetState.is(state, newStateMask);
        }

        public boolean isAny(TCPTargetState... states) {
            return TCPTargetState.isAny(newStateMask, states);
        }
    }
}
