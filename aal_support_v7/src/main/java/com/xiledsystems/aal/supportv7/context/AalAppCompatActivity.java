package com.xiledsystems.aal.supportv7.context;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.AalService;
import com.xiledsystems.aal.context.ActivityManager;
import com.xiledsystems.aal.context.IAalActivity;
import com.xiledsystems.aal.control.VCUtil;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.util.ViewUtil;


@State(DevelopmentState.PRODUCTION)
public abstract class AalAppCompatActivity extends AppCompatActivity implements IAalActivity {

    private ActivityManager manager = new ActivityManager(this);

    @Override
    public ActivityManager getManager() {
        return manager;
    }

    /**
     * Override this method to perform operations when the screen is first
     * initialized (first time it reports a width/height greater than 0).
     * This only is called once after the Activity is created.
     */
    @Override
    public void onScreenInitialized() {
    }

    @Override
    public void onServiceBound(AalService service) {
    }

    @Override
    public void onServiceDisconnected() {
    }

    @Override
    public void onBackPressed() {
        if (!manager.onBackPressed()) {
            doNativeBackPressed();
        }
    }

    public void doNativeBackPressed() {
        super.onBackPressed();
    }

    // ***** Android Activity lifecycle methods which ActivityManager needs for Components *****

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager.onCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        manager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        manager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        manager.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        manager.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        manager.onStart();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        manager.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        manager.onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        manager.onDestroy();
        super.onDestroy();
    }

    /**
     * Convenience method for retrieving the {@link AalService} object this
     * AalActivity has been bound to. If it's not bound to any {@link AalService}, then
     * null is returned. This automatically casts your class (as along as it extends
     * {@link AalService}).
     * @param <T>
     * @return
     */
    public <T extends AalService> T getService() {
        return manager.getService();
    }

    /**
     * When this is set to true, this will shut the service down after
     * unbinding from it.
     *
     * @param stop
     */
    public void stopOnUnbind(boolean stop) {
        manager.stopOnUnbind(stop);
    }

    /**
     * Returns a Handler for posting to the UI thread.
     *
     * @return
     */
    public Handler getHandler() {
        return manager.getHandler();
    }

    /**
     * Use this method to bind to an AalService. When the service has been successfully bound to
     * this Activity, the onServiceBound() method will be called. Make sure to override that method
     * so you know when the service actually is bound.
     *
     * @param serviceClass
     */
    public void bindToService(Class<?> serviceClass) {
        manager.bindToService(serviceClass);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, Enum)}
     */
    public void setClickEvent(View view, Enum category) {
        VCUtil.setClickEvent(view, category);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, Enum, Object...)}
     */
    public void setClickEvent(View view, Enum category, Object... data) {
        VCUtil.setClickEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, com.xiledsystems.aal.events.Event.Builder)}
     */
    public void setClickEvent(View view, Event.Builder builder) {
        VCUtil.setClickEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Enum)}
     */
    public void setCheckedEvent(CompoundButton view, Enum category) {
        VCUtil.setCheckedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Enum, Object...)}
     */
    public void setCheckedEvent(CompoundButton view, Enum category, Object... data) {
        VCUtil.setCheckedEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Event.Builder)}
     */
    public void setCheckedEvent(CompoundButton view, Event.Builder builder) {
        VCUtil.setCheckedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Enum)}
     */
    public void setTextChangedEvent(TextView view, Enum category) {
        VCUtil.setTextChangedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Enum, Object...)}
     */
    public void setTextChangedEvent(TextView view, Enum category, Object... data) {
        VCUtil.setTextChangedEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Event.Builder)}
     */
    public void setTextChangedEvent(TextView view, Event.Builder builder) {
        VCUtil.setTextChangedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Event.Builder)}
     */
    public void setProgressChangedEvent(SeekBar view, Event.Builder builder) {
        VCUtil.setProgressChangedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Enum)}
     */
    public void setProgressChangedEvent(SeekBar view, Enum category) {
        VCUtil.setProgressChangedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Enum, Object...)}
     */
    public void setProgressChangedEvent(SeekBar view, Enum category, Object... args) {
        VCUtil.setProgressChangedEvent(view, category, args);
    }

    /**
     * Forwards {@link ViewUtil#find(int, Activity)}.
     */
    public <T extends View> T find(int viewResId) {
        return ViewUtil.find(viewResId, this);
    }

    /**
     * Forwards {@link ViewUtil#find(int, View)}.
     */
    public <T extends View> T find(int viewResId, View parent) {
        return ViewUtil.find(viewResId, parent);
    }

    /**
     * Forwards {@link ViewUtil#create(Activity, int)}.
     */
    public <T extends View> T create(int layoutResId) {
        return ViewUtil.create(this, layoutResId);
    }

    /**
     * Forwards {@link ViewUtil#showKeyboard(Context)}.
     */
    public void showKeyboard() {
        ViewUtil.showKeyboard(this);
    }

    /**
     * Forwards {@link ViewUtil#hideKeyboard(Context, View)}.
     */
    public void hideKeyboard(View view) {
        ViewUtil.hideKeyboard(this, view);
    }

    /**
     * Forwards {@link ViewUtil#getScreenSize(Activity)}.
     */
    public Point getScreenSize() {
        return ViewUtil.getScreenSize(this);
    }

    /**
     * Forwards {@link ViewUtil#getAvailableSize(Activity)}.
     */
    public Point getAvailableSize() {
        return ViewUtil.getAvailableSize(this);
    }
}
