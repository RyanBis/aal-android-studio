package com.xiledsystems.aal.supportv4.control.internal;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.control.internal.AndroidScreenModel;
import com.xiledsystems.aal.control.internal.SubScreenModel;

import java.lang.reflect.Method;


@State(DevelopmentState.ALPHA)
public abstract class AndroidSubScreenModelv4 extends SubScreenModel
{

    private static final String TAG = "SubScreenControllerv4_%s";

    @Override
    protected AndroidSubViewControllerv4 getSubViewController() {
        return super.getSubViewController();
    }

    protected AndroidSubScreenModelv4(Fragment frag) {
        this(frag, null);
    }

    protected AndroidSubScreenModelv4(Fragment frag, Class<? extends Enum> category) {
        super(new AndroidSubViewControllerv4(frag));
        setCategory(category);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode) {
        Class<?> actClas = getOtherActivityClass(classToGoTo);
        if (actClas != null) {
            getSubViewController().gotoScreen(actClas, requestCode);
        } else {
            getSubViewController().gotoScreen(classToGoTo, requestCode);
        }
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode, Bundle data) {
        Class<?> actClas = getOtherActivityClass(classToGoTo);
        if (actClas != null) {
            getSubViewController().gotoScreen(actClas, requestCode, data);
        } else {
            getSubViewController().gotoScreen(classToGoTo, requestCode, data);
        }
    }

    public void gotoScreen(Class<?> classToGoTo, Bundle data) {
        Class<?> actClas = getOtherActivityClass(classToGoTo);
        if (actClas != null) {
            getSubViewController().gotoScreen(actClas, data);
        } else {
            getSubViewController().gotoScreen(classToGoTo, data);
        }
    }

    Class<?> getOtherActivityClass(Class<?> screenClass) {
        AndroidScreenModel screen = null;
        try {
            screen = (AndroidScreenModel) screenClass.newInstance();
        } catch (Exception e) {
            // Try a couple constructors
            Log.e(String.format(TAG, screenClass.getSimpleName()), "Unable to find no-argument constructor! Starting another activity from this controller will not work!");
        }
        if (screen != null) {
            try {
                Method getActClass = screenClass.getDeclaredMethod("getActivityClass");
                Class<?> actClass = (Class<?>) getActClass.invoke(screen, new Object[0]);
                return actClass;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
