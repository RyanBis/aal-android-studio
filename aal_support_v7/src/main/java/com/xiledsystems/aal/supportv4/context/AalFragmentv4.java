package com.xiledsystems.aal.supportv4.context;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.xiledsystems.aal.context.BaseFragmentManager;
import com.xiledsystems.aal.context.IAalFragment;
import com.xiledsystems.aal.control.VCUtil;
import com.xiledsystems.aal.events.Event;


public class AalFragmentv4 extends Fragment implements IAalFragment
{

    private SupportFragmentManager manager = new SupportFragmentManager(this);

    @Override public BaseFragmentManager getManager()
    {
        return manager;
    }

    @Override
    public void onPause() {
        super.onPause();
        manager.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        manager.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        manager.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        manager.onStop();
    }

    @Override
    public void onDestroy() {
        manager.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        manager.onLowMemory();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        manager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, Enum)}
     */
    public void setClickEvent(View view, Enum category) {
        VCUtil.setClickEvent(view, category);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, Enum, Object...)}
     */
    public void setClickEvent(View view, Enum category, Object... data) {
        VCUtil.setClickEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, Event.Builder)}
     */
    public void setClickEvent(View view, Event.Builder builder) {
        VCUtil.setClickEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Enum)}
     */
    public void setCheckedEvent(CompoundButton view, Enum category) {
        VCUtil.setCheckedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Enum, Object...)}
     */
    public void setCheckedEvent(CompoundButton view, Enum category, Object... data) {
        VCUtil.setCheckedEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Event.Builder)}
     */
    public void setCheckedEvent(CompoundButton view, Event.Builder builder) {
        VCUtil.setCheckedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Enum)}
     */
    public void setTextChangedEvent(TextView view, Enum category) {
        VCUtil.setTextChangedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Enum, Object...)}
     */
    public void setTextChangedEvent(TextView view, Enum category, Object... data) {
        VCUtil.setTextChangedEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Event.Builder)}
     */
    public void setTextChangedEvent(TextView view, Event.Builder builder) {
        VCUtil.setTextChangedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Event.Builder)}
     */
    public void setProgressChangedEvent(SeekBar view, Event.Builder builder) {
        VCUtil.setProgressChangedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Enum)}
     */
    public void setProgressChangedEvent(SeekBar view, Enum category) {
        VCUtil.setProgressChangedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Enum, Object...)}
     */
    public void setProgressChangedEvent(SeekBar view, Enum category, Object... args) {
        VCUtil.setProgressChangedEvent(view, category, args);
    }
}
