package com.xiledsystems.aal.tests;

import android.content.ContentValues;
import android.database.Cursor;

import com.xiledsystems.aal.sql.DBObject;
import com.xiledsystems.aal.sql.DBObjectFactory;

import java.util.Arrays;


class SqlDbTestObject extends DBObject {

    final static String NAME = "name";
    final static String AGE = "age";
    final static String CREATED = "created";
    final static String DATA = "data";
    final static String TABLE_NAME = "testees";

    String name;
    int age;
    long timeCreated;
    byte[] data;

    static DBObjectFactory<SqlDbTestObject> factory = new DBObjectFactory<SqlDbTestObject>() {
        @Override
        public SqlDbTestObject newObject(Cursor cursor) {
            return new SqlDbTestObject(cursor);
        }
    };


    public SqlDbTestObject() {
    }

    public SqlDbTestObject(Cursor cursor) {
        super(cursor);
    }

    @Override
    public void loadFromCursor(Cursor c) {
        name = getCursorString(NAME, c);
        age = getCursorInt(AGE, c);
        timeCreated = getCursorLong(CREATED, c);
        data = getCursorBlob(DATA, c);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public ContentValues getCreateValues() {
        ContentValues val = new ContentValues(3);
        val.put(NAME, name);
        val.put(AGE, age);
        val.put(CREATED, timeCreated);
        val.put(DATA, data);
        return val;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SqlDbTestObject) {
            SqlDbTestObject o2 = (SqlDbTestObject) o;
            return name.equals(o2.name) && age == o2.age && timeCreated == o2.timeCreated && Arrays.equals(data, o2.data);
        }
        return super.equals(o);
    }
}
