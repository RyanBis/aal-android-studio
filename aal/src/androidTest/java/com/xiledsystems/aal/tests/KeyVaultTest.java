package com.xiledsystems.aal.tests;


import com.xiledsystems.aal.encrypt.AES;
import com.xiledsystems.aal.encrypt.AESStrength;
import com.xiledsystems.aal.encrypt.Key;
import com.xiledsystems.aal.encrypt.RSA;
import com.xiledsystems.aal.encrypt.RSAStrength;
import com.xiledsystems.aal.util.Rand;
import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public final class KeyVaultTest extends MainTest {

    @Test
    public void aesPutGetTest() throws Exception {
        final String namespace = "MyNamespace";
        final String password = Rand.randomNumCharString(64);
        AES aes = new AES(AESStrength.HIGH);
        Key key = aes.generateKey(password, AES.generateSalt());
        aes.putKey(activity, namespace, key);
        Key key2 = aes.getKey(activity, namespace);
        assertNotNull(key2);
        assertFalse(key2.isNull());
        assertTrue(key.equals(key2));
    }

    @Test
    public void rsaPutGetTest() throws Exception {
        final String namespace = "MyNamespace";
        RSA rsa = new RSA(RSAStrength.HIGH);
        Key key = rsa.generateKey();
        rsa.putKey(activity, namespace, key);
        Key key2 = rsa.getKey(activity, namespace);
        assertNotNull(key2);
        assertFalse(key2.isNull());
        assertTrue(key.equals(key2));
    }

}
