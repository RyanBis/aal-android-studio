package com.xiledsystems.aal.tests;


import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.events.EventManagerConfig;
import com.xiledsystems.aal.util.ByteUtil;
import com.xiledsystems.aal.util.TextUtil;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import static org.junit.Assert.assertTrue;


public class EventTest2 extends MainTest {

    private enum TestEvent {
        ONE,
        TWO,
        THREE
    }

    @Test
    public void allEventListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = EventManager.getInstance();

        eventManager.registerAllEventListener(event -> {
            TestEvent tEvent = event.getEventKey(TestEvent.class);
            if (tEvent != null) {
                switch (tEvent) {
                    case ONE:
                        eventManager.registerAllEventListener(event1 -> false);
                        eventManager.postEvent(TestEvent.TWO);
                        return true;
                    case TWO:
                        eventManager.postEvent(TestEvent.THREE);
                        eventManager.registerAllEventListener(event12 -> false);
                        return true;
                    case THREE:
                        release(s);
                        return true;
                }
            }
            return false;
        });
        for (int i = 0; i < 1000; i++) {
            eventManager.registerAllEventListener(event -> false);
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 1000);
        s.acquire();
    }

    @Test
    public void categoryListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = EventManager.getInstance();

        eventManager.registerCategoryListener(TestEvent.class, event -> {
            TestEvent tEvent = event.getEventKey(TestEvent.class);
            if (tEvent != null) {
                switch (tEvent) {
                    case ONE:
                        eventManager.registerCategoryListener(TestEvent.class, event1 -> {
                            List<byte[]> byteList = new ArrayList<>();
                            for (int i = 0; i < 50000; i++) {
                                byteList.add(ByteUtil.random(1024));
                            }
                            return false;
                        });
                        eventManager.postEvent(TestEvent.TWO);
                        return true;
                    case TWO:
                        eventManager.postEvent(TestEvent.THREE);
                        eventManager.registerCategoryListener(TestEvent.class, event12 -> false);
                        return true;
                    case THREE:
                        release(s);
                        return true;
                }
            }
            return false;
        });
        for (int i = 0; i < 5000; i++) {
            eventManager.registerCategoryListener(TestEvent.class, event -> false);
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 5000);
        s.acquire();
    }

    @Test
    public void eventListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = EventManager.getInstance();
        eventManager.registerListener(TestEvent.ONE, event -> {
            eventManager.registerListener(TestEvent.TWO, event1 -> {
                List<byte[]> byteList = new ArrayList<>();
                for (int i = 0; i < 50000; i++) {
                    byteList.add(ByteUtil.random(1024));
                }
                return false;
            });
            eventManager.postEvent(TestEvent.TWO);
            return true;
        });
        eventManager.registerListener(TestEvent.TWO, event -> {
            eventManager.registerListener(TestEvent.THREE, event12 -> {
                List<byte[]> byteList = new ArrayList<>();
                for (int i = 0; i < 50000; i++) {
                    byteList.add(ByteUtil.random(1024));
                }
                return false;
            });
            eventManager.postEvent(TestEvent.THREE);
            return true;
        });
        eventManager.registerListener(TestEvent.THREE, event -> {
            eventManager.registerListener(TestEvent.ONE, event13 -> false);
            release(s);
            return true;
        });
        for (int i = 0; i < 5000; i++) {
            eventManager.registerListener(TestEvent.ONE, event -> true);
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 5000);
        s.acquire();
    }

    @Test(timeout = 3000)
    public void delayedEventTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        final AtomicInteger delayLength = new AtomicInteger(100);
        final AtomicLong start = new AtomicLong(0);
        eventManager.registerCategoryListener(TestEvent.class, event -> {
            long now = System.currentTimeMillis();
            long diff = now - start.get();
            assertTrue("Didn't delay the proper time. Now: " + now + ", Diff: " + diff, diff > delayLength.get());
            // Allow for as much as one cycle + once cycle - 1 ms of buffer, as it depends on when the event is posted
            // relative to the event cycle
            int max = delayLength.get() + (((1000 / eventManager.getCurrentEPS()) * 3) - 1);
            assertTrue(TextUtil.string("Diff: ", diff, " Max: ", max), diff < max);
            if (delayLength.get() < 600) {
                delayLength.set(delayLength.get() + 100);
                start.set(System.currentTimeMillis());
                eventManager.postDelayedEvent(TestEvent.ONE, delayLength.get());
            } else {
                release(s);
            }
            return true;
        });
        start.set(System.currentTimeMillis());
        eventManager.postDelayedEvent(TestEvent.ONE, delayLength.get());
        s.acquire();
    }

    @Test(timeout = 3000)
    public void delayedEventConfigTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        final EventManagerConfig config = new EventManagerConfig().withLogging(true).withDefaultDelay(100L);
        eventManager.setConfig(config);
        final AtomicLong start = new AtomicLong(0);
        eventManager.registerCategoryListener(TestEvent.class, event -> {
            long diff = System.currentTimeMillis() - start.get();
            assertTrue(TextUtil.string("Diff was less than expected. Diff: ", diff, "\tExpected diff: ", config.defaultDelay), diff > config.defaultDelay);
            // Allow for as much as one cycle + once cycle - 1 ms of buffer, as it depends on when the event is posted
            // relative to the event cycle
            int max = (int) (config.defaultDelay + (((1000 / eventManager.getCurrentEPS()) * 2) - 1));
            assertTrue(TextUtil.string("Diff: ", diff, " Max: ", max), diff < max);
            if (config.defaultDelay < 600) {
                config.defaultDelay = config.defaultDelay + 100;
                start.set(System.currentTimeMillis());
                eventManager.setConfig(config);
                eventManager.postEvent(TestEvent.ONE);
            } else {
                release(s);
            }
            return true;
        });
        start.set(System.currentTimeMillis());
        eventManager.postEvent(TestEvent.ONE);
        s.acquire();
    }

    @Test
    public void maxExecutionTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = EventManager.getInstance();
        final int max = 3;
        eventManager.registerListener(TestEvent.ONE, event -> {
            int i = 0;
            i++;
            return true;
        });
        eventManager.registerListener(TestEvent.ONE, event -> true);
        eventManager.registerListener(TestEvent.ONE, event -> {
            assertTrue(event.currentExecutions() + 1 == max);
            release(s);
            return true;
        });
        eventManager.postEventMax(max, TestEvent.ONE);
        s.acquire();
    }

}
