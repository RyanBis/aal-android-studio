package com.xiledsystems.aal.tests;


import android.app.Activity;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.events.EventManagerConfig;
import com.xiledsystems.aal.util.DeviceUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import java.util.concurrent.Semaphore;


@RunWith(AndroidJUnit4.class)
@LargeTest
public abstract class MainTest {

    @Rule
    public ActivityTestRule<Activity> mRule = new ActivityTestRule<>(Activity.class);

    Activity activity;
    EventManager eventManager;


    @Before
    public void setup() {
        Log.e("Test", "Setting up...");
        activity = mRule.getActivity();
        resetManager();
    }

    public void release(Semaphore s) {
        s.release();
    }

    @After
    public void cleanUp() {
        Log.e("Test", "Cleaning up...");
        if (eventManager != null) {
            eventManager.shutdown();
        }
        DeviceUtil.clearAppData(activity, false);
    }

    void resetManager() {
        final EventManagerConfig config = new EventManagerConfig().withLogging(true);
        if (eventManager == null) {
            Log.e("Test", "Assigning EventManager instance");
            eventManager = EventManager.getInstance(config);
        } else {
            eventManager.setConfig(config);
        }
        // Call init() here to ensure we get listeners registered when running multiple tests in a row
        eventManager.init();
    }
}
