package com.xiledsystems.aal.tests;

import com.xiledsystems.aal.encrypt.AES;
import com.xiledsystems.aal.encrypt.AESDecryptedData;
import com.xiledsystems.aal.encrypt.AESEncryptedData;
import com.xiledsystems.aal.encrypt.AESStrength;
import com.xiledsystems.aal.encrypt.Encryptor;
import com.xiledsystems.aal.encrypt.Key;
import com.xiledsystems.aal.encrypt.RSA;
import com.xiledsystems.aal.encrypt.RSAData;
import com.xiledsystems.aal.encrypt.RSAStrength;
import com.xiledsystems.aal.util.TextUtil;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class EncryptionTest extends MainTest {

    @Test
    public void aes128Test() throws Exception {
        doSmallAesTest(AESStrength.LOW, null);
        doSmallAesTest(AESStrength.LOW, "ThisIsUsingAKeyNow");
        doLargeAesTest(AESStrength.LOW, null);
        doLargeAesTest(AESStrength.LOW, "ThisIsUsingAKeyNow");
    }

    @Test
    public void aes192Test() throws Exception {
        doSmallAesTest(AESStrength.MEDIUM, null);
        doSmallAesTest(AESStrength.MEDIUM, "ThisIsUsingAKeyNow");
        doLargeAesTest(AESStrength.MEDIUM, null);
        doLargeAesTest(AESStrength.MEDIUM, "ThisIsUsingAKeyNow");
    }

    @Test
    public void aes256Test() throws Exception {
        doSmallAesTest(AESStrength.HIGH, null);
        doSmallAesTest(AESStrength.HIGH, "ThisIsUsingAKeyNow");
        doLargeAesTest(AESStrength.HIGH, null);
        doLargeAesTest(AESStrength.HIGH, "ThisIsUsingAKeyNow");
    }

    @Test
    public void rsa256Test() throws Exception {
        doSmallRsaTest(RSAStrength.LOW);
        doLargeRsaTest(RSAStrength.LOW);
    }

    @Test
    public void rsa512Test() throws Exception {
        doSmallRsaTest(RSAStrength.MEDIUM);
        doLargeRsaTest(RSAStrength.MEDIUM);
    }

    @Test
    public void rsa1024Test() throws Exception {
        doSmallRsaTest(RSAStrength.HIGH);
        doLargeRsaTest(RSAStrength.HIGH);
    }

    @Test
    public void rsa2048Test() throws Exception {
        doSmallRsaTest(RSAStrength.VERY_HIGH);
        doLargeRsaTest(RSAStrength.VERY_HIGH);
    }

    @Test
    public void rsa4096Test() throws Exception {
        doSmallRsaTest(RSAStrength.ULTRA_HIGH);
        doLargeRsaTest(RSAStrength.ULTRA_HIGH);
    }


    private void doSmallAesTest(AESStrength strength, String password) {
        doAESTest(strength, "This is my data to encrypt.".getBytes(), password);
    }

    private void doLargeAesTest(AESStrength strength, String password) {
        doAESTest(strength, Encryptor.randomBytes((int) ((strength.getBitLength() / 8) * 2.5)), password);
    }

    private void doAESTest(AESStrength strength, byte[] dataToEncrypt, String password) {
        AES aes = new AES(strength);
        final Key key;
        if (TextUtil.isEmpty(password)) {
            key = aes.generateKey();
        } else {
            key = aes.generateKey(password, Encryptor.randomBytes(16));
        }
        AESEncryptedData encData = aes.encryptData(key, dataToEncrypt);
        assertTrue(encData.success());
        assertFalse(Arrays.equals(encData.getData(), dataToEncrypt));
        AESDecryptedData decryptedData = aes.decryptData(key, encData);
        assertTrue(decryptedData.success());
        assertTrue(Arrays.equals(decryptedData.getBytes(), dataToEncrypt));
    }

    private void doSmallRsaTest(RSAStrength strength) {
        doRsaTest(strength, "This is my data.".getBytes());
    }

    private void doLargeRsaTest(RSAStrength strength) {
        doRsaTest(strength, Encryptor.randomBytes((int) ((strength.getBitLength() / 8) * 2.5)));
    }

    private void doRsaTest(RSAStrength strength, byte[] dataToEncrypt) {
        RSA rsa = new RSA(strength);
        final Key key = rsa.generateKey();
        RSAData encData = rsa.encryptData(key, dataToEncrypt);
        assertTrue(encData.getFailure().toString(), encData.success());
        assertFalse(Arrays.equals(encData.getBytes(), dataToEncrypt));
        RSAData decryptedData = rsa.decryptData(key, encData);
        assertTrue(TextUtil.string("Failed to decrypt with error ", decryptedData.getFailure()), decryptedData.success());
        assertTrue(TextUtil.string("\nOriginal byte[]: ", Arrays.toString(dataToEncrypt), "\nDecrypted byte[]: ", Arrays.toString(decryptedData.getBytes())),
                Arrays.equals(decryptedData.getBytes(), dataToEncrypt));
    }

}
