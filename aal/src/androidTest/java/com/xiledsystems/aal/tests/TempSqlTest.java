package com.xiledsystems.aal.tests;


import android.content.ContentValues;
import android.database.Cursor;
import com.xiledsystems.aal.sql.Column;
import com.xiledsystems.aal.sql.DBConfig;
import com.xiledsystems.aal.sql.DataType;
import com.xiledsystems.aal.sql.Query;
import com.xiledsystems.aal.sql.ResultList;
import com.xiledsystems.aal.sql.SqlDAO;
import com.xiledsystems.aal.sql.SqlDb;
import com.xiledsystems.aal.sql.Table;
import com.xiledsystems.aal.sql.annotations.SQLColumn;
import com.xiledsystems.aal.sql.annotations.SQLTable;
import com.xiledsystems.aal.util.Rand;
import com.xiledsystems.aal.util.StopWatch;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;


public class TempSqlTest extends MainTest {

    @Test
    public void addColumnTest() throws Exception {
        DBConfig config = new DBConfig();
        config.addTable(new Table("bands").addColumn("name"));
        SqlDb db = new SqlDb(activity, config);
        db.initialize();

        db.startBatch();
        for (int i = 0; i < 200; i++) {
            db.insert("bands", Rand.getRandomString(25));
        }

        db.endBatch();

        db.addColumnToExistingTable("bands", new Column("albums", DataType.INTEGER), "2");

        ArrayList<String> names = db.getColumnNames("bands");
        assertTrue(names.contains("albums"));

        Query q = new Query().select().from("bands");
        ResultList list = q.execute(db);
        assertFalse(list.isEmpty());
        assertTrue(list.get(0).getInt("albums") == 2);
        db.closeDb();
    }

    @Test
    public void foreignKeyTest() throws Exception {
        DBConfig config = new DBConfig();
        Table customers = new Table("customer").addColumn("name");
        config.addTable(customers);
        Table reps = new Table("reps").addColumn("name");
        config.addTable(reps);
        Table orders = new Table("orders").addColumn(new Column("customer_id", DataType.INTEGER).setForeignKey(customers));
        orders.addColumn(new Column("rep_id", DataType.INTEGER).setForeignKey(reps));
        config.addTable(orders);
        SqlDb db = new SqlDb(activity, config);
        db.initialize();

        long cust1Id;
        long rep2Id;

        ContentValues val = new ContentValues();
        val.put("name", "joe customer 1");
        cust1Id = db.insert(customers.getName(), val);
        val.clear();
        val.put("name", "bob customer 2");
        db.insert(customers.getName(), val);

        val.clear();
        val.put("name", "Jack Rep 1");
        db.insert(reps.getName(), val);
        val.clear();
        val.put("name", "Will Rep 2");
        rep2Id = db.insert(reps.getName(), val);

        val.clear();
        val.put("customer_id", cust1Id);
        val.put("rep_id", rep2Id);
        db.insert(orders.getName(), val);
        db.closeDb();
    }

    @Test
    public void tableNameTest() throws Exception {
        DBConfig config = new DBConfig();
        config.addTable(new Table("first").addColumn("name").addColumn("age", DataType.INTEGER).addColumn("weight", DataType.FLOAT).
                            addColumn("data", DataType.BLOB).addColumn(new Column("uniqueNumber").setIsUnique(true)));
        SqlDb db = new SqlDb(activity, config);
        db.initialize();
        String path = db.getDBPath();
        db.closeDb();
        db = new SqlDb(activity);
        db.openDBFile(path);
        DBConfig genConfig = db.getDbConfig();
        assertTrue(genConfig.equals(config));
        db.closeDb();
    }

    @Test
    public void dbConfigAddTableTest() throws Exception {
        DBConfig config = new DBConfig();
        Table first = new Table("first");
        first.addColumns("name", "address");
        config.addTable(first);
        SqlDb db = new SqlDb(activity, config);
        db.initialize();
        ArrayList<String> tableNames = db.getTableNames();
        assertTrue(tableNames.contains("first"));
        Table second = new Table("second").addColumn("email");
        config.addTable(second);
        db.setDBConfig(config);
        tableNames = db.getTableNames();
        assertTrue(tableNames.contains("second"));
        db.closeDb();
    }

    @Test
    public void daoSingleTest() throws Exception {
        DBConfig config = new DBConfig();
        Table t = new Table("some_table");
        t.addColumns("name", "age", "dub", "floats", "longs", "shorts", "bytes", "bools", "arrays");
        config.addTable(t);
        SqlDb db = new SqlDb(activity, config);
        db.initialize();
        Thing thing = new Thing();
        thing.name = "John";
        thing.age = 20;
        thing.addData("name", thing.name);
        thing.addData("age", thing.age);
        StopWatch timer = new StopWatch();
        timer.start();
        thing.insertWithStatement(db);
        long insertTime = timer.stop();
        timer.start();
        Query q = new Query().select().from("some_table");
        List<Thing> list = q.getObjectList(db, Thing::new);
        long loadTime = timer.stop();
        int i = 0;
        i++;
    }

    @Test
    public void daoMultiTest() throws Exception {
        DBConfig config = new DBConfig();
        Table t = new Table("some_table");
        t.addColumns("name", "age", "dub", "floats", "longs", "shorts", "bytes", "bools", "arrays");
        config.addTable(t);
        SqlDb db = new SqlDb(activity, config);

        db.startBatch();
        StopWatch timer = new StopWatch();
        timer.start();
        byte[] bytes = Rand.randomBytes(128);
        for (int i = 0; i < 5000; i++) {
            Thing thing = new Thing();
            thing.name = "John";
            thing.age = 20;
            thing.array = bytes;
            thing.bool = true;
            thing.byt = 0x58;
            thing.dub = 3.14;
            thing.flt = 1.1f;
            thing.lng = 80239895984L;
            thing.sht = 4000;
            thing.insertWithStatement(db);
            int j = 0;
            j++;
        }
        db.endBatch();
        long insertTime = timer.stop();
        timer.start();
        Query q = new Query().select().from("some_table");
        List<Thing> list = q.getObjectList(db, Thing::new);
        long loadTime = timer.stop();
        int i = 0;
       i++;
    }

    @Test
    public void daoMultiUpdateTest() throws Exception {
        DBConfig config = new DBConfig();
        Table t = new Table("some_table");
        t.addColumns("name", "age", "dub", "floats", "longs", "shorts", "bytes", "bools", "arrays");
        config.addTable(t);
        SqlDb db = new SqlDb(activity, config);

        db.startBatch();
        StopWatch timer = new StopWatch();
        timer.start();
        byte[] bytes = Rand.randomBytes(128);
        for (int i = 0; i < 2500; i++) {
            Thing thing = new Thing();
            thing.name = "John";
            thing.age = 20;
            thing.array = bytes;
            thing.bool = true;
            thing.byt = 0x58;
            thing.dub = 3.14;
            thing.flt = 1.1f;
            thing.lng = 80239895984L;
            thing.sht = 4000;
            thing.insertWithStatement(db);
            int j = 0;
            j++;
        }
        db.endBatch();
        long insertTime = timer.stop();
        timer.start();
        Query q = new Query().select().from("some_table");
        List<Thing> list = q.getObjectList(db, Thing::new);
        long loadTime = timer.stop();
        int size = list.size();
        timer.start();
        db.startBatch();
        for (int i = 0; i < size; i++) {
            Thing thing = list.get(i);
            thing.age += i;
            thing.bool = false;
            thing.name = "Frank";
            thing.update(db);
        }
        db.endBatch();
        long updateTime = timer.stop();
        q = new Query().select().from("some_table");
        list = q.getObjectList(db, Thing::new);
        int i = 0;
        i++;
    }

    @SQLTable(tableName = "some_table")
    public class Thing extends SqlDAO {

        @SQLColumn(columnName = "name")
        private String name;

        @SQLColumn(columnName = "age")
        private int age;

        @SQLColumn(columnName = "dub")
        private double dub;

        @SQLColumn(columnName = "floats")
        private float flt;

        @SQLColumn(columnName = "longs")
        private Long lng;

        @SQLColumn(columnName = "shorts")
        private short sht;

        @SQLColumn(columnName = "bytes")
        private Byte byt;

        @SQLColumn(columnName = "bools")
        private boolean bool;

        @SQLColumn(columnName = "arrays")
        private byte[] array;


        public Thing() {
            super(null);
        }

        protected Thing(Cursor cursor) {
            super(cursor);
        }
    }

}
