package com.xiledsystems.aal.sql;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@State(DevelopmentState.PRODUCTION)
public class DBConfig {

    private ArrayList<Table> tables;
    private Set<Index> indices = new HashSet<>();
    private int curIndex = -1;
    private String dbName = null;

    public DBConfig() {
        tables = new ArrayList<Table>();
    }

    public void setDbName(String name) {
        dbName = name;
    }

    public String getDbName() {
        return dbName;
    }

    public void addTable(Table table) {
        tables.add(table);
    }

    public void addTable(String table, String[] columns) {
        Table t = new Table(table);
        t.addColumns(columns);
        tables.add(t);
    }

    public void addIndex(String table, String column, String indexName) {
        indices.add(new Index(table, column, indexName));
    }

    public Iterator<Index> getIndexIterator() {
        return indices.iterator();
    }

    public Iterator<Table> getTableIterator() {
        return tables.iterator();
    }

    public int tableCount() {
        return tables.size();
    }

    public Table getTable(int index) {
        return tables.get(index);
    }

    public Table getTable(String tableName) {
        int index = getTableIndex(tables, tableName);
        if (index != -1) {
            return tables.get(index);
        } else {
            return null;
        }
    }

    public ArrayList<Table> getTables() {
        return tables;
    }

    public Table next() {
        curIndex++;
        if (curIndex > tables.size() - 1) {
            curIndex = tables.size() - 1;
        }
        if (curIndex == -1) {
            return null;
        }
        return tables.get(curIndex);
    }

    public Table previous() {
        curIndex--;
        if (curIndex < 0 && tables.size() > 0) {
            curIndex = 0;
        } else {
            return null;
        }
        return tables.get(curIndex);
    }

    public int tableIndex(String tableName) {
        return getTableIndex(tables, tableName);
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("Schema: ").append(tables.toString()).append(" Indices: ").append(indices.toString());
        return b.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof DBConfig) {
            DBConfig other = (DBConfig) o;
            return other.tables.equals(tables) && other.indices.equals(indices) && other.dbName.equals(dbName);
        }
        return false;
    }

    DBSchema toDBSchema() {
        DBSchema config = new DBSchema();
        config.setDbName(dbName);
        Iterator<Index> it = indices.iterator();
        if (it != null) {
            while (it.hasNext()) {
                Index i = it.next();
                config.addIndex(i.table, i.column, i.name);
            }
        }
        for (int i = 0; i < tables.size(); i++) {
            config.addTable(tables.get(i));
        }
        return config;
    }

    private static int getTableIndex(ArrayList<Table> tables, String tableName) {
        int size = tables.size();
        for (int i = 0; i < size; i++) {
            if (tables.get(i).getName().equals(tableName)) {
                return i;
            }
        }
        return -1;
    }

    protected class Index {
        public String table;
        public String name;
        public String column;

        public Index() {
        }

        public Index(String table, String column, String name) {
            this.table = table;
            this.column = column;
            this.name = name;
        }

        @Override
        public String toString() {
            StringBuilder b = new StringBuilder();
            b.append(name).append(" on column ").append(column).append(" in table ").append(table);
            return b.toString();
        }
    }
}
