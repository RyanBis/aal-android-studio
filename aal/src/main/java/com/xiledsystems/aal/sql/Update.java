package com.xiledsystems.aal.sql;


public class Update extends Query {

    private static final String UPDATE = "UPDATE";
    private static final String SET = "SET";


    public Update(String table) {
        getStatementBuilder().append(UPDATE).append(" ").append(SqlDb.checkTableName(table)).append(" ");
    }

    public Update set(String column, long value) {
        set_private(column);
        checkValues();
        getValues().add(String.valueOf(value));
        return this;
    }

    public Update set(String column, String value) {
        set_private(column);
        checkValues();
        getValues().add(value);
        return this;
    }

    public Update set(String column, float value) {
        set_private(column);
        checkValues();
        getValues().add(String.valueOf(value));
        return this;
    }

    public Update isNull() {
        getStatementBuilder().append("IS NULL");
        return this;
    }

    @Override
    public Update where(String columnName) {
        return (Update) super.where(columnName);
    }

    private void set_private(String column) {
        getStatementBuilder().append(SET).append(" ").append(SqlDb.checkColumnName(column)).append(" = ? ");
    }

}
