package com.xiledsystems.aal.sql;

import android.database.Cursor;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ThreadManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

@State(DevelopmentState.PRODUCTION)
public class Query extends BaseOp {

	protected final static String SELECT = "SELECT";
	protected final static String WHERE = "WHERE";
	protected final static String FROM = "FROM";
	protected final static String LIKE = "LIKE";
	protected final static String AND = "AND";
	protected final static String OR = "OR";
	protected final static String AS = "AS";
	protected final static String JOIN = "JOIN";
	protected final static String ON = "ON";
	protected final static String NOT = "NOT";
	protected final static String ORDER = "ORDER BY";
	protected final static String LIMIT = "LIMIT";
	protected final static String OFFSET = "OFFSET";
	protected final static String LOWER = "LOWER";
	protected final static String UPPER = "UPPER";
	protected final static String MIN = "MIN";
	protected final static String MAX = "MAX";
	protected final static String AVG = "AVG";
	protected final static String SUM = "SUM";
	protected static final String UPDATE = "UPDATE";
	protected static final String SET = "SET";
	
	private StringBuilder statement = new StringBuilder();
	
	private ArrayList<String> values;
		
	/**
	 * Use select() [no arguments] to return all columns. Otherwise, provide a string array of the columns
	 * you wish returned. (For one, you can just use a single string)
	 *
	 * 
	 * @param whatToSelect
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query select(String... whatToSelect) {
		statement.append(SELECT);
		if (whatToSelect == null || whatToSelect.length == 0) {
			statement.append(" * ");
		} else {			
			statement.append(" ").append(getString(whatToSelect));
		}
		return this;
	}

	/**
	 * Alternative select method, which lowercases the requested
	 * columns.
	 *
	 * @param whatToSelect
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query selectLowerCase(String... whatToSelect) {
		statement.append(SELECT);
		statement.append(" ").append(LOWER).append("(");
		if (whatToSelect == null || whatToSelect.length == 0) {
			statement.append(" * ");
		} else {
			statement.append(" ").append(getString(whatToSelect));
		}
		statement.append(") ");
		return this;
	}

	/**
	 * Alternative select method which upper cases the requested
	 * columns.
	 *
	 * @param whatToSelect
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query selectUpperCase(String... whatToSelect) {
		statement.append(SELECT);
		statement.append(" ").append(UPPER).append("(");
		if (whatToSelect == null || whatToSelect.length == 0) {
			statement.append(" * ");
		} else {
			statement.append(" ").append(getString(whatToSelect));
		}
		statement.append(") ");
		return this;
	}

	/**
	 * Alternative select method which finds the lowest value in the requested
	 * column.
	 * @param whatToSelect
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query selectLowestValue(String whatToSelect) {
		statement.append(SELECT);
		statement.append(" ").append(MIN).append("(CAST(");
		statement.append(whatToSelect);
		statement.append(" as INTEGER)) ");
		return this;
	}

	/**
	 * Alternative select method which finds the highest value in the requested
	 * column.
	 * @param whatToSelect
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query selectHighestValue(String whatToSelect) {
		statement.append(SELECT);
		statement.append(" ").append(MAX).append("(CAST(");
		statement.append(whatToSelect);
		statement.append(" as INTEGER)) ");
		return this;
	}

	/**
	 * Alternative select method which finds the average value in the requested
	 * column.
	 * @param whatToSelect
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query selectAverageValue(String whatToSelect) {
		statement.append(SELECT);
		statement.append(" ").append(AVG).append("(CAST(");
		statement.append(whatToSelect);
		statement.append(" as INTEGER)) ");
		return this;
	}

	/**
	 * Alternative select method which finds the sum of the values in the requested
	 * column.
	 * @param whatToSelect
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query selectSumValue(String whatToSelect) {
		statement.append(SELECT);
		statement.append(" ").append(SUM).append("(CAST(");
		statement.append(whatToSelect);
		statement.append(" as INTEGER)) ");
		return this;
	}

	/**
	 * Alternative select method which finds the count of rows in the requested column (usually just use selectCount() ).
	 * column.
	 * @param whatToCount
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
    public Query selectCount(String... whatToCount) {
        statement.append(SELECT);
        statement.append(" COUNT(");
        if (whatToCount == null) {
            statement.append("*");
        } else {
            statement.append(getString(whatToCount).trim());
        }
        statement.append(") ");
        return this;
    }
	
	/**
	 * Sets the table to pull this query from.
	 * 
	 * @param tableName
	 * @return this Query object, allowing for chaining of methods to build the query.
	 * @exception IllegalArgumentException If the table name contains a single quotation mark
	 */
	public Query from(String tableName) {
		statement.append(FROM);
		if (tableName.contains("'")) {
			throw new IllegalArgumentException("Table names cannot contain single quotation marks.");
		}
		statement.append(" ").append(SqlDb.checkTableName(tableName)).append(" ");
		return this;
	}
		
	/**
	 * Set the where statement.
	 *
	 * @param columnName
	 * @return this Query object, allowing for chaining of methods to build the query.
	 * @exception IllegalArgumentException If the column name contains a double quotation mark
	 */
	public Query where(String columnName) {
		statement.append(WHERE);
		if (columnName.contains("\"")) {
			throw new IllegalArgumentException("Column names cannot contain double quotation marks.");
		}
		statement.append(" ").append(SqlDb.checkColumnName(columnName)).append(" ");
		return this;
	}

	/**
	 * NOT statement
	 *
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
    public Query not() {
        statement.append(" ").append(NOT).append(" ");
        return this;
    }

	/**
	 * Not equal to statement ( <code>!= ?</code> ).
	 * This wraps the supplied value in the whereargs.
	 *
	 * @param value
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query notEqualTo(String value) {
		statement.append("!=? ");
		checkValues();
		values.add(value);
		return this;
	}

	/**
	 * @inheritDoc
	 */
	public Query notEqualTo(int value) {
		return notEqualTo(String.valueOf(value));
	}

	/**
	 * @inheritDoc
	 */
	public Query notEqualTo(long value) {
		return notEqualTo(String.valueOf(value));
	}

	/**
	 * Not equal to statement ( <code>!= ?</code> ).
	 * This wraps the supplied value in the whereargs.
	 * For boolean, it converts it to a <code>1</code> if
	 * <code>true</code>, <code>0</code> otherwise.
	 *
	 * @param value
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query notEqualTo(boolean value) {
		return notEqualTo(String.valueOf(value ? 1 : 0));
	}

	/**
	 * Equal to statement ( <code>= ?</code> ).
	 * This wraps the supplied value in the whereargs.
	 *
	 * @param value
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query equalTo(String value) {
		statement.append("=? ");
		checkValues();
		values.add(value);
		return this;
	}

	/**
	 * @inheritDoc
	 */
	public Query equalTo(int value) {
		return equalTo(String.valueOf(value));
	}

	/**
	 * @inheritDoc
	 */
	public Query equalTo(long value) {
		return equalTo(String.valueOf(value));
	}

	/**
	 * Equal to statement ( <code>= ?</code> ).
	 * This wraps the supplied value in the whereargs.
	 * For boolean, it converts it to a <code>1</code> if
	 * <code>true</code>, <code>0</code> otherwise.
	 *
	 * @param value
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query equalTo(boolean value) {
		return equalTo(String.valueOf(value));
	}

	/**
	 * Less than statement ( <code>< ?</code> ).
	 * This wraps the supplied value in the whereargs.
	 *
	 * @param value
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query lessThan(String value) {
		statement.append("<? ");
		checkValues();
		values.add(value);
		return this;
	}

	/**
	 * @inheritDoc
	 */
	public Query lessThan(int value) {
		return lessThan(String.valueOf(value));
	}

	/**
	 * @inheritDoc
	 */
	public Query lessThan(long value) {
		return lessThan(String.valueOf(value));
	}

	/**
	 * Less than equal to statement ( <code><= ?</code> ).
	 * This wraps the supplied value in the whereargs.
	 *
	 * @param value
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query lessThanEqual(String value) {
		statement.append("<=? ");
		checkValues();
		values.add(value);
		return this;
	}

	/**
	 * @inheritDoc
	 */
	public Query lessThanEqual(int value) {
		return lessThanEqual(String.valueOf(value));
	}

	/**
	 * @inheritDoc
	 */
	public Query lessThanEqual(long value) {
		return lessThanEqual(String.valueOf(value));
	}

	/**
	 * Less than equal to statement ( <code><= ?</code> ).
	 * This wraps the supplied value in the whereargs.
	 *
	 * @param value
	 * @return this Query object, allowing for chaining of methods to build the query.
	 */
	public Query greaterThan(String value) {
		statement.append(">? ");
		checkValues();
		values.add(value);
		return this;
	}

	public Query greaterThan(int value) {
		return greaterThan(String.valueOf(value));
	}

	public Query greaterThan(long value) {
		return greaterThan(String.valueOf(value));
	}
	
	public Query greaterThanEqual(String value) {
		statement.append(">=? ");
		checkValues();
		values.add(value);
		return this;
	}

	public Query greaterThanEqual(int value) {
		return greaterThanEqual(String.valueOf(value));
	}

	public Query greaterThanEqual(long value) {
		return greaterThanEqual(String.valueOf(value));
	}
	
	public Query as(String as) {
		statement.append(AS);
		statement.append(" ").append(as).append(" ");
		return this;
	}
	
	public Query join(String table) {
		statement.append(JOIN);
		statement.append(" ").append(table).append(" ");
		return this;
	}
	
	public Query on(String onstatement) {
		statement.append(ON);
		statement.append(" ").append(onstatement).append(" ");
		return this;
	}
	
	public Query like(String value) {
		return like(LikeType.PERC_PERC, value);
	}

	public Query like(int value) {
		return like(String.valueOf(value));
	}

	public Query like(long value) {
		return like(String.valueOf(value));
	}

	public Query like(LikeType type, String value) {
		statement.append(LIKE).append(" '");
		statement = appendLikeType(statement, type);
		statement.append(value);
		statement = appendLikeType(statement, type);
		statement.append("' ");
		return this;
	}

	public Query like(LikeType type, int value) {
		return like(type, String.valueOf(value));
	}

	public Query like(LikeType type, long value) {
		return like(type, String.valueOf(value));
	}
	
	public Query and() {
		statement.append(AND).append(" ");
		return this;
	}

    public Query and(String column) {
        statement.append(AND).append(" ").append(column);
        return this;
    }

	/**
	 * Add a column name to the query statement. This is useful when using AND, OR, or
	 * parenthesis when constructing complex queries.
     */
	public Query col(String column) {
		statement.append(" ").append(column);
		return this;
	}
	
	/**
	 * Open parenthesis. Don't foget to call clp to close the parenthesis. This is useful when
	 * making a more robust query (for example, you want one column to match, and one of many
	 * others). <code>"( "</code>
	 */
	public Query opp() {
		statement.append("( ");
		return this;
	}
	
	/**
	 * Close parenthesis. <code>") "</code>
	 * @return
	 */
	public Query clp() {
		statement.append(") ");
		return this;
	}

	/**
	 * OR <code>"OR "</code>
	 * @return
	 */
	public Query or() {
		statement.append(OR).append(" ");
		return this;
	}

	/**
	 * OR <code>"OR &lt;column&gt;"</code>
	 * @return
     */
	public Query or(String column) {
		statement.append(OR).append(" ").append(column);
		return this;
	}

	/**
	 * ORDER BY statement <code>"ORDER BY <b>column</b> "</code>
	 * @param column - the column to order the results by
	 * @return
	 */
	public Query order(String column) {
		statement.append(ORDER);
		statement.append(" ").append(column).append(" ");
		return this;
	}

	/**
	 * LIMIT statement <code>"LIMIT <b>limit</b>"</code>
	 *
	 * @param limit
	 * @return
	 */
	public Query limit(String limit) {
		statement.append(LIMIT);
		statement.append(" ").append(limit);
		return this;
	}

	/**
	 * LIMIT statement <code>"LIMIT <b>limit</b>"</code>
	 * @param limit
	 * @return
	 */
	public Query limit(int limit) {
		return limit(String.valueOf(limit));
	}

	/**
	 * OFFSET statement <code>"OFFSET <b>offSet</b>"</code>
	 * @param offSet
	 * @return
	 */
	public Query offSet(int offSet) {
		statement.append(OFFSET);
		statement.append(" ").append(offSet);
		return this;
	}

	/**
	 * OFFSET statement <code>"OFFSET <b>offSet</b>"</code>
	 * @param offSet
	 * @return
	 */
	public Query offSet(String offSet) {
		statement.append(OFFSET);
		statement.append(" ").append(offSet);
		return this;
	}

	/**
	 * If you would prefer to write your own SQL statement, use this method.
	 * It first clears the current statement, so unlike most other methods in
	 * this class, you can not chain this method. It is best to use
	 * {@link #manualStatement(String, String[])} and supply the whereargs,
	 * rather than wrapping everything into one string.
	 *
	 * @param statement
	 */
	public void manualStatement(String statement) {
		this.statement = new StringBuilder();
		this.statement.append(statement);
	}

	/**
	 * If you would prefer to supply the SQL statement, and whereargs, you can use this
	 * method. Like {@link #manualStatement(String)}}, this method can not be chained.
	 *
	 * @param statement
	 * @param args
	 */
	public void manualStatement(String statement, String[] args) {
		this.statement = new StringBuilder();
		this.statement.append(statement);
		this.values = new ArrayList<String>(Arrays.asList(args));
	}

	/**
	 * Convenience method to return a list of DBObjects from the db. This handles
	 * synchronizing the database, so it's thread safe, and creates a list, loops through the
	 * {@link Cursor}, adding entries to the list, then closing the {@link Cursor} for you.
	 *
	 * @param db
	 * @param factory
	 * @param <T>
	 * @return
	 */
	public <T extends DBObject> ArrayList<T> getObjectList(SqlDb db, DBObjectFactory<T> factory) {
		ArrayList<T> list = new ArrayList<>();
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						T item = factory.newObject(c);
						list.add(item);
					} while (c.moveToNext());
				}
				c.close();
			}
		}
		return list;
	}

	/**
	 * Convenience method to return a list of DBObjects from the db. This handles
	 * synchronizing the database, so it's thread safe, and creates a list, loops through the
	 * {@link Cursor}, adding entries to the list, then closing the {@link Cursor} for you.
	 * This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param db
	 * @param factory
	 * @param listener
	 * @param <T>
	 */
	public <T extends DBObject> void getObjectListAsync(final SqlDb db, final DBObjectFactory<T> factory, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				ArrayList<T> list = new ArrayList<>();
				synchronized (db.getLock()) {
					Cursor c = executeQuery(db);
					if (c != null) {
						if (c.moveToFirst()) {
							do {
								T item = factory.newObject(c);
								list.add(item);
							} while (c.moveToNext());
						}
						c.close();
					}
					db.callListener_internal(listener, list, ReturnType.ARRAYLIST_DBOBJECT);
				}
			}
		});
	}

	/**
	 * Convenience method to return a list of DBObjects from the db. This handles
	 * synchronizing the database, so it's thread safe, and creates a list, loops through the
	 * {@link Cursor}, adding entries to the list, then closing the {@link Cursor} for you.
	 * For best performance, use {@link #getObjectList(SqlDb, DBObjectFactory)}}.
	 * This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param db
	 * @param clazz
	 * @param listener
	 * @param <T>
	 */
	public <T extends DBObject> void getObjectListAsync(final SqlDb db, final Class<T> clazz, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				ArrayList<T> list = new ArrayList<>();
				try {
					Constructor<T> ctor = clazz.getConstructor(Cursor.class);
					synchronized (db.getLock()) {
						Cursor c = executeQuery(db);
						if (c != null) {
							if (c.moveToFirst()) {
								do {
									T item = ctor.newInstance(c);
									list.add(item);
								} while (c.moveToNext());
							}
							c.close();
						}
					}
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
				db.callListener_internal(listener, list, ReturnType.ARRAYLIST_DBOBJECT);
			}
		});
	}

	/**
	 * Convenience method to return a list of DBObjects from the db. This handles
	 * synchronizing the database, so it's thread safe, and creates a list, loops through the
	 * {@link Cursor}, adding entries to the list, then closing the {@link Cursor} for you.
	 * For best performance, use {@link #getObjectList(SqlDb, DBObjectFactory)}}
	 *
	 * @param clazz
	 * @param db
	 * @param <T>
	 * @return
	 */
	public <T extends DBObject> ArrayList<T> getObjectList(Class<T> clazz, SqlDb db) {
		ArrayList<T> list = new ArrayList<>();
		try {
			Constructor<T> ctor = clazz.getDeclaredConstructor(Cursor.class);
			ctor.setAccessible(true);
			synchronized (db.getLock()) {
				Cursor c = executeQuery(db);
				if (c != null) {
					if (c.moveToFirst()) {
						do {
							T item = ctor.newInstance(c);
							list.add(item);
						} while (c.moveToNext());
					}
					c.close();
				}
			}
			return list;
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return null;
		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Convenience method to get the count when using {@link #selectCount(String...)} to return
	 * the count of this {@link Query}. This is the same as {@link #getInt(SqlDb)}.
	 * @param db
	 * @return
	 */
	public int getCount(SqlDb db) {
		return getInt(db);
	}

	/**
	 * A version of {@link #getCount(SqlDb)}, which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getCountAsync(final SqlDb db, final SqlDbReturn listener) {
		getIntAsync(db, listener);
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting an int.
	 * @param db
	 * @return
	 */
	public int getInt(SqlDb db) {
		int value = 0;
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					value = c.getInt(0);
				}
				c.close();
			}
		}
		return value;
	}

	/**
	 * A version of {@link #getInt(SqlDb)}, which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getIntAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				int count = getInt(db);
				db.callListener_internal(listener, count, ReturnType.INT);
			}
		});
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting a long.
	 * @param db
	 * @return
	 */
	public long getLong(SqlDb db) {
		long value = 0;
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					value = c.getLong(0);
				}
				c.close();
			}
		}
		return value;
	}

	/**
	 * A version of {@link #getLong(SqlDb)}, which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getLongAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				long count = getLong(db);
				db.callListener_internal(listener, count, ReturnType.LONG);
			}
		});
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting a String.
	 * @param db
	 * @return
	 */
	public String getString(SqlDb db) {
		String value = null;
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					value = c.getString(0);
				}
				c.close();
			}
		}
		return value;
	}

	/**
	 * A version of {@link #getString(SqlDb)}, which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getStringAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				String val = getString(db);
				db.callListener_internal(listener, val, ReturnType.STRING);
			}
		});
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting a blob (byte[]).
	 * @param db
	 * @return
	 */
	public byte[] getBlob(SqlDb db) {
		byte[] blob = null;
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					blob = c.getBlob(0);
				}
				c.close();
			}
		}
		return blob;
	}

	/**
	 * A version of {@link #getBlob(SqlDb)}, which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getBlobAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				byte[] blob = getBlob(db);
				db.callListener_internal(listener, blob, ReturnType.BLOB);
			}
		});
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting a ArrayList of Strings.
	 * @param db
	 * @return
	 */
	public ArrayList<String> getStringList(SqlDb db) {
		ArrayList<String> list = new ArrayList<>();
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						list.add(c.getString(0));
					} while (c.moveToNext());
				}
				c.close();
			}
		}
		return list;
	}

	/**
	 * A version of {@link #getStringList(SqlDb)} which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getStringListAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				ArrayList<String> list = getStringList(db);
				db.callListener_internal(listener, list, ReturnType.ARRAYLIST_STRING);
			}
		});
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting a ArrayList of ints.
	 * @param db
	 * @return
	 */
	public ArrayList<Integer> getIntList(SqlDb db) {
		ArrayList<Integer> list = new ArrayList<>();
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						list.add(c.getInt(0));
					} while (c.moveToNext());
				}
				c.close();
			}
		}
		return list;
	}

	/**
	 * A version of {@link #getIntList(SqlDb)} which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getIntListAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				ArrayList<Integer> list = getIntList(db);
				db.callListener_internal(listener, list, ReturnType.ARRAYLIST_INT);
			}
		});
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting a ArrayList of longs.
	 * @param db
	 * @return
	 */
	public ArrayList<Long> getLongList(SqlDb db) {
		ArrayList<Long> list = new ArrayList<>();
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						list.add(c.getLong(0));
					} while (c.moveToNext());
				}
				c.close();
			}
		}
		return list;
	}

	/**
	 * A version of {@link #getLongList(SqlDb)} which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getLongListAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				ArrayList<Long> list = getLongList(db);
				db.callListener_internal(listener, list, ReturnType.ARRAYLIST_LONG);
			}
		});
	}

	/**
	 * Convenience method for executing this {@link Query}, and getting a ArrayList of blobs (byte[]).
	 * @param db
	 * @return
	 */
	public ArrayList<byte[]> getBlobList(SqlDb db) {
		ArrayList<byte[]> list = new ArrayList<>();
		synchronized (db.getLock()) {
			Cursor c = executeQuery(db);
			if (c != null) {
				if (c.moveToFirst()) {
					do {
						list.add(c.getBlob(0));
					} while (c.moveToNext());
				}
				c.close();
			}
		}
		return list;
	}

	/**
	 * A version of {@link #getBlobList(SqlDb)} which is executed in a background thread.
	 * @param db
	 * @param listener
	 */
	public void getBlobListAsync(final SqlDb db, final SqlDbReturn listener) {
		ThreadManager.getInstance(true).execute(new Runnable() {
			@Override
			public void run() {
				ArrayList<byte[]> list = getBlobList(db);
				db.callListener_internal(listener, list, ReturnType.ARRAYLIST_BLOB);
			}
		});
	}

	protected StringBuilder getStatementBuilder() {
		return statement;
	}

	protected ArrayList<String> getValues() {
		return values;
	}

	/**
	 * Internal method used by AAL's backend.
	 * @return the backing statement.
	 */
	@Override
	public String getStatement() {
		if (statement != null) {
			return statement.toString();
		}
		return "";
	}

	/**
	 * Internal method used by AAL's backend.
	 * @return the backing whereArgs.
	 */
	@Override
	public String[] getSelectionArgs() {
		if (values == null) {
			return null;
		} else {
			return values.toArray(new String[0]);
		}
	}

	void checkValues() {
		if (values == null) {
			values = new ArrayList<>();
		}
	}

	private static String getString(String[] what) {
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < what.length; i++) {
			b.append(SqlDb.checkColumnName(what[i]));
			if (i != what.length - 1) {
				b.append(", ");
			} else {
				b.append(" ");
			}
		}
		return b.toString();
	}

	private static StringBuilder appendLikeType(StringBuilder statement, LikeType type) {
		switch (type) {
			case PERC_NONE:
			case PERC_PERC:
			case PERC_UNDER:
				statement.append("%");
				break;
			case UNDER_NONE:
			case UNDER_PERC:
			case UNDER_UNDER:
				statement.append("_");
				break;
			default:
				break;
		}
		return statement;
	}

	/**
	 * {@link Enum} for setting how the like statement's value should be
	 * interpreted in the query (for example match amy amount of characters
	 * before this <code>value</code>, and any single character after it).
	 * The default {@link #like(String)}} method uses PERC_PERC (match anything
	 * that contains the <code>value</code>.
	 */
	public enum LikeType {

		/**
		 * Anything before, and any single character after the value.
		 * <code>%value_</code>
		 */
		PERC_UNDER,

		/**
		 * Anything before and after the value.
		 * <code>%value%</code>
		 */
		PERC_PERC,

		/**
		 * Anything before, and nothing after the value.
		 * <code>%value</code>
		 */
		PERC_NONE,

		/**
		 * Any single character before, and anything after the value.
		 * <code>_value%</code>
		 */
		UNDER_PERC,

		/**
		 * Any single character before, and after value.
		 * <code>_value_</code>
		 */
		UNDER_UNDER,

		/**
		 * Any character before the value.
		 * <code>_value</code>
		 */
		UNDER_NONE,

		/**
		 * Any characters after the value.
		 * <code>value%</code>
		 */
		NONE_PERC,

		/**
		 * Any single character after the value.
		 * <code>value_</code>
		 */
		NONE_UNDER
	}

}