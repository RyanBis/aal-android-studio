package com.xiledsystems.aal.sql;


public class SqlDbException extends RuntimeException {

    public SqlDbException() {
        super();
    }

    public SqlDbException(String message) {
        super(message);
    }
}
