package com.xiledsystems.aal.sql;

import android.database.Cursor;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public abstract class BaseOp {
	

	public abstract String getStatement();
	public abstract String[] getSelectionArgs();
	
	public Cursor executeQuery(SqlDb db) {
		return db.query(this);
	}
	
	/**
	 * Executes the query, returning a ResultList of the data. The ResultList
	 * is an ArrayList, which also has the column Names.
	 * @param db
	 * @return
	 */
	public ResultList execute(SqlDb db) {
		Object lock = db.getLock();
		ResultList results = null;
		synchronized (lock) {
			final Cursor cursor = db.query(this);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					String[] names = cursor.getColumnNames();
					results = new ResultList(names);
					do {
						Row result = new Row();						
						String[] data = getDataArray(cursor);
						result.setColumnData(data);
						results.add(result);
					} while (cursor.moveToNext());
				}
				cursor.close();
			}
		}
		return results;
	}

	protected static String[] getDataArray(Cursor cursor) {
		int size = cursor.getColumnCount();
		String[] data = new String[size];
		for (int i = 0; i < size; i++) {
			data[i] = cursor.getString(i);
		}
		return data;
	}

}
