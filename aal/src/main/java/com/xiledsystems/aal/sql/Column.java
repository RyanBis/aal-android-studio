package com.xiledsystems.aal.sql;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;


@State(DevelopmentState.PRODUCTION)
public final class Column {

	private final String name;
	private DataType dataType = DataType.STRING;
	private boolean isUnique = false;
	private String foreignKeyTable;
	private String foreignKeyColumn;
	
	
	public Column(String name) {
		this.name = name;
	}

	public Column(String name, DataType dataType) {
		this.name = name;
		this.dataType = dataType;
	}
	
	public final String getName() {
		return name;
	}

	public final DataType getDataType() {
		return dataType;
	}
	
	public final String getDataType_string() {
		return dataType.dataName();
	}

	/**
	 * Map this column to a foreign key from another {@link Table}.
	 * This simply overloads {@link #setForeignKey(String, String)}.
	 * @param table
	 * @return
     */
	public final Column setForeignKey(Table table) {
		return setForeignKey(table.getName(), table.getIdColumn());
	}

	/**
	 * Map this column to a foreign key from another table.
	 * @param tableName
	 * @param idColumnName
     * @return
     */
	public final Column setForeignKey(String tableName, String idColumnName) {
		foreignKeyTable = tableName;
		foreignKeyColumn = idColumnName;
		return this;
	}

	public final String getForeignKeyTableName() {
		return foreignKeyTable;
	}

	public final String getForeignKeyIdColumnName() {
		return foreignKeyColumn;
	}

	public final Column setDataType(DataType type) {
		dataType = type;
		return this;
	}

	public final boolean isForeignKey() {
		return !TextUtil.isEmpty(foreignKeyTable) && !TextUtil.isEmpty(foreignKeyColumn);
	}

	public final boolean isUnique() {
		return isUnique;
	}

	public final Column setIsUnique(boolean unique) {
		isUnique = unique;
		return this;
	}

	@Override
	public final String toString() {
		return name;
	}

	@Override
	public final boolean equals(Object o) {
		if (o != null && o instanceof Column) {
			Column c = (Column) o;
			return c.dataType.equals(dataType) && c.name.equals(name) && c.isUnique == isUnique;
		}
		return false;
	}
}
