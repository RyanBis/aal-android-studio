package com.xiledsystems.aal.sql;

import java.util.ArrayList;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.sql.Row.IndexSender;


@State(DevelopmentState.PRODUCTION)
public final class ResultList extends ArrayList<Row> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5581089212310965964L;
	private final String[] columnNames;
	private static IndexSender indexSender;
	
	
	public ResultList(String[] columnNames) {
		this.columnNames = columnNames;
		indexSender = new IndexSender() {
			@Override
			public int getColumnIndex(String name) {
				return ResultList.this.getColumnIndex(name);
			}
		};
	}
	
	public final String[] getColumnNames() {
		return columnNames;
	}
	
	public final int getColumnIndex(String name) {
		for (int i = 0; i < columnNames.length; i++) {
			if (columnNames[i].equalsIgnoreCase(name)) {
				return i;
			}
		}
		return -1;
	}
	
	@Override
	public final boolean add(Row object) {
		object.setList(indexSender);
		return super.add(object);
	}
	
	@Override
	public final void add(int index, Row object) {
		object.setList(indexSender);
		super.add(index, object);
	}
		
}
