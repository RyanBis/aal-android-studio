package com.xiledsystems.aal.sql;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.LifeCycle;
import com.xiledsystems.aal.annotations.LifeCycleMethod;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.util.AalHandler;
import com.xiledsystems.aal.util.AalLogger;
import com.xiledsystems.aal.internal.Component;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import com.xiledsystems.aal.util.FileUtil;
import com.xiledsystems.aal.util.Prefs;
import com.xiledsystems.aal.util.TextUtil;
import com.xiledsystems.aal.util.ThreadManager;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.Build;
import android.os.Bundle;


/**
 * SQLite convenience class. This wraps {@link SQLiteDatabase}, and
 * {@link SQLiteOpenHelper}, so you don't have to bother with them.
 * You can always supply your own {@link SQLiteOpenHelper} if you want
 * as well.
 * This class also makes using the backing database thread safe.
 */
@TargetApi(Build.VERSION_CODES.ECLAIR)
@State(DevelopmentState.PRODUCTION)
@LifeCycle(LifeCycleMethod.ON_DESTROY)
public class SqlDb extends NonDisplayComponent {

	private final static int ADDING_TABLE = 1 << 0;
	private final static int ADDING_COLUMN = 1 << 1;

	private final static int RECENTLY_UPGRADED = 1 << 31;

	private final static String THREAD_NAME = "SqlDb Thread";

	public static final String ID = "_id";
	public static final String UNIQUE = "UNIQUE";
	public static final String FOREIGN_KEY = "FOREIGN KEY";
	public static final String REFERENCES = "REFERENCES";
	private static final ConcurrentHashMap<String, Integer> refCounts = new ConcurrentHashMap<>();
	private static final Object LOCK = new Object();
	private static final String VERSION = "dbversion_%s";
	private static final int MAX_COUNT = 250;

	private DBConfig dbConfig;
	private SQLiteOpenHelper dbHelper;
	private SQLiteOpenHelper setHelper;
	private SQLiteDatabase db;
	private String dbName;
	private int dbVersion = 1;
	private boolean batch;
	private int batchCount;
	private int maxBatchCount = MAX_COUNT;
	private boolean addingTable = false;
	private ArrayList<Table> addedTables = new ArrayList<Table>();
	private ArrayList<ColumnAdd> addedColumns = new ArrayList<>();
	private AalHandler handler;
	private int upgradeStateMask;


	/**
	 * Use this constructor if you are doing custom things on your own. 99% of the time,
	 * {@link #SqlDb(Context, DBSchema)} is the method you will need.
	 *
	 * @param context
	 */
	public SqlDb(Context context) {
		super(context);
		dbName = defaultDBName();
		final Prefs p = new Prefs(context);
		// Check old storage location first. If anything there, convert it to the new
		// format.
		int version = p.getInt(VERSION.replace("_%s", ""), -1);
		if (version != -1) {
			p.storeInt(key(dbName), version);
		} else {
			version = p.getInt(key(dbName), 1);
		}
		dbVersion = version;
	}

	private String defaultDBName() {
		return getContext().getPackageName() + ".db";
	}

	/**
	 * This is no longer the recommended constructor to use to instantiate a SqlDb {@link Component}.
	 *
	 * @deprecated Use {@link #SqlDb(Context, DBConfig)} instead.
	 * @param context
	 * @param schema
	 */
	@Deprecated
	public SqlDb(Context context, DBSchema schema) {
		super(context);
		if (TextUtil.isEmpty(schema.getDbName())) {
			dbName = defaultDBName();
			schema.setDbName(dbName);
		} else {
			dbName = schema.getDbName();
		}
		final Prefs p = new Prefs(context);
		// Check old storage location first. If anything there, convert it to the new
		// format.
		int version = p.getInt(VERSION.replace("_%s", ""), -1);
		if (version != -1) {
			p.storeInt(key(dbName), version);
		} else {
			version = p.getInt(key(dbName), 1);
		}
		dbVersion = version;
		setDBSchema(schema);
	}

	/**
	 * This is the recommended constructor to use to instantiate a SqlDb {@link Component}.
	 *
	 * @param context
	 * @param config
	 */
	public SqlDb(Context context, DBConfig config) {
		super(context);
		if (TextUtil.isEmpty(config.getDbName())) {
			dbName = defaultDBName();
			config.setDbName(dbName);
		} else {
			dbName = config.getDbName();
		}
		final Prefs p = new Prefs(context);
		// Check old storage location first. If anything there, convert it to the new
		// format.
		int version = p.getInt(VERSION.replace("_%s", ""), -1);
		if (version != -1) {
			p.storeInt(key(dbName), version);
		} else {
			version = p.getInt(key(dbName), 1);
		}
		dbVersion = version;
		setDBConfig(config);
	}

	/**
	 * Use this constructor if you plan on implementing your own DB Helper class to construct the db on your own.
	 * 
	 * Make sure to call {@link #setDBName(String)} before calling any methods on the database, to ensure SqlDb
	 * has the correct name.
	 * 
	 * @param context
	 * @param openHelper
	 */
	public SqlDb(Context context, SQLiteOpenHelper openHelper) {
		super(context);
		this.dbConfig = null;
		dbName = defaultDBName();
		setHelper = openHelper;
	}

	/**
	 * Set this before initializing the db for the first time, so the db gets built
	 * properly. If you are using your own SQLiteOpenHelper, this does not need to
	 * be called.
	 * @deprecated Use {@link #setDBConfig(DBConfig)} instead.
	 * @param dbSetup
	 */
	@Deprecated
	public void setDBSchema(DBSchema dbSetup) {
		setDBConfig(dbSetup.toDBConfig());
	}

	public void setDBConfig(DBConfig dbConfig) {
		this.dbConfig = dbConfig;
		if (isOpen()) {
			checkTablesAndIndeces();
		}
	}

	/**
	 * @return the lock for this SqlDb for synchronizing the db in a multi-threaded environment
	 */
	public Object getLock() {
		return LOCK;
	}

	/**
	 * This sets the name of the db (filename). This defaults to your packagename.db
	 * @param name
	 */
	public void setDBName(String name) {
		dbName = name;
	}

	/**
	 *
	 * @return - the path to the db file. If the db has not been initialized, this will
	 * initialize it, then close it right after. If the db IS initialized, then it will
	 * just return the path (in other words, it won't close it).
	 */
	public String getDBPath() {
		if (db != null) {
			return db.getPath();
		} else {
			initialize();
			try {
				return db.getPath();
			} finally {
				closeDb();
			}
		}
	}

	/**
	 * This opens the database, and creates it, if necessary. This is automatically called by any method
	 * which needs to interact with the db, so it should be unnecessary to manually call this method.
	 *
	 * <b>Note:</b> If the {@link Context} you provided in the constructor does not extend either
	 * {@link com.xiledsystems.aal.context.AalActivity} or {@link com.xiledsystems.aal.context.AalService}, then you must
	 * remember to call {@link #closeDb()} when you are done with the database, to release memory.
	 */
	public void initialize() {
		// Make sure to fill in dbdata object on initialize
		if (setHelper != null) {
			dbHelper = setHelper;
		} else {
			dbHelper = new SQLOpenHelper(getContext());
		}
		final ThreadManager mgr = ThreadManager.getInstance();
		handler = mgr.hasAalHandler(THREAD_NAME) ? mgr.getAalHandler(THREAD_NAME) : mgr.newAalHandler(THREAD_NAME);
		db = dbHelper.getWritableDatabase();
		incrementRefCount(dbName, getLogger());
		if (dbConfig == null || (upgradeStateMask & RECENTLY_UPGRADED) != 0) {
			upgradeStateMask = 0;
			dbConfig = getDbData(this);
			getLogger().logD("Built DBSchema from db file. " + dbConfig.toString());
		}
		checkTablesAndIndeces();
	}

	/**
	 * Puts the db into batch mode. Use this to insert, or update a large amount of items,
	 * as it will increase speed greatly. If you try to read the db while batch mode is active,
	 * you will get exceptions. Manage carefully.
	 */
	public void startBatch() {
		batch = true;
		ensureDbIsOpen();
		db.beginTransaction();
	}

	/**
	 * Ends batch mode. This is safe to call, even if nothing was done while in batch mode.
	 */
	public void endBatch() {
		batch = false;
		try {
			db.setTransactionSuccessful();
			db.endTransaction();
		} catch (IllegalStateException e) {
			getLogger().logE("Ended batch caused an Exception. Most likely due to either nothing being written to the db, or the batch wasn't started." +
					" This was caught, so things are safe. Exception message: " + e.getMessage());
		}
		batchCount = 0;
	}

	/**
	 * Insert data into the specified table. This assumes you are inserting data into
	 * all existing columns of the db, and in the proper order.
	 *
	 * @param table
	 * @param data
	 * @return the id of the inserted data, -1 if the insert failed.
	 */
	public long insert(String table, String... data) {
		ensureDbIsOpen();
		synchronized (LOCK) {
			checkBatch();
			if (data != null) {
				int amt = data.length;
				ContentValues values = new ContentValues();
				for (int i = 0; i < amt; i++) {
					Table t = dbConfig.getTable(table);
					if (t == null) {
						getLogger().logE("Unable to find table " + table + " in the database!");
						return -1;
					}
					String column = checkColumnName(t.getColumns().get(i).getName());
					values.put(column, data[i]);
				}				
				try {
					long rowId = db.insert(checkTableName(table), null, values);
					return rowId;
				} catch (SQLException e) {
					getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
					if (isLoggingEnabled()) {
						e.printStackTrace();
					}
					return -1;
				}
			} else {
				try {					
					long rowid = db.insert(checkTableName(table), null, null);
					return rowid;
				} catch (SQLException e) {
					getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
					if (isLoggingEnabled()) {
						e.printStackTrace();
					}
					return -1;
				}
			}
		}
	}

	/**
	 * Insert data into the specified table. This assumes you are inserting data into
	 * all existing columns of the db in order. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param data
	 * @return the id of the inserted data, -1 if the insert failed.
	 */
	public void insertAsync(final SqlDbReturn returnListener, final String table, final String... data) {
		ensureDbIsOpen();
		//final Returner r = new Returner(returnListener);
		handler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (LOCK) {
					checkBatch();
					if (data != null) {
						int amt = data.length;
						ContentValues values = new ContentValues();
						for (int i = 0; i < amt; i++) {
							Table t = dbConfig.getTable(table);
							if (t == null) {
								getLogger().logE("Unable to find table " + table + " in the database!");
								callListener(returnListener, newReturnedObject(-1, ReturnType.LONG));
								return;
							}
							String column = checkColumnName(t.getColumns().get(i).getName());
							values.put(column, data[i]);
						}
						try {
							long rowId = db.insert(checkTableName(table), null, values);
							callListener(returnListener, newReturnedObject(rowId, ReturnType.LONG));
						} catch (SQLException e) {
							getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
							if (isLoggingEnabled()) {
								e.printStackTrace();
							}
							callListener(returnListener, newReturnedObject(-1L, ReturnType.LONG));
						}
					} else {
						try {
							long rowid = db.insert(checkTableName(table), null, null);
							callListener(returnListener, newReturnedObject(rowid, ReturnType.LONG));
						} catch (SQLException e) {
							getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
							if (isLoggingEnabled()) {
								e.printStackTrace();
							}
							callListener(returnListener, newReturnedObject(-1L, ReturnType.LONG));
						}
					}
				}
			}
		});
	}

	/**
	 * Insert data into the specified table. This assumes you are inserting data into
	 * all existing columns of the db in order. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param data
	 */
	public void insertAsync(final String table, final String... data) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (LOCK) {
					checkBatch();
					if (data != null) {
						int amt = data.length;
						ContentValues values = new ContentValues();
						for (int i = 0; i < amt; i++) {
							Table t = dbConfig.getTable(table);
							if (t == null) {
								getLogger().logE("Unable to find table " + table + " in the database!");
								return;
							}
							String column = checkColumnName(t.getColumns().get(i).getName());
							values.put(column, data[i]);
						}
						try {
							db.insert(checkTableName(table), null, values);
						} catch (SQLException e) {
							getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
							if (isLoggingEnabled()) {
								e.printStackTrace();
							}
						}
					} else {
						try {
							db.insert(checkTableName(table), null, null);
						} catch (SQLException e) {
							getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
							if (isLoggingEnabled()) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		});
	}

	/**
	 * Insert data into the specified table. This is the recommended method to use when inserting, as you don't
	 * have to worry about the values being in the right order.
	 *
	 * @param table
	 * @param values
	 * @return the id of the inserted data, -1 if the insert failed.
	 */
	public long insert(String table, ContentValues values) {
		ensureDbIsOpen();
		synchronized (LOCK) {
			checkBatch();
			try {
				long rowId = db.insert(checkTableName(table), null, values);
				return rowId;
			} catch (SQLException e) {
				getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
				if (isLoggingEnabled()) {
					e.printStackTrace();
				}
				return -1;
			}
		}
	}

	/**
	 * Insert data into the specified table. This is the recommended method to use when inserting with async, as you don't
	 * have to worry about the values being in the right order. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param values
	 * @return the id of the inserted data, -1 if the insert failed.
	 */
	public void insertAsync(final String table, final ContentValues values) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (LOCK) {
					checkBatch();
					try {
						db.insert(checkTableName(table), null, values);
					} catch (SQLException e) {
						getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
						if (isLoggingEnabled()) {
							e.printStackTrace();
						}
					}
				}
			}
		});
	}

	/**
	 * Insert data into the specified table. This is the recommended method to use when inserting with async, as you don't
	 * have to worry about the values being in the right order. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param values
	 * @return the id of the inserted data, -1 if the insert failed.
	 */
	public void insert(final String table, final ContentValues values, final SqlDbReturn listener) {
		ensureDbIsOpen();
		handler.post(() -> {
            synchronized (LOCK) {
                checkBatch();
                try {
                    long rowid = db.insert(checkTableName(table), null, values);
                    callListener(listener, newReturnedObject(rowid, ReturnType.LONG));
                } catch (SQLException e) {
                    getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
                    if (isLoggingEnabled()) {
                        e.printStackTrace();
                    }
                    callListener(listener, newReturnedObject(-1L, ReturnType.LONG));
                }
            }
        });
	}

	/**
	 * Insert data into the specified table. This is the recommended method to use when inserting with async, as you don't
	 * have to worry about the values being in the right order. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param values
	 * @return the id of the inserted data, -1 if the insert failed.
	 */
	public void insertAsync(final String table, final ContentValues values, final SqlDbReturn listener) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (LOCK) {
					checkBatch();
					try {
						long rowid = db.insert(checkTableName(table), null, values);
						callListener(listener, newReturnedObject(rowid, ReturnType.LONG));
					} catch (SQLException e) {
						getLogger().logE("Unable to insert data. Either the table doesn't exist, or incorrect amount of data items." + table);
						if (isLoggingEnabled()) {
							e.printStackTrace();
						}
						callListener(listener, newReturnedObject(-1L, ReturnType.LONG));
					}
				}
			}
		});
	}

	/**
	 * Update a specific column, in the specified table, with the rowId provided with the
	 * data provided.
	 *
	 * @param table
	 * @param column
	 * @param rowId
	 * @param data
	 */
	public void update(String table, String column, long rowId, String data) {
		ensureDbIsOpen();
		synchronized (LOCK) {
			checkBatch();
			ContentValues values = new ContentValues(1);
			values.put(column, data);
			String where = "_id=?";
			String[] whereArgs = { String.valueOf(rowId) };
			db.update(checkTableName(table), values, where, whereArgs);
		}
	}

	/**
	 * Update a specific column, in the specified table, with the rowId provided with the
	 * data provided. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param column
	 * @param rowId
	 * @param data
	 */
	public void updateAsync(final String table, final String column, final long rowId, final String data) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (LOCK) {
					checkBatch();
					ContentValues values = new ContentValues(1);
					values.put(column, data);
					String where = "_id=?";
					String[] whereArgs = {String.valueOf(rowId)};
					db.update(checkTableName(table), values, where, whereArgs);
				}
			}
		});
	}

	/**
	 * Update a specific column, in the specified table, with the rowId provided with the
	 * blob data provided.
	 *
	 * @param table
	 * @param column
	 * @param rowId
	 * @param data
	 */
	public void update(String table, String column, long rowId, byte[] data) {
		ensureDbIsOpen();
		synchronized (LOCK) {
			checkBatch();
			ContentValues values = new ContentValues(1);
			values.put(column, data);
			String where = "_id=?";
			String[] whereArgs = { String.valueOf(rowId) };
			db.update(checkTableName(table), values, where, whereArgs);
		}
	}

	/**
	 * Update a specific column, in the specified table, with the rowId provided with the
	 * blob data provided. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param column
	 * @param rowId
	 * @param data
	 */
	public void updateAsync(final String table, final String column, final long rowId, final byte[] data) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (LOCK) {
					checkBatch();
					ContentValues values = new ContentValues(1);
					values.put(column, data);
					String where = "_id=?";
					String[] whereArgs = {String.valueOf(rowId)};
					db.update(checkTableName(table), values, where, whereArgs);
				}
			}
		});
	}

	/**
	 * Update the specified table of the db with the provided {@link ContentValues}, using the
	 * supplied where statement, and arguments (use ? for values in the statement, and the
	 * values in the whereargs)
	 *
	 * @param table
	 * @param values
	 * @param where
	 * @param whereArgs
	 * @return
	 */
	public int update(String table, ContentValues values, String where, String[] whereArgs) {
		ensureDbIsOpen();
		synchronized (LOCK) {
			return db.update(checkTableName(table), values, where, whereArgs);
		}
	}

	/**
	 * Update the specified table of the db with the provided {@link ContentValues}, using the
	 * supplied where statement, and arguments (use ? for values in the statement, and the
	 * values in the whereargs). This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param values
	 * @param where
	 * @param whereArgs
	 * @return
	 */
	public void updateAsync(final String table, final ContentValues values, final String where, final String[] whereArgs, final SqlDbReturn listener) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				synchronized (LOCK) {
					int amt = db.update(checkTableName(table), values, where, whereArgs);
					callListener(listener, newReturnedObject(amt, ReturnType.INT));
				}
			}
		});
	}

	/**
	 * Perform a query on the db with the provided {@link Query} class. This returns a {@link Cursor} object,
	 * and you must make sure to close it when you are done.
	 *
	 * @param query
	 * @return
	 */
	public Cursor query(BaseOp query) {
		ensureDbIsOpen();
		return db.rawQuery(query.getStatement(), query.getSelectionArgs());
	}

	/**
	 * Perform a query on the db with the provided {@link Query} class. This returns a {@link Cursor} object,
	 * and you must make sure to close it when you are done. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param query
	 * @return
	 */
	public void queryAsync(final BaseOp query, final SqlDbReturn listener) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				Cursor c = db.rawQuery(query.getStatement(), query.getSelectionArgs());
				callListener(listener, newReturnedObject(c, ReturnType.CURSOR));
			}
		});
	}

	/**
	 * Run a raw query on the backing Sqlite database.
	 *
	 * @param query
	 * @param selectionArgs
	 * @return
	 */
	public Cursor rawQuery(String query, String[] selectionArgs) {
		ensureDbIsOpen();
		return db.rawQuery(query, selectionArgs);
	}

	/**
	 * Run a raw query on the backing Sqlite database. This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param query
	 * @param selectionArgs
	 * @return
	 */
	public void rawQueryAsync(final String query, final String[] selectionArgs, final SqlDbReturn listener) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				Cursor c = db.rawQuery(query, selectionArgs);
				callListener(listener, newReturnedObject(c, ReturnType.CURSOR));
			}
		});
	}

	/**
	 * Loads a pre-existing Sqlite database.
	 *
	 * @param path
	 */
	public void openDBFile(String path) {
		db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
		dbName = scrubPath(path);
		incrementRefCount(dbName, getLogger());
		dbConfig = getDbData(this);
		getLogger().logE("Built DBSchema from db file. " + dbConfig.toString());
		dbVersion = db.getVersion();
		new Prefs(getContext()).storeInt(key(dbName), dbVersion);
	}

	/**
	 *
	 * @return if the database is open or not.
	 */
    public boolean isOpen() {
        return db != null && db.isOpen();
    }

	/**
	 * Close the database.
	 *
	 * <b>Note:</b> You only need to call this method if the {@link Context} you
	 * provided in the constructor does not extend either {@link com.xiledsystems.aal.context.AalActivity}, or
	 * {@link com.xiledsystems.aal.context.AalService}.
	 *
	 * <b>Note 2:</b> This class has a reference counter on the open
	 * database. So, if you have a database you're accessing in multiple
	 * {@link android.app.Activity}s, you can safely call this method to
	 * "close" the database. If there is another Activity which has yet to
	 * call {@link #closeDb()}, then the database will remain open.
	 */
	public void closeDb() {
		if (decrementRefCount(dbName, getLogger())) {
			closeDb_internal();
		}
	}

	/**
	 * This will force close the backing {@link SQLiteDatabase}, no matter
	 * how many instances may have yet to call {@link #closeDb()}. You should
	 * never have to call this, it's here as a last resort, or for special
	 * circumstances.
	 */
	public void forceCloseDb() {
		closeDb_internal();
		refCounts.remove(dbName);
	}

	/**
	 * This is essentially the same as {@link #forceCloseDb()}, only it wipes out all
	 * reference counts for all physical databases (SqlDb objects with different
	 * dbNames result in a different physical file).
	 */
	public void nukeAll() {
		closeDb_internal();
		refCounts.clear();
	}
	
	/**
	 * This allows you to add a table to an already existing database using
	 * the {@link Table} class. Note that this will close the database,
	 * and reopen it, as it upgrades the database version.
	 */
	public void addTableToExistingDatabase(Table table) {
		dbVersion++;
		new Prefs(getContext()).storeInt(key(dbName), dbVersion);
		addedTables.add(table);
		upgradeStateMask |= ADDING_TABLE;
		closeDb();
		initialize();
	}

	/**
	 * This allows you to add a column to an already existing table in the
	 * database using the {@link Column} class. Note that this will close the
	 * database and reopen it, as it upgrades the database version.
	 *
	 * This will prefill all newly created columns with a default value of
	 * "" for strings, and 0 for integer, 0.0 for float, and an empty byte array
	 * for blobs.
	 *
	 * @param tableName
	 * @param columnToAdd
     */
	public void addColumnToExistingTable(String tableName, Column columnToAdd) {
		addColumnToExistingTable(tableName, columnToAdd, null);
	}

	/**
	 * This allows you to add a column to an already existing table in the
	 * database using the {@link Column} class. Note that this will close the
	 * database and reopen it, as it upgrades the database version. The rows
	 * in the table will be updated with the supplied defaultValue.
	 *
	 * @param tableName
	 * @param columnToAdd
	 */
	public void addColumnToExistingTable(String tableName, Column columnToAdd, String defaultValue) {
		dbVersion++;
		new Prefs(getContext()).storeInt(key(dbName), dbVersion);
		if (!TextUtil.isEmpty(defaultValue)) {
			addedColumns.add(new ColumnAdd(tableName, columnToAdd, defaultValue));
		} else {
			addedColumns.add(new ColumnAdd(tableName, columnToAdd));
		}
		upgradeStateMask |= ADDING_COLUMN;
		closeDb();
		initialize();
	}

	/**
	 *
	 * @return the backing {@link SQLiteDatabase} object
	 */
	public SQLiteDatabase getSQLiteDb() {
		return db;
	}

	/**
	 *
	 * @return the current {@link SQLiteOpenHelper} used.
	 */
	public SQLiteOpenHelper getDBHelper() {
		return dbHelper;
	}

	/**
	 *
	 * @return the database version
	 */
	public int getDBVersion() {
		return dbVersion;
	}

	/**
	 * Set the version of the database. NOTE: It's best NOT to use this method, unless you
	 * really know what you're doing (it may cause you to lose all your data).
	 * @param version
	 */
	public void setDBVersion(int version) {
		dbVersion = version;
		new Prefs(getContext()).storeInt(key(dbName), dbVersion);
	}

	/**
	 * Delete all rows in the specified table.
	 * @param table
	 * @return the number of rows deleted
	 */
	public int clearTable(String table) {
		ensureDbIsOpen();
		int rmvd = -1;
		synchronized (LOCK) {
			rmvd = db.delete(table, "1", null);
			db.execSQL("VACUUM");
			closeDb();
			initialize();			
		}
		return rmvd;
	}

	/**
	 * Deletes the entire database, and all data in it.
	 *
	 * @return true if the database was successfully deleted
     */
	public boolean deleteDatabase() {
		forceCloseDb();
		return getContext().deleteDatabase(dbConfig.getDbName());
	}

	/**
	 * Delete from the specified table using the provided where statement, and arguments.
	 *
	 * @param table
	 * @param where
	 * @param whereArgs
	 * @return the number of rows deleted
	 */
    public int delete(String table, String where, String[] whereArgs) {
        int amt;
		ensureDbIsOpen();
        synchronized (LOCK) {
            amt = db.delete(table, where, whereArgs);
        }
        return amt;
    }

	/**
	 * Delete from the specified table using the provided where statement, and arguments.
	 * This method is run on a background thread, so it
	 * will not block the UI thread.
	 *
	 * @param table
	 * @param where
	 * @param whereArgs
	 */
	public void deleteAsync(final String table, final String where, final String[] whereArgs, final SqlDbReturn listener) {
		ensureDbIsOpen();
		handler.post(new Runnable() {
			@Override
			public void run() {
				int amt;
				synchronized (LOCK) {
					amt = db.delete(table, where, whereArgs);
				}
				callListener(listener, newReturnedObject(amt, ReturnType.INT));
			}
		});
	}

	/**
	 * Check if an index exists in the db.
	 * 
	 * @param indexName
	 * @return
	 */
	public boolean indexExists(String indexName) {
		boolean b = false;
		ensureDbIsOpen();
		synchronized (LOCK) {
			final String query = "SELECT * FROM sqlite_master WHERE type=? AND name=?";
			Cursor c = db.rawQuery(query, new String[] { "index", indexName });
			if (c != null) {
				b = c.moveToFirst();
				c.close();
			}
		}
		return b;
	}

	/**
	 * Check if a table exists in the db. This does an explicit check on the underlying Sqlite database.
	 *
	 * @param table
	 * @return
     */
	public boolean tableExists(String table) {
		boolean b = false;
		ensureDbIsOpen();
		synchronized (LOCK) {
			final String query = "SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name=?";
			Cursor c = db.rawQuery(query, new String[] { table });
			if (c != null) {
				b = c.moveToFirst();
				c.close();
			}
		}
		return b;
	}

	/**
	 * Create an index. You don't have to call this method if you add indexes in the
	 * {@link DBConfig} provided.
	 *
	 * @param table
	 * @param columnToIndex
	 * @param indexName
     */
	public void createIndex(String table, String columnToIndex, String indexName) {
		if (!indexExists(indexName)) {
			ensureDbIsOpen();
            StringBuilder b = new StringBuilder();
            b.append("CREATE INDEX ").append(indexName).append(" ON ").append(table).append("(").append(columnToIndex).append(")");
            db.execSQL(b.toString());
        }
	}

	/**
	 * Delete an index from the database.
	 *
	 * @param indexName
     */
    public void deleteIndex(String indexName) {
        if (indexExists(indexName)) {
			ensureDbIsOpen();
            db.execSQL("DROP INDEX " + indexName + ";");
        }
    }

	/**
	 * Thi will allow you to run a raw SQL string on the backing {@link SQLiteDatabase}. This is only here as a last resort, if you
	 * absolutely need it. 99% of what you would need to do is provided within this class, or the other supporting SqlDb classes.
	 *
	 * @param sqlString
     */
	public void execSQL(String sqlString) {
		ensureDbIsOpen();
		db.execSQL(sqlString);
	}

	/**
	 * Lists the names of all the tables in the database. This reads the database to get it's list of names.
	 *
	 * @return
     */
	public ArrayList<String> getTableNames() {
		return getTableNames(this);
	}

	/**
	 *
	 * @param table
	 * @return an ArrayList of the column names in the table provided. This reads the column names from the backing
	 * {@link SQLiteDatabase}.
     */
	public ArrayList<String> getColumnNames(String table) {
		return getColumnNames(this, table);
	}

	@Deprecated
	public DBSchema getDBSchema() {
		if (dbConfig == null) {
			dbConfig = getDbData(this);
			getLogger().logE("Built DBSchema from db file. " + dbConfig.toString());
		}
		return dbConfig.toDBSchema();
	}

	/**
	 *
	 * @return the instance of {@link DBConfig} this class is holding.
     */
	public DBConfig getDbConfig() {
		if (dbConfig == null) {
			dbConfig = getDbData(this);
			getLogger().logE("Built DBSchema from db file. " + dbConfig.toString());
		}
		return dbConfig;
	}

	/**
	 * Set the size of the batch count. When the number of writes/updates hits this number, the
	 * batch is closed, and a new one is opened.
	 * @param batchCount
     */
	public void setBatchCount(int batchCount) {
		maxBatchCount = batchCount;
	}

	@Override
	protected OnCreateDestroyListener getCreateDestroyListener() {
		return new OnCreateDestroyListener() {
			@Override
			public void onCreate(Bundle savedInstanceState) {
			}

			@Override
			public void onDestroy() {
				closeDb();
			}
		};
	}

	@Override
	protected OnActivityResultListener getActivityResultListener() {
		return null;
	}

	@Override
	protected OnResumePauseListener getPauseResumeListener() {
		return null;
	}

	@Override
	protected OnNewIntentListener getNewIntentListener() {
		return null;
	}

	@Override
	protected OnLowMemoryListener getLowMemoryListener() {
		return null;
	}

	@Override
	protected OnStartStopListener getStartStopListener() {
		return null;
	}

	@Override
	protected CoreServiceLifecycleListener getServiceLifecycleListener() {
		return new ServiceLifecycleListener() {
			@Override
			public void onDestroy() {
				closeDb();
			}
		};
	}

	@Override
	protected OnRequestPermissionsListener getPermissionsListener() {
		return null;
	}

	protected <T extends Object> void callListener_internal(final SqlDbReturn listener, T obj, ReturnType type) {
		callListener(listener, newReturnedObject(obj, type));
	}

	SQLiteStatement compileStatement(String statement) {
		ensureDbIsOpen();
		return db.compileStatement(statement);
	}

	private SqlDb ensureDbIsOpen() {
		if (db == null || !db.isOpen()) {
			initialize();
		}
		return this;
	}

	private String scrubPath(String path) {
		if (path.contains(File.separator)) {
			int last = path.lastIndexOf(File.separator);
			path = path.substring(last, path.length());
		}
		return path;
	}

	private void closeDb_internal() {
		if (db != null && db.isOpen()) {
			db.close();
		}
		if (dbHelper != null) {
			dbHelper.close();
		}
		SQLiteDatabase.releaseMemory();
	}

	private void checkTablesAndIndeces() {
		final Iterator<Table> tit = dbConfig.getTableIterator();
		while (tit.hasNext()) {
			Table table = tit.next();
			if (!tableExists(table.getName())) {
				addTableToExistingDatabase(table);
				getLogger().logD(String.format("Creating table %s, as it was not found to exist.", table.getName()));
			}
		}
		final Iterator<DBConfig.Index> it = dbConfig.getIndexIterator();
		while (it.hasNext()) {
			DBConfig.Index indx = it.next();
			if (!indexExists(indx.name)) {
				createIndex(indx.table, indx.column, indx.name);
				getLogger().logD(String.format("Creating index %s for column %s in table %s.", indx.name, indx.column, indx.table));
			}
		}
	}

	private void checkBatch() {
		if (batch) {
			if (batchCount >= maxBatchCount) {
				endBatch();
				startBatch();
			}
			batchCount++;
		}
	}
	
	private static DBConfig getDbData(SqlDb db) {
		DBConfig dbConfig = new DBConfig();
		final String path = db.getDBPath();
		dbConfig.setDbName(path.substring(path.lastIndexOf("/") + 1, path.length()));
		ArrayList<String> tableNames = getTableNames(db);		
		int size = tableNames.size();
		for (int i = 0; i < size; i++) {			
			final Cursor cursor = db.query(new Query().select("sql").from("sqlite_master").where("tbl_name").equalTo(tableNames.get(i)).and("type").equalTo("table"));
			Table table = new Table(tableNames.get(i).replace("'", "").replace("\"", ""));
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					Column col;
					do {
						String res = cursor.getString(0);
						String split = res.split("\\(")[1].split("\\)")[0];
						String[] cols = split.split(",");
						String[] items;
						for (int j = 0; j < cols.length; j++) {
							items = cols[j].trim().split(" ");
							col = new Column(items[0].replace("'", "").replace("\"", ""));
							if (items.length > 1) {
								col.setDataType(DataType.fromString(items[1]));
							}
							if (items.length > 2) {
								String option = items[2];
								if (option.equalsIgnoreCase("unique")) {
									col.setIsUnique(true);
								} else if (option.equalsIgnoreCase("primary")) {
									if (items.length > 3 && items[3].equalsIgnoreCase("key")) {
										table.setIDColumn(items[0].replace("'", "").replace("\"", ""));
										continue;
									}
								}
							}
							table.addColumn(col);
						}
					} while (cursor.moveToNext());
				}
				dbConfig.addTable(table);
				cursor.close();
			}
		}
		return dbConfig;
	}

	/**
	 * Moves a sql db file to the path supplied. Make sure the
	 * db is initialized before calling this method.
	 *
	 * @param db
	 * @param newPath
	 * @return
	 */
	public static boolean copyDbFile(SqlDb db, String newPath) {
		String dbPath = db.getDBPath();
		db.closeDb();
		boolean success = FileUtil.copyFile(dbPath, newPath);
		db.initialize();
		return success;
	}
	
	public static ArrayList<String> getTableNames(SqlDb db) {
		ArrayList<String> tables = new ArrayList<String>();
		Object lock = db.getLock();
		synchronized (lock) {
			final Cursor cursor = db.query(new Query().select("name").from("sqlite_master").where("type").equalTo("table"));
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						String table = cursor.getString(cursor.getColumnIndex("name"));
						if (!table.equals("android_metadata") && !table.equals("dummy") && !table.equals("sqlite_sequence") && 
								!table.equals("_id")) {
							tables.add(table);
						}
					} while (cursor.moveToNext());
				}
				cursor.close();
			}
		}
		return tables;
	}
	
	public static ArrayList<String> getColumnNames(SqlDb db, String table) {
		DBConfig config = getDbData(db);
		Table t = config.getTable(table);
		if (t != null) {
			return t.getColumnNames();
		}
		else {
			return new ArrayList<>(0);
		}
	}

	/**
	 * This wraps the input table name in single quotes. If there are any single quotes in the table name, they will
	 * be removed. This is handy if you absolutely MUST have spaces in your table name.
	 *
	 * @param name
	 * @return
     */
	public static String checkTableName(String name) {
		String n = name.trim();
		n = n.replace("'", "");
		n = "'" + n + "'";
		return n;
	}

	/**
	 * This wraps the input column name in double quotes. If there are any double quotes in the name, they will be
	 * removed. This is handy if you absolutely MUST have spaces in your column name.
	 *
	 * @param name
	 * @return
     */
	public static String checkColumnName(String name) {
		if (name.startsWith("\"") && name.endsWith("\"")) {
			return name;
		} else {			
			name = name.replace("\"", "");			
			return "\"" + name + "\"";
		}
	}

	AalHandler getThread() {
		return handler;
	}

	protected static void executeInsertStatementAsync(final Object lock, final SQLiteStatement statement, final SqlDbReturn listener, AalHandler handler) {
		handler.post(() -> {
            final long id;
            synchronized (lock) {
                id = statement.executeInsert();
            }
            callListener(listener, newReturnedObject(id, ReturnType.LONG));
        });
	}

	private static String key(String dbName) {
		return String.format(VERSION, dbName);
	}

	private static void callListener(final SqlDbReturn listener, final ReturnedObject object) {
		if (listener != null) {
			if (listener.returnOnUIThread()) {
				ThreadManager.getHandler().post(() -> listener.objectReturned(object));
			} else {
				listener.objectReturned(object);
			}
		}
	}

	private static <T> ReturnedObject newReturnedObject(T o, ReturnType type) {
		return new ReturnedObject(o, type);
	}

	private static void incrementRefCount(String dbName, AalLogger logger) {
		Integer i = refCounts.get(dbName);
		if (i == null) {
			i = 0;
		}
		i++;
		refCounts.put(dbName, i);
		logger.logE("Assigning new reference for " + dbName + ". Current count: " + i);
	}

	/**
	 *
	 * @param dbName
	 * @param logger
	 * @return <code>true</code> if no more references are in memory
	 */
	private static boolean decrementRefCount(String dbName, AalLogger logger) {
		Integer i = refCounts.get(dbName);
		if (i != null) {
			i--;
			if (i < 1) {
				refCounts.remove(dbName);
				logger.logE("Last reference removed for " + dbName + ".");
				return true;
			} else {
				refCounts.put(dbName, i);
				return false;
			}
		}
		return true;
	}

	private static String generateAddColumnStatement(String table, Column column, String defaultVal) {
		StringBuilder b = new StringBuilder();
		b.append("ALTER TABLE ");
		b.append(checkTableName(table));
		b.append(" ADD COLUMN ");
		b.append(checkColumnName(column.getName()));
		b.append(" ");
		b.append(column.getDataType_string());
		if (column.isUnique()) {
			b.append(" ").append(UNIQUE);
		}
		if (defaultVal != null) {
			b.append(" DEFAULT ").append(defaultVal);
		}
		return b.toString();
	}

	private static String generateCreateStatement(Table table) {
		StringBuilder b = new StringBuilder();
		ArrayList<Column> columns = table.getColumns();
		int size = columns.size();
		b.append(" CREATE TABLE IF NOT EXISTS ");
		b.append(checkTableName(table.getName()));
		b.append(" (");
		b.append(checkColumnName(table.getIdColumn()));
		b.append(" INTEGER PRIMARY KEY AUTOINCREMENT, ");
		ArrayList<Column> foreignKeyColumns = new ArrayList<>();
		for (int i = 0; i < size; i++) {
			Column c = columns.get(i);
			b.append(checkTableName(c.getName())).append(" ");
			b.append(c.getDataType_string());
			if (c.isUnique()) {
				b.append(" ").append(UNIQUE);
			}
			if (c.isForeignKey()) {
				foreignKeyColumns.add(c);
			}
			if (i != size - 1) {
				b.append(", ");
			}
		}
		size = foreignKeyColumns.size();
		if (size > 0) {
			for (int i = 0; i < size; i++) {
				Column c = foreignKeyColumns.get(i);
				b.append(", ").append(FOREIGN_KEY).append("(").append(checkTableName(c.getName())).append(") ");
				b.append(REFERENCES).append(" ").append(c.getForeignKeyTableName()).append("(").append(c.getForeignKeyIdColumnName());
				b.append(")");
			}
		}
		b.append(")");
		return b.toString();
	}

	private class ColumnAdd {
		private Column column;
		private String table;
		private String defaultValue;


		public ColumnAdd(String table, Column column) {
			this(table, column, getDefaultVal(column));
		}

		public ColumnAdd(String table, Column colum, String defaultValue) {
			this.table = table;
			this.column = colum;
			this.defaultValue = defaultValue;
		}
	}

	private static String getDefaultVal(Column column) {
		String val = "";
		if (column != null) {
			switch (column.getDataType()) {
				case FLOAT:
					return "0.0";
				case INTEGER:
					return "1";
			}
		}
		return val;
	}

	private class SQLOpenHelper extends SQLiteOpenHelper {

		public SQLOpenHelper(Context context) {
			super(context, dbName, null, dbVersion);
			if (dbConfig == null) {
				throw new RuntimeException("Table name list is empty! Can't create database.");
			}
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			int cnt = dbConfig.tableCount();
			for (int i = 0; i < cnt; i++) {
				db.execSQL(generateCreateStatement(dbConfig.next()));
				getLogger().logD("[Create] Adding table " + dbConfig.getTable(i).getName() + " to " + dbName + ".");
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if ((upgradeStateMask & ADDING_TABLE) != 0) {
				for (Table tbl : addedTables) {					
					db.execSQL(generateCreateStatement(tbl));
					getLogger().logD("[Upgrade] Adding table " + tbl.getName() + " to " + dbName + ".");
				}
				addedTables.clear();
			}
			if ((upgradeStateMask & ADDING_COLUMN) != 0) {
				// Do column addition
				int cnt = addedColumns.size();
				ColumnAdd add;
				for (int i = 0; i < cnt; i++) {
					add = addedColumns.get(i);
					db.execSQL(generateAddColumnStatement(add.table, add.column, null));
					final String fillOld = TextUtil.string("UPDATE ", checkTableName(add.table), " SET ", checkColumnName(add.column.getName()), "=? WHERE ",
							checkColumnName(add.column.getName()), " IS NULL");
					db.execSQL(fillOld, new String[] { add.defaultValue });
				}
				addedColumns.clear();
			}
			if (upgradeStateMask == 0) {
				getLogger().logD("Upgrading database from version " + oldVersion + " to " + newVersion + ". This wipes out all old information!");
				int tblcnt = dbConfig.tableCount();
				// TODO - Right now this is a destructive upgrade. It wipes
				// the old structure, and creates the new, destroying old data in
				// the process. 
				for (int i = 0; i < tblcnt; i++) {
					db.execSQL("DROP TABLE IF EXISTS " + checkTableName(dbConfig.getTable(i).getName()));
				}
				onCreate(db);
			} else {
				upgradeStateMask = 0;
			}
			upgradeStateMask = RECENTLY_UPGRADED;
		}
	}

}