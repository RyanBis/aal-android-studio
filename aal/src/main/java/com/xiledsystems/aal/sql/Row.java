package com.xiledsystems.aal.sql;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.PRODUCTION)
public final class Row {
	
	
	private String[] columns;
	private IndexSender list;
	
	
	public Row() {		
	}
	
	public final void setList(IndexSender list) {
		this.list = list;
	}
	
	public final void setColumnData(String[] data) {
		columns = data;
	}
	
	public final int columns() {
		if (columns != null) {
			return columns.length;
		} else {
			return 0;
		}
	}
	
	public final long getLong(int column) {
		return Long.parseLong(columns[column]);
	}
	
	public final long getLong(String columnName) {
		return Long.parseLong(columns[list.getColumnIndex(columnName)]);
	}
	
	public final int getInt(int column) {
		return Integer.parseInt(columns[column]);
	}
	
	public final int getInt(String columnName) {
		return Integer.parseInt(columns[list.getColumnIndex(columnName)]);
	}
	
	public final boolean getBoolean(int column) {
		return Boolean.parseBoolean(columns[column]);
	}
	
	public final boolean getBoolean(String columnName) {
		return Boolean.parseBoolean(columns[list.getColumnIndex(columnName)]);
	}
	
	public final String getString(int column) {
		return columns[column];
	}
	
	public final String getString(String columnName) {
		return columns[list.getColumnIndex(columnName)];
	}
	
	public interface IndexSender {
		public int getColumnIndex(String name);
	}

}