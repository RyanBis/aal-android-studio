package com.xiledsystems.aal.sql;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import android.support.annotation.CallSuper;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


@State(DevelopmentState.PRODUCTION)
public abstract class DBObject {

    final static Map<Class, String> insertStatements = new HashMap<>();

	Map<String, Object> dataToUpdate = new TreeMap<String, Object>();
	private boolean manualSet;

    // The table name this object is in
    private String table;
    long id;


    /**
     * This method sets the name of the table that this DBObject belongs to.
     * @return
     */
    public abstract String getTableName();

    /**
     * This is called in the {@link #DBObject(Cursor)} constructor to load this class's data.
     * See {@link #getCursorBlob(String, Cursor)}, {@link #getCursorBoolean(String, Cursor)} , {@link #getCursorDouble(String, Cursor)},
     * {@link #getCursorFloat(String, Cursor)}, {@link #getCursorInt(String, Cursor)}, {@link #getCursorLong(String, Cursor)},
     * {@link #getCursorShort(String, Cursor)}, and {@link #getCursorString(String, Cursor)}.
     *
     * @param c
     */
    public abstract void loadFromCursor(Cursor c);

    /**
     * This gets called when {@link #insert(SqlDb)}, {@link #insertAsync(SqlDb)}, or {@link #insertAsync(SqlDb, SqlDbReturn)} is called to
     * retrieve the data to insert into the db.
     */
    public abstract ContentValues getCreateValues();


    public DBObject() {
        table = getTableName();
    }

    public DBObject(Cursor c) {
        table = getTableName();
        id = getCursorLong(SqlDb.ID, c);
        loadFromCursor(c);
    }

    /**
     * This method should be called in all of the set methods in your extending
     * class. This adds the data to the list of updates to be posted to the db.
     * This also will ignore data when in manual mode, so you don't have to manage
     * that yourself.
     *
     * @param column
     * @param data
     */
    public void addData(String column, Object data) {
        if (!manualSet) {
            if (data instanceof Boolean) {
                dataToUpdate.put(column, (boolean) data ? 1 : 0);
            } else {
                dataToUpdate.put(column, data);
            }
        }
    }

    /**
     *
     * @return - the id of this object in the database.
     */
    public long getId() {
        return id;
    }

    /**
     * This is called automatically when you call insert(db). You should not ever have to
     * use this method manually.
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Set the table this object resides in
     * @param table
     */
    public void setTable(String table) {
        this.table = table;
    }

    public String getTable() {
        return table;
    }

    /**
     * Insert this object's data into the database. This method returns the id of the object.
     * This method will throw a SqlDBException if the table name has not been set.
     * This is method syncs the db to it's lock automatically, so you don't have to worry
     * about it.
     * @param db
     * @return
     */
    public long insert(SqlDb db) {
        if (TextUtil.isEmpty(table)) {
            throw new SqlDbException("Table name is empty! Specify table name before inserting.");
        }
        id = db.insert(table, getCreateValues());
        return id;
    }

    /**
     * Insert this object's data into the database. This method returns the id of the object.
     * This method will throw a SqlDBException if the table name has not been set.
     * This is method syncs the db to it's lock automatically, so you don't have to worry
     * about it. This method is run on a background thread, so it
     * will not block the UI thread.
     * @param db
     */
    public void insertAsync(SqlDb db, SqlDbReturn listener) {
        if (TextUtil.isEmpty(table)) {
            throw new SqlDbException("Table name is empty! Specify table name before inserting.");
        }
        db.insertAsync(table, getCreateValues(), listener);
    }

    /**
     * Insert this object's data into the database. This method returns the id of the object.
     * This method will throw a SqlDBException if the table name has not been set.
     * This is method syncs the db to it's lock automatically, so you don't have to worry
     * about it. This method is run on a background thread, so it
     * will not block the UI thread.
     * @param db
     */
    public void insertAsync(SqlDb db) {
        if (TextUtil.isEmpty(table)) {
            throw new SqlDbException("Table name is empty! Specify table name before inserting.");
        }
        db.insertAsync(table, getCreateValues());
    }

    /**
     * This is the fastest way to insert data. This is best used when using the
     * start/endBatch methods.
     * @param db
     * @return
     */
    public long insertWithStatement(SqlDb db) {
        final String sql = getInsertStatement();
        final long id;
        synchronized (db.getLock()) {
            final SQLiteStatement stmt = createStatement(dataToUpdate, db, sql);
            id = stmt.executeInsert();
        }
        return id;
    }

    /**
     * This is the fastest way to insert data. However, it is not synchronized, so it is
     * up to you to synchronize the db to it's lock. This is best used when using the
     * start/endBatch methods. This method is run on a background thread, so it
     * will not block the UI thread.
     * @param db
     * @return
     */
    public void insertWithStatementAsync(SqlDb db, SqlDbReturn listener) {
        final String sql = getInsertStatement();
        final SQLiteStatement stmt;
        synchronized (db.getLock()) {
            stmt = createStatement(dataToUpdate, db, sql);
        }
        SqlDb.executeInsertStatementAsync(db.getLock(), stmt, listener, db.getThread());
    }

    /**
     * This is the fastest way to insert data. However, it is not synchronized, so it is
     * up to you to synchronize the db to it's lock. This is best used when using the
     * start/endBatch methods. This method is run on a background thread, so it
     * will not block the UI thread.
     * @param db
     * @return
     */
    public void insertWithStatementAsync(SqlDb db) {
        final String sql = getInsertStatement();
        final SQLiteStatement stmt;
        synchronized (db.getLock()) {
            stmt = db.getSQLiteDb().compileStatement(sql);
        }
        SqlDb.executeInsertStatementAsync(db.getLock(), stmt, null, db.getThread());
    }

    /**
     * Update this object's data to the db. If there is no data to update, nothing happens.
     * This will throw SqlDBException if the table name has not been set.
     * @param db
     */
    public void update(SqlDb db) {
        if (hasUpdates()) {
            if (TextUtil.isEmpty(table)) {
                throw new SqlDbException("Table name is empty! Specify table name before updating.");
            }
            db.update(table, getUpdatedValues(), "_id=?", new String[] { String.valueOf(id) });
        }
    }

    /**
     * This forwards {@link #updateAsync(SqlDb, SqlDbReturn)}, with a null {@link SqlDbReturn} listener.
     */
    public void updateAsync(SqlDb db) {
        updateAsync(db, null);
    }

    /**
     * Update this object's data to the db. If there is no data to update, nothing happens.
     * This will throw SqlDBException if the table name has not been set. This method is run on a background thread, so it
     * will not block the UI thread.
     * @param db
     */
    public void updateAsync(SqlDb db, SqlDbReturn listener) {
        if (hasUpdates()) {
            if (TextUtil.isEmpty(table)) {
                throw new SqlDbException("Table name is empty! Specify table name before updating.");
            }
            db.updateAsync(table, getUpdatedValues(), "_id=?", new String[]{String.valueOf(id)}, listener);
        }
    }

    /**
     * Deletes this object from the db provided. This will throw a SqlDBException if the
     * table name has not been set.
     *
     * @param db
     */
    public void delete(SqlDb db) {
        if (TextUtil.isEmpty(table)) {
            throw new SqlDbException("Table name is empty! Specify table name before deleting.");
        }
        db.delete(table, "_id=?", new String[] { String.valueOf(id) });
    }

    /**
     * This forwards {@link #deleteAsync(SqlDb, SqlDbReturn)} with a null {@link SqlDbReturn} listener.
     */
    public void deleteAsync(SqlDb db) {
        deleteAsync(db, null);
    }

    /**
     * Deletes this object from the db provided. This will throw a SqlDBException if the
     * table name has not been set. This method is run on a background thread, so it
     * will not block the UI thread.
     *
     * @param db
     */
    public void deleteAsync(SqlDb db, SqlDbReturn listener) {
        if (TextUtil.isEmpty(table)) {
            throw new SqlDbException("Table name is empty! Specify table name before deleting.");
        }
        db.deleteAsync(table, "_id=?", new String[] { String.valueOf(id) }, listener);
    }

    /**
     * Use this method if you want to change data in this object, but NOT
     * have it save the changes to the db when running update().
     *
     * @param manual
     */
	public void setManual(boolean manual) {
		manualSet = manual;
	}

    /**
     *
     * @return if this object is in manual mode
     */
	public boolean isManual() {
		return manualSet;
	}

    /**
     *
     * @return if this object has updated data to write to the db.
     */
	public boolean hasUpdates() {
		return dataToUpdate.size() > 0;
	}

    /**
     * Clear any pending updates. (This won't reset the data in this object, however)
     */
	public void clearUpdates() {
		dataToUpdate.clear();
	}

    public void clearInsertStatement() {
        insertStatements.remove(getClass());
    }

    static void putStatement(SQLiteStatement stmt, Object o, int index) {
        if (o == null) {
            stmt.bindNull(index);
        } else if (o instanceof Boolean) {
            stmt.bindLong(index, ((boolean) o) ? 1 : 0);
        } else if (o.getClass().isPrimitive() || o instanceof Byte ||o instanceof Short || o instanceof Integer || o instanceof Long) {
            stmt.bindLong(index, Long.parseLong(o.toString()));
        } else if (o instanceof Double || o instanceof Float) {
            stmt.bindDouble(index, Double.parseDouble(o.toString()));
        } else if (o instanceof byte[] || o instanceof Byte[]) {
            stmt.bindBlob(index, (byte[]) o);
        } else {
            stmt.bindString(index, o.toString());
        }
    }

    static String getValuesString(int count, StringBuilder b) {
        b.append(" VALUES(");
        for (int i = 0; i < count; i++) {
            if (i != count -1) {
                b.append("?,");
            } else {
                b.append("?");
            }
        }
        b.append(");");
        return b.toString();
    }

    String getColumnsString() {
        StringBuilder b = new StringBuilder();
        b.append("(");
        int size = dataToUpdate.size();
        int cnt = 1;
        for (String column : dataToUpdate.keySet()) {
            b.append(column);
            if (cnt != size) {
                b.append(", ");
            }
            cnt++;
        }
        b.append(") ");
        return b.toString();
    }

    String getInsertStatement() {
        String insertStatement = insertStatements.get(getClass());
        if (TextUtil.notEmpty(insertStatement)) {
            return insertStatement;
        } else {
            StringBuilder b = new StringBuilder();
            b.append("INSERT INTO ").append(table);
            b.append(getColumnsString());
            insertStatement = getValuesString(dataToUpdate.size(), b);
            insertStatements.put(getClass(), insertStatement);
            return insertStatement;
        }
    }

    private static SQLiteStatement createStatement(Map<String, Object> dataToUpdate, SqlDb db, String sql) {
        final SQLiteStatement stmt = db.compileStatement(sql);
        int cur = 1;
        stmt.clearBindings();
        final Collection<Object> values = dataToUpdate.values();
        for (Object o : values) {
            putStatement(stmt, o, cur);
            cur++;
        }
        return stmt;
    }

    private static String columnsToString(String[] columns, StringBuilder b) {
        b.append("(");
        for (int i = 0; i < columns.length; i++) {
            b.append(columns[i]);
            if (i != columns.length - 1) {
                b.append(", ");
            }
        }
        b.append(") ");
        return b.toString();
    }

    static ContentValues createUpdatedValues(Map<String, Object> data, ContentValues values) {
        Object o;
        for (String column : data.keySet()) {
            o = data.get(column);
            if (o instanceof Byte) {
                values.put(column, Byte.parseByte(o.toString()));
            } else if (o instanceof Short) {
                values.put(column, Short.parseShort(o.toString()));
            } else if (o instanceof Integer) {
                values.put(column, Integer.parseInt(o.toString()));
            } else if (o instanceof Long) {
                values.put(column, Long.parseLong(o.toString()));
            } else if (o instanceof Double) {
                values.put(column, Double.parseDouble(o.toString()));
            } else if (o instanceof Float) {
                values.put(column, Float.parseFloat(o.toString()));
            } else {
                values.put(column, o.toString());
            }
        }
        return values;
    }

    protected ContentValues getUpdatedValues() {
        return createUpdatedValues(dataToUpdate, new ContentValues(dataToUpdate.size()));
    }

    /**
     * Convenience method for returning a String from a {@link Cursor}. Returns an empty
     * String if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static String getCursorString(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getString(id);
        }
        return "";
    }

    /**
     * Convenience method for returning a boolean from a {@link Cursor}. Returns false
     * if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static boolean getCursorBoolean(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getInt(id) == 1;
        }
        return false;
    }

    /**
     * Convenience method for returning a Double from a {@link Cursor}. Returns 0.0
     * if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static double getCursorDouble(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getDouble(id);
        }
        return 0.0;
    }

    /**
     * Convenience method for returning a Float from a {@link Cursor}. Returns 0.0f
     * if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static float getCursorFloat(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getFloat(id);
        }
        return 0.0f;
    }

    /**
     * Convenience method for returning a Long from a {@link Cursor}. Returns 0
     * if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static long getCursorLong(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getLong(id);
        }
        return 0;
    }

    /**
     * Convenience method for returning a Short from a {@link Cursor}. Returns 0
     * if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static short getCursorShort(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getShort(id);
        }
        return 0;
    }

    /**
     * Convenience method for returning a int from a {@link Cursor}. Returns 0
     * if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static int getCursorInt(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getInt(id);
        }
        return 0;
    }

    /**
     * Convenience method for returning a byte[] from a {@link Cursor}. Returns an empty
     * array if the column was not found.
     * @param column
     * @param c
     * @return
     */
    public static byte[] getCursorBlob(String column, Cursor c) {
        int id = c.getColumnIndex(column);
        if (id != -1) {
            return c.getBlob(id);
        }
        return new byte[0];
    }

}
