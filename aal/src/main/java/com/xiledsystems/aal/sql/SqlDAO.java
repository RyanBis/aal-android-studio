package com.xiledsystems.aal.sql;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.sql.annotations.Id;
import com.xiledsystems.aal.sql.annotations.SQLTable;
import com.xiledsystems.aal.sql.annotations.SQLColumn;
import com.xiledsystems.aal.util.TextUtil;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@State(DevelopmentState.ALPHA)
public class SqlDAO extends DBObject {

    private static Map<Class, String> tables = new HashMap<>();
    private static Map<Class, List<Field>> columns = new HashMap<>();
    private static Map<Field, String> fieldColumnNames = new HashMap<>();
    private static Map<Class, Field> tableIds = new HashMap<>();
    private static Map<Field, String> idNames = new HashMap<>();

    private String columnInsertString;


    protected SqlDAO(Cursor cursor) {
        loadFromCursor(cursor);
    }

    @Override
    public final long insertWithStatement(SqlDb db) {
        final String sql = getInsertStatement();
        final List<Field> fields = getColumnFields(this);
        final long id;
        synchronized (db.getLock()) {
            final SQLiteStatement stmt = createStatement(fields, this, db, sql);
            id = stmt.executeInsert();
        }
        return id;
    }

    @Override
    public final void insertWithStatementAsync(SqlDb db) {
        insertWithStatementAsync(db, null);
    }

    @Override
    public final void insertWithStatementAsync(SqlDb db, SqlDbReturn listener) {
        final String sql = getInsertStatement();
        final List<Field> fields = getColumnFields(this);
        final SQLiteStatement stmt;
        synchronized (db.getLock()) {
            stmt = createStatement(fields, this, db, sql);
        }
        SqlDb.executeInsertStatementAsync(db.getLock(), stmt, listener, db.getThread());
    }

    @Override
    public final void update(SqlDb db) {
        final Map<String, Object> updatedData = getUpdatedData(this);
        if (updatedData.size() > 0) {
            if (TextUtil.isEmpty(getTableName())) {
                throw new SqlDbException("Table name is empty! Specify table name before updating.");
            }
            String idColumn = "%s=?";
            idColumn = String.format(idColumn, getIdColumn(this));
            db.update(getTableName(), getUpdatedValues(updatedData), idColumn, new String[] { String.valueOf(id) });
        }
    }

    private static String getIdColumn(SqlDAO dao) {
        return getIdFromField(tableIds.get(dao.getClass()));
    }

    @Override
    public final void updateAsync(SqlDb db) {
        updateAsync(db, null);
    }

    @Override
    public final void updateAsync(SqlDb db, SqlDbReturn listener) {
        final Map<String, Object> updatedData = getUpdatedData(this);
        if (updatedData.size() > 0) {
            if (TextUtil.isEmpty(getTableName())) {
                throw new SqlDbException("Table name is empty! Specify table name before updating.");
            }
            String idColumn = "%s=?";
            idColumn = String.format(idColumn, getIdColumn(this));
            db.updateAsync(getTableName(), getUpdatedValues(updatedData), idColumn, new String[] { String.valueOf(id) }, listener);
        }
    }

    /**
     * This method is stubbed out. It will not do anything, there's no reason to call it. This class figures out
     * what needs updating automatically when update() is called.
     */
    @Override
    public final void addData(String column, Object data) {
        // Stubbed out. Don't use this method in this class!
    }

    @Override
    public final void loadFromCursor(Cursor c) {
        if (c != null) {
            load(this, c);
        }
    }

    @Override
    public final String getTableName() {
        return getTableNameFromAnnotation(this);
    }

    @Override
    public final ContentValues getCreateValues() {
        return getCreateValuesFromAnnotations(this);
    }

    @Override
    final String getColumnsString() {
        if (TextUtil.isEmpty(columnInsertString)) {
            final List<Field> fields = getColumnFields(this);
            final int size = fields.size();
            final StringBuilder b = new StringBuilder();
            b.append("(");
            int cnt = 1;
            for (int i = 0; i < size; i++) {
                b.append(getColumnFromField(fields.get(i)));
                if (cnt != size) {
                    b.append(", ");
                }
                cnt++;
            }
            b.append(") ");
            columnInsertString = b.toString();
        }
        return columnInsertString;
    }

    @Override
    final String getInsertStatement() {
        String insertStatement = insertStatements.get(getClass());
        if (TextUtil.notEmpty(insertStatement)) {
            return insertStatement;
        } else {
            final List<Field> fields = getColumnFields(this);
            final StringBuilder b = new StringBuilder();
            b.append("INSERT INTO ").append(getTableName());
            b.append(getColumnsString(fields));
            final int size = fields.size();
            insertStatement = getValuesString(size, b);
            insertStatements.put(getClass(), insertStatement);
            return insertStatement;
        }
    }

    private ContentValues getUpdatedValues(Map<String, Object> data) {
        return createUpdatedValues(data, new ContentValues(data.size()));
    }

    /**
     * Clears all cached items. SqlDAO caches things like table names, column names, insert
     * statements, etc. This is for efficiency, so that reflection is only used once, then cached.
     * This method allows you to clear the cache.
     */
    public static void clearCache() {
        tables.clear();
        columns.clear();
        fieldColumnNames.clear();
        tableIds.clear();
        idNames.clear();
        insertStatements.clear();
    }



    private static Map<String, Object> getUpdatedData(SqlDAO dao) {
        final Map<String, Object> map = new HashMap<>();
        List<Field> fields = getColumnFields(dao);
        final int size = fields.size();
        Field f;
        Object curObject;
        Object oldObject;
        String column;
        try {
            for (int i = 0; i < size; i++) {
                f = fields.get(i);
                curObject = f.get(dao);
                column = getColumnFromField(f);
                oldObject = dao.dataToUpdate.get(column);
                if (oldObject != null && !oldObject.equals(curObject)) {
                    map.put(column, curObject);
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return map;
    }

    private static String getColumnsString(List<Field> fields) {
        final int size = fields.size();
        StringBuilder b = new StringBuilder();
        b.append("(");
        int cnt = 1;
        for (int i = 0; i < size; i++) {
            b.append(getColumnFromField(fields.get(i)));
            if (cnt != size) {
                b.append(", ");
            }
            cnt++;
        }
        b.append(") ");
        return b.toString();
    }

    private static SQLiteStatement createStatement(List<Field> fields, SqlDAO dao, SqlDb db, String sql) {
        final SQLiteStatement stmt = db.compileStatement(sql);
        try {
            final int size = fields.size();
            int cur = 1;
            stmt.clearBindings();
            Object obj;
            Field field;
            for (int i = 0; i < size; i++) {
                field = fields.get(i);
                obj = field.get(dao);
                putStatement(stmt, obj, cur);
                dao.addData(getColumnFromField(field), obj);
                cur++;
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return stmt;
    }

    private static void load(SqlDAO dao, Cursor c) {
        final List<Field> fields = getColumnFields(dao);
        for (Field f : fields) {
            final String columnName = getColumnFromField(f);
            try {
                setColumnObject(dao, f, columnName, c);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        String idColumn = getIdFromField(tableIds.get(dao.getClass()));
        dao.id = getCursorLong(idColumn, c);
    }

    private static String getTableNameFromAnnotation(SqlDAO dao) {
        final String name = tables.get(dao.getClass());
        if (TextUtil.notEmpty(name)) {
            return name;
        } else {
            final SQLTable table = dao.getClass().getAnnotation(SQLTable.class);
            if (table != null) {
                final String tableName = table.tableName();
                tables.put(dao.getClass(), tableName);
                return tableName;
            }
            throw new RuntimeException("Missing @Table annotation!");
        }
    }

    private static ContentValues getCreateValuesFromAnnotations(SqlDAO dao) {
        final List<Field> fields = getColumnFields(dao);
        final ContentValues vals = new ContentValues(fields.size());
        for (Field f : fields) {
            final String columnName = getColumnFromField(f);
            try {
                putValue(vals, columnName, f.get(dao));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return vals;
    }

    private static String getColumnFromField(Field f) {
        String name = fieldColumnNames.get(f);
        if (TextUtil.isEmpty(name)) {
            final SQLColumn col = f.getAnnotation(SQLColumn.class);
            if (!f.isAccessible()) {
                f.setAccessible(true);
            }
            name = col.columnName();
            fieldColumnNames.put(f, name);
        }
        return name;
    }

    private static String getIdFromField(Field f) {
        if (f == null)
            return SqlDb.ID;

        String name = idNames.get(f);
        if (TextUtil.isEmpty(name)) {
            final Id id = f.getAnnotation(Id.class);
            if (!f.isAccessible()) {
                f.setAccessible(true);
            }
            name = id.columnName();
            idNames.put(f, name);
        }
        return name;
    }

    private static List<Field> getColumnFields(SqlDAO object) {
        final Class<?> objClass = object.getClass();
        List<Field> list = columns.get(objClass);
        if (list == null) {
            list = new ArrayList<>();
            final Field[] fields = objClass.getDeclaredFields();
            for (Field f : fields) {
                if (f.isAnnotationPresent(SQLColumn.class)) {
                    list.add(f);
                }
                if (f.isAnnotationPresent(Id.class)) {
                    tableIds.put(objClass, f);
                }
            }
            columns.put(objClass, list);
        }
        return list;
    }

    private static void putValue(ContentValues vals, String columnName, Object value) {
        if (value instanceof Boolean) {
            vals.put(columnName, ((Boolean) value) ? 1 : 0);
        } else if (value instanceof Long) {
            vals.put(columnName, (Long) value);
        } else if (value instanceof Integer) {
            vals.put(columnName, (Integer) value);
        } else if (value instanceof Short) {
            vals.put(columnName, (Short) value);
        } else if (value instanceof Byte) {
            vals.put(columnName, (Byte) value);
        } else if (value instanceof byte[]) {
            vals.put(columnName, (byte[]) value);
        } else if (value instanceof Double) {
            vals.put(columnName, (Double) value);
        } else if (value instanceof Float) {
            vals.put(columnName, (Float) value);
        } else if (value instanceof String) {
            vals.put(columnName, (String) value);
        } else {
            vals.put(columnName, value.toString());
        }
    }

    private static void setColumnObject(SqlDAO dao, Field f, String columnName, Cursor c) throws IllegalAccessException {
        final Class type = getFieldType(f);
        if (type == Boolean.class || type == boolean.class) {
            final boolean b = getCursorBoolean(columnName, c);
            if (type.isPrimitive()) {
                f.setBoolean(dao, b);
            } else {
                f.set(dao, b);
            }
            dao.dataToUpdate.put(columnName, b);
        } else if (type == Double.class || type == double.class) {
            final double d = getCursorDouble(columnName, c);
            if (type.isPrimitive()) {
                f.setDouble(dao, d);
            } else {
                f.set(dao, d);
            }
            dao.dataToUpdate.put(columnName, d);
        } else if (type == Float.class || type == float.class) {
            final float fl = getCursorFloat(columnName, c);
            if (type.isPrimitive()) {
                f.setFloat(dao, fl);
            } else {
                f.set(dao, fl);
            }
            dao.dataToUpdate.put(columnName, fl);
        } else if (type == Long.class || type == long.class) {
            final long l = getCursorLong(columnName, c);
            if (type.isPrimitive()) {
                f.setLong(dao, l);
            } else {
                f.set(dao, l);
            }
            dao.dataToUpdate.put(columnName, l);
        } else if (type == Integer.class || type == int.class) {
            final int i = getCursorInt(columnName, c);
            if (type.isPrimitive()) {
                f.setInt(dao, i);
            } else {
                f.set(dao, i);
            }
            dao.dataToUpdate.put(columnName, i);
        } else if (type == Short.class || type == short.class) {
            final short s = getCursorShort(columnName, c);
            if (type.isPrimitive()) {
                f.setShort(dao, s);
            } else {
                f.set(dao, s);
            }
            dao.dataToUpdate.put(columnName, s);
        } else if (type == Byte.class || type == byte.class) {
            final byte b = (byte) getCursorInt(columnName, c);
            if (type.isPrimitive()) {
                f.setByte(dao, b);
            } else {
                f.set(dao, b);
            }
            dao.dataToUpdate.put(columnName, b);
        } else if (type == byte[].class) {
            final byte[] ba = getCursorBlob(columnName, c);
            f.set(dao, ba);
            dao.dataToUpdate.put(columnName, ba);
        } else {
            final String st = getCursorString(columnName, c);
            f.set(dao, st);
            dao.dataToUpdate.put(columnName, st);
        }
    }

    private static Class<?> getFieldType(Field field) {
        return field.getType();
    }

}
