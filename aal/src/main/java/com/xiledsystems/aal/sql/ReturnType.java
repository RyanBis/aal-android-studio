package com.xiledsystems.aal.sql;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Convenience enum for the different types of returns when using the Async
 * methods in {@link SqlDb}.
 *
 */
@State(DevelopmentState.UNTESTED)
public enum ReturnType {
    BOOLEAN,
    INT,
    LONG,
    STRING,
    BLOB,
    ARRAYLIST_STRING,
    ARRAYLIST_DBOBJECT,
    ARRAYLIST_INT,
    ARRAYLIST_LONG,
    ARRAYLIST_BLOB,
    CURSOR,
    UNKNOWN
}
