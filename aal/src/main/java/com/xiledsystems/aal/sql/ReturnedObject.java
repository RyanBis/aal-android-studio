package com.xiledsystems.aal.sql;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public class ReturnedObject {

    private Object obj;
    private ReturnType returnType;

    public ReturnedObject(Object o, ReturnType returnType) {
        this.returnType = returnType;
        this.obj = o;
    }

    /**
     * Get the value of the returned object from the SqlDb operation. This can be
     * any of the options found in {@link ReturnType}.
     *
     * @return
     */
    public <T> T get() {
        return (T) obj;
    }

    /**
     * Convenience method to tell the {@link ReturnType} of this ReturnedObject.
     *
     * @return
     */
    public ReturnType getType() {
        return returnType;
    }
}
