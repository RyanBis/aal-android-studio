package com.xiledsystems.aal.sql;


import android.database.Cursor;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Interface used when pulling a list of DBObjects from a SqlDb database, using
 * {@link Query#getObjectList(SqlDb, DBObjectFactory)}.
 *
 * @param <T> Your class which extends DBObject
 */
@State(DevelopmentState.PRODUCTION)
public interface DBObjectFactory<T extends DBObject> {
    T newObject(Cursor cursor);
}
