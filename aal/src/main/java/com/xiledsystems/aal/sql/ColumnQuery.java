package com.xiledsystems.aal.sql;

import java.util.ArrayList;
import android.database.Cursor;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.PRODUCTION)
public class ColumnQuery extends BaseOp {
	
	private final String columnName;
	private final String tableName;
	
	public ColumnQuery(String table, String name) {
		columnName = name;
		tableName = table;
	}
	
	public ArrayList<String> executeList(SqlDb db) {
		ArrayList<String> results = new ArrayList<String>();
		synchronized (db.getLock()) {
			final Cursor cursor = db.query(this);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						results.add(cursor.getString(0));
					} while (cursor.moveToNext());
				}
				cursor.close();
			}
		}
		return results;		
	}

	@Override
	public String getStatement() {
		return new Query().select(columnName).from(tableName).getStatement();
	}

	@Override
	public String[] getSelectionArgs() {
		return null;
	}

}