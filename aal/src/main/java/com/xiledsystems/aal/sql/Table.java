package com.xiledsystems.aal.sql;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;
import java.util.ArrayList;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@State(DevelopmentState.PRODUCTION)
public final class Table {

	private final static String NULL = "null";
	private final static String TEXT = "text";
	private final static String INT = "integer";
	private final static String BLOB = "blob";

	private final String name;
	private String idColumn = SqlDb.ID;
	private ArrayList<Column> columns = new ArrayList<Column>();


	public Table(String name) {
		this.name = name;
	}
	
	public final String getName() {
		return name;
	}
	
	public final Table setIDColumn(String columnName) {
		idColumn = columnName;
		return this;
	}
	
	public final String getIdColumn() {
		return idColumn;
	}
	
	/**
	 * This adds column(s) to the table, defaulting the {@link DataType} to text ({@link DataType#STRING)}
	 * @param name
	 */
	public final Table addColumns(String... name) {
		if (name != null && name.length > 0) {
			for (String n : name) {
				columns.add(new Column(n));
			}
		}
		return this;
	}

	/**
	 * Adds the given column(s) to the table, with the {@link DataType} set to {@link DataType#INTEGER}.
	 * @param columnNames
	 * @return
     */
	public final Table addIntegerColumns(String... columnNames) {
		if (columnNames != null && columnNames.length > 0) {
			for (String name : columnNames) {
				columns.add(new Column(name, DataType.INTEGER));
			}
		}
		return this;
	}

	/**
	 * Adds the given column(s) to the table, with the {@link DataType} set to {@link DataType#FLOAT}.
	 * @param columnNames
	 * @return
	 */
	public final Table addFloatColumns(String... columnNames) {
		if (columnNames != null && columnNames.length > 0) {
			for (String name : columnNames) {
				columns.add(new Column(name, DataType.FLOAT));
			}
		}
		return this;
	}

	/**
	 * Adds the given column(s) to the table, with the {@link DataType} set to {@link DataType#BLOB}.
	 * @param columnNames
	 * @return
	 */
	public final Table addBlobColumns(String... columnNames) {
		if (columnNames != null && columnNames.length > 0) {
			for (String name : columnNames) {
				columns.add(new Column(name, DataType.BLOB));
			}
		}
		return this;
	}

	/**
	 * Adds the given column to the table, with the {@link DataType} set to {@link DataType#STRING}.
	 * @return
	 */
	public final Table addColumn(String name) {
		if (!TextUtil.isEmpty(name)) {
			columns.add(new Column(name));
		}
		return this;
	}

	/**
	 * Adds the given column to the table, with the given {@link DataType}.
	 * @return
	 */
	public final Table addColumn(String name, DataType dataType) {
		Column c = new Column(name);
		if (dataType != null) {
			c.setDataType(dataType);
		}
		columns.add(c);
		return this;
	}

	/**
	 * Adds the given {@link Column} to the table.
	 * @return
	 */
	public final Table addColumn(Column column) {
		columns.add(column);
		return this;
	}

	/**
	 *
	 * @return a list of the {@link Column}s in this table.
     */
	public final ArrayList<Column> getColumns() {
		return columns;
	}

	/**
	 *
	 * @return a list of the column names in this table.
     */
	public final ArrayList<String> getColumnNames() {
		int size = columns.size();
		ArrayList<String> list = new ArrayList<String>(size);
		for (int i = 0; i < size; i++) {
			list.add(columns.get(i).getName());
		}
		return list;
	}
	
	/**
	 * This returns the column names, and datatypes in this table, separated by comma
	 * 
	 */
	public final String getColumnsString() {
		StringBuilder b = new StringBuilder();
		int size = columns.size();
		for (int i = 0; i < size; i++) {
			b.append(columns.get(i).getName());
			b.append(" " + columns.get(i).getDataType_string());
			if (i != size - 1) {
				b.append(", ");
			}			
		}
		return b.toString();
	}

	@Override
	public final String toString() {
		StringBuilder b = new StringBuilder();
		b.append("Table: ").append(name).append(" Columns: ").append(columns.toString());
		return b.toString();
	}

	@Override
	public final boolean equals(Object o) {
		if (o != null && o instanceof Table) {
			Table t = (Table) o;
			return t.columns.equals(columns) && t.idColumn.equals(idColumn) && t.name.equals(name);
		}
		return false;
	}

	private static DataType getType(int type) {
		switch (type) {
			case Cursor.FIELD_TYPE_BLOB:
				return DataType.BLOB;
			case Cursor.FIELD_TYPE_FLOAT:
				return DataType.FLOAT;
			case Cursor.FIELD_TYPE_INTEGER:
				return DataType.INTEGER;
			case Cursor.FIELD_TYPE_NULL:
				return DataType.NULL;
			default:
				return DataType.STRING;
		}
	}

}
