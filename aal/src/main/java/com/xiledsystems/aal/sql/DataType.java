package com.xiledsystems.aal.sql;


public enum DataType {

    /**
     * This can be a byte, short, int, or long
     */
    INTEGER("INTEGER"),

    /**
     * This can be either a Float, or a Double
     */
    FLOAT("REAL"),

    STRING("TEXT"),

    /**
     * This is a byte[]
     */
    BLOB("BLOB"),

    NULL("NULL");

    private static DataType[] values;

    private final String dataName;

    DataType(String dataName) {
        this.dataName = dataName;
    }

    public String dataName() {
        return dataName;
    }

    public static DataType[] VALUES() {
        if (values == null) {
            values = values();
        }
        return values;
    }

    public static DataType fromString(String type) {
        for (DataType t : VALUES()) {
            if (t.dataName().equalsIgnoreCase(type)) {
                return t;
            }
        }
        return STRING;
    }

}
