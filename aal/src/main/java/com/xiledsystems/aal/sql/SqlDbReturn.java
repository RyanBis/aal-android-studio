package com.xiledsystems.aal.sql;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Interface to use when using the Async methods in {@link SqlDb}.
 */
@State(DevelopmentState.UNTESTED)
public interface SqlDbReturn {

	/**
	 * Returns the requested object inside a {@link ReturnedObject}. Just
	 * use {@link ReturnedObject#get()} to get whatever it is (one of
	 * the types listed in {@link ReturnType}).
	 *
	 * @param value
	 */
    void objectReturned(ReturnedObject value);

	/**
	 * When this returns true, {@link #objectReturned(ReturnedObject)} is posted
	 * on the UI thread. Otherwise, it is called on the Thread that made
	 * the query.
	 *
	 * @return
	 */
	boolean returnOnUIThread();

}
