package com.xiledsystems.aal;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Enumeration for Major_Minor versions of AAL.
 */
@State(DevelopmentState.PRODUCTION)
public enum AalVersion {

    VERSION_1_0,
    VERSION_1_1,
    VERSION_1_2,
    VERSION_1_3,
    VERSION_1_4,
    VERSION_2_0,
    VERSION_2_1,
    VERSION_2_2,
    VERSION_2_3,
    VERSION_2_4;

    public static AalVersion getCurrentVersion() {
        int last = values().length - 1;
        return values()[last];
    }

}
