package com.xiledsystems.aal.ads.base;


import android.app.Activity;
import com.xiledsystems.aal.ads.AdInterface;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;


@State(DevelopmentState.ALPHA)
public abstract class BaseBannerAd extends BaseAd {

    private final Activity context;
    private final AdInterface adHandler;
    private final String appKey;


    public BaseBannerAd(Activity context, String appKey, AdInterface adHandler) {
        super(context);
        this.context = context;
        this.adHandler = adHandler;
        this.appKey = appKey;
    }

    /**
     * Use this method when using the AalActivity class. After instantiating this class,
     * you can then add test devices, if needed, then call this method to setup
     * the ad to load.
     */
    public void setup() {
        if (isIAal()) {
            onCreate(context, appKey);
            loadAd();
        } else {
            throw new RuntimeException("setup() called when NOT in AalActivity! You must call onCreate(), and loadad() manually.");
        }
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new LifeListener();
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }


    private class LifeListener extends LifecycleListener {

        @Override
        public void onDestroy() {
            BaseBannerAd.this.onDestroy();
        }
    }
}
