package com.xiledsystems.aal.ads;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.PRODUCTION)
public interface AdInterface {
    void adClosed();
    void adError(String errorMsg);
    void adLoaded();
    void adLoadingError(String errorMsg);
}
