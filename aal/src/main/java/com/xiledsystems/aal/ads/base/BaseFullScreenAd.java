package com.xiledsystems.aal.ads.base;


import android.app.Activity;
import android.os.Bundle;
import com.xiledsystems.aal.ads.AdInterface;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.LifeCycle;
import com.xiledsystems.aal.annotations.LifeCycleMethod;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;


@State(value = DevelopmentState.PRODUCTION, note = "Don't directly use this class. Instead, use FullScreenAd instead.")
@LifeCycle(value = {LifeCycleMethod.ON_CREATE, LifeCycleMethod.ON_DESTROY})
public abstract class BaseFullScreenAd extends BaseAd {

    private final Activity context;
    private final AdInterface adHandler;
    private final String appKey;


    public abstract void showAd();
    public abstract void addTestDevice(String device);


    public BaseFullScreenAd(Activity context, String appKey, AdInterface adHandler) {
        super(context);
        this.context = context;
        this.adHandler = adHandler;
        this.appKey = appKey;
    }

    public Activity getContext() {
        return context;
    }

    public AdInterface getAdHandler() {
        return adHandler;
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new LifeListener();
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return null;
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    private class LifeListener extends LifecycleListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            BaseFullScreenAd.this.onCreate(context, appKey);
            loadAd();
        }

        @Override
        public void onDestroy() {
            BaseFullScreenAd.this.onDestroy();
        }
    }
}
