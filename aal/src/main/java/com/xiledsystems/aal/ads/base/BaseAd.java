package com.xiledsystems.aal.ads.base;


import android.app.Activity;
import android.content.Context;

import com.xiledsystems.aal.internal.DisplayComponent;
import com.xiledsystems.aal.internal.NonDisplayComponent;


public abstract class BaseAd extends DisplayComponent {


    public BaseAd(Context context) {
        super(context);
    }

    public abstract void onCreate(Activity context, String appKey);
    public abstract void loadAd();
    public abstract void showAd();
    public abstract boolean isAdLoaded();
    public abstract void onDestroy();
    public abstract void addTestDevice(String device);

}
