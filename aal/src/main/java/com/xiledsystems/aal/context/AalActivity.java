package com.xiledsystems.aal.context;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.control.VCUtil;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.util.ViewUtil;


@TargetApi(Build.VERSION_CODES.ECLAIR)
@State(DevelopmentState.PRODUCTION)
public abstract class AalActivity extends Activity implements IAalActivity {


    private final ActivityManager activityManager = new ActivityManager(this);


    // ******* IAalActivity method overrides. *******

    @Override
    public ActivityManager getManager() {
        return activityManager;
    }

    /**
     * Override this method to perform operations when the screen is first
     * initialized (first time it reports a width/height greater than 0).
     * This is only called once after the Activity is created.
     */
    @Override
    public void onScreenInitialized() {
    }

    @Override
    public void onBackPressed() {
        if (!activityManager.onBackPressed()) {
            doNativeBackPressed();
        }
    }

    public void doNativeBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onServiceBound(AalService service) {
    }

    @Override
    public void onServiceDisconnected() {
    }


    // ***** Android Activity lifecycle methods which ActivityManager needs for Components *****

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityManager.onCreate(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        activityManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        activityManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        activityManager.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityManager.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityManager.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        activityManager.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        activityManager.onStart();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        activityManager.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        activityManager.onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        activityManager.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        activityManager.onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Convenience method for retrieving the {@link AalService} object this
     * AalActivity has been bound to. If it's not bound to any {@link AalService}, then
     * null is returned. This automatically casts your class (as along as it extends
     * {@link AalService}).
     * @param <T>
     * @return
     */
    public <T extends AalService> T getService() {
        return activityManager.getService();
    }

    /**
     * When this is set to true, this will shut the service down after
     * unbinding from it.
     *
     * @param stop
     */
    public void stopOnUnbind(boolean stop) {
        activityManager.stopOnUnbind(stop);
    }

    /**
     * Returns a Handler for posting to the UI thread.
     *
     * @return
     */
    public Handler getHandler() {
        return activityManager.getHandler();
    }

    /**
     * Use this method to bind to an AalService. When the service has been successfully bound to
     * this Activity, the onServiceBound() method will be called. Make sure to override that method
     * so you know when the service actually is bound.
     *
     * @param serviceClass
     */
    public void bindToService(Class<?> serviceClass) {
        activityManager.bindToService(serviceClass);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, Enum)}
     */
    public final void setClickEvent(View view, Enum category) {
        VCUtil.setClickEvent(view, category);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, Enum, Object...)}
     */
    public final void setClickEvent(View view, Enum category, Object... data) {
        VCUtil.setClickEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setClickEvent(View, com.xiledsystems.aal.events.Event.Builder)}
     */
    public final void setClickEvent(View view, Event.Builder builder) {
        VCUtil.setClickEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Enum)}
     */
    public final void setCheckedEvent(CompoundButton view, Enum category) {
        VCUtil.setCheckedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Enum, Object...)}
     */
    public final void setCheckedEvent(CompoundButton view, Enum category, Object... data) {
        VCUtil.setCheckedEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setCheckedEvent(CompoundButton, Event.Builder)}
     */
    public final void setCheckedEvent(CompoundButton view, Event.Builder builder) {
        VCUtil.setCheckedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Enum)}
     */
    public final void setTextChangedEvent(TextView view, Enum category) {
        VCUtil.setTextChangedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Enum, Object...)}
     */
    public final void setTextChangedEvent(TextView view, Enum category, Object... data) {
        VCUtil.setTextChangedEvent(view, category, data);
    }

    /**
     * See {@link VCUtil#setTextChangedEvent(TextView, Event.Builder)}
     */
    public final void setTextChangedEvent(TextView view, Event.Builder builder) {
        VCUtil.setTextChangedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Event.Builder)}
     */
    public final void setProgressChangedEvent(SeekBar view, Event.Builder builder) {
        VCUtil.setProgressChangedEvent(view, builder);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Enum)}
     */
    public final void setProgressChangedEvent(SeekBar view, Enum category) {
        VCUtil.setProgressChangedEvent(view, category);
    }

    /**
     * See {@link VCUtil#setProgressChangedEvent(SeekBar, Enum, Object...)}
     */
    public final void setProgressChangedEvent(SeekBar view, Enum category, Object... args) {
        VCUtil.setProgressChangedEvent(view, category, args);
    }

    /**
     * Forwards {@link ViewUtil#find(int, Activity)}.
     */
    public final <T extends View> T find(int viewResId) {
        return ViewUtil.find(viewResId, this);
    }

    /**
     * Forwards {@link ViewUtil#find(int, View)}.
     */
    public final <T extends View> T find(int viewResId, View parent) {
        return ViewUtil.find(viewResId, parent);
    }

    /**
     * Forwards {@link ViewUtil#create(Activity, int)}.
     */
    public final <T extends View> T create(int layoutResId) {
        return ViewUtil.create(this, layoutResId);
    }

    /**
     * Forwards {@link ViewUtil#showKeyboard(Context)}.
     */
    public final void showKeyboard() {
        ViewUtil.showKeyboard(this);
    }

    /**
     * Forwards {@link ViewUtil#hideKeyboard(Context, View)}.
     */
    public final void hideKeyboard(View view) {
        ViewUtil.hideKeyboard(this, view);
    }

    /**
     * Forwards {@link ViewUtil#getScreenSize(Activity)}.
     */
    public final Point getScreenSize() {
        return ViewUtil.getScreenSize(this);
    }

    /**
     * Forwards {@link ViewUtil#getAvailableSize(Activity)}.
     */
    public final Point getAvailableSize() {
        return ViewUtil.getAvailableSize(this);
    }

}