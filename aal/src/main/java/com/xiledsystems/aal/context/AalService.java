package com.xiledsystems.aal.context;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.UNTESTED)
public abstract class AalService extends Service implements IAalService {



    private final ServiceManager serviceManager = new ServiceManager(this);


    public void post(Runnable action) {
        serviceManager.getHandler().post(action);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        serviceManager.onCreate();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        serviceManager.onLowMemory();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return serviceManager.onStartCommand(intent, flags, startId, super.onStartCommand(intent, flags, startId));
    }

    @Override
    public IBinder onBind(Intent intent) {
        return serviceManager.onBind(intent);
    }

    @Override
    public void onDestroy() {
        serviceManager.onDestroy();
        super.onDestroy();
    }

    @Override
    public ServiceManager getManager() {
        return serviceManager;
    }
}