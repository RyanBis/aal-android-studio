package com.xiledsystems.aal.context;


public interface OnRequestPermissionsListener {
    void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
}
