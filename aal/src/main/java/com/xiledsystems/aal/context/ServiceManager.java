package com.xiledsystems.aal.context;


import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import com.xiledsystems.aal.util.ThreadManager;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class ServiceManager {

    private final AalBinder aBinder = new AalBinder();
    private Handler handler;
    private Set<CoreServiceLifecycleListener> lifeListeners = new HashSet<>();
    private Service context;
    private IAalService service;
    private Integer startInt;


    public ServiceManager(Service service) {
        this.context = service;
        if (service instanceof IAalService) {
            this.service = (IAalService) service;
        } else {
            throw new IllegalAccessError("ServiceManager can only be initialized with a Service which implements the IAalService interface!");
        }
    }

    public void startSticky(boolean sticky) {
        if (sticky) {
            startInt = Service.START_STICKY;
        } else {
            startInt = Service.START_NOT_STICKY;
        }
    }

    public void setStartFlag(int flag) {
        startInt = flag;
    }

    public int onStartCommand(Intent intent, int flags, int startId, int superInt) {
        synchronized (lifeListeners) {
            final Iterator<CoreServiceLifecycleListener> it = lifeListeners.iterator();
            CoreServiceLifecycleListener listener;
            while (it.hasNext()) {
                listener = it.next();
                if (listener != null) {
                    listener.onStartCommand(intent, flags, startId);
                }
            }
        }
        if (startInt == null) {
            return superInt;
        } else {
            return startInt;
        }
    }

    public void onCreate() {
        handler = ThreadManager.getHandler();
        service.onCreateService();
        synchronized (lifeListeners) {
            final Iterator<CoreServiceLifecycleListener> it = lifeListeners.iterator();
            CoreServiceLifecycleListener listener;
            while (it.hasNext()) {
                listener = it.next();
                if (listener != null) {
                    listener.onCreateService();
                }
            }
        }
    }

    public Service getService() {
        return context;
    }

    public IBinder onBind(Intent intent) {
        return aBinder;
    }

    public Handler getHandler() {
        return handler;
    }

    public void onLowMemory() {
        synchronized (lifeListeners) {
            final Iterator<CoreServiceLifecycleListener> it = lifeListeners.iterator();
            CoreServiceLifecycleListener listener;
            while (it.hasNext()) {
                listener = it.next();
                if (listener != null) {
                    listener.onLowMemory();
                }
            }
        }
    }

    public void onDestroy() {
        synchronized (lifeListeners) {
            final Iterator<CoreServiceLifecycleListener> it = lifeListeners.iterator();
            CoreServiceLifecycleListener listener;
            while (it.hasNext()) {
                listener = it.next();
                if (listener != null) {
                    listener.onDestroy();
                }
            }
        }
        lifeListeners.clear();
        service = null;
    }

    public class AalBinder extends Binder {
        public <T extends Service> T getService() {
            return (T) service;
        }
    }

    public void registerLifecycleListener(CoreServiceLifecycleListener listener) {
        lifeListeners.add(listener);
    }

}
