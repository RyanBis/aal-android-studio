package com.xiledsystems.aal.context;


import android.content.Intent;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.BETA)
public interface CoreServiceLifecycleListener {

    void onCreateService();
    int onStartCommand(Intent intent, int flags, int startId);
    void onLowMemory();
    void onDestroy();

}
