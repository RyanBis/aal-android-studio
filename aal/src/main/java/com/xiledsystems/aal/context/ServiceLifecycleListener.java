package com.xiledsystems.aal.context;


import android.app.Service;
import android.content.Intent;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Convenience class in case you only need to implement one method of the
 * lifecycle.
 */
@State(DevelopmentState.BETA)
public class ServiceLifecycleListener implements CoreServiceLifecycleListener {

    @Override
    public void onCreateService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onLowMemory() {
    }

    @Override
    public void onDestroy() {
    }
}
