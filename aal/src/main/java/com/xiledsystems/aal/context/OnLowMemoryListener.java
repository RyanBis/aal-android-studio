package com.xiledsystems.aal.context;


public interface OnLowMemoryListener {
    void onLowMemory();
}
