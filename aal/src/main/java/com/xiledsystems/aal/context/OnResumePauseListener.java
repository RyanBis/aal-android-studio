package com.xiledsystems.aal.context;


public interface OnResumePauseListener {
    void onResume();
    void onPause();
}
