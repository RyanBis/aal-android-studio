package com.xiledsystems.aal.context;


import android.content.ComponentName;


public interface AalServiceListener {
    void onServiceBound(AalService service);
    void onServiceDisconnected(ComponentName componentName);
}
