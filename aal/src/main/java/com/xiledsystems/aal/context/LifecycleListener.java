package com.xiledsystems.aal.context;


import android.content.Intent;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Convenience class to use, if you only want to override a couple of
 * lifecycle events.
 */
@State(DevelopmentState.BETA)
public class LifecycleListener implements OnCreateDestroyListener, OnActivityResultListener, OnLowMemoryListener, OnNewIntentListener,
                                          OnResumePauseListener, OnStartStopListener, OnRequestPermissionsListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    @Override
    public void onNewIntent(Intent intent) {
    }

    @Override
    public void onLowMemory() {
    }

    @Override
    public void onDestroy() {
    }
}
