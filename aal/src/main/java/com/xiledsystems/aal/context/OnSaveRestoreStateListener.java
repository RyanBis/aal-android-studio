package com.xiledsystems.aal.context;


import android.os.Bundle;

public interface OnSaveRestoreStateListener {

    void onSaveInstanceState(Bundle state);
    void onRestoreInstanceState(Bundle savedState);

}
