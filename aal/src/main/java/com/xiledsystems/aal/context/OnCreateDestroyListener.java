package com.xiledsystems.aal.context;


import android.os.Bundle;

public interface OnCreateDestroyListener {
    void onCreate(Bundle savedInstanceState);
    void onDestroy();
}
