package com.xiledsystems.aal.context;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class FragmentManager extends BaseFragmentManager {

    private Fragment fragment;


    public FragmentManager(Fragment fragment) {
        this.fragment = fragment;
        if (fragment instanceof IAalFragment) {
        } else {
            throw new IllegalAccessError("The context instantiating FragmentManager must implement the IAalFragment interface!");
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment = null;
    }

    public Fragment getFragment() {
        return fragment;
    }

    @Override
    public Context getContext() {
        return fragment.getActivity();
    }

    @Override
    public Activity getActivity() {
        return fragment.getActivity();
    }
}
