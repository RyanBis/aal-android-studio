package com.xiledsystems.aal.context;


import android.content.Intent;


public interface OnNewIntentListener {
    void onNewIntent(Intent intent);
}
