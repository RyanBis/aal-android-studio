package com.xiledsystems.aal.context;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public abstract class BaseFragmentManager
{

    private Set<OnLowMemoryListener> lowMemoryListeners = new HashSet<>(0);
    private Set<OnCreateDestroyListener> createDestroyListeners = new HashSet<>(0);
    private Set<OnActivityResultListener> activityResultListeners = new HashSet<>(0);
    private Set<OnResumePauseListener> resumePauseListeners = new HashSet<>(0);
    private Set<OnStartStopListener> startStopListeners = new HashSet<>(0);


    public abstract Context getContext();
    public abstract Activity getActivity();

    public void onResume() {
        synchronized (resumePauseListeners) {
            final Iterator<OnResumePauseListener> it = resumePauseListeners.iterator();
            OnResumePauseListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onResume();
            }
        }
    }

    public void onPause() {
        synchronized (resumePauseListeners) {
            final Iterator<OnResumePauseListener> it = resumePauseListeners.iterator();
            OnResumePauseListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onPause();
            }
        }
    }

    public void onStop() {
        synchronized (startStopListeners) {
            final Iterator<OnStartStopListener> it = startStopListeners.iterator();
            OnStartStopListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onStop();
            }
        }
    }

    public void onStart() {
        synchronized (startStopListeners) {
            final Iterator<OnStartStopListener> it = startStopListeners.iterator();
            OnStartStopListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onStart();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        synchronized (activityResultListeners) {
            final Iterator<OnActivityResultListener> it = activityResultListeners.iterator();
            OnActivityResultListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    public void onLowMemory() {
        synchronized (lowMemoryListeners) {
            final Iterator<OnLowMemoryListener> it = lowMemoryListeners.iterator();
            OnLowMemoryListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onLowMemory();
            }
        }
    }

    public void onDestroy() {
        synchronized (createDestroyListeners) {
            final Iterator<OnCreateDestroyListener> it = createDestroyListeners.iterator();
            OnCreateDestroyListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onDestroy();
            }
        }
        createDestroyListeners.clear();
        lowMemoryListeners.clear();
        activityResultListeners.clear();
        startStopListeners.clear();
        resumePauseListeners.clear();
    }

    public void registerOnActivityResultListener(OnActivityResultListener listener) {
        synchronized (activityResultListeners) {
            activityResultListeners.add(listener);
        }
    }

    public void registerOnCreateDestroyListener(OnCreateDestroyListener listener) {
        synchronized (createDestroyListeners) {
            createDestroyListeners.add(listener);
        }
    }

    public void registerOnResumePauseListener(OnResumePauseListener listener) {
        synchronized (resumePauseListeners) {
            resumePauseListeners.add(listener);
        }
    }

    public void registerOnStartStopListener(OnStartStopListener listener) {
        synchronized (startStopListeners) {
            startStopListeners.add(listener);
        }
    }

    public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
        synchronized (lowMemoryListeners) {
            lowMemoryListeners.add(listener);
        }
    }

    public void unregisterOnActivityResultListener(OnActivityResultListener listener) {
        synchronized (activityResultListeners) {
            activityResultListeners.remove(listener);
        }
    }

    public void unregisterOnCreateDestroyListener(OnCreateDestroyListener listener) {
        synchronized (createDestroyListeners) {
            createDestroyListeners.remove(listener);
        }
    }

    public void unregisterOnResumePauseListener(OnResumePauseListener listener) {
        synchronized (resumePauseListeners) {
            resumePauseListeners.remove(listener);
        }
    }

    public void unregisterOnStartStopListener(OnStartStopListener listener) {
        synchronized (startStopListeners) {
            startStopListeners.remove(listener);
        }
    }

    public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
        synchronized (lowMemoryListeners) {
            lowMemoryListeners.remove(listener);
        }
    }

}
