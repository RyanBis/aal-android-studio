package com.xiledsystems.aal.context;


public interface OnStartStopListener {
    void onStart();
    void onStop();
}
