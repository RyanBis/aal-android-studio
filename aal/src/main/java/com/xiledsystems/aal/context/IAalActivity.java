package com.xiledsystems.aal.context;


import android.os.Bundle;

public interface IAalActivity {
    ActivityManager getManager();
    void onCreateActivity(Bundle savedInstanceState);
    void onScreenInitialized();
    void onServiceBound(AalService service);
    void onServiceDisconnected();
    void doNativeBackPressed();
}
