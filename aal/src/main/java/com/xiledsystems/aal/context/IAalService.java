package com.xiledsystems.aal.context;


public interface IAalService {
    ServiceManager getManager();
    void onCreateService();
}
