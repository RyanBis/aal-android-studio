package com.xiledsystems.aal.context;


public interface IAalFragment {

    BaseFragmentManager getManager();

}
