package com.xiledsystems.aal.context;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.view.ViewGroup;
import com.xiledsystems.aal.util.ThreadManager;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;


public class ActivityManager {

    private Activity context;
    private final Set<OnLowMemoryListener> lowMemoryListeners;
    private final Set<OnActivityResultListener> activityResultListeners;
    private final Set<OnCreateDestroyListener> createDestroyListeners;
    private final Set<OnNewIntentListener> newIntentListeners;
    private final Set<OnResumePauseListener> resumePauseListeners;
    private final Set<OnStartStopListener> startStopListeners;
    private final Set<OnRequestPermissionsListener> requestPermissionsListeners;
    private final Set<OnSaveRestoreStateListener> saveRestoreStateListeners;
    private Runnable onBackPressed;
    private Handler uiHandler;
    private ServiceListener serviceListener = new ServiceListener();
    private Class<?> boundServiceClass;
    protected AalService boundService;
    private AalServiceListener sListener;
    private boolean stopOnUnbind = false;
    protected float density;
    private IAalActivity activity;


    public ActivityManager(Activity context) {
        this.context = context;
        lowMemoryListeners = new HashSet<>(0);
        activityResultListeners = new HashSet<>(0);
        createDestroyListeners = new HashSet<>(0);
        newIntentListeners = new HashSet<>(0);
        resumePauseListeners = new HashSet<>(0);
        startStopListeners = new HashSet<>(0);
        requestPermissionsListeners = new HashSet<>(0);
        saveRestoreStateListeners = new HashSet<>(0);
        if (context instanceof IAalActivity) {
            activity = (IAalActivity) context;
        } else {
            throw new IllegalAccessError("The context instantiating ActivityManager must implement the IAalActivity interface!");
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        uiHandler = ThreadManager.getHandler();
        density = context.getResources().getDisplayMetrics().density;
        activity.onCreateActivity(savedInstanceState);
        synchronized (createDestroyListeners) {
            final Iterator<OnCreateDestroyListener> it = createDestroyListeners.iterator();
            OnCreateDestroyListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onCreate(savedInstanceState);
            }
        }
        waitForScreenInit();
    }

    // Returns true if this ActivityManager is set to handle the back pressed event.
    // This could just mean it's being used with an AalScreenModel
    public boolean onBackPressed() {
        if (onBackPressed != null) {
            onBackPressed.run();
            return true;
        }
        return false;
    }

    public void setOnBackPressed(Runnable action) {
        onBackPressed = action;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        synchronized (activityResultListeners) {
            final Iterator<OnActivityResultListener> it = activityResultListeners.iterator();
            OnActivityResultListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        synchronized (requestPermissionsListeners) {
            final Iterator<OnRequestPermissionsListener> it = requestPermissionsListeners.iterator();
            OnRequestPermissionsListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    public void onNewIntent(Intent intent) {
        synchronized (newIntentListeners) {
            final Iterator<OnNewIntentListener> it = newIntentListeners.iterator();
            OnNewIntentListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onNewIntent(intent);
            }
        }
    }

    public void onResume() {
        synchronized (resumePauseListeners) {
            final Iterator<OnResumePauseListener> it = resumePauseListeners.iterator();
            OnResumePauseListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onResume();
            }
        }
    }

    public void onPause() {
        synchronized (resumePauseListeners) {
            final Iterator<OnResumePauseListener> it = resumePauseListeners.iterator();
            OnResumePauseListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onPause();
            }
        }
    }

    public void onStop() {
        synchronized (startStopListeners) {
            final Iterator<OnStartStopListener> it = startStopListeners.iterator();
            OnStartStopListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onStop();
            }
        }
    }

    public void onStart() {
        synchronized (startStopListeners) {
            final Iterator<OnStartStopListener> it = startStopListeners.iterator();
            OnStartStopListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onStart();
            }
        }
    }

    public void onSaveInstanceState(Bundle state) {
        synchronized (saveRestoreStateListeners) {
            final Iterator<OnSaveRestoreStateListener> it = saveRestoreStateListeners.iterator();
            OnSaveRestoreStateListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onSaveInstanceState(state);
            }
        }
    }

    public void onRestoreInstanceState(Bundle state) {
        synchronized (saveRestoreStateListeners) {
            final Iterator<OnSaveRestoreStateListener> it = saveRestoreStateListeners.iterator();
            OnSaveRestoreStateListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onRestoreInstanceState(state);
            }
        }
    }

    public void onLowMemory() {
        synchronized (lowMemoryListeners) {
            final Iterator<OnLowMemoryListener> it = lowMemoryListeners.iterator();
            OnLowMemoryListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onLowMemory();
            }
        }
    }

    public void onDestroy() {
        synchronized (createDestroyListeners) {
            final Iterator<OnCreateDestroyListener> it = createDestroyListeners.iterator();
            OnCreateDestroyListener listener;
            while (it.hasNext()) {
                listener = it.next();
                listener.onDestroy();
            }
        }
        createDestroyListeners.clear();
        lowMemoryListeners.clear();
        saveRestoreStateListeners.clear();
        activityResultListeners.clear();
        startStopListeners.clear();
        resumePauseListeners.clear();
        newIntentListeners.clear();
        context = null;
    }

    public void setServiceListener(AalServiceListener listener) {
        sListener = listener;
    }

    public void registerOnActivityResultListener(OnActivityResultListener listener) {
        synchronized (activityResultListeners) {
            activityResultListeners.add(listener);
        }
    }

    public void registerOnRequestPermissionsListener(OnRequestPermissionsListener listener) {
        synchronized (requestPermissionsListeners) {
            requestPermissionsListeners.add(listener);
        }
    }

    public void registerOnCreateDestroyListener(OnCreateDestroyListener listener) {
        synchronized (createDestroyListeners) {
            createDestroyListeners.add(listener);
        }
    }

    public void registerOnResumePauseListener(OnResumePauseListener listener) {
        synchronized (resumePauseListeners) {
            resumePauseListeners.add(listener);
        }
    }

    public void registerOnStartStopListener(OnStartStopListener listener) {
        synchronized (startStopListeners) {
            startStopListeners.add(listener);
        }
    }

    public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
        synchronized (lowMemoryListeners) {
            lowMemoryListeners.add(listener);
        }
    }

    public void registerOnNewIntentListener(OnNewIntentListener listener) {
        synchronized (newIntentListeners) {
            newIntentListeners.add(listener);
        }
    }

    public void registerOnSaveRestoreListener(OnSaveRestoreStateListener listener) {
        synchronized (saveRestoreStateListeners) {
            saveRestoreStateListeners.add(listener);
        }
    }

    public void unregisterOnActivityResultListener(OnActivityResultListener listener) {
        synchronized (activityResultListeners) {
            activityResultListeners.remove(listener);
        }
    }

    public void unregisterOnRequestPermissionsListener(OnRequestPermissionsListener listener) {
        synchronized (requestPermissionsListeners) {
            requestPermissionsListeners.remove(listener);
        }
    }

    public void unregisterOnCreateDestroyListener(OnCreateDestroyListener listener) {
        synchronized (createDestroyListeners) {
            createDestroyListeners.remove(listener);
        }
    }

    public void unregisterOnResumePauseListener(OnResumePauseListener listener) {
        synchronized (resumePauseListeners) {
            resumePauseListeners.remove(listener);
        }
    }

    public void unregisterOnStartStopListener(OnStartStopListener listener) {
        synchronized (startStopListeners) {
            startStopListeners.remove(listener);
        }
    }

    public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
        synchronized (lowMemoryListeners) {
            lowMemoryListeners.remove(listener);
        }
    }

    public void unregisterOnNewIntentListener(OnNewIntentListener listener) {
        synchronized (newIntentListeners) {
            newIntentListeners.remove(listener);
        }
    }

    public void unregisterSaveRestoreListener(OnSaveRestoreStateListener listener) {
        synchronized (saveRestoreStateListeners) {
            saveRestoreStateListeners.remove(listener);
        }
    }

    /**
     * When this is set to true, this will shut the service down after
     * unbinding from it.
     *
     * @param stop
     */
    public void stopOnUnbind(boolean stop) {
        stopOnUnbind = stop;
    }

    /**
     * Returns a Handler for posting to the UI thread.
     *
     * @return
     */
    public Handler getHandler() {
        return uiHandler;
    }

    /**
     * Use this method to bind to an AalService. When the service has been successfully bound to
     * this Activity, the onServiceBound() method will be called. Make sure to override that method
     * so you know when the service actually is bound.
     *
     * @param serviceClass
     */
    public void bindToService(Class<?> serviceClass) {
        boundServiceClass = serviceClass;
        context.bindService(new Intent(context, boundServiceClass), serviceListener, Context.BIND_AUTO_CREATE);
    }

    public Activity getContext() {
        return context;
    }

    /**
     * Convenience method for retrieving the {@link AalService} object this
     * AalActivity has been bound to. If it's not bound to any {@link AalService}, then
     * null is returned. This automatically casts your class (as along as it extends
     * {@link AalService}).
     * @param <T>
     * @return
     */
    public <T extends AalService> T getService() {
        if (boundService != null) {
            return (T) boundService;
        } else {
            return null;
        }
    }

    private void waitForScreenInit() {
        final ViewGroup outerLayout = (ViewGroup) ((ViewGroup) context.findViewById(android.R.id.content)).getChildAt(0);
        getHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (outerLayout != null) {
                    if (outerLayout.getWidth() != 0 && outerLayout.getHeight() != 0) {
                        activity.onScreenInitialized();
                    } else {
                        getHandler().postDelayed(this, 25);
                    }
                }
            }
        }, 25);
    }

    private class ServiceListener implements ServiceConnection {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            boundService = ((ServiceManager.AalBinder) iBinder).getService();
            if (boundService != null && sListener != null) {
                sListener.onServiceBound(boundService);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            boundService = null;
            if (sListener != null) {
                sListener.onServiceDisconnected(componentName);
            }
            if (stopOnUnbind) {
                if (context != null) {
                    context.stopService(new Intent(context, boundServiceClass));
                }
            }
        }
    }

}