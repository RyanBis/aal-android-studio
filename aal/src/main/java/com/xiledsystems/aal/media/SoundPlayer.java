package com.xiledsystems.aal.media;


import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.LifeCycle;
import com.xiledsystems.aal.annotations.LifeCycleMethod;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.internal.ComponentRunner;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import com.xiledsystems.aal.util.LollipopUtil;
import com.xiledsystems.aal.util.TextUtil;
import com.xiledsystems.aal.util.ThreadManager;
import com.xiledsystems.aal.util.VersionUtil;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class for playing shorter sounds. This wraps the {@link SoundPool} class. The default
 * is 10 max streams, and can be changed using {@link #setMaxStreams(int)}. Put all of
 * your sounds in the res/raw directory, and use the raw resourceId for loading/playing/
 * stopping sounds.
 *
 * Note that this is ONLY for playing SHORT sounds! This class is only good for use of sounds
 * under about 5 seconds (you can get longer sounds to play with lower bitrate files, but that
 * will sacrifice the quality).
 */
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@State(DevelopmentState.UNTESTED)
@LifeCycle(value = {LifeCycleMethod.ON_RESUME, LifeCycleMethod.ON_STOP, LifeCycleMethod.ON_DESTROY})
public class SoundPlayer extends NonDisplayComponent {

    private static final String TAG = "SoundPlayer";
    private static final int MAX_STREAMS = 10;
    private static final int LOOP_MODE_NO_LOOP = 0;
    private static final float VOLUME_FULL = 1.0f;
    private static final float PLAYBACK_RATE_NORMAL = 1.0f;

    public enum SoundErrors {
        UNABLE_TO_PLAY_SOUND,
        SOUND_NOT_LOADED
    }

    private SoundPool soundPool;
    private ConcurrentHashMap<Integer, Integer> soundMap;
    private Set<Integer> resIds;
    private Sound playId;
    private Set<Sound> streamIds;
    private AtomicInteger soundId = new AtomicInteger(0);
    private AtomicInteger streamId = new AtomicInteger(0);
    private AtomicInteger maxStreams = new AtomicInteger(MAX_STREAMS);
    private AtomicBoolean initialized = new AtomicBoolean(false);
    private boolean autoPause = true;
    private SoundCallbacks soundCallbacks;
    private InitializeRunner initializer = new InitializeRunner();
    private Set<ComponentRunner> runners;
    private ThreadManager tMan;
    private int usage = 14;
    private int contentType = 0;


    public SoundPlayer(Context context) {
        super(context);
        soundMap = new ConcurrentHashMap<>();
        resIds = Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>());
        streamIds = Collections.newSetFromMap(new ConcurrentHashMap<Sound, Boolean>());
        runners = Collections.newSetFromMap(new ConcurrentHashMap<ComponentRunner, Boolean>());
    }

    /**
     * Sets the maximum streams to be able to be played at once. Note that if you
     * call this after calling {@link #initialize()}}, the backing SoundPool will be
     * re-instantiated.
     * @param maxStreams
     */
    public void setMaxStreams(int maxStreams) {
        if (maxStreams != this.maxStreams.get()) {
            this.maxStreams.set(maxStreams);
            if (initialized.get()) {
                soundPool.release();
                soundPool = loadSoundPool(this.maxStreams.get(), usage, contentType);
                getLogger().logD("Reloaded backing soundpool to adjust maximum streams.");
            } else {
                getLogger().logD("Max streams set. Will take effect upon initialization.");
            }
        }
    }

    /**
     *
     * @return the current maximum amount of streams (sounds that can play at the same time)
     */
    public int getMaxStreams() {
        return maxStreams.get();
    }

    /**
     * Set {@link SoundCallbacks} listener for this {@link SoundPlayer} object.
     *
     * @param callback
     */
    public void setSoundCallbacks(SoundCallbacks callback) {
        soundCallbacks = callback;
    }

    /**
     * <b>This is only used on devices running Lollipop, or higher.</b>
     * <br></br>
     * Use this to set the USAGE for the backing {@link SoundPool}. This <b>must</b> be
     * called before the {@link #initialize()} method is called.<br></br>
     * Default: <code>AudioAttributes.USAGE_GAME</code>
     * @param usage
     */
    public void setUsage(int usage) {
        this.usage = usage;
    }

    /**
     * <b>This is only used on devices running Lollipop, or higher.</b>
     * <br></br>
     * Use this to set the CONTENT_TYPE for the backing {@link SoundPool}. This <b>must</b> be
     * called before the {@link #initialize()} method is called.<br></br>
     * Default: <code>AudioAttributes.CONTENT_TYPE_UNKNOWN</code>
     * @param contentType
     */
    public void setContentType(int contentType) {
        this.contentType = contentType;
    }

    /**
     * Add a sound to the soundPool list. Sounds are loaded when {@link #initialize()} is called.
     * If this method is called after, the sound is loaded in a background thread just like initialize().
     *
     * To check if a sound is ready to play, use {@link #isLoaded(int)}.
     * @param resId
     */
    public void addSound(final int resId) {
        if (!initialized.get()) {
            resIds.add(resId);
            getLogger().logD(TextUtil.string("Sound with resource Id '", resId, "' added to load queue and will be loaded on initialization."));
        } else {
            LoadSoundRunner run = new LoadSoundRunner(resId);
            runners.add(run);
            tMan.execute(run);
        }
    }

    /**
     * Initializes all sound manager objects, and loads added sounds into
     * memory in a background thread. The {@link SoundCallbacks#onInitialized(boolean)} method
     * is called in the UI Thread. But you must make sure you call {@link #setSoundCallbacks(SoundCallbacks)} before
     * calling this method.
     */
    public void initialize() {
        if (!initialized.get() && resIds.size() > 0) {
            getLogger().logD("Starting initialization...");
            tMan = ThreadManager.getInstance(true);
            tMan.execute(initializer);
        }
    }

    /**
     * Play a preloaded sound. This will send the @{link SoundErrors#SOUND_NOT_LOADED} enum to onError in
     * the {@link SoundCallbacks} callback, if the sound is not
     * loaded. You must first call {@link #addSound(int)} before trying to play it.
     * @param resId
     */
    public void play(int resId) {
        if (soundId.get() != 0 && soundMap.containsKey(resId)) {
            int sId = soundPool.play(soundMap.get(resId), VOLUME_FULL, VOLUME_FULL, 0, LOOP_MODE_NO_LOOP, PLAYBACK_RATE_NORMAL);
            if (sId == 0) {
                if (soundCallbacks != null) {
                    soundCallbacks.onError(SoundErrors.UNABLE_TO_PLAY_SOUND);
                }
                return;
            }
            getLogger().logD(TextUtil.string("Playing sound with resource Id '", resId, "'..."));
            playId = new Sound(resId, sId);
            streamIds.add(playId);
            streamId.set(sId);
        } else {
            if (soundCallbacks != null) {
                soundCallbacks.onError(SoundErrors.SOUND_NOT_LOADED);
            }
        }
    }

    /**
     * Stops the sound with the resId provided.
     * @param resId
     */
    public void stop(int resId) {
        if (streamId.get() != 0) {
            Iterator<Sound> it = streamIds.iterator();
            while (it.hasNext()) {
                playId = it.next();
                if (playId.resId == resId) {
                    soundPool.stop(playId.streamId);
                    getLogger().logD(TextUtil.string("Sound with resource Id '", resId, "' stopped."));
                    it.remove();
                    return;
                }
            }
        }
    }

    /**
     * Stops ALL sounds from playing.
     */
    public void stopAll() {
        if (streamIds.size() > 0) {
            Iterator<Sound> it = streamIds.iterator();
            while (it.hasNext()) {
                playId = it.next();
                soundPool.stop(playId.streamId);
                it.remove();
            }
            getLogger().logD("All sounds stopped.");
        }
    }

    public void unload(int resId) {
        if (soundMap.containsKey(resId)) {
            soundPool.unload(soundMap.get(resId));
            soundMap.remove(resId);
            getLogger().logD(TextUtil.string("Sound with resource Id '", resId, "' unloaded."));
        }
    }

    public void unloadAll() {
        if (soundMap.size() > 0) {
            List<Integer> keys = Collections.list(soundMap.keys());
            for (Integer res : keys) {
                soundPool.unload(soundMap.get(res));
            }
            soundMap.clear();
            getLogger().logD("All sounds unloaded.");
        }
    }

    /**
     * @param resId
     * @return <code>true</code> if the sound has been loaded, and is ready to play
     */
    public boolean isLoaded(int resId) {
        return soundMap.get(resId) != null;
    }

    public void dispose() {
        clearRunners(runners, tMan);
        if (streamId.get() != 0) {
            final List<Integer> keys = Collections.list(soundMap.keys());
            for (Integer resId : keys) {
                soundPool.stop(soundMap.get(resId));
                soundPool.unload(soundMap.get(resId));
            }
            soundMap.clear();
            streamIds.clear();
        }
        soundPool.release();
        soundPool = null;
        getLogger().logD("All objects disposed.");
    }

    private static SoundPool loadSoundPool(int maxStreams, int usage, int contentType) {
        if (VersionUtil.isLollipop()) {
            return LollipopUtil.newPool(maxStreams, usage, contentType);
        } else {
            return new SoundPool(maxStreams, AudioManager.STREAM_MUSIC, 0);
        }
    }

    private static void clearRunners(Set<ComponentRunner> runners, ThreadManager tMan) {
        if (runners.size() > 0) {
            for (ComponentRunner run : runners) {
                tMan.remove(run);
            }
        }
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return new OnResumePauseListener() {
            @Override
            public void onResume() {
                if (autoPause && soundPool != null && streamId.get() != 0) {
                    soundPool.autoResume();
                }
            }

            @Override
            public void onPause() {
            }
        };
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return new OnStartStopListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onStop() {
                if (autoPause && soundPool != null && streamId.get() != 0) {
                    soundPool.autoPause();
                }
            }
        };
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new OnCreateDestroyListener() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
            }

            @Override
            public void onDestroy() {
                if (!inService() && soundPool != null) {
                    dispose();
                }
                outOfActivity();
            }
        };
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return new ServiceLifecycleListener() {
            @Override
            public void onDestroy() {
                if (soundPool != null) {
                    dispose();
                }
                outOfService();
            }
        };
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    private class InitializeRunner extends ComponentRunner {

        @Override
        public void onRun() {
            soundPool = loadSoundPool(maxStreams.get(), usage, contentType);
            for (Integer id : resIds) {
                int sId = soundPool.load(getContext(), id, 1);
                soundMap.put(id, sId);
                soundId.set(sId);
            }
            initialized.set(true);
            getLogger().logD("Initialization complete.");
            ThreadManager.getHandler().post(getUIAction());
        }

        @Override
        public Runnable getUIAction() {
            return new Runnable() {
                @Override
                public void run() {
                    if (soundCallbacks != null) {
                        soundCallbacks.onInitialized(initialized.get());
                    }
                }
            };
        }

        @Override
        public void finished() {
            runners.remove(this);
        }
    }

    private class LoadSoundRunner extends ComponentRunner {

        private final int resId;

        public LoadSoundRunner(final int resId) {
            this.resId = resId;
        }

        @Override
        public void onRun() {
            int sId = soundPool.load(getContext(), resId, 1);
            soundMap.put(resId, sId);
            soundId.set(sId);
            getLogger().logD(TextUtil.string("Sound with resource Id '", resId, "' loaded."));
            ThreadManager.getHandler().post(getUIAction());
        }

        @Override
        public Runnable getUIAction() {
            if (soundCallbacks != null) {
                return new Runnable() {
                    @Override
                    public void run() {
                        if (soundCallbacks != null) {
                            soundCallbacks.onSoundLoaded(resId);
                        }
                    }
                };
            } else {
                return null;
            }
        }

        @Override
        public void finished() {
            runners.remove(this);
        }
    }

    private static class Sound {
        private int resId;
        private int streamId;

        public Sound(int resId, int streamId) {
            this.resId = resId;
            this.streamId = streamId;
        }
    }

}