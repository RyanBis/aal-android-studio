package com.xiledsystems.aal.media;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.LifeCycle;
import com.xiledsystems.aal.annotations.LifeCycleMethod;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import com.xiledsystems.aal.util.TextUtil;
import java.io.IOException;

/**
 * Class for playing songs, or long sounds. It's meant to only play one at a time.
 * If not using in an {@link com.xiledsystems.aal.context.AalActivity}, or
 * {@link com.xiledsystems.aal.context.AalService}, then remember to call
 * {@link #dispose()} when you are done with the player, usually in onDestroy().
 *
 */
@State(DevelopmentState.UNTESTED)
@LifeCycle(LifeCycleMethod.ON_DESTROY)
public class SongPlayer extends NonDisplayComponent {

    private final static String TAG = "SongPlayer";

    private MediaPlayer player;

    private enum PlayerState {
        UNPREPARED,
        PREPARED,
        ACTIVE
    }

    public enum SongErrors {
        NO_RESOURCE_SPECIFIED,
        UNABLE_TO_PLAY_SONG,
        FAILED_TO_LOAD_PLAYER,
        UNABLE_TO_LOAD_SONG,
        SONG_NOT_LOADED
    }

    private PlayerState playerState = PlayerState.UNPREPARED;
    private int soundRes = -1;
    private SongCallbacks listener;
    private boolean loop = false;


    public SongPlayer(Context context) {
        super(context);
    }

    /**
     * Set the resource of the song (or long sound) you'd like to play. This must be
     * a file in the res/raw directory (ex R.raw.mysong1).
     * Note that you must call {@link #load()} before playing.
     * @param resId
     */
    public void setSoundResource(int resId) {
        soundRes = resId;
    }

    /**
     *
     * @return the resource ID of the song this player is set to use.
     */
    public int getSoundResourceId() {
        return soundRes;
    }

    /**
     * Set whether this player will loop the song resource it's set to play.
     * @param loop
     */
    public void setLooping(boolean loop) {
        this.loop = loop;
        if (playerState != PlayerState.UNPREPARED) {
            player.setLooping(loop);
        }
    }

    /**
     *
     * @return <code>true</code> if this player is set to loop
     */
    public boolean isLooping() {
        return loop;
    }

    /**
     * This needs to be called before calling {@link #play()}. This loads the
     * actual resource into the backing {@link MediaPlayer} object.
     */
    public void load() {
        if (soundRes != -1) {
            if (playerState != PlayerState.UNPREPARED) {
                getLogger().logD("Backing MediaPlayer previously prepared, stopping it to recreate.");
                player.stop();
            }
            playerState = PlayerState.UNPREPARED;
            if (player != null) {
                player.release();
                player = null;
            }
            player = MediaPlayer.create(getContext(), soundRes);
            if (player == null) {
                getLogger().logE("Failed to load player.");
                if (listener != null) {
                    listener.onError(SongErrors.FAILED_TO_LOAD_PLAYER);
                }
            } else {
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.setLooping(loop);
                playerState = PlayerState.PREPARED;
                getLogger().logD("Backing MediaPlayer loaded, and prepared with song resource.");
            }
        } else {
            getLogger().logE("No resource specified! Player NOT loaded/prepared.");
            if (listener != null) {
                listener.onError(SongErrors.NO_RESOURCE_SPECIFIED);
            }
        }
    }

    /**
     * Go to the position provided in the song. This methods expects seconds.
     *
     * @param position
     */
    public void seekTo(int position) {
        if (playerState != PlayerState.UNPREPARED) {
            position = position * 1000;
            player.seekTo(position);
        }
    }

    /**
     * For finer control, this method expects milliseconds. Will put the player at
     * the specified position in the song.
     * @param position
     */
    public void seekToMs(int position) {
        if (playerState != PlayerState.UNPREPARED) {
            player.seekTo(position);
        }
    }

    /**
     *
     * @return the current position in the song, in seconds
     */
    public int getPosition() {
        if (player != null) {
            return player.getCurrentPosition() / 1000;
        } else {
            return -1;
        }
    }

    /**
     *
     * @return the position in the song, in milliseconds
     */
    public int getPositionMs() {
        if (player != null) {
            return player.getCurrentPosition();
        } else {
            return -1;
        }
    }

    public boolean canPlay() {
        return soundRes != -1 && playerState != PlayerState.UNPREPARED;
    }

    /**
     * Play the song this player is set to play. Note that you must call
     * {@link #load()} before trying to play a song. You can use the
     * {@link #canPlay()} method to check if the player is ready to play
     * the song.
     */
    public void play() {
        if (playerState != PlayerState.UNPREPARED) {
            getLogger().logD(TextUtil.string("Playing song with resource id '", soundRes, "'."));
            player.start();
            playerState = PlayerState.ACTIVE;
        } else {
            getLogger().logD("Song has not been loaded! Please call load(), then call play().");
            if (listener != null) {
                listener.onError(SongErrors.SONG_NOT_LOADED);
            }
        }
    }

    /**
     * Pause the current song. Does nothing if the player is not actually
     * playing anything.
     */
    public void pause() {
        if (playerState == PlayerState.ACTIVE) {
            player.pause();
            getLogger().logD("Player has paused.");
        }
    }

    /**
     * Stops the current song. This will reload the song into memory, thus when
     * calling play again, it will start at the beginning.
     */
    public void stop() {
        if (playerState != PlayerState.UNPREPARED) {
            player.stop();
            prepare();
            getLogger().logD("Player has been stopped, and prepared and is ready to play again.");
        }
    }

    public void dispose() {
        if (player != null) {
            player.stop();
            player.release();
            playerState = PlayerState.UNPREPARED;
            player = null;
            getLogger().logD("Disposed of resource objects.");
        }
    }

    private void prepare() {
        try {
            player.prepare();
            playerState = PlayerState.PREPARED;
            getLogger().logD("Player has successfully prepared.");
        } catch (IOException e) {
            e.printStackTrace();
            player.release();
            player = null;
            playerState = PlayerState.UNPREPARED;
            if (listener != null) {
                listener.onError(SongErrors.UNABLE_TO_LOAD_SONG);
            }
            getLogger().logE(TextUtil.string("Unable to load song! Exception message: ", e.getMessage()));
        }
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new OnCreateDestroyListener() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
            }

            @Override
            public void onDestroy() {
                dispose();
            }
        };
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return new ServiceLifecycleListener() {
            @Override
            public void onDestroy() {
                dispose();
            }
        };
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

}