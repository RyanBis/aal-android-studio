package com.xiledsystems.aal.media;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.media.SoundPlayer.SoundErrors;


/**
 * Interface for listening for {@link SoundPlayer} events.
 */
@State(DevelopmentState.UNTESTED)
public interface SoundCallbacks {
    /**
     * This method is called when the {@link SoundPlayer} is initialized. Any sounds added
     * with {@link SoundPlayer#addSound(int)} will be loaded, and the boolean dictates
     * success/failure of loading sounds into the backing {@link android.media.SoundPool}
     * @param success
     */
    void onInitialized(boolean success);

    /**
     * This is called when an error has occurred in {@link SoundPlayer}
     * @param error
     */
    void onError(SoundErrors error);

    /**
     * This method is called when {@link SoundPlayer#addSound(int)} is called <i>after</i>
     * the {@link SoundPlayer} has been initialized, and the sounds has been loaded into
     * the backing {@link android.media.SoundPool}
     * @param resId
     */
    void onSoundLoaded(int resId);
}
