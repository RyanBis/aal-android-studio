package com.xiledsystems.aal.media;

import com.xiledsystems.aal.media.SongPlayer.SongErrors;

public interface SongCallbacks {

    void onError(SongErrors error);

}
