package com.xiledsystems.aal.internal;


import com.xiledsystems.aal.util.AalLogger;

public final class AalComponentLogger extends AalLogger {

    private final Component component;


    public AalComponentLogger(Component component) {
        this.component = component;
    }

    @Override
    public final void logE(String message) {
        logEMethod(component, message);
    }

    @Override
    public final void logW(String message) {
        logWMethod(component, message);
    }

    @Override
    public final void logD(String message) {
        logDMethod(component, message);
    }

    @Override
    public final void logI(String message) {
        logIMethod(component, message);
    }
}
