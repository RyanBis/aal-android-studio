package com.xiledsystems.aal.internal;


import android.content.Context;
import com.xiledsystems.aal.context.IAalService;


public abstract class NonDisplayComponent extends Component {

    public NonDisplayComponent(Context context) {
        super(context);
        if (context instanceof IAalService) {
            addFlag(FLAG_IN_SERVICE);
            setIsIAal(true);
            ((IAalService) context).getManager().registerLifecycleListener(getServiceLifecycleListener());
        }
    }

    public boolean inService() {
        return (getFlags() & FLAG_IN_SERVICE) == FLAG_IN_SERVICE;
    }

    protected void outOfService() {
        if (inService()) {
            setFlags((getFlags() & ~FLAG_IN_SERVICE));
        }
    }
}
