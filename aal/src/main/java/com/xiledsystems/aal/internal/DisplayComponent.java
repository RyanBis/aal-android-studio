package com.xiledsystems.aal.internal;


import android.content.Context;

public abstract class DisplayComponent extends Component {

    public DisplayComponent(Context context) {
        super(context);
    }
}
