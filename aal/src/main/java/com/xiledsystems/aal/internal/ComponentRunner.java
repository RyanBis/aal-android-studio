package com.xiledsystems.aal.internal;


import com.xiledsystems.aal.util.ThreadManager;

public abstract class ComponentRunner implements Runnable {

    @Override
    public void run() {
        onRun();
        if (getUIAction() != null) {
            ThreadManager.getHandler().post(getUIAction());
        }
        finished();
    }

    public abstract void onRun();
    public abstract Runnable getUIAction();
    public abstract void finished();
}
