package com.xiledsystems.aal.internal;


import android.content.Context;
import com.xiledsystems.aal.context.ActivityManager;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.IAalActivity;
import com.xiledsystems.aal.context.IAalService;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.util.IAalLogger;


public abstract class Component implements IAalLogger {


    public final static int FLAG_IN_ACTIVITY = 0x01;
    public final static int FLAG_IN_SERVICE = 0x02;
    public final static int FLAG_DISPOSED = 0x04;
    public final static int FLAG_LOGGING_ENABLED = 0x8000000;

    private final Context context;
    private int flags;
    private boolean isIAal = false;
    private AalComponentLogger mLogger;


    public Component(Context context) {
        this.context = context;
        if (context != null) {
            setupListeners();
        }
        if (isDisposed()) {
            setFlags((getFlags()) & ~FLAG_DISPOSED);
        }
        mLogger = new AalComponentLogger(this);
    }

    public Context getContext() {
        return context;
    }

    public boolean isDisposed() {
        return (getFlags() & FLAG_DISPOSED) == FLAG_DISPOSED;
    }

    public boolean isIAal() {
        return isIAal;
    }

    protected void setIsIAal(boolean iaIAal) {
        this.isIAal = isIAal;
    }

    protected void setDisposed() {
        setFlags((getFlags() | FLAG_DISPOSED));
    }

    public boolean inActivity() {
        return (getFlags() & FLAG_IN_ACTIVITY) == FLAG_IN_ACTIVITY;
    }

    @Override
    public boolean isLoggingEnabled() {
        return (getFlags() & FLAG_LOGGING_ENABLED) == FLAG_LOGGING_ENABLED;
    }

    /**
     * Enable logging in this component. This simply allows a component
     * to write log entries.
     *
     * @param enable
     */
    public void setLoggingEnabled(boolean enable) {
        if (enable) {
            setFlags(getFlags() | FLAG_LOGGING_ENABLED);
        } else {
            setFlags(getFlags() & ~FLAG_LOGGING_ENABLED);
        }
    }

    protected void outOfActivity() {
        if (inActivity()) {
            setFlags((getFlags() & ~FLAG_IN_ACTIVITY));
        }
    }

    protected int getFlags() {
        return flags;
    }

    protected void addFlag(int flag) {
        setFlags(getFlags() | flag);
    }

    protected void setFlags(int flags) {
        this.flags = flags;
    }

    protected AalComponentLogger getLogger() {
        return mLogger;
    }


    protected abstract OnActivityResultListener getActivityResultListener();
    protected abstract OnCreateDestroyListener getCreateDestroyListener();
    protected abstract OnStartStopListener getStartStopListener();
    protected abstract OnResumePauseListener getPauseResumeListener();
    protected abstract OnLowMemoryListener getLowMemoryListener();
    protected abstract OnNewIntentListener getNewIntentListener();
    protected abstract CoreServiceLifecycleListener getServiceLifecycleListener();
    protected abstract OnRequestPermissionsListener getPermissionsListener();


    private void setupListeners() {
        if (context instanceof IAalActivity) {
            addFlag(FLAG_IN_ACTIVITY);
            isIAal = true;
            final ActivityManager mgr = ((IAalActivity) context).getManager();
            final OnActivityResultListener al = getActivityResultListener();
            if (al != null) {
                mgr.registerOnActivityResultListener(al);
            }
            final OnCreateDestroyListener cl = getCreateDestroyListener();
            if (cl != null) {
                mgr.registerOnCreateDestroyListener(cl);
            }
            final OnResumePauseListener rl = getPauseResumeListener();
            if (rl != null) {
                mgr.registerOnResumePauseListener(rl);
            }
            final OnStartStopListener sl = getStartStopListener();
            if (sl != null) {
                mgr.registerOnStartStopListener(sl);
            }
            final OnLowMemoryListener ll = getLowMemoryListener();
            if (ll != null) {
                mgr.registerOnLowMemoryListener(ll);
            }
            final OnNewIntentListener nl = getNewIntentListener();
            if (nl != null) {
                mgr.registerOnNewIntentListener(nl);
            }
            final OnRequestPermissionsListener rpl = getPermissionsListener();
            if (rpl != null) {
                mgr.registerOnRequestPermissionsListener(rpl);
            }
        } else if (context instanceof IAalService) {
            addFlag(FLAG_IN_SERVICE);
            isIAal = true;
            ((IAalService) context).getManager().registerLifecycleListener(getServiceLifecycleListener());
        }
    }

}
