package com.xiledsystems.aal.billing;

public interface OnBillingEvent {
	
	public void onBillingEvent(String event, Object... args);

}
