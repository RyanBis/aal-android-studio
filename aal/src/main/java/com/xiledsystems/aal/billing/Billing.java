package com.xiledsystems.aal.billing;

import android.content.Intent;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * In addition to the manifest changes listed for each Billing type (AmazonBilling, NookBilling, etc), you
 * should make sure to run a few methods in your activity.
 *
 * in onResume of you Activity, run this class's onResume method. Likewise in your Activity's onActivityResult,
 * run this class's onActivityResult method.
 *
 * Also, in your Activity's onDestroy, you should call the close() method.
 *
 * Setting the billinglistener will allow you to do things on billing events (error, purchase success, inventory return).
 *
 */
@State(DevelopmentState.ALPHA)
public class Billing {
	
	private BillingInterface billIntf;


	public Billing(BillingInterface billIntf) {
		this.billIntf = billIntf;	
	}
	
	public void setupBilling() {
		billIntf.setupBilling();
	}
	
	public void purchaseItem(String sku, String type) {
		billIntf.purchaseItem(sku, type);
	}
	
	public void setBillingListener(OnBillingEvent listener) {
		billIntf.setBillingListener(listener);
	}
	
	public void onResume() {
		billIntf.onResume();
	}
	
	public void close() {
		billIntf.close();
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		billIntf.onActivityResult(requestCode, resultCode, data);
	}
	
}