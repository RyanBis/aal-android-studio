package com.xiledsystems.aal.billing;

import android.content.Intent;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;



public interface BillingInterface {

	public static final String BILLING_ERROR = "ErrorInBilling";
	public static final String INVENTORY_RETURN = "BillingInventoryReturned";
	public static final String PURCHASE_SUCCESS = "BillingPurchaseSuccessful";
	public static final String PURCHASE_CANCEL = "BillingPurchaseCanceled";
	public static final String PURCHASE_FAIL = "BillingPurchaseFailed";
	
	
	public void setupBilling();
	
	public void purchaseItem(String sku, String type);
		
	public void setBillingListener(OnBillingEvent listener);
	
	public void onResume();
	
	public void close();
	
	public void onActivityResult(int requestCode, int resultCode, Intent data);
	
}
