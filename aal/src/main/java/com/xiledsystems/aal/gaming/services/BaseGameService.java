package com.xiledsystems.aal.gaming.services;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.LifeCycle;
import com.xiledsystems.aal.annotations.LifeCycleMethod;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import com.xiledsystems.aal.util.Prefs;

import java.util.HashMap;
import java.util.List;


@State(value = DevelopmentState.PRODUCTION, note = "Don't use this class directly. Instead, use the GameService class.")
@LifeCycle(value = {LifeCycleMethod.ON_ACTIVITY_RESULT,
                    LifeCycleMethod.ON_RESUME,
                    LifeCycleMethod.ON_STOP,
                    LifeCycleMethod.ON_PAUSE,
                    LifeCycleMethod.ON_START,
                    LifeCycleMethod.ON_DESTROY})
public abstract class BaseGameService extends NonDisplayComponent {

    private final static String KEY_SIGNED_IN = "signed_in";

    public final static int ALL_TIME = 0;
    public final static int DAY = 1;
    public final static int WEEK = 2;


    protected GameServiceListener listener;
    private GameServiceListener userListener;
    private HashMap<String, Integer> incrementingAchievements;

    private Prefs prefs;


    public BaseGameService(Activity context, GameServiceListener serviceListener) {
        super(context);
        userListener = serviceListener;
        setGameListener(new GServiceListener());
        prefs = new Prefs(context);
        prefs.setFileName("game_services_info");
    }

    public abstract void setup();
    public abstract void onActivityResult(int requestCode, int resultCode, Intent data);
    public abstract void onStart();
    public abstract void onStop();
    public abstract void onResume();
    public abstract void onPause();
    public abstract void signIn();
    public abstract void signOut();
    public abstract boolean isAmazon();
    public abstract boolean isSignedIn();
    public abstract void showLeaderBoard(String leaderBoard);
    public abstract void showLeaderBoards();
    public abstract void showAchievements();
    public abstract void unlockAchievement(String achievementId);
    public abstract void incrementAchievement(String achievementId, int amount);
    public abstract void getPlayersAchievements();
    public abstract void sendScore(String leaderboardId, long score);
    public abstract void getPlayerScore(String leaderboardId, int timeType);

    /**
     * Internal method. This should never need to be called manually.
     * @param signedIn
     */
    protected void setShouldBeSignedIn(boolean signedIn) {
        prefs.storeBoolean(KEY_SIGNED_IN, true);
    }

    private boolean shouldBeSignedIn() {
        return prefs.getBoolean(KEY_SIGNED_IN, false);
    }

    public void setGameListener(GameServiceListener listener) {
        this.listener = listener;
    }

    public Activity getActivity() {
        return (Activity) getContext();
    }



    private HashMap<String, Integer> createIncAchievementMap(List<Achievement> list) {
        HashMap<String, Integer> map = new HashMap<>();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (list.get(i).isIncremental()) {
                map.put(list.get(i).getId(), list.get(i).getProgress());
            }
        }
        return map;
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return null;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return new OnActivityResultListener() {
            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent data) {
                BaseGameService.this.onActivityResult(requestCode, resultCode, data);
            }
        };
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return new OnStartStopListener() {
            @Override
            public void onStart() {
                if (shouldBeSignedIn()) {
                    BaseGameService.this.onStart();
                }
            }

            @Override
            public void onStop() {
                BaseGameService.this.onStop();
            }
        };
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return new OnResumePauseListener() {
            @Override
            public void onResume() {
                if (shouldBeSignedIn()) {
                    BaseGameService.this.onResume();
                }
            }

            @Override
            public void onPause() {
                BaseGameService.this.onPause();
            }
        };
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return null;
    }

    private class LifeListener extends LifecycleListener {
        @Override
        public void onCreate(Bundle savedInstanceState) {
        }

        @Override
        public void onDestroy() {
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            BaseGameService.this.onActivityResult(requestCode, resultCode, data);
        }

        @Override
        public void onStart() {
            if (shouldBeSignedIn()) {
                BaseGameService.this.onStart();
            }
        }

        @Override
        public void onPause() {
            BaseGameService.this.onPause();
        }

        @Override
        public void onResume() {
            if (shouldBeSignedIn()) {
                BaseGameService.this.onResume();
            }
        }

        @Override
        public void onStop() {
            BaseGameService.this.onStop();
        }
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    private class GServiceListener implements GameServiceListener {
        @Override
        public void onSignInSuccess() {
            if (isAmazon()) {
                getPlayersAchievements();
            }
            if (userListener != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userListener.onSignInSuccess();
                    }
                });
            }
        }

        @Override
        public void onSignInFail() {
            if (userListener != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userListener.onSignInFail();
                    }
                });
            }
        }

        @Override
        public void onError(final String errorMsg) {
            if (userListener != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userListener.onError(errorMsg);
                    }
                });
            }
        }

        @Override
        public void onScoreReceived(final String leaderboard, final int rank, final long score) {
            if (userListener != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userListener.onScoreReceived(leaderboard, rank, score);
                    }
                });
            }
        }

        @Override
        public void onAchievementsReceived(final List<Achievement> achievements) {
            if (isAmazon()) {
                incrementingAchievements = createIncAchievementMap(achievements);
            }
            if (userListener != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        userListener.onAchievementsReceived(achievements);
                    }
                });
            }
        }
    }

}
