package com.xiledsystems.aal.gaming.services;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class GameDataUtil {


    private GameDataUtil() {}


    public static String toString(List<String> list) {
        StringBuilder b = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            b.append(list.get(i));
            if (i != size - 1) {
                b.append(",");
            }
        }
        return b.toString();
    }

    public static List<String> toList(String stringList) {
        return Arrays.asList(stringList.split(","));
    }

    public static String toString(Set<String> set) {
        StringBuilder b = new StringBuilder();
        for (String s : set) {
            b.append(s);
            b.append(",");
        }
        b.deleteCharAt(b.lastIndexOf(","));
        return b.toString();
    }

    public Set<String> toSet(String stringSet) {
        return new HashSet<>(Arrays.asList(stringSet.split(",")));
    }

}
