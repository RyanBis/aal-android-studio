package com.xiledsystems.aal.gaming.services;


public enum GameDataType {

    INTEGER(0),
    LONG(1),
    STRING(2),
    STRING_SET(3),
    STRING_LIST(4);

    private final int id;

    GameDataType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static GameDataType fromId(int id) {
        for (GameDataType type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return INTEGER;
    }
}