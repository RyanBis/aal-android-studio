package com.xiledsystems.aal.gaming.services;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.util.List;

@State(DevelopmentState.PRODUCTION)
public interface GameServiceListener {

    void onSignInSuccess();
    void onSignInFail();
    void onError(String errorMsg);
    void onScoreReceived(String leaderboard, int rank, long score);
    void onAchievementsReceived(List<Achievement> achievements);
}
