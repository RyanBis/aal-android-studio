package com.xiledsystems.aal.annotations;


import java.lang.annotation.Documented;

/**
 * Annotation to dictate which {@link android.app.Activity} lifecycle methods
 * a {@link com.xiledsystems.aal.internal.Component} needs access to. This is only
 * useful if you are NOT using the {@link com.xiledsystems.aal.internal.Component}
 * in an {@link com.xiledsystems.aal.context.AalActivity} or {@link com.xiledsystems.aal.context.AalService}.
 */
@Documented
public @interface LifeCycle {
    LifeCycleMethod[] value();
}
