package com.xiledsystems.aal.annotations;

/**
 * Defines an enumeration for annotation {@link State}, indicating the state of the class
 * as far as development is concerned.
 */
public enum DevelopmentState {
    /**
     * This class may have bugs, and has a high chance of fields/methods getting
     * refactored and changed drastically.
     */
    ALPHA,

    /**
     * This class is mostly working, except for some possible bugs, or slight
     * performance issues. There's a small chance of some refactoring.
     */
    BETA,

    /**
     * This class is mostly complete, but hasn't been tested thoroughly, or hasn't
     * been exposed to a large number of people.
     */
    UNTESTED,

    /**
     * This class has been used in production apps. Classes marked with this are
     * generally the most stable, and efficient of them all.
     */
    PRODUCTION
}
