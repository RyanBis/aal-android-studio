package com.xiledsystems.aal.annotations;

import java.lang.annotation.Documented;

/**
 * This annotation denotes the development status for the class it is
 * used in. {@see DevelopmentState} for possible states and details.
 */
@Documented
public @interface State {
    String note() default "";
    DevelopmentState value();
}
