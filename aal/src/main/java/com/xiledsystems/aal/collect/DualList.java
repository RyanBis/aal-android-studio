package com.xiledsystems.aal.collect;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;


@State(DevelopmentState.PRODUCTION)
public class DualList<Type1, Type2> implements Serializable {

    private ArrayList<Type1> list1;
    private ArrayList<Type2> list2;

    public DualList() {
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
    }

    public DualList(int size) {
        list1 = new ArrayList<>(size);
        list2 = new ArrayList<>(size);
    }

    public DualList(ArrayList<Type1> list1, ArrayList<Type2> list2) {
        this.list1 = list1;
        this.list2 = list2;
    }

    public void add(Type1 firstItem, Type2 secondItem) {
        list1.add(firstItem);
        list2.add(secondItem);
    }

    public void add(int index, Type1 firstItem, Type2 secondItem) {
        list1.add(index, firstItem);
        list2.add(index, secondItem);
    }

    public void remove(int index) {
        list1.remove(index);
        list2.remove(index);
    }

    public Type1 getFirstItem(int index) {
        return list1.get(index);
    }

    public Type2 getSecondItem(int index) {
        return list2.get(index);
    }

    public ArrayList<Type1> getFirstList() {
        return list1;
    }

    public ArrayList<Type2> getSecondList() {
        return list2;
    }

    public DualList<Type1, Type2> sublist(int start, int end) {
        DualList<Type1, Type2> list = new DualList<>();
        for (int i = start; i <= end; i++) {
            list.add(list1.get(i), list2.get(i));
        }
        return list;
    }

    public boolean firstListContains(Type1 item) {
        return list1.contains(item);
    }

    public boolean secondListContains(Type2 item) {
        return list2.contains(item);
    }

    public int size() {
        int one = list1.size();
        int two = list2.size();
        if (one != two) {
            return -1;
        }
        return one;
    }

    public int indexFirst(Type1 item) {
        return list1.indexOf(item);
    }

    public int indexSecond(Type2 item) {
        return list2.indexOf(item);
    }

    public int indexAll(Object item) {
        int idx = list1.indexOf(item);
        if (idx == -1) {
            idx = list2.indexOf(item);
        }
        return idx;
    }

    public void clear() {
        list1.clear();
        list2.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof DualList) {
            DualList listcheck = (DualList) o;
            if (listcheck.getFirstList().equals(list1) && listcheck.getSecondList().equals(list2)) {
                return true;
            }
        }
        return false;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        int size = size();
        out.writeInt(size);

        for (int i = 0; i < size; i++) {
            out.writeObject(list1.get(i));
            out.writeObject(list2.get(i));
        }

    }

    @SuppressWarnings("unchecked")
    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {

        int size = in.readInt();
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list1.add((Type1) in.readObject());
            list2.add((Type2) in.readObject());
        }

    }

}
