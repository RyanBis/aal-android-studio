package com.xiledsystems.aal.encrypt;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public abstract class Data {


    public enum Failure {
        NONE,
        NO_SUCH_ALGORITHM,
        NO_SUCH_PADDING,
        INVALID_KEY,
        INVALID_ALGORITHM_PARAMETER,
        BAD_PADDING,
        ILLEGAL_BLOCK_SIZE,
        NO_SUCH_PROVIDER,
        GENERAL_EXCEPTION,
        DATA_LENGTH,
        WRONG_ENCRYPTION_TYPE,
        BAD_INTEGRITY,
        NULL_DATA,
        BAD_WRAPPING,
        UNKNOWN
    }

    private Failure failure;

    public abstract byte[] getBytes();

    void setFailure(Failure fail) {
        failure = fail;
    }

    public boolean success() {
        return getBytes() != null && failure == Failure.NONE;
    }

    public Failure getFailure() {
        return failure;
    }

}
