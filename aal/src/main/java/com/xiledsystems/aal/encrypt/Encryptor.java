package com.xiledsystems.aal.encrypt;


import android.content.Context;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.AalLogger;
import com.xiledsystems.aal.util.TextUtil;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Random;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;


@State(DevelopmentState.UNTESTED)
public abstract class Encryptor {

    public final static String UTF_8 = "UTF-8";

    public static final String BOUNCY_CASTLE = "BC";
    public static final String SPONGY_CASTLE = "SC";
    public static final String OPEN_SSL = "AndroidOpenSSL";

    static AalLogger logger = new AalLogger();
    private static SecureRandom rand;
    private static String provider = BOUNCY_CASTLE;

    abstract Provider getProvider();
    public abstract Data decryptData(Key key, Data encryptedData);
    public abstract Data encryptData(Key key, byte[] dataToEncrypt);
    public abstract Key generateKey(String key, byte[] salt);
    public abstract Key generateKey();

    /**
     * @deprecated {@link #getKey(Context, String)} replaces this method. However, if you previously saved a {@link Key} using
     * {@link #saveKey(Context, String, Key)}, then you will need to call this method first, then save it using {@link #putKey(Context, String, Key)}.
     */
    @Deprecated
    public abstract Key loadKey(Context context, String nameSpace);

    /**
     * @deprecated Use {@link #putKey(Context, String, Key)} instead.
     */
    @Deprecated
    public abstract void saveKey(Context context, String nameSpace, Key key);

    /**
     * Get a previously saved key with {@link #putKey(Context, String, Key)}. An instance of {@link Key} is always
     * returned, however, you should check {@link Key#isNull()} to ensure that a valid {@link Key} was returned.
     *
     * @param context
     * @param nameSpace
     * @return
     */
    public abstract Key getKey(Context context, String nameSpace);

    /**
     * Save a {@link Key} to internal storage. You can retrieve the {@link Key} later by calling {@link #getKey(Context, String)}.
     *
     * @param context
     * @param nameSpace
     * @param key
     */
    public abstract void putKey(Context context, String nameSpace, Key key);


    public abstract Data getEncryptedObject(byte[] encryptedBytes);


    /**
     * Returns the encryption provider name being used.<br></br>
     * Default is {@link #BOUNCY_CASTLE}
     *
     */
    public static String getProviderName() {
        return provider;
    }

    /**
     * Set the provider name of the encryption provider to use. For best availability, it's best
     * to keep the default of {@link #BOUNCY_CASTLE}, as not all phones will have {@link #OPEN_SSL}, or
     * any newer provider. If you are targeting newer APIs, then feel free to change this. This setting
     * affects all encryption in this library.
     */
    public static void setProviderName(String providerName) {
        provider = providerName;
    }

    /**
     * Returns a random byte array generated from {@link SecureRandom}, using
     * SHA1PRNG.
     * @param length - the desired length of the random byte array
     * @return
     */
    public static byte[] randomBytes(int length) {
        PRNGFixes.apply();
        byte[] bytes = new byte[length];
        if (rand == null) {
            try {
                rand = SecureRandom.getInstance("SHA1PRNG");
                rand.nextBytes(bytes);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                final Random r = new Random();
                r.nextBytes(bytes);
            }
        } else {
            rand.nextBytes(bytes);
        }
        return bytes;
    }

    protected static Cipher getCipher(String cipherType, String provider, Provider explicitProvider) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException {
        if (explicitProvider != null) {
            return Cipher.getInstance(cipherType, explicitProvider);
        } else if (provider != null) {
            return Cipher.getInstance(cipherType, provider);
        } else {
            return Cipher.getInstance(cipherType);
        }
    }

    public static void getAvailableAlogrithms() {
        Provider[] providers = Security.getProviders();
        for (Provider provider : providers) {
            logger.logE("provider: " + provider.getName());
            Set<Provider.Service> services = provider.getServices();
            for (Provider.Service service : services) {
                logger.logE("  algorithm: "+service.getAlgorithm());
            }
        }
    }

    static String getFilePath(Context context, String nameSpace, String template) {
        return TextUtil.string(context.getFilesDir().getAbsolutePath(), nameSpace, template);
    }
}
