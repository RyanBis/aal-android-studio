package com.xiledsystems.aal.encrypt;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public interface EncrytionStrength {
    int getBitLength();
    EncrytionStrength fromBits(int bitLength);
}
