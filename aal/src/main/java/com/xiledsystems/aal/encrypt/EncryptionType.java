package com.xiledsystems.aal.encrypt;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.lang.reflect.Constructor;


@State(DevelopmentState.UNTESTED)
public class EncryptionType {

    private final int bitLength;
    private final Class<? extends Encryptor> encryptorClass;
    private final Encryptor cryptor;

    public EncryptionType(Class<? extends Encryptor> encryptorClass, int bitLength) {
        this.encryptorClass = encryptorClass;
        this.bitLength = bitLength;
        cryptor = getEncryptor_private(encryptorClass, bitLength);
    }

    public <T extends Encryptor> T getEncryptor() {
        if (cryptor != null) {
            return (T) cryptor;
        } else {
            return null;
        }
    }

    public int getBitLength() {
        return bitLength;
    }


    public static Aes128 newAes128() {
        return new Aes128();
    }

    public static Aes192 newAes192() {
        return new Aes192();
    }

    public static Aes256 newAes256() {
        return new Aes256();
    }

    public static Rsa256 newRsa256() {
        return new Rsa256();
    }

    public static Rsa512 newRsa512() {
        return new Rsa512();
    }

    public static Rsa1024 newRsa1024() {
        return new Rsa1024();
    }

    public static Rsa2048 newRsa2048() {
        return new Rsa2048();
    }

    public static Rsa4096 newRsa4096() {
        return new Rsa4096();
    }

    public final static class None extends EncryptionType {
        public None() {
            super(null, 0);
        }

        @Override
        public String toString() {
            return "No Encryption";
        }
    }

    public abstract static class Aes extends EncryptionType {
        public Aes(Class<? extends Encryptor> encryptorClass, int bitLength) {
            super(encryptorClass, bitLength);
        }
    }

    public final static class Aes128 extends Aes {
        public Aes128() {
            super(AES.class, 128);
        }

        @Override
        public String toString() {
            return "AES_128bit";
        }

        public static String name() {
            return "AES_128bit";
        }
    }

    public final static class Aes192 extends Aes {
        public Aes192() {
            super(AES.class, 192);
        }

        @Override
        public String toString() {
            return "AES_192bit";
        }

        public static String name() {
            return "AES_192bit";
        }
    }

    public final static class Aes256 extends Aes {
        public Aes256() {
            super(AES.class, 256);
        }

        @Override
        public String toString() {
            return "AES_256bit";
        }

        public static String name() {
            return "AES_256bit";
        }
    }

    public abstract static class Rsa extends EncryptionType {
        public Rsa(Class<? extends Encryptor> encryptorClass, int bitLength) {
            super(encryptorClass, bitLength);
        }
    }

    public final static class Rsa256 extends Rsa {
        public Rsa256() {
            super(RSA.class, 256);
        }

        @Override
        public String toString() {
            return "RSA_256bit";
        }

        public static String name() {
            return "RSA_256bit";
        }
    }

    public final static class Rsa512 extends Rsa {
        public Rsa512() {
            super(RSA.class, 512);
        }

        @Override
        public String toString() {
            return "RSA_512bit";
        }

        public static String name() {
            return "RSA_512bit";
        }
    }

    public final static class Rsa1024 extends Rsa {
        public Rsa1024() {
            super(RSA.class, 1024);
        }

        @Override
        public String toString() {
            return "RSA_1024bit";
        }

        public static String name() {
            return "RSA_1024bit";
        }
    }

    public final static class Rsa2048 extends Rsa {
        public Rsa2048() {
            super(RSA.class, 2048);
        }

        @Override
        public String toString() {
            return "RSA_2048bit";
        }

        public static String name() {
            return "RSA_2048bit";
        }
    }

    public final static class Rsa4096 extends Rsa {
        public Rsa4096() {
            super(RSA.class, 4096);
        }

        @Override
        public String toString() {
            return "RSA_4096bit";
        }

        public static String name() {
            return "RSA_4096bit";
        }
    }

    private static Encryptor getEncryptor_private(Class<? extends Encryptor> clazz, int bitLength) {
        if (clazz != null) {
            Encryptor enc = null;
            try {
                Constructor<? extends Encryptor> con = clazz.getDeclaredConstructor(int.class);
                con.setAccessible(true);
                enc = con.newInstance(bitLength);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return enc;
        } else {
            return null;
        }
    }

}