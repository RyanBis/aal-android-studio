package com.xiledsystems.aal.encrypt;


import android.content.Context;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


@State(DevelopmentState.UNTESTED)
public final class AES extends Encryptor {

    public static Provider explicitProvider = null;

    private static final String TAG = "AES Encryptor";
    private static final String HMAC_ALGORITHM = "HmacSHA256";
    private static final String CIPHER = "AES";
    private static final String CIPHER_TRANSFORM = "AES/CBC/PKCS5Padding";
    private static final String VKS = "sk.sv";
    private static final String VKI = "ik.sv";
    private static final int HMAC_KEY_LENGTH_BITS = 256;
    private static final int PBE_ITERATION_COUNT = 10000;
    private static final int IV_LENGTH_BYTES = 16;
    private static final int PBE_SALT_LENGTH_BITS = 64;
    private static final String PBE_ALGORITHM = "PBKDF2WithHmacSHA1";

    private final AESStrength strength;


    public AES(AESStrength strength) {
        this.strength = strength;
    }

    public AES() {
        this(AESStrength.LOW);
    }

    AES(int bitLength) {
        this(AESStrength.fromBitLength(bitLength));
    }

    public AESStrength getEncryptionStrength() {
        return strength;
    }


    @Override
    Provider getProvider() {
        return explicitProvider;
    }

    @Override
    public AESDecryptedData decryptData(Key key, Data encryptedData) {
        return doDecryption(key, encryptedData);
    }

    @Override
    public AESEncryptedData encryptData(Key key, byte[] dataToEncrypt) {
        return doEncryption(key, dataToEncrypt);
    }

    @Override
    public Key generateKey(String key, byte[] salt) {
        return doKeyGeneration(key, salt, strength);
    }

    @Override
    public Key generateKey() {
        return doKeyGeneration(strength);
    }

    @Override
    public Data getEncryptedObject(byte[] encryptedBytes) {
        return new AESEncryptedData(TextUtil.byteToString(encryptedBytes, "UTF-8"));
    }

    @Deprecated
    @Override
    public Key loadKey(Context context, String nameSpace) {
        return loadEncryptionKey(context, nameSpace + "_" + strength.getBitLength());
    }

    @Deprecated
    @Override
    public void saveKey(Context context, String nameSpace, Key key) {
        saveEncryptionKey(context, nameSpace + "_" + strength.getBitLength(), key);
    }

    @Override
    public void putKey(Context context, String nameSpace, Key key) {
        KeyVault.get(context).putKey(nameSpace, key);
    }

    @Override
    public Key getKey(Context context, String nameSpace) {
        return KeyVault.get(context).getKey(nameSpace, KeyVault.KeyType.AES);
    }

    public static byte[] generateSalt() {
        return randomBytes(PBE_SALT_LENGTH_BITS);
    }

    public static byte[] generateIV() {
        return randomBytes(IV_LENGTH_BYTES);
    }

    public static boolean constantTimeEq(byte[] first, byte[] second) {
        if (first.length != second.length) {
            return false;
        }
        int result = 0;
        for (int i = 0; i < first.length; i++) {
            result |= first[i] ^ second[i];
        }
        return result == 0;
    }

    public static byte[] generateMac(byte[] data, SecretKey integrityKey) {
        byte[] mac = null;
        try {
            final Mac sha256Mac = Mac.getInstance(HMAC_ALGORITHM);
            sha256Mac.init(integrityKey);
            mac = sha256Mac.doFinal(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return mac;
    }

    public static Key loadEncryptionKey(Context context, String nameSpace) {
        Key k;
        SecretKey key = null;
        SecretKey ikey = null;
        try {
            byte[] fHalf;
            File file = new File(getFilePath(context, nameSpace, VKS));
            FileInputStream in = new FileInputStream(getFilePath(context, nameSpace, VKS));
            fHalf = new byte[(int) file.length()];
            in.read(fHalf);
            in.close();

            byte[] lHalf;
            file = new File(getFilePath(context, nameSpace, VKI));
            in = new FileInputStream(getFilePath(context, nameSpace, VKI));
            lHalf = new byte[(int) file.length()];
            in.read(lHalf);
            in.close();
            key = new SecretKeySpec(fHalf, CIPHER);
            ikey = new SecretKeySpec(lHalf, HMAC_ALGORITHM);
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
        k = new Key(key, ikey);
        return k;
    }

    public static boolean saveEncryptionKey(Context context, String nameSpace, Key key) {
        try {
            final  byte[] keyBytes = key.getKey().getEncoded();
            File file = new File(getFilePath(context, nameSpace, VKS));
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream out = new FileOutputStream(getFilePath(context, nameSpace, VKS));
            out.write(keyBytes);
            out.close();

            final byte[] intBytes = key.getIntegrityKey().getEncoded();
            file = new File(getFilePath(context, nameSpace, VKI));
            if (!file.exists()) {
                file.createNewFile();
            }
            out = new FileOutputStream(getFilePath(context, nameSpace, VKI));
            out.write(intBytes);
            out.close();
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    private static AESDecryptedData doDecryption(Key key, Data encData) {
        Data.Failure fail;
        byte[] decrypted = null;
        if (encData instanceof AESEncryptedData) {
            final AESEncryptedData encryptedData = (AESEncryptedData) encData;
            final byte[] ivData = AESEncryptedData.ivDataConcat(encryptedData.getIv(), encryptedData.getData());
            final byte[] computedMac = generateMac(ivData, key.getIntegrityKey());
            if (constantTimeEq(computedMac, encryptedData.getMac())) {
                try {
                    final Cipher cipher = getCipher(CIPHER_TRANSFORM, getProviderName(), explicitProvider);
                    cipher.init(Cipher.DECRYPT_MODE, key.getKey(), new IvParameterSpec(encryptedData.getIv()));
                    decrypted = cipher.doFinal(encryptedData.getData());
                    fail = Data.Failure.NONE;
                } catch (NoSuchAlgorithmException e) {
                    fail = Data.Failure.NO_SUCH_ALGORITHM;
                } catch (NoSuchPaddingException e) {
                    fail = Data.Failure.NO_SUCH_PADDING;
                } catch (InvalidKeyException e) {
                    fail = Data.Failure.INVALID_KEY;
                } catch (InvalidAlgorithmParameterException e) {
                    fail = Data.Failure.INVALID_ALGORITHM_PARAMETER;
                } catch (BadPaddingException e) {
                    fail = Data.Failure.BAD_PADDING;
                } catch (IllegalBlockSizeException e) {
                    fail = Data.Failure.ILLEGAL_BLOCK_SIZE;
                } catch (NoSuchProviderException e) {
                    fail = Data.Failure.NO_SUCH_PROVIDER;
                }
            } else {
                fail = Data.Failure.BAD_INTEGRITY;
            }
        } else {
            fail = Data.Failure.WRONG_ENCRYPTION_TYPE;
        }
        final AESDecryptedData d = new AESDecryptedData(decrypted);
        d.setFailure(fail);
        return d;
    }

    private static AESEncryptedData doEncryption(Key key, byte[] data) {
        byte[] encrypted = null;
        byte[] iv = generateIV();
        byte[] mac = null;
        Data.Failure fail;
        try {
            final Cipher cipher = getCipher(CIPHER_TRANSFORM, getProviderName(), explicitProvider);
            cipher.init(Cipher.ENCRYPT_MODE, key.getKey(), new IvParameterSpec(iv));
            iv = cipher.getIV();
            encrypted = cipher.doFinal(data);
            final byte[] ivData = AESEncryptedData.ivDataConcat(iv, encrypted);
            mac = generateMac(ivData, key.getIntegrityKey());
            fail = Data.Failure.NONE;
        } catch (NoSuchAlgorithmException e) {
            fail = Data.Failure.NO_SUCH_ALGORITHM;
        } catch (NoSuchPaddingException e) {
            fail = Data.Failure.NO_SUCH_PADDING;
        } catch (InvalidKeyException e) {
            fail = Data.Failure.INVALID_KEY;
        } catch (InvalidAlgorithmParameterException e) {
            fail = Data.Failure.INVALID_ALGORITHM_PARAMETER;
        } catch (IllegalBlockSizeException e) {
            fail = Data.Failure.ILLEGAL_BLOCK_SIZE;
        } catch (BadPaddingException e) {
            fail = Data.Failure.BAD_PADDING;
        } catch (NoSuchProviderException e) {
            fail = Data.Failure.NO_SUCH_PROVIDER;
        }
        final AESEncryptedData d = new AESEncryptedData(encrypted, iv, mac);
        d.setFailure(fail);
        return d;
    }

    private static Key doKeyGeneration(AESStrength strength) {
        return KeyVault.newAesKey((explicitProvider != null ? explicitProvider.getName() : getProviderName()), strength, HMAC_KEY_LENGTH_BITS / 8, CIPHER, HMAC_ALGORITHM);
    }

    private static Key doKeyGeneration(String key, byte[] salt, AESStrength strength) {
        return KeyVault.newAesKey(key, salt, strength, HMAC_KEY_LENGTH_BITS / 8, CIPHER, PBE_ITERATION_COUNT, PBE_ALGORITHM, HMAC_ALGORITHM);
    }

}