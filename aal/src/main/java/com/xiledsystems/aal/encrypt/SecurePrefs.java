package com.xiledsystems.aal.encrypt;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.util.ByteUtil;
import com.xiledsystems.aal.util.TextUtil;
import java.io.UnsupportedEncodingException;


/**
 * Class used to store small amounts of data (Preferences) securely. This class
 * uses {@link SharedPreferences} to store the data, however, all data that is stored
 * is encrypted using the specified {@link EncryptionType}. If none is specified, the default
 * is {@link com.xiledsystems.aal.encrypt.EncryptionType.Aes256}.
 */
@TargetApi(Build.VERSION_CODES.FROYO)
@State(DevelopmentState.UNTESTED)
public class SecurePrefs extends SecureComponent {

    private final static String TAG = "SPrefs";
    private final static String NULL = "*-NULL-*";
    private final static String META = "Metadata_%s.prefs";
    private final static String K = "ks";
    private final static String S = "sl";
    private final static String KSV = "k_pe_s";

    private final String prefsFileName;
    private final String stringEncoding;
    private final SharedPreferences prefs;


    @Override
    String getLogTag() {
        return TAG;
    }

    @Override
    String getKeyTag() {
        return KSV;
    }

    /**
     * Overload of {@link #SecurePrefs(Context, String, EncryptionType, String)}. This uses a default
     * namespace created from your app's packagename, and {@link com.xiledsystems.aal.encrypt.EncryptionType.Aes256}
     * default encryption.
     */
    public SecurePrefs(Context context) {
        this(context, context.getPackageName() + ".spref", EncryptionType.newAes256());
    }

    /**
     * Overload of {@link #SecurePrefs(Context, String, EncryptionType, String)}. This uses a default
     * encryption type of {@link com.xiledsystems.aal.encrypt.EncryptionType.Aes256}
     */
    public SecurePrefs(Context context, String fileName) {
        this(context, fileName, EncryptionType.newAes256());
    }

    /**
     * Overload of {@link #SecurePrefs(Context, String, EncryptionType, String)}. This uses a default
     * namespace created from your app's packagename.
     */
    public SecurePrefs(Context context, EncryptionType encryptionType) {
        this(context, context.getPackageName() + ".spref", encryptionType);
    }

    /**
     * Overload of {@link #SecurePrefs(Context, String, EncryptionType, String)}. Like all other
     * overloads, this uses a default string encoding of UTF-8.
     */
    public SecurePrefs(Context context, String fileName, EncryptionType encryptionType) {
        this(context, fileName, encryptionType, Encryptor.UTF_8);
    }

    /**
     * Create a new {@link SecurePrefs} instance with the supplied values.
     * @param context
     * @param fileName
     * @param encryptionType
     * @param stringEncoding - The encoding of strings you intend to store. default is UTF-8
     */
    public SecurePrefs(Context context, String fileName, EncryptionType encryptionType, String stringEncoding) {
        super(context, encryptionType);
        prefsFileName = fileName;
        prefs = loadPrefsInstance(context, prefsFileName);
        this.stringEncoding = stringEncoding;
    }

    /**
     * Returns the {@link EncryptionType} this {@link SecurePrefs} instance is using for encryption.
     */
    public <T extends EncryptionType> T getEncryptionType() {
        return (T) encryptionType();
    }

    /**
     * Store a string with the specified key. The key is encoded in Base64 format, and the value is
     * encrypted before storing to disk.
     *
     * @param key
     * @param value
     */
    public void putString(final String key, final String value) {
        if (isKeyReady()) {
            doPutString(key, value);
        } else {
            EventManager.getInstance().postEvent(Event.ERROR_KEY_NOT_INITIALIZED);
            getLogger().logE(TextUtil.string("Tried to put an item before the encryption key has been initialized! Item NOT saved!\n Key: ", key, "\nValue: ", value));
        }
    }

    /**
     * Retrieve a stored String with the specified key. If the key is not found, defValue will be returned.
     * Returned value is already decrypted. All get methods are synchronous. The only time this will really
     * make a difference is upon initial instantiation. The encryption key can take a little while to load or create
     * depending on the encryption type, and strength. (You can either use {@link #init(KeyListener)}, or listen for
     * the {@link com.xiledsystems.aal.encrypt.SecureComponent.Event#KEY_INITIALIZED} event to know when the key is
     * ready).
     * @param key
     * @param defValue
     * @return
     */
    public String getString(final String key, final String defValue) {
        if (isKeyReady()) {
            return doGetString(key, defValue);
        } else {
            getLogger().logE(TextUtil.string("Tried to get a value before encryption key has been initialized! Returning empty string. \nKey: ", key));
            return "";
        }
    }

    /**
     * Overload of {@link #putString(String, String)}. All data is stored as a String when put to
     * disk. This is just a convenience method.
     */
    public void putInt(String key, int value) {
        putString(key, String.valueOf(value));
    }

    /**
     * Overload of {@link #getString(String, String)}. All data is stored as a String when put to
     * disk. This is just a convenience method.
     */
    public int getInt(String key, int defValue) {
        String val = getString(key, String.valueOf(defValue));
        try {
            return Integer.parseInt(val);
        } catch (Exception e) {
            return defValue;
        }
    }

    /**
     * Overload of {@link #putString(String, String)}. All data is stored as a String when put to
     * disk. This is just a convenience method.
     */
    public void putLong(String key, long value) {
        putString(key, String.valueOf(value));
    }

    /**
     * Overload of {@link #getString(String, String)}. All data is stored as a String when put to
     * disk. This is just a convenience method.
     */
    public long getLong(String key, long defValue) {
        String val = getString(key, String.valueOf(defValue));
        try {
            return Long.parseLong(val);
        } catch (Exception e) {
            return defValue;
        }
    }

    /**
     * Overload of {@link #putString(String, String)}. All data is stored as a String when put to
     * disk. This is just a convenience method.
     */
    public void putBoolean(String key, boolean value) {
        putString(key, String.valueOf(value));
    }

    /**
     * Overload of {@link #getString(String, String)}. All data is stored as a String when put to
     * disk. This is just a convenience method.
     */
    public boolean getBoolean(String key, boolean defValue) {
        String val = getString(key, String.valueOf(defValue));
        try {
            return Boolean.parseBoolean(val);
        } catch (Exception e) {
            return defValue;
        }
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return new ServiceLifecycleListener();
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    private void doPutString(String key, String value) {
        final byte[] encDataBytes = encrypt(encryptionType(), key(), value, stringEncoding, getLogger());
        if (encDataBytes == null) {
            getLogger().logE("Unable to encrypt value! Value " + value + " was NOT stored!");
            return;
        }
        final String encKey = encryptKey(key, stringEncoding);
        final String encValue = ByteUtil.bytesToBase64(encDataBytes);
        if (encValue == null) {
            getLogger().logE("Unable to convert encrypted key to string! Value " + value + " was NOT stored!");
            return;
        }
        prefs.edit().putString(encKey, encValue).commit();
    }

    private String doGetString(String key, String defValue) {
        final String encKey = encryptKey(key, stringEncoding);
        if (encKey == null) {
            getLogger().logE("Unable to convert encrypted key to string! Returning default value.");
            return defValue;
        }
        final String encryptedVal = prefs.getString(encKey, NULL);
        if (encryptedVal.equals(NULL)) {
            return defValue;
        } else {
            return decrypt(encryptionType(), key(), encryptedVal, stringEncoding, getLogger());
        }
    }

    private static String encryptKey(String value, String stringEncoding) {
        byte[] bytes;
        try {
            bytes = value.getBytes(stringEncoding);
        } catch (UnsupportedEncodingException e) {
            bytes = value.getBytes();
        }
        return Base64.encodeToString(bytes, ByteUtil.BASE64_FLAGS);
    }

}