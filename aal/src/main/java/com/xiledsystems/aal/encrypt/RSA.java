package com.xiledsystems.aal.encrypt;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ByteUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;


@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@State(DevelopmentState.UNTESTED)
public final class RSA extends Encryptor {

    public static Provider explicitProvider = null;

    private final RSAStrength strength;
    private final int maxBytes;
    private final int maxInputBytes;
//    private final static String CIPHER = "RSA";
    private final static String CIPHER = "RSA/ECB/PKCS1Padding";
    private final static String PUB_SV = "pb.sv";
    private final static String PRIV_SV = "pr.sv";
    private final static byte[] HEADER = {'a', 'a', 'l'};


    public RSA(RSAStrength strength) {
        this.strength = strength;
        maxBytes = (strength.getBitLength() / 8);
        maxInputBytes = maxBytes - 11;
    }

    public RSA() {
        this(RSAStrength.LOW);
    }

    RSA(int bitLength) {
        this(RSAStrength.fromBitLength(bitLength));
    }

    public RSAStrength getEncryptionStrength() {
        return strength;
    }


    @Override
    Provider getProvider() {
        return explicitProvider;
    }

    @Override
    public RSAData decryptData(Key key, Data encryptedData) {
        return doWrappedDecryption(key.getPrivateKey(), encryptedData, maxBytes);
    }

    @Override
    public RSAData encryptData(Key key, byte[] dataToEncrypt) {
        return doWrappedEncryption(key.getPublicKey(), dataToEncrypt, maxInputBytes);
    }

    @Override
    public Key generateKey(String key, byte[] salt) {
        return KeyVault.newRsaKey(explicitProvider != null ? explicitProvider.getName() : getProviderName(), strength);
    }

    @Override
    public Key generateKey() {
        return KeyVault.newRsaKey(explicitProvider != null ? explicitProvider.getName() : getProviderName(), strength);
    }

    @Override
    public Data getEncryptedObject(byte[] encryptedBytes) {
        return new RSAData(encryptedBytes);
    }

    @Override
    public Key loadKey(Context context, String nameSpace) {
        return loadEncryptionKey(context, nameSpace + "_" + strength.getBitLength());
    }

    @Override
    public void saveKey(Context context, String nameSpace, Key key) {
        saveEncryptionKey(context, nameSpace + "_" + strength.getBitLength(), key.getKeyPair());
    }

    @Override
    public Key getKey(Context context, String nameSpace) {
        return KeyVault.get(context).getKey(nameSpace, KeyVault.KeyType.RSA);
    }

    @Override
    public void putKey(Context context, String nameSpace, Key key) {
        KeyVault.get(context).putKey(nameSpace, key);
    }

    public static Key loadEncryptionKey(Context context, String nameSpace) {
        KeyPair key = null;
        final String pathp = context.getFilesDir().getAbsolutePath() + nameSpace + PUB_SV;
        final String pathpr = context.getFilesDir().getAbsolutePath() + nameSpace + PRIV_SV;
        Key k;
        final PrivateKey prKey = loadEncryptionPrivateKey(pathpr);
        final PublicKey pKey = loadEncryptionPublicKey(pathp);
        if (prKey != null && pKey != null) {
            key = new KeyPair(pKey, prKey);
        }
        return new Key(key);
    }

    public static PublicKey loadEncryptionPublicKey(String filePath) {
        PublicKey pubKey = null;
        try {
            byte[] prData;
            final File file = new File(filePath);
            prData = new byte[(int) file.length()];
            final FileInputStream in = new FileInputStream(filePath);
            in.read(prData);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pubKey;
    }

    public static PrivateKey loadEncryptionPrivateKey(String filePath) {
        PrivateKey pubKey = null;
        try {
            byte[] pubData;
            final File file = new File(filePath);
            pubData = new byte[(int) file.length()];
            final FileInputStream in = new FileInputStream(filePath);
            in.read(pubData);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pubKey;
    }

    public static boolean saveEncryptionKey(Context context, String nameSpace, KeyPair key) {
        return savePublicKey(context, nameSpace, key.getPublic()) && savePrivateKey(context, nameSpace, key.getPrivate());
    }

    public static boolean savePrivateKey(Context context, String nameSpace, PrivateKey key) {
        try {
            final PKCS8EncodedKeySpec pSpec = new PKCS8EncodedKeySpec(key.getEncoded());
            final FileOutputStream out = new FileOutputStream(getFilePath(context, nameSpace, PRIV_SV));
            out.write(pSpec.getEncoded());
            out.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean savePublicKey(Context context, String nameSpace, PublicKey key) {
        try {
            final X509EncodedKeySpec keyspec = new X509EncodedKeySpec(key.getEncoded());
            final FileOutputStream out = new FileOutputStream(getFilePath(context, nameSpace, PUB_SV));
            out.write(keyspec.getEncoded());
            out.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    private static RSAData doWrappedDecryption(PrivateKey key, Data data, int maxBytes) {
        if (data == null) {
            return new RSAData(null, Data.Failure.NULL_DATA);
        }
        if (data instanceof RSAData) {
            if (data.getBytes().length <= maxBytes) {
                return doDecryption(key, data);
            } else {
                byte[] decryptedBytes = new byte[0];
                final byte[] bytes = data.getBytes();
                boolean keepGoing = true;
                int index = 0;

                while (keepGoing) {
                    byte[] header = Arrays.copyOfRange(bytes, index, index + 3);
                    if (!Arrays.equals(header, HEADER)) {
                        return new RSAData(null, Data.Failure.BAD_WRAPPING);
                    }
                    index += 3;
                    byte[] count = Arrays.copyOfRange(bytes, index, index + 4);
                    index += 4;
                    int size = ByteUtil.bytesToInt(count);
                    byte[] encData = Arrays.copyOfRange(bytes, index, index + size);
                    RSAData decD = doDecryption(key, new RSAData(encData));
                    final int chunkSize = decD.getBytes().length;
                    // If NOT on the last chunk, and the chunk size doesn't match the reported size, then pad the rest with
                    // 0, as the RSA decryption function will strip off 0s, thereby returning an incorrect total byte array
                    // after all chunks are put back together.
//                    if (chunkSize + 1 != size && !((index + size) >= bytes.length)) {
////                        final byte[] newBytes = ByteUtil.append(decD.getBytes(), (byte) 0, size - chunkSize);
//                        final byte[] newBytes = ByteUtil.padStart(decD.getBytes(), size - chunkSize);
//                        decD = new RSAData(newBytes);
//                        decD.setFailure(Data.Failure.NONE);
//                    }
                    if (!decD.success()) {
                        return new RSAData(null, decD.getFailure());
                    }
                    decryptedBytes = ByteUtil.append(decryptedBytes, decD.getBytes());
                    index += size;
                    if (index >= bytes.length) {
                        keepGoing = false;
                    }
                }
                return new RSAData(decryptedBytes, Data.Failure.NONE);
            }
        } else {
            return new RSAData(null, Data.Failure.WRONG_ENCRYPTION_TYPE);
        }
    }

    private static RSAData doDecryption(PrivateKey priv, Data data) {
        byte[] decryptedData = null;
        Data.Failure fail;
        try {
            final Cipher cipher = getCipher(CIPHER, getProviderName(), explicitProvider);
            cipher.init(Cipher.DECRYPT_MODE, priv);
            decryptedData = cipher.doFinal(data.getBytes());
            fail = Data.Failure.NONE;
        } catch (NoSuchPaddingException e) {
            fail = Data.Failure.NO_SUCH_PADDING;
        } catch (NoSuchAlgorithmException e) {
            fail = Data.Failure.NO_SUCH_ALGORITHM;
        } catch (InvalidKeyException e) {
            fail = Data.Failure.INVALID_KEY;
        } catch (BadPaddingException e) {
            fail = Data.Failure.BAD_PADDING;
        } catch (IllegalBlockSizeException e) {
            fail = Data.Failure.ILLEGAL_BLOCK_SIZE;
        } catch (NoSuchProviderException e) {
            fail = Data.Failure.NO_SUCH_PROVIDER;
        }
        final RSAData d = new RSAData(decryptedData);
        d.setFailure(fail);
        return d;
    }

    private static RSAData doWrappedEncryption(PublicKey key, byte[] data, int maxInputBytes) {
        if (data == null) {
            return new RSAData(null, Data.Failure.NULL_DATA);
        }
        if (data.length <= maxInputBytes) {
            return doEncryption(key, data);
        } else {
            final int length;
            if (data.length % maxInputBytes == 0) {
                length = data.length / maxInputBytes;
            } else {
                length = (data.length / maxInputBytes) + 1;
            }
            byte[] e = new byte[0];
            int startIndex;
            int endIndex;
            for (int i = 0; i < length; i++) {
                e = ByteUtil.append(e, HEADER);
                startIndex = i * maxInputBytes;
                endIndex = Math.min(startIndex + maxInputBytes, data.length);
                if (startIndex > endIndex) {
                    int tmp = startIndex;
                    startIndex = endIndex;
                    endIndex = tmp;
                }
                final RSAData ed = doEncryption(key, Arrays.copyOfRange(data, startIndex, endIndex));
                if (!ed.success()) {
                    return new RSAData(null, ed.getFailure());
                }
                e = ByteUtil.append(e, ByteUtil.intToBytes(ed.getBytes().length));
                e = ByteUtil.append(e, ed.getBytes());
            }
            return new RSAData(e, Data.Failure.NONE);
        }
    }

    private static RSAData doEncryption(PublicKey pub, byte[] data) {
        byte[] encryptedData = null;
        Data.Failure fail;
        try {
            final Cipher cipher = getCipher(CIPHER, getProviderName(), explicitProvider);
            cipher.init(Cipher.ENCRYPT_MODE, pub);
            encryptedData = cipher.doFinal(data);
            fail = Data.Failure.NONE;
        } catch (NoSuchAlgorithmException e) {
            fail = Data.Failure.NO_SUCH_ALGORITHM;
        } catch (InvalidKeyException e) {
            fail = Data.Failure.INVALID_KEY;
        } catch (NoSuchPaddingException e) {
            fail = Data.Failure.NO_SUCH_PADDING;
        } catch (IllegalBlockSizeException e) {
            fail = Data.Failure.ILLEGAL_BLOCK_SIZE;
        } catch (BadPaddingException e) {
            fail = Data.Failure.BAD_PADDING;
        } catch (NoSuchProviderException e) {
            fail = Data.Failure.NO_SUCH_PROVIDER;
        } catch (Exception e) {
            fail = Data.Failure.GENERAL_EXCEPTION;
        }
        final RSAData d = new RSAData(encryptedData);
        d.setFailure(fail);
        return d;
    }

}
