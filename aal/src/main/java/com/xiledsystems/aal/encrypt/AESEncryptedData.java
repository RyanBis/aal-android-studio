package com.xiledsystems.aal.encrypt;


import android.annotation.TargetApi;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.AalLogger;
import com.xiledsystems.aal.util.Base64;
import com.xiledsystems.aal.util.ByteUtil;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;


@TargetApi(Build.VERSION_CODES.FROYO)
@State(DevelopmentState.UNTESTED)
public class AESEncryptedData extends Data {

    private final byte[] data;
    private final byte[] iv;
    private final byte[] mac;

    public AESEncryptedData(byte[] data, byte[] iv, byte[] mac) {
        if (data != null) {
            this.data = new byte[data.length];
            System.arraycopy(data, 0, this.data, 0, data.length);
        } else {
            this.data = ByteUtil.EMPTY_ARRAY;
        }
        if (iv != null) {
            this.iv = new byte[iv.length];
            System.arraycopy(iv, 0, this.iv, 0, iv.length);
        } else {
            this.iv = ByteUtil.EMPTY_ARRAY;
        }
        if (mac != null) {
            this.mac = new byte[mac.length];
            System.arraycopy(mac, 0, this.mac, 0, mac.length);
        } else {
            this.mac = ByteUtil.EMPTY_ARRAY;
        }
    }

    public AESEncryptedData(byte[] data, byte[] iv, byte[] mac, Failure failure) {
        this(data, iv, mac);
        setFailure(failure);
    }

    public AESEncryptedData(String dataIvMacString) {
        String[] dataArray = dataIvMacString.split(":");
        if (dataArray.length != 3) {
            throw new IllegalArgumentException("Cannot parse data string!");
        }
        iv = Base64.decode(dataArray[0], ByteUtil.BASE64_FLAGS);
        mac = Base64.decode(dataArray[1], ByteUtil.BASE64_FLAGS);
        data = Base64.decode(dataArray[2], ByteUtil.BASE64_FLAGS);
    }

    public AESEncryptedData(String dataIvMacString, Failure failure) {
        this(dataIvMacString);
        setFailure(failure);
    }

    public byte[] getData() {
        return data;
    }

    public byte[] getIv() {
        return iv;
    }

    public byte[] getMac() {
        return mac;
    }

    public static byte[] ivDataConcat(byte[] iv, byte[] data) {
        byte[] combined = new byte[iv.length + data.length];
        System.arraycopy(iv, 0, combined, 0, iv.length);
        System.arraycopy(data, 0, combined, iv.length, data.length);
        return combined;
    }

    @Override
    public byte[] getBytes() {
        try {
            return toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            AalLogger logger = new AalLogger();
            logger.logE("Unable to get UTF-8 bytes! Returning default charset type.");
            return toString().getBytes();
        }
    }

    @Override
    public int hashCode() {
        final int prime = 71;
        int result = 1;
        result = prime * result + Arrays.hashCode(data);
        result = prime * result + Arrays.hashCode(iv);
        result = prime * result + Arrays.hashCode(mac);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof AESEncryptedData) {
            AESEncryptedData other = (AESEncryptedData) o;
            return Arrays.equals(data, other.data) && Arrays.equals(iv, other.iv) && Arrays.equals(mac, other.mac);
        } else {
            return super.equals(o);
        }
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append(iv.length == 0 ? "null" : Base64.encodeToString(iv, ByteUtil.BASE64_FLAGS));
        b.append(":");
        b.append(mac.length == 0 ? "null" : Base64.encodeToString(mac, ByteUtil.BASE64_FLAGS));
        b.append(":");
        b.append(data.length == 0 ? "null" : Base64.encodeToString(data, ByteUtil.BASE64_FLAGS));
        return b.toString();
    }
}
