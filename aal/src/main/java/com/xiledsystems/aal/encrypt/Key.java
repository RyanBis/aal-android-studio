package com.xiledsystems.aal.encrypt;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;

import javax.crypto.SecretKey;


@State(DevelopmentState.UNTESTED)
public final class Key {

    private final SecretKey key;
    private final SecretKey integKey;
    private final KeyPair keyPair;


    public Key(SecretKey aesKey, SecretKey integrityKey) {
        key = aesKey;
        integKey = integrityKey;
        keyPair = null;
    }

    public Key(KeyPair keyPair) {
        this.keyPair = keyPair;
        key = null;
        integKey = null;
    }

    public final SecretKey getKey() {
        return key;
    }

    public final SecretKey getIntegrityKey() {
        return integKey;
    }

    public final PublicKey getPublicKey() {
        return keyPair.getPublic();
    }

    public final PrivateKey getPrivateKey() {
        return keyPair.getPrivate();
    }

    public final KeyPair getKeyPair() {
        return keyPair;
    }

    public final boolean isAesKey() {
        return keyPair == null;
    }

    public final boolean isNull() {
        return key == null && keyPair == null && integKey == null;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Key) {
            Key otherKey = (Key) obj;
            if (otherKey.isNull() || isNull()) {
                if (otherKey.isNull() && isNull()) {
                    return true;
                }
            }
            if (otherKey.isAesKey() || isAesKey()) {
                if (otherKey.isAesKey() && isAesKey()) {
                    if (otherKey.key.equals(key) && otherKey.integKey.equals(integKey)) {
                        return true;
                    }
                }
            } else {
                if (!otherKey.isAesKey() && !isAesKey()) {
                    return otherKey.keyPair.getPrivate().equals(keyPair.getPrivate()) && otherKey.keyPair.getPublic().equals(keyPair.getPublic());
                }
            }
        }
        return false;
    }
}
