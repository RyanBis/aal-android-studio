package com.xiledsystems.aal.encrypt;


import android.content.Context;
import android.content.SharedPreferences;

import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.util.AalLogger;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import com.xiledsystems.aal.util.ByteUtil;
import com.xiledsystems.aal.util.Rand;
import com.xiledsystems.aal.util.TextUtil;
import com.xiledsystems.aal.util.ThreadManager;
import java.util.concurrent.atomic.AtomicBoolean;


public abstract class SecureComponent extends NonDisplayComponent {


    public interface KeyListener {
        void onKeyInitialized();
    }

    public enum Event {
        KEY_INITIALIZED,
        ERROR_KEY_NOT_INITIALIZED
    }


    private final static String SALT_BASE = "sa%slt";
    private final static String META_BASE = "me%sta";

    private final EncryptionType encryptionType;
    private byte[] salt;
    private String keySecret;
    private Key dataKey;
    private SharedPreferences metaPrefs;
    private AtomicBoolean keyReady;
    private final ThreadManager threadMgr;


    public SecureComponent(Context context) {
        this(context, EncryptionType.newAes256());
    }

    public SecureComponent(Context context, EncryptionType encryptionType) {
        super(context);
        this.encryptionType = encryptionType;
        keyReady = new AtomicBoolean(false);
        threadMgr = ThreadManager.getInstance(true);
    }

    /**
     * This method must be called first to initialize the encryption key.
     *
     * @param listener
     */
    public void init(final KeyListener listener) {
        init_private(getContext(), listener, null);
    }

    /**
     * Does the same as {@link #init(KeyListener)}, with no {@link com.xiledsystems.aal.encrypt.SecureComponent.KeyListener}
     * attached. You can use {@link #isKeyReady()} to check if the key is ready to be used.
     */
    public void init() {
        init_private(getContext(), null, null);
    }

    /**
     * Call this method to initialize this encrypted component, if you wish to supply
     * your own {@link Key}.
     *
     * @param listener
     * @param key
     */
    public void init(final KeyListener listener, final Key key) {
        init_private(getContext(), listener, key);
    }

    /**
     * Returns whether the encryption key is ready or not. You should call this before your first
     * call to any get methods in this class, to ensure the key is loaded/created first. (Or use
     * one of the init() methods which accept {@link com.xiledsystems.aal.encrypt.SecureComponent.KeyListener} to
     * know when it's ready)
     */
    public boolean isKeyReady() {
        return keyReady.get();
    }

    void postOnMain(Runnable action) {
        ThreadManager.getHandler().post(action);
    }

    abstract String getLogTag();

    abstract String getKeyTag();

    EncryptionType encryptionType() {
        return encryptionType;
    }

    Key key() {
        return dataKey;
    }

    private String getModKey() {
        return TextUtil.string(getKeyTag(), "_", encryptionType.toString());
    }

    private void init_private(final Context context, final KeyListener listener, final Key key) {
        if (key == null) {
            threadMgr.execute(new Runnable() {
                @Override
                public void run() {
                    metaPrefs = loadPrefsInstance(context, String.format(META_BASE, getKeyTag()));
                    salt = getOrCreateSalt(metaPrefs, String.format(SALT_BASE, getModKey()));
                    keySecret = getOrCreateKeySecret(metaPrefs, getModKey());
                    dataKey = getOrCreateKey(context, keySecret, salt, encryptionType, getModKey(), getLogger());
                    keyReady.set(true);
                    EventManager.getInstance().postEvent(Event.KEY_INITIALIZED);
                    if (listener != null) {
                        postOnMain(new Runnable() {
                            @Override
                            public void run() {
                                listener.onKeyInitialized();
                            }
                        });
                    }
                }
            });
        } else {
            dataKey = key;
            keyReady.set(true);
            if (listener != null) {
                postOnMain(new Runnable() {
                    @Override
                    public void run() {
                        listener.onKeyInitialized();
                    }
                });
            }
            EventManager.getInstance().postEvent(Event.KEY_INITIALIZED);
        }
    }

    static SharedPreferences loadPrefsInstance(Context context, String namespace) {
        return context.getSharedPreferences(namespace, Context.MODE_PRIVATE);
    }

    static String decrypt(EncryptionType enc, Key key, String encryptedData, String stringEncoding, AalLogger logger) {
        final byte[] encData = ByteUtil.base64ToBytes(encryptedData);
        final Data dataPacket = enc.getEncryptor().getEncryptedObject(encData);
        final Data decryptPacket = enc.getEncryptor().decryptData(key, dataPacket);
        if (decryptPacket.success()) {
            return TextUtil.byteToString(decryptPacket.getBytes(), stringEncoding);
        } else {
            logger.logE("Decryption failed due to " + decryptPacket.getFailure());
            return null;
        }
    }

    static String decrypt(EncryptionType enc, Key key, byte[] encryptedData, String stringEncoding, AalLogger logger) {
        final Data dataPacket = enc.getEncryptor().getEncryptedObject(encryptedData);
        final Data decryptPacket = enc.getEncryptor().decryptData(key, dataPacket);
        if (decryptPacket.success()) {
            return TextUtil.byteToString(decryptPacket.getBytes(), stringEncoding);
        } else {
            logger.logE("Decryption failed due to " + decryptPacket.getFailure());
            return null;
        }
    }

    static byte[] decrypt(EncryptionType enc, Key key, byte[] encryptedData, AalLogger logger) {
        final Data dataPacket = enc.getEncryptor().getEncryptedObject(encryptedData);
        final Data decryptPacket = enc.getEncryptor().decryptData(key, dataPacket);
        if (decryptPacket.success()) {
            return decryptPacket.getBytes();
        } else {
            logger.logE("Decryption failed due to " + decryptPacket.getFailure());
            return null;
        }
    }

    static byte[] encrypt(EncryptionType enc, Key key, String value, String stringEncoding, AalLogger logger) {
        final byte[] valueBytes;
        valueBytes = TextUtil.stringToBytes(value, stringEncoding);
        final Data encData = enc.getEncryptor().encryptData(key, valueBytes);
        if (encData.success()) {
            return enc.getEncryptor().encryptData(key, valueBytes).getBytes();
        } else {
            logger.logE("Unable to encrypt data due to " + encData.getFailure());
            return null;
        }
    }

    static byte[] encrypt(EncryptionType enc, Key key, byte[] data, AalLogger logger) {
        final Data encData = enc.getEncryptor().encryptData(key, data);
        if (encData.success()) {
            return enc.getEncryptor().encryptData(key, data).getBytes();
        } else {
            logger.logE("Unable to encrypt data due to " + encData.getFailure());
            return null;
        }
    }

    private static byte[] getOrCreateSalt(SharedPreferences metaPrefs, String tag) {
        final byte[] salt;
        final String saltstring = metaPrefs.getString(tag, null);
        if (saltstring == null) {
            salt = AES.generateSalt();
            metaPrefs.edit().putString(tag, ByteUtil.bytesToHex(salt)).commit();
        } else {
            salt = ByteUtil.hexToBytes(saltstring);
        }
        return salt;
    }

    private static String getOrCreateKeySecret(SharedPreferences metaPrefs, String tag) {
        final String k;
        final String secret = metaPrefs.getString(tag, null);
        if (secret == null) {
            k = Rand.randomNumCharString(Rand.rndInt(32, 64));
            metaPrefs.edit().putString(tag, k).commit();
        } else {
            k = secret;
        }
        return k;
    }

    private static <T extends EncryptionType> Key getOrCreateKey(Context context, String keySecret, byte[] salt, T encryptionType, String keyName, AalLogger logger) {
        Key key = encryptionType.getEncryptor().loadKey(context, keyName);
        if (key.isNull()) {
            key = encryptionType.getEncryptor().generateKey(keySecret, salt);
            encryptionType.getEncryptor().saveKey(context, keyName, key);
            logger.logD("Created and saved new encryption key.");
        } else {
            logger.logD("Encryption key loaded.");
        }
        return key;
    }

}