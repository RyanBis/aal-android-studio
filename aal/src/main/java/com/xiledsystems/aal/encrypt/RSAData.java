package com.xiledsystems.aal.encrypt;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public class RSAData extends Data {

    private final byte[] data;

    public RSAData(byte[] data) {
        this.data = data;
    }

    public RSAData(byte[] data, Failure failure) {
        this(data);
        setFailure(failure);
    }

    @Override
    public byte[] getBytes() {
        return data;
    }

}
