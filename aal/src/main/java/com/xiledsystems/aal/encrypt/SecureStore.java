package com.xiledsystems.aal.encrypt;


import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import android.util.Base64;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.LifeCycle;
import com.xiledsystems.aal.annotations.LifeCycleMethod;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.sql.DBConfig;
import com.xiledsystems.aal.sql.DataType;
import com.xiledsystems.aal.sql.Query;
import com.xiledsystems.aal.sql.SqlDb;
import com.xiledsystems.aal.sql.Table;
import com.xiledsystems.aal.util.ByteUtil;
import com.xiledsystems.aal.util.TextUtil;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * A class to use for encrypting, then storing lists of things to disk. Each tag is unique, so if you
 * store a boolean to the tag "tag", then put an int to the same tag, they are treated as separate lists.
 * All data is encrypted, then stored as bytes using an instance of {@link SqlDb}. This things that are supported
 * are:<pre>
 *     Booleans
 *     Integers
 *     Longs
 *     Strings
 *     Serializables
 * </pre>
 * If you want to store a list of some other class, you must make sure it implements the {@link Serializable} interface,
 * and contains these two methods (handling how your class should be read/written):
 * <pre>
 *     private void writeObject(java.io.ObjectOutputStream out) throws IOException {
 *     }
 *
 *     private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
 *     }
 * </pre>
 *
 * All data that is stored is encrypted using the specified {@link EncryptionType}. If none is specified, the default
 * is {@link com.xiledsystems.aal.encrypt.EncryptionType.Aes256}.
 */
@State(DevelopmentState.BETA)
@LifeCycle(value = {LifeCycleMethod.ON_LOW_MEMORY, LifeCycleMethod.ON_DESTROY})
@TargetApi(Build.VERSION_CODES.FROYO)
public class SecureStore extends SecureComponent {

    public interface StoreListener<T> {
        void gotList(ArrayList<T> list);
    }

    private enum Type {
        INT,
        LONG,
        STRING,
        BOOLEAN,
        SERIALIZABLE
    }


    private final static String TAG = "SecureStore";
    private final static String DATA = "data";
    private final static String TYPE = "type";
    private final static String CLASS = "class";
    private final static String KEY_NAME = "ye2ks5s";
    private final static int INITIAL_BATCH_SIZE = 1000;

    private final String stringEncoding;

    private boolean keepOpen = true;
    private int batchSize = INITIAL_BATCH_SIZE;
    private SqlDb db;


    /**
     * Overload of {@link #SecureStore(Context, String, EncryptionType, String)}. This uses a default
     * encryption type of {@link com.xiledsystems.aal.encrypt.EncryptionType.Aes256}.
     */
    public SecureStore(Context context) {
        this(context, generateName(context, EncryptionType.Aes256.name()), EncryptionType.newAes256());
    }

    /**
     * Overload of {@link #SecureStore(Context, String, EncryptionType, String)}. This uses a default
     * encryption type of {@link com.xiledsystems.aal.encrypt.EncryptionType.Aes256}
     */
    public SecureStore(Context context, String fileName) {
        this(context, fileName, EncryptionType.newAes256());
    }

    /**
     * Overload of {@link #SecureStore(Context, String, EncryptionType, String)}. This uses a default
     * namespace created from your app's packagename.
     */
    public SecureStore(Context context, EncryptionType encryptionType) {
        this(context, generateName(context, encryptionType.toString()), encryptionType);
    }

    /**
     * Overload of {@link #SecureStore(Context, String, EncryptionType, String)}. Like all other
     * overloads, this uses a default string encoding of UTF-8.
     */
    public SecureStore(Context context, String fileName, EncryptionType encryptionType) {
        this(context, fileName, encryptionType, Encryptor.UTF_8);
    }

    /**
     * Create a new {@link SecurePrefs} instance with the supplied values. You must remember to
     * call {@link #init()}, or {@link #init(KeyListener)} to initialize the encryption key.
     *
     * @param context
     * @param fileName
     * @param encryptionType
     * @param stringEncoding - The encoding of strings you intend to store. default is UTF-8
     */
    public SecureStore(Context context, String fileName, EncryptionType encryptionType, String stringEncoding) {
        super(context, encryptionType);
        DBConfig config = new DBConfig();
        config.setDbName(fileName);
        db = new SqlDb(context, config);
        db.setBatchCount(batchSize);
        this.stringEncoding = stringEncoding;
    }

    @Override
    public void setLoggingEnabled(boolean enable) {
        super.setLoggingEnabled(enable);
        db.setLoggingEnabled(enable);
    }

    /**
     * Set whether this component should keep the db open, or close it after every
     * read/write.
     * @param keepOpen
     */
    public void keepDbOpen(boolean keepOpen) {
        this.keepOpen = keepOpen;
    }

    /**
     *
     * @return <code>true</code> if this component is set to keep the db open (default is true)
     */
    public boolean isKeptOpen() {
        return keepOpen;
    }

    /**
     * Store a boolean to the specified tag.
     * @param tag
     * @param value
     */
    public void putBoolean(String tag, Boolean value) {
        doPut(tag, Type.BOOLEAN, value);
    }

    /**
     * Store an ArrayList<Boolean> to the specified tag.
     * @param tag
     * @param list
     */
    public void putBooleans(String tag, ArrayList<Boolean> list) {
        doBatchPut(tag, Type.BOOLEAN, list);
    }

    /**
     * Get all Booleans stored at the specified tag. Results are returned in the
     * provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_BOOLEANS}.
     *
     * @param tag
     * @param listener
     */
    public void getBooleans(String tag, StoreListener listener) {
        doQuery(tag, Type.BOOLEAN, null, null, listener);
    }

    /**
     * Get all Booleans stored at the specified tag after the offset provided, and restricting it to the limit provided.
     * Results are returned in the provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_BOOLEANS}.
     *
     * @param tag
     * @param listener
     */
    public void getBooleans(String tag, int offSet, int limit, StoreListener listener) {
        doQuery(tag, Type.BOOLEAN, limit, offSet, listener);
    }

    /**
     * Store a int to the specified tag.
     * @param tag
     * @param value
     */
    public void putInt(String tag, Integer value) {
        doPut(tag, Type.INT, value);
    }

    /**
     * Store an ArrayList<Integer> to the specified tag.
     * @param tag
     * @param list
     */
    public void putInts(String tag, ArrayList<Integer> list) {
        doBatchPut(tag, Type.INT, list);
    }

    /**
     * Get all Integers stored at the specified tag. Results are returned in the
     * provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_INTS}.
     *
     * @param tag
     * @param listener
     */
    public void getInts(String tag, StoreListener listener) {
        doQuery(tag, Type.INT, null, null, listener);
    }

    /**
     * Get all Integers stored at the specified tag after the offset provided, and restricting it to the limit provided.
     * Results are returned in the provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_INTS}.
     *
     * @param tag
     * @param listener
     */
    public void getInts(String tag, int offset, int limit, StoreListener listener) {
        doQuery(tag, Type.INT, limit, offset, listener);
    }

    /**
     * Store a long to the specified tag.
     * @param tag
     * @param value
     */
    public void putLong(String tag, Long value) {
        doPut(tag, Type.LONG, value);
    }

    /**
     * Store an ArrayList<Long> to the specified tag.
     * @param tag
     * @param list
     */
    public void putLongs(String tag, ArrayList<Long> list) {
        doBatchPut(tag, Type.LONG, list);
    }

    /**
     * Get all Longs stored at the specified tag. Results are returned in the
     * provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_LONGS}.
     *
     * @param tag
     * @param listener
     */
    public void getLongs(String tag, StoreListener listener) {
        doQuery(tag, Type.LONG, null, null, listener);
    }

    /**
     * Get all Longs stored at the specified tag after the offset provided, and restricting it to the limit provided.
     * Results are returned in the provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_LONGS}.
     *
     * @param tag
     * @param listener
     */
    public void getLongs(String tag, int offset, int limit, StoreListener listener) {
        doQuery(tag, Type.LONG, limit, offset, listener);
    }

    /**
     * Store a String to the specified tag.
     * @param tag
     * @param value
     */
    public void putString(String tag, String value) {
        doPut(tag, Type.STRING, value);
    }

    /**
     * Store an ArrayList<String> to the specified tag.
     * @param tag
     * @param list
     */
    public void putStrings(String tag, ArrayList<String> list) {
        doBatchPut(tag, Type.STRING, list);
    }

    /**
     * Get all Strings stored at the specified tag. Results are returned in the
     * provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_STRINGS}.
     *
     * @param tag
     * @param listener
     */
    public void getStrings(String tag, StoreListener listener) {
        doQuery(tag, Type.STRING, null, null, listener);
    }

    /**
     * Get all Strings stored at the specified tag after the offset provided, and restricting it to the limit provided.
     * Results are returned in the provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_STRINGS}.
     *
     * @param tag
     * @param listener
     */
    public void getStrings(String tag, int offset, int limit, StoreListener listener) {
        doQuery(tag, Type.STRING, limit, offset, listener);
    }

    /**
     * Store a class which implements Serializable to the specified tag.
     * @param tag
     * @param value
     */
    public <T extends Serializable> void putSerializable(String tag, T value) {
        doPut(tag, Type.SERIALIZABLE, value);
    }

    /**
     * Store an ArrayList<? extends Serializable> to the specified tag.
     * @param tag
     * @param list
     */
    public void putSerializables(String tag, ArrayList<? extends Serializable> list) {
        doBatchPut(tag, Type.SERIALIZABLE, list);
    }

    /**
     * Get all Serializable objects stored at the specified tag. Results are returned in the
     * provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_SERIALIZABLE}.
     *
     * @param tag
     * @param listener
     */
    public void getSerializables(String tag, StoreListener listener) {
        doQuery(tag, Type.SERIALIZABLE, null, null, listener);
    }

    /**
     * Get all Serializable objects stored at the specified tag after the offset provided, and restricting it to the limit provided.
     * Results are returned in the provided {@link com.xiledsystems.aal.encrypt.SecureStore.StoreListener}, and also
     * posted to the {@link EventManager} with the category {@link com.xiledsystems.aal.encrypt.SecureStore.Result#GOT_SERIALIZABLE}.
     *
     * @param tag
     * @param listener
     */
    public void getSerializables(String tag, int offset, int limit, StoreListener listener) {
        doQuery(tag, Type.SERIALIZABLE, limit, offset, listener);
    }

    public void dispose() {
        if (db != null && db.isOpen()) {
            db.closeDb();
        }
    }

    public enum Result {
        GOT_BOOLEANS,
        GOT_INTS,
        GOT_LONGS,
        GOT_STRINGS,
        GOT_SERIALIZABLE
    }

    @Override
    String getLogTag() {
        return TAG;
    }

    @Override
    String getKeyTag() {
        return KEY_NAME;
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new LifecycleListener() {
            @Override
            public void onDestroy() {
                dispose();
            }
        };
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return new OnLowMemoryListener() {
            @Override
            public void onLowMemory() {
                batchSize *= 0.75;
                db.setBatchCount(batchSize);
            }
        };
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return new ServiceLifecycleListener() {

            @Override
            public void onLowMemory() {
                batchSize *= 0.75;
                db.setBatchCount(batchSize);
            }

            @Override
            public void onDestroy() {
                dispose();
            }
        };
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    private void checkOpenDb() {
        if (!db.isOpen()) {
            db.initialize();
        }
    }

    private void checkCloseDb() {
        if (!keepOpen) {
            db.closeDb();
        }
    }

    private <T> void doQuery(final String tag, final Type type, final Integer limit, final Integer offset, final StoreListener listener) {
        checkOpenDb();
        final String encTag = getEncTag(tag, type, stringEncoding);
        ArrayList<T> list = new ArrayList<>();
        if (hasTable(db, encTag, false)) {
            Query q = new Query().select().from(encTag).where(TYPE).equalTo(type.ordinal()).order(SqlDb.ID);
            if (limit != null) {
                q.limit(limit);
            }
            if (offset != null) {
                q.offSet(offset);
            }
            synchronized (db.getLock()) {
                Cursor c = q.executeQuery(db);
                if (c != null) {
                    if (c.moveToFirst()) {
                        byte[] value;
                        T v;
                        do {
                            byte[] encData = c.getBlob(c.getColumnIndex(DATA));
                            value = decrypt(encryptionType(), key(), encData, getLogger());
                            v = getValue(value, type, stringEncoding);
                            list.add(v);
                        } while (c.moveToNext());
                    }
                    c.close();
                }
            }
        }
        checkCloseDb();
        postListenerResult(listener, list);
        EventManager.getInstance().postEvent(getEventType(type), tag, list);
    }

    private void postListenerResult(final StoreListener listener, final ArrayList<?> list) {
        postOnMain(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    listener.gotList(list);
                }
            }
        });
    }

    private <T> void doBatchPut(final String tag, final Type type, final ArrayList<T> values) {
        final String encTag = getEncTag(tag, type, stringEncoding);
        checkOpenDb();
        hasTable(db, encTag, true);
        final int size = values.size();
        db.startBatch();
        boolean success = false;
        final ContentValues v = new ContentValues(2);
        byte[] data;
        byte[] bytes;
        for (int i = 0; i < size; i++) {
            bytes = toBytes(type, values.get(i), stringEncoding);
            data = encrypt(encryptionType(), key(), bytes, getLogger());
            success = data != null;
            if (success) {
                v.clear();
                v.put(TYPE, type.ordinal());
                v.put(DATA, data);
                db.insert(encTag, v);
            } else {
                break;
            }
        }
        db.endBatch();
        checkCloseDb();
    }

    private <T> void doPut(final String tag, final Type type, final T value) {
        final String encTag = getEncTag(tag, type, stringEncoding);
        checkOpenDb();
        hasTable(db, encTag, true);
        final byte[] bytes = toBytes(type, value, stringEncoding);
        final byte[] data = encrypt(encryptionType(), key(), bytes, getLogger());
        final boolean success = data != null;
        if (success) {
            ContentValues v = new ContentValues(2);
            v.put(TYPE, type.ordinal());
            v.put(DATA, data);
            db.insert(encTag, v);
        }
        checkCloseDb();
    }

    private static String generateName(Context context, String encType) {
        return TextUtil.string(context.getPackageName(), "_", encType, ".sdb");
    }

    private static Result getEventType(Type type) {
        switch (type) {
            case BOOLEAN:
                return Result.GOT_BOOLEANS;
            case INT:
                return Result.GOT_INTS;
            case LONG:
                return Result.GOT_LONGS;
            case SERIALIZABLE:
                return Result.GOT_SERIALIZABLE;
            default:
                return Result.GOT_STRINGS;
        }
    }

    private static <T> byte[] toBytes(Type type, T value, String stringEncoding) {
        switch (type) {
            case BOOLEAN:
                return ByteUtil.boolToBytes((Boolean) value);
            case INT:
                return ByteUtil.intToBytes((Integer) value);
            case LONG:
                return ByteUtil.longToBytes((Long) value);
            case SERIALIZABLE:
                return toBytesFromSerializable(value);
            default:
                return TextUtil.stringToBytes((String) value, stringEncoding);
        }
    }

    private static <T> byte[] toBytesFromSerializable(T value) {
        byte[] bytes = new byte[0];
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream stream = new ObjectOutputStream(out);
            stream.writeObject(value);
            bytes = out.toByteArray();
            stream.close();
            out.close();
        } catch (Exception e) {
        }
        return bytes;
    }

    private static <T> T getValue(byte[] value, Type type, String stringEncoding) {
        switch (type) {
            case BOOLEAN:
                return (T) (Boolean) ByteUtil.bytesToBool(value);
            case INT:
                return (T) (Integer) ByteUtil.bytesToInt(value);
            case LONG:
                return (T) (Long) ByteUtil.bytesToLong(value);
            case SERIALIZABLE:
                return fromBytesSerializable(value);
            default:
                return (T) TextUtil.byteToString(value, stringEncoding);
        }
    }

    private static <T> T fromBytesSerializable(byte[] bytes) {
        T value = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bytes));
            value = (T) in.readObject();
            in.close();
        } catch (Exception e) {
        }
        return value;
    }

    private static String getEncTag(String tag, Type type, String stringEncoding) {
        final String realtag = TextUtil.string(tag, "_", type.name());
        return Base64.encodeToString(TextUtil.stringToBytes(realtag, stringEncoding), ByteUtil.BASE64_FLAGS);
    }

    private static boolean hasTable(SqlDb db, String table, boolean addIfMissing) {
        ArrayList<String> tableNames = db.getTableNames();
        if (!tableNames.contains(table)) {
            if (addIfMissing) {
                addTable(db, table, null);
            }
            return false;
        }
        return true;
    }

    private static void addTable(SqlDb db, String tag, String fqName) {
        Table table = new Table(tag);
        table.addColumn(TYPE, DataType.INTEGER);
        table.addColumn(DATA, DataType.BLOB);
        if (!TextUtil.isEmpty(fqName)) {
            table.addColumns(CLASS);
        }
        db.addTableToExistingDatabase(table);
    }

}
