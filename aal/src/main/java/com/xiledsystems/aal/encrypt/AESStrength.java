package com.xiledsystems.aal.encrypt;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public enum AESStrength implements EncrytionStrength {
    LOW(128),
    MEDIUM(192),
    HIGH(256);

    private final int bitStrength;

    AESStrength(int strength) {
        bitStrength = strength;
    }

    public int getBitLength() {
        return bitStrength;
    }

    @Override
    public AESStrength fromBits(int bitLength) {
        switch (bitLength) {
            case 192:
                return MEDIUM;
            case 256:
                return HIGH;
            default:
                return LOW;
        }
    }

    public static AESStrength fromBitLength(int bitLength) {
        return LOW.fromBits(bitLength);
    }
}
