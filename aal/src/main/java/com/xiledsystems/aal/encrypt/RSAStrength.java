package com.xiledsystems.aal.encrypt;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public enum RSAStrength implements EncrytionStrength {

    LOW(256),
    MEDIUM(512),
    HIGH(1024),
    VERY_HIGH(2048),
    ULTRA_HIGH(4096);

    private final int bitLength;

    RSAStrength(int bitLength) {
        this.bitLength = bitLength;
    }

    public int getBitLength() {
        return bitLength;
    }

    @Override
    public RSAStrength fromBits(int bitLength) {
        switch (bitLength) {
            case 512:
                return MEDIUM;
            case 1024:
                return HIGH;
            case 2048:
                return VERY_HIGH;
            case 4096:
                return ULTRA_HIGH;
            default:
                return LOW;
        }
    }

    public static RSAStrength fromBitLength(int bitLength) {
        return LOW.fromBits(bitLength);
    }
}
