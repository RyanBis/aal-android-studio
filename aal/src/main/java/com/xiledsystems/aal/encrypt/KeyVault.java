package com.xiledsystems.aal.encrypt;


import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import com.xiledsystems.aal.util.ByteUtil;
import com.xiledsystems.aal.util.Prefs;
import com.xiledsystems.aal.util.TextUtil;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


public final class KeyVault {


    public enum KeyType {
        AES,
        RSA
    }

    private final static String A = "934[@$.2fhwNWz!";
    private final static String Z = "jfew973_@#5/45";


    private static KeyVault instance;

    private final Prefs prefs;
    private final String b;
    private final String l;
    private final Key k;


    private KeyVault(Context context) {
        prefs = new Prefs(context, A);
        b = generateId(context);
        l = getOrCreateL(prefs);
        k = doKeyGeneration(b, l.getBytes(), AESStrength.HIGH);
    }

    public static KeyVault get(Context context) {
        if (instance == null) {
            instance = new KeyVault(context);
        }
        return instance;
    }


    public void putKey(String id, Key key) {
        AES aes = new AES(AESStrength.HIGH);
        if (key.isAesKey()) {
            encryptAndStore(aes, KeyStoreType.AES_KEY, id, key.getKey().getEncoded());
            encryptAndStore(aes, KeyStoreType.AES_INT, id, key.getIntegrityKey().getEncoded());
        } else {
            final PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(key.getPrivateKey().getEncoded());
            encryptAndStore(aes, KeyStoreType.RSA_PRI, id, spec.getEncoded());
            final X509EncodedKeySpec pSpec = new X509EncodedKeySpec(key.getPublicKey().getEncoded());
            encryptAndStore(aes, KeyStoreType.RSA_PUB, id, pSpec.getEncoded());
        }
    }

    public Key getKey(String id, KeyType type) {
        AES aes = new AES(AESStrength.HIGH);
        if (type == KeyType.AES) {
            return getAesKey(aes, id);
        } else {
            return getRsaKey(aes, id);
        }
    }



    private void encryptAndStore(AES aes, KeyStoreType type, String id, byte[] data) {
        Data d = aes.encryptData(k, data);
        prefs.storeString(pKey(type, id), ByteUtil.bytesToHex(d.getBytes()));
    }

    private byte[] getAndDecrypt(AES aes, KeyStoreType type, String id) {
        String sd = prefs.getString(pKey(type, id), null);
        sd = new String(ByteUtil.hexToBytes(sd));
        if (sd != null) {
            AESEncryptedData encData = new AESEncryptedData(sd);
            AESDecryptedData decData = aes.decryptData(k, encData);
            if (decData.success()) {
                return decData.getBytes();
            }
        }
        return null;
    }

    private Key getAesKey(AES aes, String id) {
        final byte[] ak = getAndDecrypt(aes, KeyStoreType.AES_KEY, id);
        final byte[] ai = getAndDecrypt(aes, KeyStoreType.AES_INT, id);
        if (ak == null || ai == null) {
            return new Key(null);
        } else {
            SecretKey key;
            SecretKey ikey;
            key = new SecretKeySpec(ak, "AES");
            ikey = new SecretKeySpec(ai, "HmacSHA256");
            return new Key(key, ikey);
        }
    }

    private Key getRsaKey(AES aes, String id) {
        final byte[] rb = getAndDecrypt(aes, KeyStoreType.RSA_PUB, id);
        final byte[] rp = getAndDecrypt(aes, KeyStoreType.RSA_PRI, id);
        PublicKey pubKey;
        PrivateKey priKey;
        if (rb == null || rp == null) {
            return new Key(null);
        } else {
            try {
                final KeyFactory kf = KeyFactory.getInstance("RSA");
                final X509EncodedKeySpec spec = new X509EncodedKeySpec(rb);
                final PKCS8EncodedKeySpec priSpec = new PKCS8EncodedKeySpec(rp);
                pubKey = kf.generatePublic(spec);
                priKey = kf.generatePrivate(priSpec);
                final KeyPair pair = new KeyPair(pubKey, priKey);
                return new Key(pair);
            } catch (Exception e) {
                e.printStackTrace();
                return new Key(null);
            }
        }
    }




    public static Key newRsaKey(String provider, RSAStrength strength) {
        KeyPair pair = null;
        try {
            final KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA", provider);
            gen.initialize(strength.getBitLength());
            pair = gen.genKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return new Key(pair);
    }

    public static Key newAesKey(String provider, AESStrength strength, int hMacByteLength, String cipher, String hMacAlgorithm) {
        PRNGFixes.apply();
        SecretKey skey = null;
        SecretKey iKey = null;
        try {
            final KeyGenerator keygen = getGenerator(cipher, provider);
            int length = strength.getBitLength();
            keygen.init(length);
            skey = keygen.generateKey();

            final byte[] integrityBytes = Encryptor.randomBytes(hMacByteLength);
            iKey = new SecretKeySpec(integrityBytes, hMacAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return new Key(skey, iKey);
    }

    public static Key newAesKey(String key, byte[] salt, AESStrength strength, int hmacByteLength, String cipher, int pbeCount, String pbeAlgorithm, String hmacAlgorithm) {
        PRNGFixes.apply();
        SecretKey skey = null;
        SecretKey iKey = null;
        try {
            final int bitLength = strength.getBitLength();
            final PBEKeySpec kspec = new PBEKeySpec(key.toCharArray(), salt, pbeCount, bitLength + (hmacByteLength * 8));
            final SecretKeyFactory kFac = SecretKeyFactory.getInstance(pbeAlgorithm);
            final byte[] keyBytes = kFac.generateSecret(kspec).getEncoded();

            final int bitEight = bitLength / 8;

            final byte[] secKeyBytes = ByteUtil.copyOfRange(keyBytes, 0, bitEight);
            final byte[] integrityBytes = ByteUtil.copyOfRange(keyBytes, bitEight, bitEight + hmacByteLength);

            skey = new SecretKeySpec(secKeyBytes, cipher);
            iKey = new SecretKeySpec(integrityBytes, hmacAlgorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return new Key(skey, iKey);
    }



    private static String pKey(KeyStoreType kType, String id) {
        return sha256(TextUtil.string(kType.kn(), id));
    }

    private static KeyGenerator getGenerator(String cipher, String provider) throws NoSuchAlgorithmException, NoSuchProviderException {
        return KeyGenerator.getInstance(cipher, provider);
    }

    private static String getOrCreateL(Prefs prefs) {
        String l = prefs.getString(Z, "");
        if (TextUtil.notEmpty(l)) {
            return l;
        }
        byte[] s = AES.generateSalt();
        l = ByteUtil.bytesToHex(s);
        prefs.storeString(Z, l);
        return l;
    }

    private static String generateId(Context context) {
        StringBuilder b = new StringBuilder();
        b.append(Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        b.append(Build.MANUFACTURER).append(Build.SERIAL).append(context.getPackageName());
        return sha512x2000(b.toString());
    }

    private static String sha512x2000(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] hashed = text.getBytes();
            int current = 0;
            while (current < 2000) {
                md.update(hashed);
                hashed = md.digest();
                current++;
            }
            return ByteUtil.bytesToHex(hashed);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String sha256(String text) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(text.getBytes());
            byte[] hashed = md.digest();
            return ByteUtil.bytesToHex(hashed);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Key doKeyGeneration(String key, byte[] salt, AESStrength strength) {
        PRNGFixes.apply();
        SecretKey skey = null;
        SecretKey iKey = null;
        try {
            final int bitLength = strength.getBitLength();
            final PBEKeySpec kspec = new PBEKeySpec(key.toCharArray(), salt, 10000, bitLength + 256);
            final SecretKeyFactory kFac = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            final byte[] keyBytes = kFac.generateSecret(kspec).getEncoded();

            final int bitEight = bitLength / 8;

            final byte[] secKeyBytes = ByteUtil.copyOfRange(keyBytes, 0, bitEight);
            final byte[] integrityBytes = ByteUtil.copyOfRange(keyBytes, bitEight, bitEight + 256 / 8);

            skey = new SecretKeySpec(secKeyBytes, "AES");
            iKey = new SecretKeySpec(integrityBytes, "HmacSHA256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return new Key(skey, iKey);
    }


    private enum KeyStoreType {

        AES_KEY("7jkG"),
        AES_INT("p4SDG"),
        RSA_PRI("qf9ngG"),
        RSA_PUB("F58vG");


        private final String kn;


        KeyStoreType(String kn) {
            this.kn = kn;
        }

        String kn() {
            return kn;
        }
    }

}
