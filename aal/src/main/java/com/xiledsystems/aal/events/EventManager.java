package com.xiledsystems.aal.events;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.AalLogger;
import com.xiledsystems.aal.util.BoxedValue;
import com.xiledsystems.aal.util.IAalLogger;
import com.xiledsystems.aal.util.TextUtil;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Singleton class which manages a priority based event system. If no priority is set, then it's FIFO (first in, first out).
 * The queue is managed in it's own thread, however, by default, the events themselves are posted on the UI thread. You can state how you want your event posted using
 * the static methods in the class {@link PostType}.<br><br>
 *
 * @see PostType#onMain()
 * @see PostType#onThread()
 * @see PostType#onCurrent()
 *
 * <br><br>
 * Note that using {@link PostType#onCurrent()} bypasses the queue completely, and should only really be used sparingly.
 * <br><br>
 * The events are posted by default at 40 eps (events per second). You can change this in {@link EventManagerConfig} using {@link EventManagerConfig#eventFps}. Remember, the
 * higher you set the eps, the more cpu cycles you take up, thereby reducing battery efficiency. 60eps is probably the highest you'd
 * want to go most of the time.
 * <br><br>
 * There are 4 ways to register listeners, which response to {@link Event}s that are posted to the system.
 * <br><br>
 * First is to register an {@link EventListener} which listens for a specific event key, an {@link Enum} value, within a category.
 * <br><br>
 * Second way, is to set a category listener, which will listen for all events in a certain category, an {@link Enum} class. The {@link EventListener} will fire
 * on all values in the category.
 * <br><br>
 * Third way, is to add {@link EventListener}s as "All Event Listeners". You can add as many as you want, though beware that this could lead to performance
 * issues if you have a lot of events, and a lot of {@link EventListener}s registered as All Event Listeners. This, obviously, will fire on ALL events passed
 * into the event system.
 * <br><br>
 * The fourth way, is to set the "Main" listener. This listens to all events using {@link EventListener}, and there can only be one main listener
 * registered at a time. This will also supercede any other {@link EventListener}s registered in the system. This should only be used if you intend to handle
 * all events within your app, without using the 3 ways mentioned above.
 * <br><br>
 * When an event is posted, it first posts to all {@link EventListener}s registered as AllEventListeners, then to registered category listeners, and then finally
 * to the appropriate {@link EventListener} for the event posted, an {@link Enum} value.
 */
@State(DevelopmentState.PRODUCTION)
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public final class EventManager implements IAalLogger {

    /**
     * Get the instance of the {@link EventManager}. If the instance has not yet been instantiated by calling this method,
     * or {@link #getInstance(EventManagerConfig)}, then a default {@link EventManagerConfig} will be used.
     *
     * @see EventManagerConfig#EventManagerConfig()
     * @return
     */
    public static EventManager getInstance() {
        if (sInstance == null) {
            sInstance = new EventManager();
            sInstance.log.logD(sInstance, () -> "Created a new instance.");
        }
        return sInstance;
    }

    /**
     * Get the instance of the {@link EventManager}, and set the given {@link EventManagerConfig} instance to the
     * manager. If it is <code>null</code>, it will be ignored.
     * @param config
     * @return
     */
    public static EventManager getInstance(EventManagerConfig config) {
        if (sInstance == null) {
            config = config != null ? config : new EventManagerConfig();
            sInstance = new EventManager(config);
            sInstance.log.logD(sInstance, () -> "Created a new instance.");
        } else if (config != null) {
            sInstance.setConfig(config);
        }
        return sInstance;
    }


    private static EventManager sInstance;
    private static EventManagerConfig DEFAULT_CONFIG = new EventManagerConfig();

    public enum CoreType {
        BROADCAST
    }

    private final static String TAG = "EventManager";

    /**
     * The default priority for all {@link EventListener}s registered.
     */
    public final static byte DEFAULT_PRIORITY = 0x1;
    private EventManagerConfig config;

    private EventThreadManager threadMan;
    private final LinkedList<Event> eventPrivateQueue = new LinkedList<>();
    private EventQueueThread eventQueueThread;
    private long queueTick;
    private EventListenerThread eventListenerThread;
    private EventRunner eventRunner;
    private static ListenerComparator sComparator = new ListenerComparator();
    private List<WrappedListener> cachedList;
    private AalLogger log;

    private boolean refTypeChanged = false;
    private int interval;
    private EventListener mainListener;

    private final List<WrappedListener> allEventListeners;
    private List<WrappedListener> allEventClone;
    private final Object allLock = new Object();
    private final ConcurrentHashMap<Class<? extends Enum>, List<WrappedListener>> categoryHandlers;
    private final Object categoryLock = new Object();
    private final ConcurrentHashMap<Enum, List<WrappedListener>> eventHandlers;
    private final Object eventLock = new Object();
    private final AtomicBoolean initialized = new AtomicBoolean(false);
    private AtomicBoolean shutDownCalled = new AtomicBoolean(false);

    private final Set<Enum> canceledEvents;
    private final Set<Enum> pushedEvents;


    private EventManager() {
        this(new EventManagerConfig());
    }

    private EventManager(EventManagerConfig config) {
        setConfig(config);
        threadMan = EventThreadManager.getInstance(getConfig().threadConfig);
        allEventListeners = Collections.synchronizedList(new ArrayList<WrappedListener>(0));
        allEventClone = new ArrayList<>();
        categoryHandlers = new ConcurrentHashMap<>(0);
        eventHandlers = new ConcurrentHashMap<>(0);
        canceledEvents = Collections.synchronizedSet(new HashSet<Enum>());
        pushedEvents = Collections.synchronizedSet(new HashSet<Enum>());
        interval = 1000 / getConfig().getDispatchFps();
        log = new AalLogger();
        initEventHandlerThreads();
    }





    /**
     * Set the {@link EventManagerConfig} for the {@link EventManager}.
     *
     * @param config
     */
    public final void setConfig(EventManagerConfig config) {
        if (this.config != null && this.config.eventListenerRefType != config.eventListenerRefType) {
            refTypeChanged = true;
            boolean threadChange = !this.config.threadConfig.equals(config.threadConfig);
            if (threadChange)
                threadMan = EventThreadManager.getInstance(config.threadConfig);
        }
        this.config = config.clone();
    }

    /**
     * Sets the EventManager to print statements to logcat.
     *
     * @param useLogging
     */
    public final void setLoggingEnabled(boolean useLogging) {
        getConfig().logging = useLogging;
    }

    /**
     * @return whether logging is enabled or not
     */
    @Override
    public final synchronized boolean isLoggingEnabled() {
        return getConfig().logging;
    }

    /**
     * Set the "main" listener for all Events to be passed to. There can only be one
     * MainListener set at a time. Events are passed to the MainListener first, then passed
     * on to {@link EventListener} registered with {@link #registerListener(Enum, EventListener)}
     *
     * @param listener
     */
    public final void setMainListener(final EventListener listener) {
        if (!shutDownCalled.get()) {
            eventListenerThread.post(new Runnable() {
                @Override
                public void run() {
                    mainListener = listener;
                    if (listener != null) {
                        log.logDMethod(EventManager.this, () ->  TextUtil.string("Main Listener ", listener.getClass().getSimpleName(), " registered."));
                    } else {
                        log.logDMethod(EventManager.this, () -> TextUtil.string("Main Listener unregistered."));
                    }
                }
            });
        }
    }

    /**
     * Overload of {@link #registerAllEventListener(EventListener, byte)}, which sets the priority of the listener
     * to {@link #DEFAULT_PRIORITY}.
     *
     * @param listener
     */
    public final void registerAllEventListener(final EventListener listener) {
        registerAllEventListener(listener, DEFAULT_PRIORITY);
    }

    /**
     * Overload of {@link #registerAllEventListener(EventListener, byte, EventManagerConfig.ListenerReferenceType)}, which sets the
     * priority to {@link #DEFAULT_PRIORITY}. This sets the {@link EventManagerConfig.ListenerReferenceType}, and will not change the
     * type, even if the setting is changed in {@link EventManagerConfig}, and re-applied to the {@link EventManager}.
     *
     * @param listener
     * @param refType
     */
    public final void registerAllEventListener(final EventListener listener, EventManagerConfig.ListenerReferenceType refType) {
        registerAllEventListener(listener, DEFAULT_PRIORITY, refType);
    }

    /**
     * Register an {@link EventListener} to listen to all Events posted, with the given priority.
     *
     * @param listener
     * @param priority
     */
    public final void registerAllEventListener(final EventListener listener, final byte priority) {
        registerAllEventListener_private(listener, priority, getConfig().eventListenerRefType, true);
    }

    /**
     * Register an {@link EventListener} to listen to all Events posted, with the given priority, and using the given
     * {@link EventManagerConfig.ListenerReferenceType} (which is not allowed to change).
     *
     * @param listener
     * @param priority
     * @param refType
     */
    public final void registerAllEventListener(final EventListener listener, final byte priority, EventManagerConfig.ListenerReferenceType refType) {
        registerAllEventListener_private(listener, priority, refType, false);
    }

    /**
     * Unregister an {@link EventListener} that is listening for all events.
     *
     * @param listener
     */
    public final void unregisterAllEventListener(final EventListener listener) {
        if (!shutDownCalled.get()) {
            if (listener != null) {
                synchronized (allLock) {
                    removeAndSortAllEventListener(wrap(listener, DEFAULT_PRIORITY, getConfig().eventListenerRefType, true));
                    log.logDMethod(EventManager.this, () -> TextUtil.string("EventListener ", listener.getClass().getSimpleName(), " queued to be unregistered. ", allEventListeners.size(), " total listeners registered."));
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("Unable to unregister EventListener, as it is null!"));
            }
        }
    }

    /**
     * Overload of {@link #registerCategoryListener(Class, EventListener, byte)}, with the priority set to {@link #DEFAULT_PRIORITY}.
     *
     * @param category
     * @param listener
     */
    public final void registerCategoryListener(final Class<? extends Enum> category, final EventListener listener) {
        registerCategoryListener(category, listener, DEFAULT_PRIORITY);
    }

    /**
     * Overload of {@link #registerCategoryListener(Class, EventListener, EventManagerConfig.ListenerReferenceType)}, with the priority set to {@link #DEFAULT_PRIORITY}.
     *
     * @param category
     * @param listener
     */
    public final void registerCategoryListener(final Class<? extends Enum> category, final EventListener listener, EventManagerConfig.ListenerReferenceType refType) {
        registerCategoryListener(category, listener, DEFAULT_PRIORITY, refType);
    }

    /**
     * Register an {@link EventListener} to listen to all Events posted to a category. This listens for the Enum
     * class, rather than a specific Enum value.
     *
     * @param category
     * @param listener
     * @param priority
     */
    public final void registerCategoryListener(final Class<? extends Enum> category, final EventListener listener, final byte priority) {
        registerCategoryListener_private(category, listener, priority, getConfig().eventListenerRefType, true);
    }

    /**
     * Register an {@link EventListener} to listen to all Events posted to a category with the given priority, and {@link EventManagerConfig.ListenerReferenceType}.
     * This listens for the Enum class, rather than a specific Enum value.
     *
     * @param category
     * @param listener
     * @param priority
     * @param refType
     */
    public final void registerCategoryListener(final Class<? extends Enum> category, final EventListener listener, final byte priority, EventManagerConfig.ListenerReferenceType refType) {
        registerCategoryListener_private(category, listener, priority, refType, false);
    }

    /**
     * Unregister a category listener.
     *
     * @param category
     * @param listener
     */
    public final void unregisterCategoryListener(final Class<? extends Enum> category, final EventListener listener) {
        if (!shutDownCalled.get()) {
            if (listener != null) {
                synchronized (categoryLock) {
                    boolean removed;
                    List<WrappedListener> list = categoryHandlers.get(category);
                    if (list == null) {
                        log.logDMethod(EventManager.this, () -> TextUtil.string("No category listener has been unregistered, as there are no listeners registered for category ", category.getSimpleName()));
                        return;
                    }
                    removed = removeAndSortFromCollection(list, wrap(listener, DEFAULT_PRIORITY, getConfig().eventListenerRefType, true));
                    if (removed) {
                        log.logDMethod(EventManager.this, () -> TextUtil.string("EventListener ", listener.getClass().getSimpleName(), " unregistered for the ", category.getSimpleName(), " category."));
                    } else {
                        log.logWMethod(EventManager.this, () -> TextUtil.string("EventListener ", listener.getClass().getSimpleName(), " was not registered for the ", category.getSimpleName(), " category." +
                                " No category listener was removed."));
                    }
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("No category listener has been unregistered, as the provided EventListener is null!"));
            }
        }
    }

    /**
     * Unregister all category listeners registered to the given category.
     *
     * @param category
     */
    public final void unregisterCategoryListeners(final Class<? extends Enum> category) {
        if (!shutDownCalled.get()) {
            synchronized (categoryLock) {
                List<WrappedListener> list = categoryHandlers.remove(category);
                if (list == null) {
                    log.logWMethod(EventManager.this, () -> TextUtil.string("No category listeners were unregistered, as there weren't any registered for the ", category.getSimpleName(), " category."));
                } else {
                    log.logDMethod(EventManager.this, () -> TextUtil.string(list.size(), " listeners were unregistered from the ", category.getSimpleName(), " category."));
                }
            }
        }
    }

    /**
     * Overload of {@link #registerListener(Enum, EventListener, byte)}, which uses a priority of {@link #DEFAULT_PRIORITY}.
     *
     * @param category
     * @param listener
     * @param <E>
     */
    public final <E extends Enum> void registerListener(final E category, final EventListener listener) {
        registerListener(category, listener, DEFAULT_PRIORITY);
    }

    /**
     * Overload of {@link #registerListener(Enum, EventListener, byte, EventManagerConfig.ListenerReferenceType)}, which uses a priority of {@link #DEFAULT_PRIORITY}.
     *
     * @param category
     * @param listener
     * @param <E>
     */
    public final <E extends Enum> void registerListener(final E category, final EventListener listener, EventManagerConfig.ListenerReferenceType refType) {
        registerListener(category, listener, DEFAULT_PRIORITY, refType);
    }

    /**
     * Register an {@link EventListener} to listen for Events of the {@link E} category with the given priority
     *
     * @param category
     * @param listener
     * @param priority
     */
    public final <E extends Enum> void registerListener(final E category, final EventListener listener, final byte priority) {
        registerListener_private(category, listener, priority, getConfig().eventListenerRefType, true);
    }

    /**
     * Register an {@link EventListener} to listen for Events of the {@link E} category with the given priority, and {@link EventManagerConfig.ListenerReferenceType}.
     *
     * @param category
     * @param listener
     * @param priority
     * @param refType
     */
    public final <E extends Enum> void registerListener(final E category, final EventListener listener, final byte priority, EventManagerConfig.ListenerReferenceType refType) {
        registerListener_private(category, listener, priority, refType, false);
    }

    /**
     * Unregister an {@link EventListener} from the {@link E} category
     *
     * @param category
     * @param listener
     * @param <E>
     */
    public final <E extends Enum> void unregisterListener(final E category, final EventListener listener) {
        if (!shutDownCalled.get()) {
            if (listener != null) {
                synchronized (eventLock) {
                    List<WrappedListener> list = eventHandlers.get(category);
                    if (list != null) {
                        removeAndSortFromCollection(list, wrap(listener, DEFAULT_PRIORITY, getConfig().eventListenerRefType, true));
                    }
                    if (isLoggingEnabled()) {
                        ConcurrentHashMap<Enum, List<WrappedListener>> map = new ConcurrentHashMap<>(eventHandlers);
                        BoxedValue<Integer> size = new BoxedValue<>(0);
                        List<Enum> keys = Collections.list(map.keys());
                        for (Enum e : keys) {
                            size.value += map.get(e).size();
                        }
                        BoxedValue<Integer> listSize = new BoxedValue<>(0);
                        if (map.get(category) != null) {
                            listSize.value = map.get(category).size();
                        }
                        int keySize = keys.size();
                        log.logDMethod(EventManager.this, () -> TextUtil.string("EventListener unregistered from category ", category.toString(), ".\n", "Total listeners for this category: ",
                                listSize, "\nTotal mapped listeners: ", size, "\nTotal categories: ", keySize));
                    }
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("Unable to unregister EventListener from category ", category.toString(), ", as the EventListener is null!"));
            }
        }
    }

    /**
     * Unregister the {@link EventListener} from all Event Categories
     *
     * @param listener
     */
    public final void unregisterListener(final EventListener listener) {
        if (!shutDownCalled.get()) {
            if (listener != null) {
                synchronized (eventLock) {
                    final List<Enum> keys = Collections.list(eventHandlers.keys());
                    for (Enum e : keys) {
                        List<WrappedListener> list = eventHandlers.get(e);
                        if (list != null) {
                            removeAndSortFromCollection(list, wrap(listener, DEFAULT_PRIORITY, getConfig().eventListenerRefType, true));
                        }
                    }
                    if (isLoggingEnabled()) {
                        final ConcurrentHashMap<Enum, List<WrappedListener>> map = new ConcurrentHashMap<>(eventHandlers);
                        BoxedValue<Integer> size = new BoxedValue<>(0);
                        final List<Enum> keys_l = Collections.list(map.keys());
                        for (Enum e : keys_l) {
                            size.value += map.get(e).size();
                        }
                        int keySize = keys_l.size();
                        log.logDMethod(EventManager.this, () -> TextUtil.string("EventListener unregistered from all categories.\nTotal mapped listeners: ", size, "\nTotal categories: ", keySize));
                    }
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("Unable to unregister EventListener from any category, as the EventListener is null!"));
            }
        }
    }

    /**
     * Unregister all {@link EventListener}s registered to the {@link E} category
     *
     * @param category
     * @param <E>
     */
    public final <E extends Enum> void unregisterListeners(final E category) {
        if (!shutDownCalled.get()) {
            if (category != null) {
                synchronized (eventLock) {
                    List<WrappedListener> list = eventHandlers.get(category);
                    if (list != null) {
                        list.clear();
                        eventHandlers.remove(category);
                    }
                    if (isLoggingEnabled()) {
                        final ConcurrentHashMap<Enum, List<WrappedListener>> map = new ConcurrentHashMap<>(eventHandlers);
                        BoxedValue<Integer> size = new BoxedValue<>(0);
                        final List<Enum> keys = Collections.list(map.keys());
                        for (Enum e : keys) {
                            size.value += map.get(e).size();
                        }
                        BoxedValue<Integer> listSize = new BoxedValue<>(0);
                        if (map.get(category) != null) {
                            listSize.value = map.get(category).size();
                        }
                        int keySize = keys.size();
                        log.logDMethod(EventManager.this, () -> TextUtil.string("All EventListeners have been unregistered for category ", category.toString(), ".\n", "Total listeners for this category: ",
                                listSize, "\nTotal mapped listeners: ", size, "\nTotal categories: ", keySize));
                    }
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("Unable to unregister any EventListeners, as the category Enum is null!"));
            }
        }
    }

    /**
     * Unregister all {@link EventListener}s
     */
    public final void unregisterEventListeners() {
        if (!shutDownCalled.get()) {
            synchronized (eventLock) {
                eventHandlers.clear();
            }
        }
    }

    /**
     * Unregisters all {@link EventListener}s. This also
     * clears out the event queue.
     */
    public final void unregisterAllListeners() {
        if (!shutDownCalled.get()) {
            synchronized (eventLock) {
                eventHandlers.clear();
                mainListener = null;
                eventPrivateQueue.clear();
                log.logDMethod(EventManager.this, () -> TextUtil.string("All listeners cleared from EventManager."));
            }
        }
    }

    /**
     * Set the eps at which events are dispatched. The default is 40eps. (events per second)
     *
     * @param eps
     */
    public final void setEventEPS(int eps) {
        getConfig().dispatchInterval = eps;
        getConfig().eventFps = null;
        interval = 1000 / getConfig().getDispatchFps();
        log.logDMethod(this, () -> TextUtil.string("Event cycle set to ", getConfig().getDispatchFps(), " eps."));
    }

    /**
     * @return the current eps of the event dispatch handler
     */
    public final int getCurrentEPS() {
        return getConfig().getDispatchFps();
    }

    /**
     * Set the default PostType for the Manager. The default is {@link PostOnMain}, so
     * events are posted on the UI thread. This applies when using any method which
     * doesn't take {@link PostType} as an argument.
     *
     * @param type
     */
    public final void setDefaultPostType(PostType type) {
        getConfig().postType = type;
        log.logDMethod(this, () -> TextUtil.string("Default PostType set to ", type.getType().name()));
    }

    /**
     * @return the current default {@link PostType}
     */
    public final PostType getDefaultPostType() {
        return getConfig().postType;
    }

    /**
     * This will initialize the backing {@link EventHandler} for the event manager. This is
     * done automatically when the first event is posted.
     */
    public final void init() {
        shutDownCalled.set(false);
        if (!initialized.get()) {
            if (!eventQueueThread.isAlive()) {
                initEventHandlerThreads();
            }
            eventRunner = new EventRunner();
            initialized.set(true);
            postEventUpdate();
            log.logDMethod(this, () -> "Event thread initialized, ready to handle events.");
        } else {
            log.logDMethod(this, () -> "init() called when already initialized.");
        }
    }


    /**
     * @return whether the {@link EventManager} is initialized or not (can execute {@link Event}s).
     */
    public final boolean initialized() {
        return initialized.get();
    }

    /**
     * Shuts down the backing thread for the Event Manager. This also clears all listeners
     * registered.<br></br>
     * To use the EventManager again, you will have to call {@link #init()} first
     */
    public synchronized final void shutdown() {
        log.logD(this, "shutdown() called...");
        shutDownCalled.set(true);
        eventQueueThread.quit();
        eventListenerThread.quit();
        threadMan.dispose();
        clearAllListeners();
        initialized.set(false);
        log.logDMethod(this, () -> "Event loop shutdown. No events will be handled until init() is called.");
    }

    /**
     * Broadcast an event to all categories in the EventManager. Posts to
     * the default {@link PostType}
     *
     * @param data
     */
    public final void broadcastEvent(Event data) {
        broadcastEvent(getConfig().postType, data);
    }

    /**
     * Broadcast an event to all categories in the EventManager, using the
     * specified {@link PostType}
     *
     * @param type
     * @param data
     */
    public final void broadcastEvent(PostType type, final Event data) {
        checkInit();
        EventListener oldListener = mainListener;
        if (mainListener != null) {
            switch (type.getType()) {
                case CURRENT:
                    mainListener.onEvent(EventFactory.newEvent(CoreType.BROADCAST, getConfig(), data));
                    break;
                case THREAD:
                    threadMan.execute((Runnable) () -> mainListener.onEvent(EventFactory.newEvent(CoreType.BROADCAST, getConfig(), data)));
                    break;
                default:
                    EventThreadManager.getUiHandler().post(() -> mainListener.onEvent(EventFactory.newEvent(CoreType.BROADCAST, getConfig(), data)));
                    break;
            }
        }
        mainListener = null;
        final List<Enum> keys = Collections.list(eventHandlers.keys());
        for (Enum e : keys) {
            postEvent(e, type, data);
        }
        mainListener = oldListener;
    }

    /**
     * Post an event to the EventManager which has no data attached, and using defaults
     * from the {@link EventManagerConfig} instance set in this manager.
     *
     * @param category
     * @param <E>
     */
    public final <E extends Enum> void postEvent(E category) {
        postEvent(EventFactory.newEvent(category, getConfig()));
    }

    /**
     * The same as {@link #postEvent(Enum)}, only adds data to the {@link Event}.
     *
     * @param category
     * @param data
     * @param <E>
     */
    public final <E extends Enum> void postEvent(E category, Object... data) {
        postEvent(EventFactory.newEvent(category, getConfig(), data));
    }

    /**
     * Post an event to the EventManager which has no data attached, and using defaults
     * from the {@link EventManagerConfig} instance set in this manager, which is only
     * allowed to be executed by the specified amount (how many listeners will end up
     * calling {@link EventListener#onEvent(Event)}).
     *
     * @param category
     * @param <E>
     */
    public final <E extends Enum> void postEventMax(int maxExecutions, E category) {
        postEvent(EventFactory.newEventMax(maxExecutions, category, getConfig()));
    }

    /**
     * The same as {@link #postEventMax(int, Enum)}, only adds data to the {@link Event}.
     *
     * @param category
     * @param data
     * @param <E>
     */
    public final <E extends Enum> void postEventMax(int maxExecutions, E category, Object... data) {
        postEvent(EventFactory.newEventMax(maxExecutions, category, getConfig(), data));
    }

    /**
     * Post an event to the EventManager with the given category, and delayed by
     * the specified amount.
     *
     * @param category
     * @param delay
     * @param <E>
     */
    public final <E extends Enum> void postDelayedEvent(E category, long delay) {
        postEvent(EventFactory.newDelayedEvent(category, delay, getConfig()));
    }

    /**
     * Same as {@link #postDelayedEvent(Enum, long)} which allows you to also
     * add data to the Event.
     *
     * @param category
     * @param delay
     * @param data
     * @param <E>
     */
    public final <E extends Enum> void postDelayedEvent(E category, long delay, Object... data) {
        postEvent(EventFactory.newDelayedEvent(category, delay, getConfig(), data));
    }

    /**
     * Post an event to the EventManager with the given category, and delayed by
     * the specified amount.
     *
     * @param category
     * @param delay
     * @param <E>
     */
    public final <E extends Enum> void postDelayedEventMax(int maxExecutions, E category, long delay) {
        postEvent(EventFactory.newDelayedEventMax(maxExecutions, category, delay, getConfig()));
    }

    /**
     * Same as {@link #postDelayedEvent(Enum, long)} which allows you to also
     * add data to the Event.
     *
     * @param category
     * @param delay
     * @param data
     * @param <E>
     */
    public final <E extends Enum> void postDelayedEventMax(int maxExecutions, E category, long delay, Object... data) {
        postEvent(EventFactory.newDelayedEventMax(maxExecutions, category, delay, getConfig(), data));
    }

    /**
     * Post an {@link Event} to the {@link EventManager} using the specified {@link com.xiledsystems.aal.events.Event.Builder}.
     *
     * @param builder
     */
    public final void postEvent(Event.Builder builder) {
        postEvent(builder.build(getConfig()));
    }

    /**
     * Cancel an {@link Event} posted to this manager. This will only work if the event is still in the queue
     * waiting to be processed.
     *
     * @param event
     */
    public final void cancelEvent(final Enum event) {
        Event e;
        final int size = eventPrivateQueue.size();
        boolean found = false;
        for (int i = 0; i < size; i++) {
            e = eventPrivateQueue.get(i);
            if (e.getEventKey().equals(event)) {
                log.logD("Canceled Event key " + e.getEventKey());
                found = true;
                e.canceled = true;
            }
        }
        if (!found && pushedEvents.contains(event)) {
            canceledEvents.add(event);
        }
    }

    /**
     * Returns whether or not the {@link EventManager} has an {@link Event} in the queue for the given {@link Enum} value.
     */
    public final boolean hasEvent(Enum event) {
        for (Event e : eventPrivateQueue) {
            if (!e.canceled && e.getEventKey().equals(event)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether or not the {@link EventManager} had {@link #shutdown()} called or not. Note that this gets cleared if {@link #getInstance()}, or
     * {@link #getInstance(EventManagerConfig)} is called.
     */
    public final boolean isShuttingDown() {
        return shutDownCalled.get();
    }

    /**
     * Attempts to cast enum from the captured class. If it fails, null will
     * be returned. This method simply attempts to cast, and is surrounded
     * with a try/catch.
     *
     * @param event
     * @return
     * @deprecated Use {@link #getEventKey(Enum)} instead.
     */
    @Deprecated
    public static <E extends Enum> E getCategory(Enum event) {
        E action = null;
        try {
            action = (E) event;
        } catch (Exception e) {
            getInstance().log.logEMethod(getInstance(), () -> TextUtil.string("Failed to cast Enum. Enum class: ", event.getClass().getSimpleName()));
        }
        return action;
    }

    /**
     * Attempts to cast enum from the captured class. If it fails, null will
     * be returned. This method simply attempts to cast, and is surrounded
     * with a try/catch.
     *
     * @param event
     * @return
     */
    public static <E extends Enum> E getEventKey(Enum event) {
        E action = null;
        try {
            action = (E) event;
        } catch (Exception e) {
            getInstance().log.logEMethod(getInstance(), () -> TextUtil.string("Failed to cast Enum. Enum class: ", event.getClass().getSimpleName()));
        }
        return action;
    }

    /**
     * Attempts to cast enum from the captured class. If it fails, null will
     * be returned. This method first checks if the event Enum is an instance
     * of the supplied class, then casts.
     *
     * @param enumClass
     * @param event
     * @return
     * @deprecated Use {@link #getEventKey(Class, Enum)} instead.
     */
    @Deprecated
    public static <E extends Enum> E getCategory(Class<E> enumClass, Enum event) {
        E action = null;
        if (enumClass.isInstance(event)) {
            action = enumClass.cast(event);
        }
        return action;
    }

    /**
     * Attempts to cast enum from the captured class. If it fails, null will
     * be returned. This method first checks if the event Enum is an instance
     * of the supplied class, then casts.
     *
     * @param enumClass
     * @param event
     * @return
     */
    public static <E extends Enum> E getEventKey(Class<E> enumClass, Enum event) {
        E action = null;
        if (enumClass.isInstance(event)) {
            action = enumClass.cast(event);
        }
        return action;
    }

    /**
     * Clears all {@link EventListener}s registered as all event listeners.
     */
    public final void clearAllEventListeners() {
        synchronized (allLock) {
            allEventListeners.clear();
        }
    }

    /**
     * Clears all {@link EventListener}s registered as category listeners.
     */
    public final void clearCategoryEventListeners() {
        synchronized (categoryLock) {
            categoryHandlers.clear();
        }
    }

    /**
     * Clears all {@link EventListener}s registered for a single {@link Event}.
     */
    public final void clearEventListeners() {
        synchronized (eventLock) {
            eventHandlers.clear();
        }
    }

    /**
     * Clears all {@link EventListener}s registered to the {@link EventManager}.
     */
    public final void clearAllListeners() {
        synchronized (allLock) {
            allEventListeners.clear();
        }
        synchronized (categoryLock) {
            categoryHandlers.clear();
        }
        synchronized (eventLock) {
            eventHandlers.clear();
        }
        log.logD(this, "All listeners cleared.");
    }

    /**
     * Returns a cloned version of the {@link EventManagerConfig} this {@link EventManager} is using currently.
     */
    public final EventManagerConfig getConfigClone() {
        return getConfig().clone();
    }





    final void update(long curTime) {
        queueTick++;
        long maxTime = curTime + getConfig().maxDispatchLength;
        while (eventPrivateQueue.size() > 0 && maxTime > System.currentTimeMillis()) {
            int index = getNextReadyIndex(curTime);
            if (index != -1) {
                handleEventDispatch(index);
                cleanUpDeletedListeners();
            } else {
                // If we got -1, then there are no events ready to be dispatched. Return here to avoid
                // needless while loop iterations
                return;
            }
        }
    }



    private <E extends Enum> void registerListener_private(final E category, final EventListener listener, final byte priority, EventManagerConfig.ListenerReferenceType refType, boolean isFlexible) {
        if (!shutDownCalled.get()) {
            if (listener != null) {
                synchronized (eventLock) {
                    List<WrappedListener> list = eventHandlers.get(category);
                    if (list == null) {
                        list = Collections.synchronizedList(new ArrayList<WrappedListener>());
                    }
                    addAndSortToCollection(list, wrap(listener, priority, refType, isFlexible));
                    eventHandlers.put(category, list);
                    if (isLoggingEnabled()) {
                        final ConcurrentHashMap<Enum, List<WrappedListener>> map = new ConcurrentHashMap<>(eventHandlers);
                        BoxedValue<Integer> size = new BoxedValue<>(0);
                        final List<Enum> keys = Collections.list(map.keys());
                        for (Enum e : keys) {
                            size.value += map.get(e).size();
                        }
                        int keySize = keys.size();
                        final BoxedValue<List<WrappedListener>> cachedList = new BoxedValue<>(list);
                        log.logDMethod(EventManager.this, () -> TextUtil.string("EventListener registered to category ", category.toString(), ".\n", "Total listeners for this category: ", cachedList.value.size(),
                                "\nTotal mapped listeners: ", size, "\nTotal categories: ", keySize));
                    }
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("Unable to register EventListener to category ", category.toString(), ", as the EventListener is null!"));
            }
        }
    }

    private void registerCategoryListener_private(final Class<? extends Enum> category, final EventListener listener, final byte priority, EventManagerConfig.ListenerReferenceType refType, boolean isFlexible) {
        if (!shutDownCalled.get()) {
            if (listener != null) {
                synchronized (categoryLock) {
                    List<WrappedListener> list = categoryHandlers.get(category);
                    if (list == null) {
                        list = Collections.synchronizedList(new ArrayList<>());
                    }
                    addAndSortToCollection(list, wrap(listener, priority, refType, isFlexible));
                    categoryHandlers.put(category, list);
                    log.logDMethod(EventManager.this, () -> TextUtil.string("EventListener ", listener.getClass().getSimpleName(), " registered for the ", category.getSimpleName(), " category.",
                            getCategoryListenerSize(), " total category listeners registered."));
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("Failed to register category listener ", category.getSimpleName(), " because the EventListener is null!"));
            }
        }
    }

    private void registerAllEventListener_private(final EventListener listener, final byte priority, EventManagerConfig.ListenerReferenceType refType, boolean flexible) {
        if (!shutDownCalled.get()) {
            if (listener != null) {
                synchronized (allLock) {
                    addAndSortAllEventListener(wrap(listener, priority, refType, flexible));
                    log.logDMethod(EventManager.this, () -> TextUtil.string("AllEventListener ", listener.getClass().getSimpleName(), " registered with a priority of ", priority, ". ",
                            allEventListeners.size(), " total listeners registered."));
                }
            } else {
                log.logEMethod(this, () -> TextUtil.string("AllEventListener failed to register, as it is null!"));
            }
        }
    }

    private void cleanUpDeletedListeners() {
        synchronized (allLock) {
            int size = allEventListeners.size();
            for (int i = size - 1; i >= 0; i--) {
                if (allEventListeners.get(i).mDeleted.get()) {
                    allEventListeners.remove(i);
                }
            }
        }
        final EventManagerConfig.ListenerReferenceType refType = getConfig().eventListenerRefType;
        if (refTypeChanged) {
            updateRefType(allEventListeners, refType);
        }
        synchronized (categoryLock) {
            final List<Class<? extends Enum>> keys = Collections.list(categoryHandlers.keys());
            for (Class c : keys) {
                List<WrappedListener> list = categoryHandlers.get(c);
                int size = list.size();
                for (int i = size - 1; i >= 0; i--) {
                    if (list.get(i).mDeleted.get()) {
                        list.remove(i);
                    }
                }
            }
        }
        if (refTypeChanged) {
            final List<Class<? extends Enum>> keys = Collections.list(categoryHandlers.keys());
            for (Class c : keys) {
                updateRefType(categoryHandlers.get(c), refType);
            }
        }
        synchronized (eventLock) {
            final List<Enum> keys = Collections.list(eventHandlers.keys());
            for (Enum e : keys) {
                List<WrappedListener> list = eventHandlers.get(e);
                int size = list.size();
                for (int i = size -1; i >= 0; i--) {
                    if (list.get(i).mDeleted.get()) {
                        list.remove(i);
                    }
                }
            }
        }
        if (refTypeChanged) {
            final List<Enum> keys = Collections.list(eventHandlers.keys());
            for (Enum e : keys) {
                updateRefType(eventHandlers.get(e), refType);
            }
            refTypeChanged = false;
        }
    }

    private void checkInit() {
        if (!initialized.get()) {
            init();
        } else if (!eventQueueThread.isAlive()) {
            log.logWMethod(this, () -> "Event queue thread died. Re-initializing handler threads.");
            initialized.set(false);
            init();
        }
    }

    private void initEventHandlerThreads() {
        eventQueueThread = new EventQueueThread();
        eventListenerThread = new EventListenerThread();
    }

    private void postEvent(final Event event) {
        checkInit();
        if (event.isDelayed()) {
            event.setAddTime(System.currentTimeMillis());
        }
        if (event.postType().getType() == PostType.Type.CURRENT) {
            if (event.isDelayed()) {
                if (getConfig().threadConfig.unitTest) {
                    doPostEvent(event);
                } else {
                    new Handler(Looper.myLooper()).postDelayed(() -> doPostEvent(event), event.delay());
                }
            } else {
                doPostEvent(event);
            }
        } else {
            log.logDMethod(this, () -> getExecutionString(event));
            pushNewEvent(event);
        }
    }

    private void pushNewEvent(final Event event) {
        if (eventQueueThread.isOnEventUpdateThread()) {
            eventPrivateQueue.add(event);
        } else {
            pushedEvents.add(event.getEventKey());
            eventQueueThread.post(() -> {
                if (canceledEvents.contains(event.getEventKey())) {
                    canceledEvents.remove(event.getEventKey());
                    log.logD("Canceled Event " + event.getEventKey());
                } else {
                    eventPrivateQueue.add(event);
                }
                pushedEvents.remove(event.getEventKey());
            });
        }
    }

    private int getCategoryListenerSize() {
        int count = 0;
        final List<Class<? extends Enum>> keys = Collections.list(categoryHandlers.keys());
        for (Class<? extends Enum> c : keys) {
            count += categoryHandlers.get(c).size();
        }
        return count;
    }

    private synchronized void processEvent(Event event) {
        if (mainListener == null && eventHandlers.size() == 0 && allEventListeners.size() == 0 && categoryHandlers.size() == 0) {
            log.logEMethod(this, () -> "Event tried to post, but no EventListeners have been registered! No one to hear the event!");
            return;
        }
        if (!handleMainEvent(event)) {
            if (!handleAllEvents(event)) {
                if (!handleCategoryEvents(event)) {
                    handleEventHandlers(event);
                }
            }
        }
    }

    private boolean handleMainEvent(Event event) {
        if (mainListener != null) {
            mainListener.onEvent(event);
            log.logDMethod(this, () -> TextUtil.string("Event dispatched with category ", event.getEventKey().toString(), " on the main EventListener."));
            return true;
        }
        return false;
    }

    private boolean handleAllEvents(Event event) {
        if (allEventListeners.size() > 0) {
            synchronized (allLock) {
                final boolean limited = event.isLimited();
                allEventClone = new ArrayList<>(allEventListeners);
                Iterator<WrappedListener> it = allEventClone.iterator();
                EventListener listener;
                while (it.hasNext()) {
                    if (shutDownCalled.get()) {
                        log.logDMethod(this, () -> "Shutdown called while handling events. All future events will be ignored.");
                        return true;
                    }
                    listener = it.next();
                    if (limited) {
                        if (event.hasMore()) {
                            if (listener.onEvent(event)) {
                                event.increaseExecution(listener);
                            }
                        } else {
                            log.logDMethod(this, () -> TextUtil.string("Event executed ", event.currentExecutions(), " times of the requested ", event.maxExecutions(), " maximum."));
                            return true;
                        }
                    } else {
                        listener.onEvent(event);
                    }
                }
                log.logDMethod(this, () -> TextUtil.string("Event dispatched with event key ", event.getEventKey().toString(), " on ", allEventListeners.size(), " AllEventListeners."));
            }
        }
        return false;
    }

    private boolean handleCategoryEvents(Event event) {
        if (categoryHandlers.size() > 0) {
            synchronized (categoryLock) {
                final boolean limited = event.isLimited();
                // Grab the real list of listeners from the category handler map. We pull this out initially to avoid
                // a null pointer exception when we go to create a cached list from the real list.
                List<WrappedListener> oList = categoryHandlers.get(event.getEventKey().getClass());
                if (oList == null)
                    oList = new ArrayList<>(0);
                final List<WrappedListener> list = new ArrayList<>(oList);
                if (list.size() > 0) {
                    Iterator<WrappedListener> it = list.iterator();
                    EventListener listener;
                    while (it.hasNext()) {
                        if (shutDownCalled.get()) {
                            log.logDMethod(this, () -> "Shutdown called while handling events. All future events will be ignored.");
                            return true;
                        }
                        listener = it.next();
                        if (limited) {
                            if (event.hasMore()) {
                                if (listener.onEvent(event)) {
                                    event.increaseExecution(listener);
                                }
                            } else {
                                log.logDMethod(this, () -> TextUtil.string("Event executed ", event.currentExecutions(), " times of the requested ", event.maxExecutions(), " maximum."));
                                return true;
                            }
                        } else {
                            listener.onEvent(event);
                        }
                    }
                    log.logDMethod(this, () -> TextUtil.string("Event dispatched to all category EventListeners for event key ", event.getEventKey().getClass().getSimpleName()));
                } else {
                    log.logWMethod(this, () -> TextUtil.string("Attempt to dispatch event to all category EventListeners failed, as there are no EventListeners registered for" +
                            " category ", event.getEventKey().getClass().getSimpleName()));
                }
            }
        }
        return false;
    }

    private void handleEventHandlers(Event event) {
        if (eventHandlers.size() > 0) {
            synchronized (eventLock) {
                final boolean limited = event.isLimited();
                boolean has = eventHandlers.containsKey(event.getEventKey());
                if (has) {
                    cachedList = new ArrayList<>(eventHandlers.get(event.getEventKey()));
                }
                if (has && cachedList.size() > 0) {
                    Iterator<WrappedListener> it = cachedList.iterator();
                    EventListener listener;
                    while (it.hasNext()) {
                        if (shutDownCalled.get()) {
                            log.logDMethod(this, () -> "Shutdown called while handling events. All future events will be ignored.");
                            return;
                        }
                        listener = it.next();
                        if (limited) {
                            if (event.hasMore()) {
                                if (listener.onEvent(event)) {
                                    event.increaseExecution(listener);
                                }
                            } else {
                                log.logDMethod(this, () -> TextUtil.string("Event executed ", event.currentExecutions(), " times of the requested ", event.maxExecutions(), " maximum."));
                                return;
                            }
                        } else {
                            listener.onEvent(event);
                        }
                    }
                    log.logDMethod(this, () -> TextUtil.string("Event dispatched to all EventListeners for event key ", event.getEventKey().toString()));
                } else {
                    log.logWMethod(this, () -> TextUtil.string("Attempt to dispatch event on EventListeners failed, as there are no EventListeners registered for event key ", event.getEventKey().toString()));
                }
            }
        }
    }

    private void doPostEvent(final Event eventPrivate) {
        if (eventPrivate != null) {
            switch (eventPrivate.postType().getType()) {
                case CURRENT:
                    log.logDMethod(this, () -> TextUtil.string("Dispatching event key ", eventPrivate.getEventKey().toString(), " on the current thread. [Queue bypassed]"));
                    processEvent(eventPrivate);
                    break;
                case THREAD:
                    log.logDMethod(this, () -> TextUtil.string("Dispatching event key ", eventPrivate.getEventKey().toString(), " on a background thread."));
                    EventThreadManager.getInstance(true).execute(new EventProcessor(eventPrivate));
                    break;
                case MAIN:
                    log.logDMethod(this, () -> TextUtil.string("Dispatching event key ", eventPrivate.getEventKey().toString(), " on the main (UI) thread."));
                    EventThreadManager.getUiHandler().post(new EventProcessor(eventPrivate));
                    break;
            }
        } else {
            log.logEMethod(this, () -> "Attempted to dispatch null event!");
        }
    }

    private void addAndSortAllEventListener(WrappedListener listener) {
        if (!allEventListeners.contains(listener)) {
            allEventListeners.add(listener);
            Collections.sort(allEventListeners, sComparator);
        }
    }

    private void removeAndSortAllEventListener(WrappedListener listener) {
        if (allEventListeners.remove(listener)) {
            Collections.sort(allEventListeners, sComparator);
        }
    }

    private void postEventUpdate() {
        eventQueueThread.post(eventRunner);
    }

    private int getNextReadyIndex(long curTime) {
        int size = eventPrivateQueue.size();
        Event nextEvent;
        int index = -1;
        int removeMask = 0;
        for (int i = 0; i < size; i++) {
            nextEvent = eventPrivateQueue.get(i);
            if (nextEvent.canceled) {
                removeMask |= (i + 1);
            }
            if (nextEvent.isDelayed()) {
                if (nextEvent.ready(curTime)) {
                    index = i;
                }
            } else {
                index = i;
            }
        }
        if (removeMask != 0) {
            Event e;
            for (int i = 0; i < size; i++) {
                if ((removeMask & (i + 1)) != 0) {
                    e = eventPrivateQueue.remove(i);
                    log.logD("Removed Event key " + e.getEventKey());
                }
            }
        }
        return index;
    }

    private void handleEventDispatch(int index) {
        Event e = eventPrivateQueue.remove(index);
        doPostEvent(e);
    }

    private EventManagerConfig getConfig() {
        if (config != null) {
            return config;
        } else {
            return DEFAULT_CONFIG;
        }
    }



    private static void updateRefType(List<WrappedListener> listeners, EventManagerConfig.ListenerReferenceType refType) {
        if (listeners != null) {
            for (WrappedListener l : listeners) {
                if (refType != l.refType) {
                    l.refType = refType;
                    l.setListener(l.getListener());
                }
            }
        }
    }

    private static String getExecutionString(Event event) {
        if (event.maxExecutions() == EventManagerConfig.UNLIMITED) {
            return TextUtil.string("Queueing event for event key ", event.getEventKey().toString(), " with unlimited executions.");
        } else {
            return TextUtil.string("Queueing event for event key ", event.getEventKey().toString(), " with a maximum of ", event.maxExecutions(), " executions.");
        }
    }

    private static WrappedListener wrap(EventListener listener, byte priority, EventManagerConfig.ListenerReferenceType refType, boolean isFlexible) {
        return new WrappedListener(listener, priority, refType, isFlexible);
    }

    private static void addAndSortToCollection(List<WrappedListener> collection, WrappedListener listener) {
        if (!collection.contains(listener)) {
            collection.add(listener);
            Collections.sort(collection, sComparator);
        }
    }

    private static boolean removeAndSortFromCollection(List<WrappedListener> collection, WrappedListener listener) {
        boolean removed = collection.remove(listener);
        Collections.sort(collection, sComparator);
        return removed;
    }

    private static boolean contains(Set<WrappedListener> col, WrappedListener listener) {
        for (WrappedListener l : col) {
            if (l.equals(listener)) {
                return true;
            }
        }
        return false;
    }

    private final class EventProcessor implements Runnable {

        private final Event eventPrivate;

        public EventProcessor(Event eventPrivate) {
            this.eventPrivate = eventPrivate;
        }

        @Override
        public final void run() {
            processEvent(eventPrivate);
        }
    }

    private final static class ListenerComparator implements Comparator<WrappedListener> {

        @Override
        public final int compare(WrappedListener lhs, WrappedListener rhs) {
            if (lhs.priority == rhs.priority) return 0;
            return lhs.priority > rhs.priority ? 1 : -1;
        }
    }

    private final static class WrappedListener implements EventListener {

        private WeakReference<EventListener> weakListener;
        private EventListener strongListener;

        private final byte priority;
        private EventManagerConfig.ListenerReferenceType refType;
        private final AtomicBoolean mDeleted;
        private final boolean flexibleType;


        WrappedListener(EventListener listener, byte priority, EventManagerConfig.ListenerReferenceType refType, boolean isFlexible) {
            this.refType = refType;
            setListener(listener);
            this.priority = priority;
            mDeleted = new AtomicBoolean(false);
            flexibleType = isFlexible;
        }

        final void setListener(EventListener listener) {
            if (refType.isStrong() || (refType.isDynamic() && listener.getClass().getSimpleName().equals(""))) {
                strongListener = listener;
                weakListener = null;
            } else {
                weakListener = new WeakReference<>(listener);
                strongListener = null;
            }
        }

        final boolean isFlexible() {
            return flexibleType;
        }

        @Override
        public final int hashCode() {
            return getListener().hashCode() + super.hashCode() * 71;
        }

        private EventListener getListener() {
            if (weakListener != null) {
                return weakListener.get();
            } else {
                return strongListener;
            }
        }

        @Override
        public final boolean equals(Object o) {
            if (o instanceof WrappedListener) {
                WrappedListener ol = (WrappedListener) o;
                if (ol.getListener() == null && getListener() == null)
                    return true;
                if (ol.getListener() == null || getListener() == null)
                    return false;
                return getListener().equals(ol.getListener());
            } else {
                return false;
            }
        }

        @Override
        public final boolean onEvent(Event event) {
            final boolean nullListener = getListener() == null;
            final boolean deleted = mDeleted.getAndSet(nullListener);
            if (!nullListener && !deleted) {
                return getListener().onEvent(event);
            }
            if (nullListener) {
                getInstance().log.logW("Lost reference to EventListener, unregister-ing automatically.");
            }
            return false;
        }
    }

    private final class EventRunner implements Runnable {

        long curTime;

        @Override
        public final void run() {
            curTime = System.currentTimeMillis();
            update(curTime);
            if (eventQueueThread.isAlive()) {
                eventQueueThread.postDelayed(this, interval);
            }
        }
    }

    private final class EventListenerThread extends EventHandler {

        EventListenerThread() {
            super(EventThreadManager.getInstance(getConfig().threadConfig), "EventListenerThread");
            id = 777;
        }

        @Override
        public final void quit() {
            super.quit();
            mainListener = null;
            synchronized (allLock) {
                allEventListeners.clear();
            }
            synchronized (categoryLock) {
                categoryHandlers.clear();
            }
            synchronized (eventLock) {
                eventHandlers.clear();
            }
        }
    }

    private final class EventQueueThread extends EventHandler {

        EventQueueThread() {
            super(EventThreadManager.getInstance(getConfig().threadConfig), "EventQueueThread");
            id = 888;
        }

        final boolean isOnEventUpdateThread() {
            return getThread() != null && Thread.currentThread() == getThread();
        }

        @Override
        public final void quit() {
            super.quit();
            eventPrivateQueue.clear();
            removeCallbacks(eventRunner);
        }
    }

}
