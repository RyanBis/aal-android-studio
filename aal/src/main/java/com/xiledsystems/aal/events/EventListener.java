package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Interface for listening for events posted with {@link EventManager}.
 */
@State(DevelopmentState.PRODUCTION)
public interface EventListener {

    /**
     * Called when an {@link Event} is to be executed. The thread this is called on depends on what
     * {@link PostType} was set in the {@link Event}. Default is {@link PostType#onMain()}.
     * @param event
     * @return
     */
    boolean onEvent(Event event);

}
