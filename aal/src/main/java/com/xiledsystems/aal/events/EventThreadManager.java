package com.xiledsystems.aal.events;


import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.AalHandler;
import com.xiledsystems.aal.util.UIHandler;
import com.xiledsystems.aal.util.DeviceUtil;
import com.xiledsystems.aal.util.IThreadManager;
import com.xiledsystems.aal.util.ThreadManager;
import com.xiledsystems.aal.util.ThreadManagerConfig;

import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class to manage a couple of different ways of thread processing.
 * <p>
 * The most basic, is {@link EventThreadManager#async(Runnable)}, which uses an
 * AsyncTask as the backing processor. This is perfect for one-off situations, where
 * it's not being run very often.
 * <p>
 * Next, is using the class as a singleton, and it becomes a Thread pool manager. The
 * default instance sets the core thread count to the number of cores in the device + 1, and
 * the maximum thread count to twice that, plus 1. The core threads can be preloaded when you
 * get the first instance of this class.
 */
@State(DevelopmentState.PRODUCTION)
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
final class EventThreadManager implements IThreadManager {

    private static EventThreadManager manager;
    private static ThreadPoolExecutor executor;
    private static UIHandler uiHandler;
    private static Set<PoolUIRunner> wrappedRunners;

    private static final AtomicBoolean initialized = new AtomicBoolean(false);
    private final AtomicInteger currentHandlers;
    private final AtomicInteger coreThreads;
    private final AtomicInteger maxThreads;

    private EventThreadManagerConfig config;


    private EventThreadManager(EventThreadManagerConfig managerConfig) {
        coreThreads = new AtomicInteger(1);
        maxThreads = new AtomicInteger(3);
        currentHandlers = new AtomicInteger(0);
        updateFromConfig(managerConfig);
        init();
    }

    private void initexecutor(int core, int max, boolean allowTimeOut, long timeoutPeriod) {
        executor = new ThreadPoolExecutor(core, max, timeoutPeriod, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());
        executor.allowCoreThreadTimeOut(allowTimeOut);
        initialized.set(true);
    }

    /**
     * Same as {@link #getInstance(boolean)} with <code>true</code> as the argument.
     */
    public static EventThreadManager getInstance() {
        if (manager == null) {
            manager = new EventThreadManager(new EventThreadManagerConfig());
        }
        return manager;
    }

    /**
     * @return singleton instance of {@link EventThreadManager} with coreThreads set to the
     * amount of cores in the device, and maxThreads set to twice that amount.
     */
    public static EventThreadManager getInstance(boolean preLoadCore) {
        return getInstance();
    }

    /**
     * @param coreThreads The number of "core" threads to process Runnable actions
     * @param maxThreads  The maximum number of threads the manager can create
     * @param preLoadCore When true, preloads the core threads into memory upon the first instance of this class
     * @return All params are ignored here, and just calls {@link #getInstance()}.
     */
    @Deprecated
    public static EventThreadManager getInstance(int coreThreads, int maxThreads, boolean preLoadCore) {
        return getInstance();
    }

    /**
     * @return singleton instance of {@link EventThreadManager} with provided {@link EventThreadManagerConfig}. This
     * will update the backing {@link EventThreadManagerConfig} with what you pass here, and update it's internal
     * configuration options accordingly.
     */
    public static EventThreadManager getInstance(EventThreadManagerConfig config) {
        if (config == null) {
            config = new EventThreadManagerConfig();
        }
        if (manager == null) {
            manager = new EventThreadManager(config);
            if (config.preLoadCore) {
                executor.prestartAllCoreThreads();
            }
        } else {
            manager.updateFromConfig(config);
        }
        return manager;
    }

    private void updateFromConfig(EventThreadManagerConfig managerConfig) {
        this.config = managerConfig.clone();
        if (config.coreThreads == null || config.maxThreads == null) {
            int cores = DeviceUtil.getNumberOfCores();
            config.coreThreads = config.coreThreads == null ? cores + 1 : config.coreThreads;
            config.maxThreads = config.maxThreads == null ? (cores * 2) + 1 : config.maxThreads;
        }
        boolean updateThreads = false;
        if (coreThreads.get() != config.coreThreads) {
            updateThreads = true;
            coreThreads.set(config.coreThreads);
        }
        if (maxThreads.get() != config.maxThreads) {
            updateThreads = true;
            maxThreads.set(config.maxThreads);
        }
        if (config.unitTest) {
            if (uiHandler == null || uiHandler instanceof EventThreadManagerConfig.DefaultUIHandler) {
                uiHandler = new EventThreadManagerConfig.UnitTestUIHandler();
            }
        } else {
            if (uiHandler == null || uiHandler instanceof EventThreadManagerConfig.UnitTestUIHandler) {
                uiHandler = new EventThreadManagerConfig.DefaultUIHandler();
            }
        }
        execute(new Runnable() {
            @Override
            public void run() {
                uiHandler.init(manager);
            }
        });
        if (executor != null) {
            executor.allowCoreThreadTimeOut(config.allowCoreTimeOut);
            if (config.allowCoreTimeOut && config.coreTimeoutPeriodMs > 0) {
                executor.setKeepAliveTime(config.coreTimeoutPeriodMs, TimeUnit.MILLISECONDS);
            }
            if (updateThreads) {
                updateThreadCounts();
            }
        }
    }

    private void checkInit() {
        if (!initialized.get()) {
            init();
        }
    }

    private void init() {
        initexecutor(coreThreads.get(), maxThreads.get(), config.allowCoreTimeOut, config.coreTimeoutPeriodMs);
    }

    /**
     * Execute a Runnable action using the backing Thread pool.
     *
     * @param action
     */
    public final void execute(Runnable action) {
        checkInit();
        executor.execute(action);
    }

    /**
     * Execute Runnable action using the backing Thread pool. Then, execute
     * the Runnable finished on the UI thread.
     *
     * @param action
     * @param finished
     */
    public final void execute(final Runnable action, final Runnable finished) {
        checkInit();
        PoolUIRunner run = new PoolUIRunner(action, finished);
        wrappedRunners.add(run);
        execute(run);
    }

    public final <T> Future<T> execute(final Callable<T> action) {
        checkInit();
        return executor.submit(action);
    }

    /**
     * Remove a Runnable action from the backing Thread pool.
     *
     * @param action
     */
    public final void remove(Runnable action) {
        if (!executor.remove(action)) {
            PoolUIRunner run = getWrappedRunner(action);
            if (run != null) {
                remove(run);
                executor.purge();
            }
        } else {
            executor.purge();
        }
    }

    /**
     * Shuts down the backing Thread pool. This may take some time.
     */
    public final void dispose() {
        if (wrappedRunners != null) {
            wrappedRunners.clear();
        }
        executor.shutdownNow();
        if (uiHandler instanceof ThreadManagerConfig.UnitTestUIHandler) {
            ((ThreadManagerConfig.UnitTestUIHandler) uiHandler).quit();
        }
        manager = null;
        initialized.set(false);
    }

    /**
     * This is a blocking method, in that it waits for 1000ms for a shutdown
     * signal from the backing Thread pool.
     *
     * @return true if received signal that the Thread pool shutdown
     */
    public final boolean disposeBlocking() {
        boolean success = false;
        executor.shutdownNow();
        initialized.set(false);
        try {
            success = executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
            return success;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    public final void setThreadCount(int coreThreads, int maxThreads) {
        this.coreThreads.set(coreThreads);
        this.maxThreads.set(maxThreads);
        updateThreadCounts();
    }

    private PoolUIRunner getWrappedRunner(Runnable action) {
        for (PoolUIRunner run : wrappedRunners) {
            if (run.getRun().equals(action)) {
                return run;
            }
        }
        return null;
    }

    /**
     * Use an AsyncTask to run an action in a background
     * thread. This is good to use for those one-off situations
     *
     * @param action
     */
    public static void async(final Runnable action) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                action.run();
                return null;
            }
        }.execute();
    }

    /**
     * Use an AsyncTask to run an action in a background
     * thread. This is good to use for those one-off situations
     * The provided uiAciton Runnable is run on the UI thread right
     * after the threadAction is complete.
     *
     * @param threadAction
     * @param uiAction
     */
    public static void async(final Runnable threadAction, final Runnable uiAction) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                if (threadAction != null) {
                    threadAction.run();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (uiAction != null) {
                    uiAction.run();
                }
            }
        }.execute();
    }

    /**
     * Returns a new instance of {@link AalHandler} with a thread from the ThreadPool backing this {@link ThreadManager}.
     */
    public final EventHandler newAalHandler() {
        return new EventHandler(this);
    }

    /**
     * Same as {@link #newAalHandler()}, with an argument which allows you to specify the name of the backing Thread.
     */
    public final EventHandler newAalHandler(String threadName) {
        return new EventHandler(this, threadName);
    }

    public final void handlerOpened() {
        currentHandlers.incrementAndGet();
        updateThreadCounts();
    }

    public final void handlerClosed() {
        currentHandlers.decrementAndGet();
        updateThreadCounts();
    }

    private void updateThreadCounts() {
        if (executor.getCorePoolSize() != coreThreads.get()) {
            executor.setCorePoolSize(coreThreads.get());
        }
        if (executor.getMaximumPoolSize() != (maxThreads.get() + currentHandlers.get())) {
            executor.setMaximumPoolSize(Math.max(coreThreads.get(), maxThreads.get() + currentHandlers.get()));
        }
    }

    public static Handler getHandler() {
        if (uiHandler == null || !(uiHandler instanceof EventThreadManagerConfig.DefaultUIHandler)) {
            return new Handler(Looper.getMainLooper());
        }
        return ((EventThreadManagerConfig.DefaultUIHandler) uiHandler).getHandler();
    }

    /**
     * @return the {@link UIHandler} for posting {@link Runnable}s to the UI thread. This replaces
     * {@link #getHandler()}, only because using this class makes it easier to unit test. When NOT testing,
     * the returned instance will contain a {@link Handler} which is linked to the UI thread.
     */
    public static UIHandler getUiHandler() {
        if (uiHandler == null) {
            uiHandler = new EventThreadManagerConfig.DefaultUIHandler();
        }
        return uiHandler;
    }

    private final static class PoolUIRunner implements Runnable {

        private final Runnable action;
        private final Runnable finished;

        PoolUIRunner(Runnable action, Runnable finished) {
            this.action = action;
            this.finished = finished;
        }

        public final Runnable getRun() {
            return action;
        }

        @Override
        public final void run() {
            action.run();
            uiHandler.post(finished);
            if (manager != null) {
                manager.remove(action);
            }
        }
    }

}