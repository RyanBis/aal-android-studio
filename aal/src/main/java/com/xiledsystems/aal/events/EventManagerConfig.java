package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ThreadManagerConfig;

/**
 * Configuration class for setting various options for the {@link EventManager}.
 */
@State(DevelopmentState.PRODUCTION)
public class EventManagerConfig implements Cloneable {

    /**
     * @deprecated
     */
    @Deprecated
    public final static int DEFAULT_EPS                     = 40;
    /**
     * Default value for {@link #dispatchInterval}. This is how often {@link Event}s are processed.
     * @see #dispatchInterval
     * @see #maxDispatchLength
     */
    public final static int DEFAULT_DISPATCH_INTERVAL       = 40;

    /**
     * Default value for {@link #defaultMaxExecutions}. {@value #UNLIMITED} means there is no limit (it will be processed
     * by every {@link EventListener} registered. This can be overriden on a case by case basis by setting it in the
     * {@link Event} using the {@link Event.Builder} class before posting it.
     * @see #defaultMaxExecutions
     */
    public final static int UNLIMITED                       = -1;

    /**
     * Default value for {@link #maxDispatchLength}. The amount of time to allow the {@link EventManager} to dispatch {@link Event}s.
     *
     * @see #maxDispatchLength
     */
    public final static int DEFAULT_MAX_DISPATCH_LENGTH     = 10;

    /**
     * The frame rate, or events per second, the {@link EventManager} will dispatch events. The default
     * is now <noce>null</noce>. If this value is NOT <code>null</code>, then this value will be used instead
     * of {@link #dispatchInterval}. This behavior will change in a future major version update.
     * @deprecated Use {@link #dispatchInterval} instead.
     *
     */
    @Deprecated
    public Integer eventFps                             = null;

    /**
     * The interval (in frames per second) of event dispatch ticks. Default value is {@link #DEFAULT_DISPATCH_INTERVAL}. This used
     * to be called eventFps.
     *
     */
    public int dispatchInterval                         = DEFAULT_DISPATCH_INTERVAL;

    int getDispatchFps() {
        if (eventFps != null) {
            return eventFps;
        } else {
            return dispatchInterval;
        }
    }

    /**
     * Specifies how {@link Event}s are posted, if no {@link PostType} is given when posting said
     * Event. The default is {@link PostType#onMain()}.
     */
    public PostType postType                            = PostType.onMain();

    /**
     * Setting this to true will print out verbose logging of the event system. The default is
     * <code>false</code>.
     */
    public boolean logging                              = false;

    /**
     * Set this to something other than {@link #UNLIMITED}, and all events posted to the system will
     * only be executed this many times, unless otherwise specified. The default is {@link #UNLIMITED}.
     */
    public int defaultMaxExecutions                     = UNLIMITED;

    /**
     * Delay all events posted to the event system by this amount, if the delay hasn't been specified when
     * posting the event. The default is <code>null</code> (no delay in executing).
     */
    public Long defaultDelay                            = null;

    /**
     * Maximum amount of time in milliseconds to allow the manager to dispatch events <b>each tick</b>, providing there
     * are multiple items in the queue waiting to be processed (and are ready to be dispatched).
     */
    public long maxDispatchLength                       = DEFAULT_MAX_DISPATCH_LENGTH;

    /**
     * By default, the {@link EventManager} will store {@link EventListener}s using {@link java.lang.ref.WeakReference}s, so if you
     * forget to unregister a listener, the manager will remove it once the instance is lost. However, if the {@link EventListener}
     * is an anonymous class, it will still be held with a hard reference. This is to prevent the listener being destroyed before
     * it ever gets a chance to respond to it's event. If you want ALL listeners to be stored via weak reference, then set this
     * option to {@link ListenerReferenceType#WEAK}.
     */
    public ListenerReferenceType eventListenerRefType    = ListenerReferenceType.DYNAMIC;

    /**
     * Default {@link ThreadManagerConfig} to use for the backing {@link EventThreadManager}. The only reason
     * you should need to change this is if you are planning on unit testing the EventManager, or any of the
     * Model classes.
     */
    public EventThreadManagerConfig threadConfig         = new EventThreadManagerConfig();


    /**
     * Default constructor which uses all default values.
     */
    public EventManagerConfig() {
    }



    /**
     * @see #withDispatchFPS(int)
     * @deprecated
     */
    @Deprecated
    public EventManagerConfig withEps(int eps) {
        eventFps = eps;
        return this;
    }

    /**
     * Set the frames per second rate at which event dispatches are timed (multiple events may be dispatched in one tick).
     */
    public final EventManagerConfig withDispatchFPS(int fps) {
        dispatchInterval = fps;
        return this;
    }

    /**
     * Set {@link #maxDispatchLength}.
     */
    public final EventManagerConfig withMaxDispatchLength(int max) {
        maxDispatchLength = max;
        return this;
    }

    /**
     * Set {@link #postType}.
     */
    public final EventManagerConfig withPostType(PostType postType) {
        this.postType = postType;
        return this;
    }

    /**
     * Set {@link #logging}.
     */
    public final EventManagerConfig withLogging(boolean logging) {
        this.logging = logging;
        return this;
    }

    /**
     * Set {@link #defaultMaxExecutions}.
     */
    public final EventManagerConfig withDefaultMaxExecutions(int maxExecutions) {
        defaultMaxExecutions = maxExecutions;
        return this;
    }

    /**
     * Set {@link #defaultDelay}.
     */
    public final EventManagerConfig withDefaultDelay(Long delay) {
        defaultDelay = delay;
        return this;
    }

    /**
     * Set {@link #threadConfig}.
     */
    public final EventManagerConfig withThreadConfig(EventThreadManagerConfig tConfig) {
        threadConfig = tConfig;
        return this;
    }

    /**
     * Set {@link #eventListenerRefType}.
     */
    public final EventManagerConfig withListenerRefType(ListenerReferenceType type) {
        eventListenerRefType = type;
        return this;
    }

    /**
     * Clone this instance of {@link EventManagerConfig}.
     */
    @Override
    protected final EventManagerConfig clone() {
        try {
            return (EventManagerConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            return this;
        }
    }

    /**
     * Enumeration for how the {@link EventManager} handles registered {@link EventListener}s in memory.
     */
    public enum ListenerReferenceType {
        /**
         * This option will force the {@link EventManager} to hold all {@link EventListener} instances in {@link java.lang.ref.WeakReference}s. This makes it
         * so that the manager can automatically unregister them when they have been destroyed. This can cause problems with anonymous
         * classes, as they can get destroyed before you intend them to (if nothing else is holding a reference to them).
         */
        WEAK,

        /**
         * This option will force the {@link EventManager} to hold all {@link EventListener} instances as strong references. This makes sure
         * that all {@link EventListener} instances will stay around until unregistered. This can cause memory leaks if you fail to unregister
         * any listeners.
         */
        STRONG,

        /**
         * This is the default option, and the other 2 are only there for extreme circumstances. This will hold all {@link EventListener} instances
         * in a WeakReference, UNLESS it is an anonymous class. This ensures that every listener will stick around if intended, but still remove
         * others. There is still potential to cause a memory leak here if you don't call {@link EventManager#shutdown()} when you close your app.
         */
        DYNAMIC;

        final boolean isStrong() {
            return this == STRONG;
        }

        final boolean isWeak() {
            return this == WEAK;
        }

        final boolean isDynamic() {
            return this == DYNAMIC;
        }

    }
}
