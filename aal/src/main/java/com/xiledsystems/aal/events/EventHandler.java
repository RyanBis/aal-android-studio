package com.xiledsystems.aal.events;


import com.xiledsystems.aal.util.IThreadManager;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;


class EventHandler {

    private final LinkedBlockingQueue<HandlerRunnable> runnables;
    private IThreadManager manager;

    private Thread thread;
    private boolean running = false;

    int id;


    EventHandler(EventThreadManager manager) {
        runnables = new LinkedBlockingQueue<>();
        this.manager = manager;
        init(manager);
    }

    EventHandler(EventThreadManager manager, final String threadName) {
        this(manager);
        post(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName(threadName);
            }
        });
    }

    EventHandler() {
        runnables = new LinkedBlockingQueue<>();
    }

    /**
     * Post a {@link Runnable} to execute on the thread backing this {@link EventHandler}.
     */
    public final void post(Runnable action) {
        runnables.add(new HandlerRunnable(action, System.currentTimeMillis(), 0));
    }

    /**
     * Post a {@link Runnable} to execute on the backing thread of this {@link EventHandler} after
     * te given delay.
     */
    public final void postDelayed(Runnable action, long delay) {
        runnables.add(new HandlerRunnable(action, System.currentTimeMillis(), delay));
    }

    /**
     * Removes any posted {@link Runnable}s posted to this {@link EventHandler}, if it has not
     * been executed yet.
     */
    public final void removeCallbacks(Runnable action) {
        Iterator<HandlerRunnable> it = runnables.iterator();
        while (it.hasNext()) {
            HandlerRunnable run = it.next();
            if (run.runnable == action) {
                run.cancel();
            }
        }
    }

    /**
     * @return if this {@link EventHandler} is still running, and can execute {@link Runnable}s posted to it.
     */
    public final boolean isAlive() {
        return running && thread != null;
    }

    /**
     * @return the {@link Thread} backing this {@link EventHandler}.
     */
    public final Thread getThread() {
        return thread;
    }

    /**
     * Stop this {@link EventHandler} from executing any more {@link Runnable}s posted. Unexecuted Runnables will remain
     * in the queue until {@link #start()} is called, or {@link #quit()} is called (which will clear the queue). This method
     * does nothing if this {@link EventHandler} is already stopped.
     */
    public final void stop() {
        if (running) {
            running = false;
            clearFromManager();
        }
    }

    /**
     * Start executing {@link Runnable}s posted to this {@link EventHandler}, if there are any in the queue. This method only needs
     * to be called if {@link #stop()} was called at some point.
     */
    public final void start() {
        init(manager);
    }

    /**
     * Clears the queue of this {@link EventHandler}, and stops the backing Thread. If you want the instance to be able to execute
     * any more {@link Runnable}s after calling this method, you must call {@link #start()} first.
     */
    public void quit() {
        running = false;
        runnables.clear();
        thread = null;
        clearFromManager();
    }

    private void clearFromManager() {
        manager.handlerClosed();
    }

    void init(IThreadManager mgr) {
        if (manager == null) {
            manager = mgr;
        }
        if (!running) {
            manager.handlerOpened();
            manager.execute(new HandleRunnable());
        }
    }

    private final static class HandlerRunnable {

        private final Runnable runnable;
        private final long timePosted;
        private final long delay;
        private boolean canceled;

        HandlerRunnable(Runnable action, long posted, long delay) {
            runnable = action;
            timePosted = posted;
            this.delay = delay;
        }

        public void run() {
            runnable.run();
        }

        void cancel() {
            canceled = true;
        }

        boolean canceled() {
            return canceled;
        }

        long timePosted() {
            return timePosted;
        }

        long delay() {
            return delay;
        }

        boolean ready(long curTime) {
            return delay == 0 || canceled || (curTime - timePosted) >= delay;
        }

        boolean ready() {
            // Check canceled here again, just so we don't bother getting the current time if
            // we're canceling anyway
            return canceled || ready(System.currentTimeMillis());
        }
    }

    private final class HandleRunnable implements Runnable {

        private long curTime;

        @Override
        public void run() {
            thread = Thread.currentThread();
            running = true;
            while (running) {
                if (Thread.interrupted()) {
                    running = false;
                    clearFromManager();
                    return;
                }
                if (!runnables.isEmpty()) {
                    curTime = System.currentTimeMillis();
                    Iterator<HandlerRunnable> it = runnables.iterator();
                    while (it.hasNext()) {
                        HandlerRunnable run = it.next();
                        if (run.ready(curTime)) {
                            it.remove();
                            if (!run.canceled()) {
                                run.run();
                            }
                        }
                    }
                }
                try {
                    Thread.sleep(5);
                } catch (Exception e) {
                }
            }
        }
    }

}
