package com.xiledsystems.aal.communication;


/**
 * Interface used to listen for a {@link Response} as a result of calling {@link HttpRequest#execute(RequestListener)}.
 */
public interface RequestListener {

    /**
     * This is called when the request has finished. This does not mean that the request was successful, only that it's done.
     * @see Response#wasSuccess()
     * @see Response#responseCode()
     * @see Response#response()
     * @see Response#error()
     * @param response
     */
    void onResponse(Response response);

    /**
     * Class which holds the result of a call using {@link HttpRequest#execute(RequestListener)}.
     */
    class Response {
        private final int responseCode;
        private final String response;
        private final String errorString;


        Response(int responseCode, String response, String error) {
            this.response = response;
            this.responseCode = responseCode;
            errorString = error;
        }

        /**
         * Returns the response code from the request.
         */
        public int responseCode() {
            return responseCode;
        }

        /**
         * Returns the response string from the request. This can be null, or empty if the request failed.
         *
         * @see #error()
         * @see #responseCode()
         */
        public String response() {
            return response;
        }

        /**
         * Returns the error string from the request. This can be null, or empty if the request succeeded.
         */
        public String error() {
            return errorString;
        }

        /**
         * Returns <code>true</code> if the result of {@link #responseCode()} is between 200 and 206 inclusive.
         */
        public boolean wasSuccess() {
            return responseCode >= 200 && responseCode <= 206;
        }
    }
}
