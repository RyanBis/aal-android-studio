package com.xiledsystems.aal.communication;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import com.xiledsystems.aal.util.TextUtil;


/**
 * Convenience class for sending/receiving SMS. Note that you will need to add the
 * {@link android.Manifest.permission#SEND_SMS} permission to your AndroidManifest.xml file in order
 * to send SMS. (Receiving is taken care of within the SmsHelper class).
 */
@TargetApi(Build.VERSION_CODES.DONUT)
@State(DevelopmentState.BETA)
public class SmsHelper extends NonDisplayComponent {

    private static final String TAG = "SmsHelper";
    private static final String RCV_INTENT = "android.provider.Telephony.SMS_RECEIVED";
    private final static String SENT_NMBR = "aal.sms.SentNumber";
    private final static String SENT_MSG = "aal.sms.SentMessage";
    private final static String SENT_ID = "aal.sms.SmsId";
    private final static String SENT_INTENT = "com.xiledsystems.aal.sms.SmsSentIntent";
    private final static String DELIVER_INTENT = "com.xiledsystems.aal.sms.SmsDeliverIntent";

    private boolean receivingEnabled;
    private SmsManager smsManager;
    private SmsListener listener;
    private SmsReceiver smsReceiver;
    private SentReceiver sentReceiver;
    private static int nextId = 0;


    public SmsHelper(Context context) {
        this(context, null);
    }

    public SmsHelper(Context context, Integer priority) {
        super(context);
        smsReceiver = new SmsReceiver();
        sentReceiver = new SentReceiver();
        IntentFilter sFilter = new IntentFilter(RCV_INTENT);
        if (priority != null) {
            sFilter.setPriority(priority);
        }
        context.registerReceiver(smsReceiver, sFilter);
        context.registerReceiver(sentReceiver, new IntentFilter(SENT_INTENT));
        smsManager = SmsManager.getDefault();
        receivingEnabled = true;
    }

    /**
     * Set the listener to listen for Sms callbacks. This includes receiving SMS,
     * and also callbacks for when a SMS is sent, and when it is delivered, as well
     * as an error callback.
     * @param listener
     */
    public void setSmsListener(SmsListener listener) {
        this.listener = listener;
    }

    /**
     *
     * @return whether this SmsHelper is set to receive SMS
     */
    public boolean isReceivingEnabled() {
        return receivingEnabled;
    }

    /**
     * Set this SmsHelper to receive, or not receive SMS.
     * @param receivingEnabled
     */
    public void setReceivingEnabled(boolean receivingEnabled) {
        this.receivingEnabled = receivingEnabled;
    }

    /**
     * Send an SMS. This does NOT protect against message length. SMS cannot exceed 160
     * characters. In fact, in some cases, it cannot exceed 70 characters, if the message
     * contains a special character outside of the charset the SMS protocol uses.
     *
     * @param phoneNumber
     * @param message
     * @return the id of the message sent
     */
    public int sendSms(String phoneNumber, String message) {
        int id = getNextId();
        Intent sIntent = new Intent(SENT_INTENT);
        sIntent.putExtra(SENT_ID, id);
        sIntent.putExtra(SENT_NMBR, phoneNumber);
        sIntent.putExtra(SENT_MSG, message);
        PendingIntent psIntent = PendingIntent.getBroadcast(getContext(), 0, sIntent, PendingIntent.FLAG_ONE_SHOT);
        Intent dIntent = new Intent(DELIVER_INTENT);
        dIntent.putExtra(SENT_ID, id);
        dIntent.putExtra(SENT_NMBR, phoneNumber);
        dIntent.putExtra(SENT_MSG, message);
        PendingIntent pdIntent = PendingIntent.getBroadcast(getContext(), 0, dIntent, PendingIntent.FLAG_ONE_SHOT);
        smsManager.sendTextMessage(phoneNumber, null, message, psIntent, pdIntent);
        return id;
    }

    private static int getNextId() {
        return nextId++;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new LifecycleListener() {
            @Override
            public void onDestroy() {
                try {
                    getContext().unregisterReceiver(smsReceiver);
                } catch (RuntimeException e) {
                }
                try {
                    getContext().unregisterReceiver(sentReceiver);
                } catch (RuntimeException e) {
                }
                smsReceiver = null;
                sentReceiver = null;
            }
        };
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return new ServiceLifecycleListener() {
            @Override
            public void onDestroy() {
                try {
                    getContext().unregisterReceiver(smsReceiver);
                } catch (RuntimeException e) {
                }
                try {
                    getContext().unregisterReceiver(sentReceiver);
                } catch (RuntimeException e) {
                }
                smsReceiver = null;
                sentReceiver = null;
            }
        };
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    public static SmsMessage[] getMessagesFromIntent(Intent intent) {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
        byte[][] pduObjs = new byte[messages.length][];

        for (int i = 0; i < messages.length; i++) {
            pduObjs[i] = (byte[]) messages[i];
        }
        byte[][] pdus = new byte[pduObjs.length][];
        int pduCount = pdus.length;
        SmsMessage[] msgs = new SmsMessage[pduCount];
        for (int i = 0; i < pduCount; i++) {
            pdus[i] = pduObjs[i];
            msgs[i] = SmsMessage.createFromPdu(pdus[i]);
        }
        return msgs;
    }

    private class SmsReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            SmsMessage[] messages = getMessagesFromIntent(intent);
            SmsMessage message = messages[0];
            if (message != null) {
                String from = message.getOriginatingAddress();
                String messageText = message.getMessageBody();
                if (listener != null && receivingEnabled) {
                    getLogger().logD(TextUtil.string("SMS received from ", from, ". Message: ", messageText));
                    listener.onSmsReceived(from, messageText);
                }
            } else {
                getLogger().logI("Sms message supposedly received but with no actual content.");
            }
        }
    }

    private class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    if (listener != null) {
                        if (intent.hasExtra(SENT_NMBR) && intent.hasExtra(SENT_MSG) && intent.hasExtra(SENT_ID)) {
                            listener.onSmsSent(intent.getStringExtra(SENT_NMBR), intent.getStringExtra(SENT_MSG), intent.getIntExtra(SENT_ID, 0));
                        }
                    }
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    if (listener != null) {
                        if (intent.hasExtra(SENT_NMBR) && intent.hasExtra(SENT_MSG) && intent.hasExtra(SENT_ID)) {
                            listener.onSmsSendFail(intent.getStringExtra(SENT_NMBR), intent.getStringExtra(SENT_MSG), intent.getIntExtra(SENT_ID, 0), SmsError.SEND_GENERIC);
                        }
                    }
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    if (listener != null) {
                        if (intent.hasExtra(SENT_NMBR) && intent.hasExtra(SENT_MSG) && intent.hasExtra(SENT_ID)) {
                            listener.onSmsSendFail(intent.getStringExtra(SENT_NMBR), intent.getStringExtra(SENT_MSG), intent.getIntExtra(SENT_ID, 0), SmsError.SEND_NO_SERVICE);
                        }
                    }
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    if (listener != null) {
                        if (intent.hasExtra(SENT_NMBR) && intent.hasExtra(SENT_MSG) && intent.hasExtra(SENT_ID)) {
                            listener.onSmsSendFail(intent.getStringExtra(SENT_NMBR), intent.getStringExtra(SENT_MSG), intent.getIntExtra(SENT_ID, 0), SmsError.SEND_NULL_PDU);
                        }
                    }
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    if (listener != null) {
                        if (intent.hasExtra(SENT_NMBR) && intent.hasExtra(SENT_MSG) && intent.hasExtra(SENT_ID)) {
                            listener.onSmsSendFail(intent.getStringExtra(SENT_NMBR), intent.getStringExtra(SENT_MSG), intent.getIntExtra(SENT_ID, 0), SmsError.SEND_RADIO_OFF);
                        }
                    }
                    break;
            }
        }
    }
}
