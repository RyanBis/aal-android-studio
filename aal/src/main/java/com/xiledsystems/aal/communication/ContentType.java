package com.xiledsystems.aal.communication;


/**
 * Enumeration of common Content-Type, for strictly typing purposes only.
 */
public enum ContentType {

    BINARY("application/octet-stream"),
    PDF("application/pdf"),
    MPEG("audio/mpeg"),
    MP3("audio/mp3"),
    MP4("audio/mp4"),
    BMP("image/bmp"),
    GIF("image/gif"),
    JPG("image/jpg"),
    PNG("image/png"),
    TIFF("image/tiff"),
    JSON("application/json"),
    CSV("text/csv"),
    TEXT("text/plain"),
    HTML("text/html"),
    XML("text/xml"),
    ZIP("application/zip");

    private final String typeString;

    ContentType(String type) {
        typeString = type;
    }

    String typeString() {
        return typeString;
    }

}
