package com.xiledsystems.aal.communication;


import android.content.Context;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import com.xiledsystems.aal.util.TextUtil;
import com.xiledsystems.aal.util.ThreadManager;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Helper class to make HTTP calls (POST, GET, PUT, DELETE).
 * <br><br>
 * You must remember to add the following to your AndroidManifest.xml
 * <br><br>
 * <pre>
 * &lt;uses-permission android:name="android.permission.INTERNET" /&gt;
 * &lt;uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" /&gt;
 * </pre>
 */
@State(DevelopmentState.BETA)
public class HttpRequest extends NonDisplayComponent {


    public final static String UNKNOWN_HOST = "Unknown Host";

    private final static String CONTENT_TYPE = "Content-Type";
    private final static String UTF8 = "UTF-8";

    private URL mUrl;
    private String baseUrl;
    private HttpURLConnection mConnection;
    private Map<String, String> requestProperties;
    private Map<String, String> urlParameters;
    private String postString;
    private InputStream postStream;
    private RequestType requestType;
    private String encoding = UTF8;
    private boolean postResponseToUi = true;
    private boolean allowRedirects = false;
    private ThreadManager threadManager;


    public HttpRequest(Context context) {
        super(context);
        requestProperties = new HashMap<>(0);
        urlParameters = new HashMap<>(0);
        threadManager = ThreadManager.getInstance(true);
    }

    public HttpRequest(Context context, RequestType requestType) {
        this(context);
        this.requestType = requestType;
    }

    /**
     * Set the type of HTTP request. One of the values in {@link RequestType}.
     */
    public void setRequestType(RequestType type) {
        requestType = type;
    }

    /**
     * Returns the current {@link RequestType},
     */
    public RequestType getRequestType() {
        return requestType;
    }

    /**
     * Set the url to make the request to.
     */
    public void setUrl(String url) {
        baseUrl = url;
    }

    /**
     * Returns the current url this request is set for.
     */
    public String getUrl() {
        return baseUrl;
    }

    /**
     * Set this request to post it's response on the UI thread. The default is true, so you
     * only need to use this if you DON'T want the response to be posted on the UI thread.
     */
    public void postResponseToUi(boolean postToUi) {
        postResponseToUi = postToUi;
    }

    /**
     * Returns whether this request will post to the UI thread or not.
     */
    public boolean isPostingResponseToUi() {
        return postResponseToUi;
    }

    /**
     * Set this request to allow redirects. If this is true, then it will check if the
     * connection is the same host as what was given in the url. Default is <code>false</code>.
     */
    public void allowRedirects(boolean allowRedirects) {
        this.allowRedirects = allowRedirects;
    }

    /**
     * Returns whether this request allows redirects. Default is <code>false</code>.
     */
    public boolean allowingRedirects() {
        return allowRedirects;
    }

    /**
     * Set String data when using {@link RequestType#POST}, {@link RequestType#PUT}, or {@link RequestType#DELETE}.
     */
    public void setData(String data) {
        postString = data;
    }

    /**
     * Set JSON data when using {@link RequestType#POST}, {@link RequestType#PUT}, or {@link RequestType#DELETE}.
     */
    public void setData(JSONObject data) {
        postString = data.toString();
    }

    /**
     * Set the {@link InputStream} when using {@link RequestType#POST}, {@link RequestType#PUT}, or {@link RequestType#DELETE}.
     */
    public void setStream(InputStream stream) {
        postStream = stream;
    }

    /**
     * Add a request property to this request.
     */
    public HttpRequest addRequestProperty(String property, String value) {
        requestProperties.put(property, value);
        return this;
    }

    /**
     * Add a Map of properites to this request (this will add to any already added).
     */
    public HttpRequest addRequestProperties(Map<String, String> properties) {
        requestProperties.putAll(properties);
        return this;
    }

    /**
     * Set a Map of properties to this request. This will wipe out any properties already added to this request.
     */
    public HttpRequest setRequestProperties(Map<String, String> properties) {
        requestProperties = new HashMap<>(properties);
        return this;
    }

    /**
     * Set the content type of this request. This is a convenience which calls {@link #addRequestProperty(String, String)}.
     */
    public void setContentType(ContentType contentType) {
        requestProperties.put(CONTENT_TYPE, contentType.typeString());
    }

    /**
     * Clear all the request properties for this request.
     */
    public void clearRequestProperties() {
        requestProperties.clear();
    }

    /**
     * Add a URL parameter which will get added onto the url you provided using {@link #setUrl(String)}. The parameter and
     * values given here will be URL encoded using {@link URLEncoder#encode(String, String)}.
     */
    public HttpRequest addUrlParameter(String param, String value) {
        urlParameters.put(param, value);
        return this;
    }

    /**
     * Clear the url parameters set for this request.
     */
    public void clearUrlParameters() {
        urlParameters.clear();
    }

    /**
     * Resets this request, which clears out the request properties, and url parameters, and nulls out all data, and the url.
     */
    public void reset() {
        clearRequestProperties();
        clearUrlParameters();
        postString = null;
        postStream = null;
        mUrl = null;
    }

    /**
     * Set the encoding to use when {@link URLEncoder#encode(String, String)} is called.
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * Returns the encoding this request is using when calling {@link URLEncoder#encode(String, String)}.
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Execute this HTTP request. Provide a {@link RequestListener} to listen for the callback, to know if the
     * request was successful or not, and to get any desired data.
     */
    public void execute(final RequestListener listener) {
        threadManager.execute(() -> {
            try {
                if (prepConnection()) {
                    if (requestType.sendingData()) {
                        addStringData();
                        addStreamData();
                    }
                    final int rCode = mConnection.getResponseCode();
                    final String response = retrieveResponseString(rCode);
                    final String error = retrieveErrorString();
                    final RequestListener.Response resp = new RequestListener.Response(rCode, response, error);
                    mConnection.disconnect();
                    if (listener != null) {
                        if (postResponseToUi) {
                            ThreadManager.getHandler().post(() -> {
                                if (listener != null) {
                                    listener.onResponse(resp);
                                }
                            });
                        } else {
                            listener.onResponse(resp);
                        }
                    }
                }
            } catch (IOException e) {
                final String error;
                if (e instanceof UnknownHostException) {
                    error = UNKNOWN_HOST;
                } else {
                    StringBuilder b = new StringBuilder();
                    b.append("Got a(n) ").append(e.getClass().getSimpleName()).append(" with a message of: ").append(e.getMessage());
                    error = b.toString();
                }
                if (listener != null) {
                    final RequestListener.Response resp = new RequestListener.Response(-1, "", error);
                    listener.onResponse(resp);
                } else {
                    getLogger().logE(e.getMessage());
                }
            }
        });
    }

    boolean prepConnection() throws IOException {
        try {
            mUrl = new URL(getFullUrl());
        } catch (UnsupportedEncodingException e) {
            getLogger().logE("Unsupported Encoding exception when URL Encoding url parameters!");
            return false;
        }
        mConnection = (HttpURLConnection) mUrl.openConnection();
        if (!allowRedirects) {
            if (!mUrl.getHost().equals(mConnection.getURL().getHost())) {
                getLogger().logE("Got redirected, and allowRedirects=false.");
                return false;
            }
        }
        mConnection.setRequestMethod(requestType.methodString());
        if (requestType.sendingData()) {
            mConnection.setDoOutput(true);
        }
        for (String prop : requestProperties.keySet()) {
            mConnection.setRequestProperty(prop, requestProperties.get(prop));
        }
        return true;
    }

    void addStringData() throws IOException {
        if (!TextUtil.isEmpty(postString)) {
            DataOutputStream dout = new DataOutputStream(mConnection.getOutputStream());
            dout.writeBytes(postString);
            dout.flush();
            dout.close();
        }
    }

    void addStreamData() throws IOException {
        if (postStream != null) {
            OutputStream out = mConnection.getOutputStream();
            byte[] buff = new byte[2048];
            while (postStream.read(buff) != -1) {
                out.write(buff);
            }
            postStream.close();
        }
    }

    private String getFullUrl() throws UnsupportedEncodingException {
        return TextUtil.string(baseUrl, paramsToString(urlParameters, encoding));
    }

    private String retrieveResponseString(int rCode) {
        if (rCode == 204)
        {
            return "";
        }
        if (rCode >=400)
        {
            return "";
        }
        try {
            InputStream in = mConnection.getInputStream();
            return getResponseString(in);
        } catch (IOException e) {
            return "";
        }
    }

    private String retrieveErrorString() {
        InputStream in = mConnection.getErrorStream();
        return getResponseString(in);
    }

    private static String getResponseString(InputStream stream) {
        try {
            if (stream != null) {
                final BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                String line;
                final StringBuilder b = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    b.append(line);
                }
                reader.close();
                return b.toString();
            }
        } catch (IOException e) {
        }
        return "";
    }

    private static String paramsToString(Map<String, String> params, String encoding) throws UnsupportedEncodingException {
        final StringBuilder b = new StringBuilder();
        final Iterator<String> it = params.keySet().iterator();
        String p;
        if (it.hasNext()) {
            b.append("?");
        }
        while (it.hasNext()) {
            p = it.next();
            b.append(URLEncoder.encode(p, encoding)).append("=").append(URLEncoder.encode(params.get(p), encoding));
            if (it.hasNext()) {
                b.append("&");
            }
        }
        return b.toString();
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return null;
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return null;
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }
}
