package com.xiledsystems.aal.communication;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.BETA)
public enum SmsError {
    SEND_GENERIC,
    SEND_NO_SERVICE,
    SEND_NULL_PDU,
    SEND_RADIO_OFF
}
