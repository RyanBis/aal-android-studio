package com.xiledsystems.aal.communication;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.BETA)
public interface SmsListener {

    void onSmsReceived(String phoneNumber, String message);
    void onSmsSendFail(String phoneNumber, String message, int id, SmsError error);
    void onSmsSent(String phoneNumber, String message, int id);
    void onSmsDelivered(String phoneNumber, String message, int id);

}
