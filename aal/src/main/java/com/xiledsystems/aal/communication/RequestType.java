package com.xiledsystems.aal.communication;


/**
 * Enumeration for strictly typing {@link HttpRequest} request types.
 */
public enum RequestType {

    POST("POST", true),
    PUT("PUT", true),
    GET("GET", false),
    DELETE("DELETE", true);


    private final String typeString;
    private final boolean sendingData;

    RequestType(String type, boolean sendingData) {
        typeString = type;
        this.sendingData = sendingData;
    }

    String methodString() {
        return typeString;
    }

    boolean sendingData() {
        return sendingData;
    }
}
