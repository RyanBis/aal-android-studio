package com.xiledsystems.aal.ui.util;


import android.content.Context;
import android.view.ViewConfiguration;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

import java.lang.reflect.Field;

@State(DevelopmentState.PRODUCTION)
public class ActionBarUtil {


    /**
     * Run this method in your Activity to force the ActionBar's overflow menu to appear
     * in the ActionBar, regardless if the device has a hard menu button or not.
     * @param context
     */
    public static void forceOverflowMenu(Context context) {
        try {
            ViewConfiguration config = ViewConfiguration.get(context);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
