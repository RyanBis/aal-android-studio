package com.xiledsystems.aal.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.xiledsystems.aal.R;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public class CustomAlertDialog extends Dialog {

	public CustomAlertDialog(Context context, int theme) {
		super(context, theme);
	}

	public CustomAlertDialog(Context context) {
		super(context);
	}

	public void setMessage(String message) {
		((TextView) findViewById(R.id.message)).setText(message);
	}

	public String getMessage() {
		return ((TextView) findViewById(R.id.message)).getText().toString();
	}

	public static class Builder {
		private Context context;
		private String title;
		private String message;
		private String positiveButtonText;
		private String negativeButtonText;
		private String neutralButtonText;
		private View contentView;
		private String[] choices;
		private int selected;
		private boolean[] mSelected;
		private boolean cancelable;
		private boolean multiChoice;
		private boolean progress;
		private int layoutId = -1;
		private int theme = R.style.Dialog;
		private int multiLayout = android.R.layout.select_dialog_multichoice;
		private int singleLayout = android.R.layout.select_dialog_singlechoice;

		private OnClickListener positiveButtonClickListener, negativeButtonClickListener, neutralButtonClickListener;
		private OnMultiChoiceClickListener multiListener;

		public Builder(Context context) {
			this(context, -1);
		}

		public Builder(Context context, int layoutResId) {
			this.context = context;
			layoutId = layoutResId;
		}

		/**
		 * Set the Dialog message from String
		 * 
		 * @param message
		 * @return
		 */
		public Builder setMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder setCancelable(boolean cancel) {
			this.cancelable = cancel;
			return this;
		}

		public Builder setMultiChoiceLayout(int layoutResId) {
			multiLayout = layoutResId;
			return this;
		}

		public Builder setSingleChoiceLayout(int layoutResId) {
			singleLayout = layoutResId;
			return this;
		}

		public Builder setProgress(boolean prog) {
			this.progress = prog;
			return this;
		}

		/**
		 * Set the Dialog message from resource
		 * 
		 * @param message
		 * @return
		 */
		public Builder setMessage(int message) {
			this.message = (String) context.getText(message);
			return this;
		}

		/**
		 * Set the Dialog title from resource
		 * 
		 * @param title
		 * @return
		 */
		public Builder setTitle(int title) {
			this.title = (String) context.getText(title);
			return this;
		}

		/**
		 * Set the Dialog title from String
		 * 
		 * @param title
		 * @return
		 */
		public Builder setTitle(String title) {
			this.title = title;
			return this;
		}

		/**
		 * Set the theme this dialog will use
		 * 
		 * @param themeResId
		 * @return
		 */
		public Builder setTheme(int themeResId) {
			theme = themeResId;
			return this;
		}

		/**
		 * Set a custom content view for the Dialog. If a message is set, the contentView is not added to the Dialog...
		 * 
		 * @param v
		 * @return
		 */
		public Builder setContentView(View v) {
			this.contentView = v;
			return this;
		}

		/**
		 * Set the positive button resource and it's listener
		 * 
		 * @param positiveButtonText
		 * @param listener
		 * @return
		 */
		public Builder setPositiveButton(int positiveButtonText, OnClickListener listener) {
			this.positiveButtonText = (String) context.getText(positiveButtonText);
			this.positiveButtonClickListener = listener;
			return this;
		}

		/**
		 * Set the positive button text and it's listener
		 * 
		 * @param positiveButtonText
		 * @param listener
		 * @return
		 */
		public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
			this.positiveButtonText = positiveButtonText;
			this.positiveButtonClickListener = listener;
			return this;
		}

		/**
		 * Set the neutral button text and it's listener
		 * 
		 * @param neutralButtonText
		 * @param listener
		 * @return
		 */
		public Builder setNeutralButton(String neutralButtonText, OnClickListener listener) {
			this.neutralButtonText = neutralButtonText;
			this.neutralButtonClickListener = listener;
			return this;
		}

		/**
		 * Set the neutral button resource and it's listener
		 * 
		 * @param neutralButtonText
		 * @param listener
		 * @return
		 */
		public Builder setNeutralButton(int neutralButtonText, OnClickListener listener) {
			this.neutralButtonText = (String) context.getText(neutralButtonText);
			this.neutralButtonClickListener = listener;
			return this;
		}

		/**
		 * Set the negative button resource and it's listener
		 * 
		 * @param negativeButtonText
		 * @param listener
		 * @return
		 */
		public Builder setNegativeButton(int negativeButtonText, OnClickListener listener) {
			this.negativeButtonText = (String) context.getText(negativeButtonText);
			this.negativeButtonClickListener = listener;
			return this;
		}

		/**
		 * Set the negative button text and it's listener
		 * 
		 * @param negativeButtonText
		 * @param listener
		 * @return
		 */
		public Builder setNegativeButton(String negativeButtonText, OnClickListener listener) {
			this.negativeButtonText = negativeButtonText;
			this.negativeButtonClickListener = listener;
			return this;
		}

		public Builder setSingleChoiceItems(String[] choices, int selected, OnClickListener listener) {
			this.positiveButtonClickListener = listener;
			this.choices = choices;
			this.selected = selected;
			return this;
		}

		public Builder setMultipleChoiceItems(String[] choices, boolean[] selected, OnMultiChoiceClickListener listener) {
			multiListener = listener;
			multiChoice = true;
			this.choices = choices;
			this.mSelected = selected;
			return this;
		}

		/**
		 * Create the custom dialog
		 */
		@SuppressLint({"InlinedApi", "Override"})
		public CustomAlertDialog create() {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final CustomAlertDialog dialog = new CustomAlertDialog(context, theme);
			dialog.setCancelable(cancelable);
			View layout;
			if (layoutId == -1) {
				layout = inflater.inflate(R.layout.dialog_layout, null);
			} else {
				layout = inflater.inflate(layoutId, null);
			}
			ProgressBar progress = (ProgressBar) layout.findViewById(R.id.progress);
			if (progress != null) {
				if (!this.progress) {
					progress.setVisibility(View.GONE);
				} else {
					progress.setVisibility(View.VISIBLE);
				}
			}
			dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			TextView titleBox = (TextView) layout.findViewById(R.id.title);
			if (titleBox != null) {
				if (title != null) {
					((TextView) layout.findViewById(R.id.title)).setText(title);
				} else {
					((TextView) layout.findViewById(R.id.title)).setVisibility(View.GONE);
				}
			}
			Button posButton = (Button) layout.findViewById(R.id.positiveButton);
			if (posButton != null) {
				if (positiveButtonText != null) {
					posButton.setText(positiveButtonText);
					posButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							if (positiveButtonClickListener != null) {
								positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
							}
							dialog.dismiss();
						}
					});

				} else {
					posButton.setVisibility(View.GONE);
				}
			}
			// set the cancel button
			Button negButton = (Button) layout.findViewById(R.id.negativeButton);
			if (negButton != null) {
				if (negativeButtonText != null) {
					negButton.setText(negativeButtonText);
					negButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							if (negativeButtonClickListener != null) {
								negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
							}
							dialog.dismiss();
						}
					});
				} else {
					// if no confirm button just set the visibility to GONE
					negButton.setVisibility(View.GONE);
				}
			}
			Button neutButton = (Button) layout.findViewById(R.id.neutralButton);
			if (neutButton != null) {
				if (neutralButtonText != null) {
					neutButton.setText(neutralButtonText);
					neutButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							if (neutralButtonClickListener != null) {
								neutralButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEUTRAL);
							}
							dialog.dismiss();
						}
					});

				} else {
					neutButton.setVisibility(View.GONE);
				}
			}
			// set the content message
			TextView msg = (TextView) layout.findViewById(R.id.message);
			if (msg != null) {
				if (message != null) {
					msg.setText(message);
				} else {
					msg.setVisibility(View.GONE);
				}
			}
			if (choices != null) {
				final ListView lView = new ListView(context);
				if (multiChoice) {
					lView.setAdapter(new ArrayAdapter<String>(context, multiLayout, choices));
					lView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
					if (mSelected != null) {
						for (int i = 0; i < mSelected.length; i++) {
							lView.setItemChecked(i, mSelected[i]);
						}
					}
					if (multiListener != null) {
						lView.setOnItemClickListener(new OnItemClickListener() {
							public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
								multiListener.onClick(dialog, pos, lView.isItemChecked(pos));
							}
						});
					}
				} else {
					lView.setAdapter(new ArrayAdapter<String>(context, singleLayout, choices));
					lView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
					if (selected > -1 && selected < choices.length) {
						lView.setSelection(selected);
					}
					lView.setOnItemClickListener(new OnItemClickListener() {
						public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
							positiveButtonClickListener.onClick(dialog, pos);
						}
					});
				}
				LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT);
				parms.bottomMargin = 8;
				((LinearLayout) layout.findViewById(R.id.content)).addView(lView, new LinearLayout.LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0));
			}
			if (contentView != null) {
				// if no message set, add the contentView to the dialog body
				((LinearLayout) layout.findViewById(R.id.content)).addView(contentView, new LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.WRAP_CONTENT));
			}
			dialog.setContentView(layout);
			return dialog;
		}

		public CustomAlertDialog show() {
			CustomAlertDialog diag = create();
			diag.show();
			return diag;
		}
	}

}