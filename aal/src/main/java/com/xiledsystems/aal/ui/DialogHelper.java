package com.xiledsystems.aal.ui;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ThreadManager;


@TargetApi(Build.VERSION_CODES.CUPCAKE)
@State(DevelopmentState.PRODUCTION)
public class DialogHelper {

	private final Context context;
	private static Dialog curDialog;
	private static boolean useCustom = false;
	private static int layoutId = -1;
	private static boolean fullSize;

	public DialogHelper(Context context) {
		this.context = context;
	}

	/**
	 * Set the layout resource Id to use for all dialog layouts.
	 * See {@link #useCustomLayout(boolean)}.
	 * @param resourceId
	 */
	public void setLayoutResourceId(int resourceId) {
		layoutResourceId(resourceId);
	}

	/**
	 * Return the resource id the {@link DialogHelper} is using for it's dialog layout.
	 */
	public int getLayoutResourceId() {
		return layoutResourceId();
	}

	/**
	 * Show a basic {@link Dialog} with an "OK" button, which just closes when tapped.
	 * @param title
	 * @param message
     */
	public void showBasicDialog(final String title, final String message) {
		showBasicDialog(context, title, message);
	}

	/**
	 * Show a basic {@link Dialog} with an "OK" button, which just closes when tapped. This method allows you to pass in
	 * the string resource id to pull from your app's string resources.
	 *
	 * @param title
	 * @param message
	 */
	public void showBasicDialog(final int title, final int message) {
		showBasicDialog(context, context.getString(title), context.getString(message));
	}

	/**
	 * Show a {@link Dialog} with the given attributes. The buttonText argument is the text used on the buttons displayed.
	 * This array can have a max of 3 items. The {@link DialogListener} is used to determine which button was clicked on the dialog.
	 *
	 * @param title
	 * @param message
	 * @param clickListener
	 * @param buttonText
     */
	public void showDialog(final String title, final String message, final DialogListener clickListener, final String... buttonText) {
		showDialog(context, title, message, clickListener, buttonText);
	}

	/**
	 * Show a {@link Dialog} with the given attributes. The buttonText argument is the text used on the buttons displayed.
	 * This array can have a max of 3 items. The {@link DialogListener} is used to determine which button was clicked on the dialog.
	 * This method allows you to pass in the string resource id to pull from your app's string resources.
	 *
	 * @param title
	 * @param message
	 * @param clickListener
	 * @param buttonText
	 */
	public void showDialog(final int title, final int message, final DialogListener clickListener, final int... buttonText) {
		showDialog(context, title, message, clickListener, buttonText);
	}

	/**
	 * @return whether a {@link Dialog} is being displayed by the {@link DialogHelper}.
	 */
	public boolean isShowing() {
		return showing();
	}

	/**
	 * @return whether a {@link Dialog} is being displayed by the {@link DialogHelper}.
     */
	public boolean showing() {
		return curDialog != null && curDialog.isShowing();
	}

	/**
	 * Set the layout resource Id to use for all dialog layouts.
	 * See {@link #useCustomLayout(boolean)}.
	 * @param resourceId
     */
	public static void layoutResourceId(int resourceId) {
		layoutId = resourceId;
	}

	/**
	 * Return the resource id the {@link DialogHelper} is using for it's dialog layout.
     */
	public static int layoutResourceId() {
		return layoutId;
	}

	/**
	 * Tell the {@link DialogHelper} to use a custom layout when showing dialogs.
	 * You must also set the layout resouce with {@link #setLayoutResourceId(int)}, or
	 * {@link #layoutResourceId(int)}. Note that only one layout may be used for the {@link DialogHelper} at
	 * a time. Once the layout is changed, that layour will be used for all other calls to show a Dialog.
     */
	public static void useCustomLayout(boolean custom) {
		useCustom = custom;
	}

	/**
	 * @return whether the {@link DialogHelper} is using a custom layout for the dialog UI.
     */
	public static boolean isCustomLayout() {
		return useCustom;
	}

	/**
	 * Convenience method for showing a {@link Toast} message. This can be safely called from any
	 * Thread, as it's posted to the UI thread, if it isn't calld from said thread. This shows the
	 * {@link Toast} with the {@link Toast#LENGTH_LONG} argument.
	 * @param context
	 * @param message
     */
	public static void showToast(Context context, String message) {
		showtoast(context, message, Toast.LENGTH_LONG);
	}

	/**
	 * Convenience method for showing a {@link Toast} message. This can be safely called from any
	 * Thread, as it's posted to the UI thread, if it isn't calld from said thread. This shows the
	 * {@link Toast} with the {@link Toast#LENGTH_SHORT} argument.
	 *
	 * @param context
	 * @param message
     */
	public static void showToastShort(final Context context, final String message) {
		showtoast(context, message, Toast.LENGTH_SHORT);
	}

	/**
	 * Displays a basic dialog message. This adds an "OK" button on the button which just dismisses the dialog. This is
	 * safe to call from other threads.
	 * 
	 * @param context
	 * @param title
	 * @param message
	 */
	public static void showBasicDialog(final Context context, final String title, final String message) {
		if (!onMainThread()) {
			getHandler().post(new Runnable() {
				@Override
				public void run() {
					if (useCustom) {
						curDialog = buildBasicCustomDialog(context, layoutId, title, message).setPositiveButton("OK", null).create();
						show();
					} else {
						curDialog = buildBasicDialog(context, title, message).setPositiveButton("OK", null).create();
						show();
					}
				}
			});
		} else {
			if (useCustom) {
				curDialog = buildBasicCustomDialog(context, layoutId, title, message).setPositiveButton("OK", null).create();
				show();
			} else {
				curDialog = buildBasicDialog(context, title, message).setPositiveButton("OK", null).create();
				show();
			}
		}
	}

	/**
	 * Displays a basic dialog message. This adds an "OK" button on the button which just dismisses the dialog. This is
	 * safe to call from other threads.
	 *
	 * @param context
	 * @param title
	 * @param message
	 */
	public static void showBasicDialog(final Context context, final int title, final int message) {
		if (!onMainThread()) {
			getHandler().post(new Runnable() {
				@Override
				public void run() {
					if (useCustom) {
						curDialog = buildBasicCustomDialog(context, layoutId, context.getString(title), context.getString(message)).setPositiveButton("OK", null).create();
						show();
					} else {
						curDialog = buildBasicDialog(context, context.getString(title), context.getString(message)).setPositiveButton("OK", null).create();
						show();
					}
				}
			});
		} else {
			if (useCustom) {
				curDialog = buildBasicCustomDialog(context, layoutId, context.getString(title), context.getString(message)).setPositiveButton("OK", null).create();
				show();
			} else {
				curDialog = buildBasicDialog(context, context.getString(title), context.getString(message)).setPositiveButton("OK", null).create();
				show();
			}
		}
	}
	
	public static void setFull(boolean full) {
		fullSize = full;
	}
	
	private static void show() {
		curDialog.show();
		if (fullSize) {
			curDialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		}
	}

	/**
	 * Show a dialog to the user. This is safe to call from any thread (it will get posted to the UI thread if it is
	 * called from anywhere else). If the button text array is over 3 in size, an exception will be thrown. An exception
	 * will also be thrown if the listener is null.
	 * 
	 * @param context
	 * @param title
	 * @param message
	 * @param clickListener
	 * @param buttonText
	 */
	public static void showDialog(final Context context, final String title, final String message, final DialogListener clickListener,
			final String... buttonText) {
		if (buttonText == null || buttonText.length == 0 && clickListener == null) {
			showBasicDialog(context, title, message);
		} else {
			if (useCustom) {
				final CustomAlertDialog.Builder builder = buildBasicCustomDialog(context, layoutId, title, message);
				addCustomButtons(builder, buttonText, clickListener);
				curDialog = builder.create();
			} else {
				final AlertDialog.Builder builder = buildBasicDialog(context, title, message);
				addDefaultButtons(builder, buttonText, clickListener);
				curDialog = builder.create();
			}
			if (onMainThread()) {
				show();
			} else {
				getHandler().post(new Runnable() {
					@Override
					public void run() {
						show();
					}
				});
			}
		}
	}

	/**
	 * Show a dialog to the user. This is safe to call from any thread (it will get posted to the UI thread if it is
	 * called from anywhere else). If the button text array is over 3 in size, an exception will be thrown. An exception
	 * will also be thrown if the listener is null.
	 *
	 * @param context
	 * @param title
	 * @param message
	 * @param clickListener
	 * @param buttonText
	 */
	public static void showDialog(final Context context, final int title, final int message, final DialogListener clickListener,
								  final int... buttonText) {
		final String t = context.getString(title);
		final String m = context.getString(message);
		final String[] text;
		if (buttonText != null && buttonText.length > 0) {
			final int size = Math.min(3, buttonText.length);
			text = new String[size];
			for (int i = 0; i < text.length; i++) {
				text[i] = context.getString(buttonText[i]);
			}
		} else {
			text = null;
		}
		if (text == null || text.length == 0 && clickListener == null) {
			showBasicDialog(context, t, m);
		} else {
			if (useCustom) {
				final CustomAlertDialog.Builder builder = buildBasicCustomDialog(context, layoutId, t, m);
				addCustomButtons(builder, text, clickListener);
				curDialog = builder.create();
			} else {
				final AlertDialog.Builder builder = buildBasicDialog(context, t, m);
				addDefaultButtons(builder, text, clickListener);
				curDialog = builder.create();
			}
			if (onMainThread()) {
				show();
			} else {
				getHandler().post(new Runnable() {
					@Override
					public void run() {
						show();
					}
				});
			}
		}
	}

	private static void showtoast(final Context context, final String message, final int length) {
		if (!onMainThread()) {
			getHandler().post(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context, message, length).show();
				}
			});
		} else {
			Toast.makeText(context, message, length).show();
		}
	}
	
	private static void addCustomButtons(CustomAlertDialog.Builder builder, final String[] buttonText, final DialogListener listener) {
		int btnCount = buttonText.length;
		if (btnCount > 3) {
			throw new IllegalStateException("The dialog only allows 3 buttons, " + btnCount + " titles for buttons were input!");
		}
		if (listener == null) {
			throw new IllegalStateException("Button listener cannot be null.");
		}
		builder.setPositiveButton(buttonText[0], new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listener.buttonClicked(buttonText[0]);
			}
		});
		if (btnCount > 1) {
			builder.setNegativeButton(buttonText[1], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[1]);
				}
			});
		}
		if (btnCount > 2) {
			builder.setNegativeButton(buttonText[2], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[2]);
				}
			});
		}		
	}

	private static void addDefaultButtons(AlertDialog.Builder builder, final String[] buttonText, final DialogListener listener) {
		int btnCount = buttonText.length;
		if (btnCount > 3) {
			throw new IllegalStateException("The dialog only allows 3 buttons, " + btnCount + " titles for buttons were input!");
		}
		if (listener == null) {
			throw new IllegalStateException("Button listener cannot be null.");
		}
		builder.setPositiveButton(buttonText[0], new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listener.buttonClicked(buttonText[0]);
			}
		});
		if (btnCount > 1) {
			builder.setNegativeButton(buttonText[1], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[1]);
				}
			});
		}
		if (btnCount > 2) {
			builder.setNegativeButton(buttonText[2], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[2]);
				}
			});
		}		
	}

	private static AlertDialog.Builder buildBasicDialog(Context context, String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		return builder;
	}

	private static CustomAlertDialog.Builder buildBasicCustomDialog(Context context, int layoutId, String title, String message) {
		CustomAlertDialog.Builder builder = new CustomAlertDialog.Builder(context, layoutId);
		builder.setTitle(title);
		builder.setMessage(message);
		return builder;
	}

	private static Handler getHandler() {
		return ThreadManager.getHandler();
	}

	private static boolean onMainThread() {
		return Thread.currentThread() == Looper.getMainLooper().getThread();
	}

	public interface DialogListener {
		void buttonClicked(String buttonText);
	}

}