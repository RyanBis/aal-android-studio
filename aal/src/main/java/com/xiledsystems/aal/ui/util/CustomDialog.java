package com.xiledsystems.aal.ui.util;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.xiledsystems.aal.R;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.UNTESTED)
public class CustomDialog extends Dialog {


    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }


    public static class Builder {

        private Context context;
        private String title = "";
        private String message = "";
        private String button1Text, button2Text, button3Text;
        private boolean cancelable;
        private int theme = R.style.Dialog;
        private int layoutId = R.layout.custom_dialog_layout;
        private DialogClickListener clickListener;


        public Builder(Context context) {
            this.context = context;
        }

        public Builder(Context context, int theme) {
            this.context = context;
            this.theme = theme;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getButton1Text() {
            return button1Text;
        }

        public void setButton1Text(String button1Text) {
            this.button1Text = button1Text;
        }

        public String getButton2Text() {
            return button2Text;
        }

        public void setButton2Text(String button2Text) {
            this.button2Text = button2Text;
        }

        public String getButton3Text() {
            return button3Text;
        }

        public void setButton3Text(String button3Text) {
            this.button3Text = button3Text;
        }

        public boolean isCancelable() {
            return cancelable;
        }

        public void setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
        }

        public int getLayoutId() {
            return layoutId;
        }

        public void setLayoutId(int layoutId) {
            this.layoutId = layoutId;
        }

        public DialogClickListener getClickListener() {
            return clickListener;
        }

        public void setClickListener(DialogClickListener clickListener) {
            this.clickListener = clickListener;
        }

        @SuppressLint("Override")
        public CustomDialog create() {
            LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final CustomDialog dialog = new CustomDialog(context, theme);
            View layout = inflator.inflate(layoutId, null);
            TextView titleBox = (TextView) layout.findViewById(R.id.title);
            if (titleBox != null) {
                titleBox.setText(title);
            }
            TextView messageBox = (TextView) layout.findViewById(R.id.message);
            if (messageBox != null) {
                messageBox.setText(message);
            }
            Button b1 = (Button) layout.findViewById(R.id.button1);
            if (b1 != null) {
                b1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (clickListener != null) {
                            clickListener.onClick(dialog, 1);
                        }
                    }
                });
            }
            Button b2 = (Button) layout.findViewById(R.id.button2);
            if (b2 != null) {
                b2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (clickListener != null) {
                            clickListener.onClick(dialog, 2);
                        }
                    }
                });
            }
            Button b3 = (Button) layout.findViewById(R.id.button3);
            if (b3 != null) {
                b3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (clickListener != null) {
                            clickListener.onClick(dialog, 3);
                        }
                    }
                });
            }
            dialog.setContentView(layout);
            return dialog;
        }

        public CustomDialog show() {
            CustomDialog d = create();
            d.show();
            return d;
        }

    }

    public interface DialogClickListener {
        public void onClick(CustomDialog dialog, int buttonClicked);
    }
}
