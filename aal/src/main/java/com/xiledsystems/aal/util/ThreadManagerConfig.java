package com.xiledsystems.aal.util;


import android.os.Handler;
import android.os.Looper;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.util.concurrent.Callable;


@State(DevelopmentState.PRODUCTION)
public class ThreadManagerConfig implements Cloneable {


    /**
     * Default is <code>null</code>. The {@link ThreadManager} will determine based on how many cores the device's CPU has<br></br><br></br>
     * The amount of threads considered to be the core. These threads get started automatically when the {@link ThreadManager} is
     * initialized, as long as {@link #preLoadCore} is <code>true</code>.
     */
    public Integer coreThreads              = null;

    /**
     * Default is <code>null</code>. The {@link ThreadManager} will determine based on how many cores the device's CPU has<br></br><br></br>
     * The maximum amount of threads the {@link ThreadManager} is allowed to spin up. If this number is reached, and another call
     * is made for another thread, then that call will block until one is available.
     */
    public Integer maxThreads               = null;

    /**
     * Default is <code>true</code><br></br><br></br>
     * Specifies whether to spin up the core threads when initialized.
     */
    public boolean preLoadCore              = true;

    /**
     * Default is <code>false</code><br></br><br></br>
     * Dictates whether the {@link ThreadManager} will allow the core threads to timeout due to inactivity.
     */
    public boolean allowCoreTimeOut         = false;

    /**
     * Default is <code>3000</code><br></br><br></br>
     * Dictates how long the period of inactivity is before the {@link ThreadManager} will start to shutdown inactive core
     * threads. This only applies if {@link #allowCoreTimeOut} is <code>true</code>, otherwise it's ignored.
     */
    public long coreTimeoutPeriodMs         = 3000;

    /**
     * Default is <code>false</code><br></br><br></br>
     *
     * Used to tell the {@link ThreadManager} it's running in a unit test environment, and don't try to use
     * a {@link Handler}, or {@link Looper} class, as they don't typically unit test very well.
     */
    public boolean unitTest                 = false;


    /**
     * Set the thread counts of threads the {@link ThreadManager} will use in it's
     * backing {@link java.util.concurrent.ThreadPoolExecutor}.
     *
     * @param coreThreads How many threads will get loaded (if {@link #preLoadCore} is <code>true</code>. This is also
     *                    affected by {@link #allowCoreTimeOut}, and {@link #coreTimeoutPeriodMs}.
     * @param maxThreads The maximum anount of {@link Thread}s the {@link ThreadManager} can use at max. All other calls
     *                   to {@link ThreadManager#execute(Runnable)}, or {@link ThreadManager#execute(Callable)}, or
     *                   {@link ThreadManager#execute(Runnable, Runnable)} will block until a Thread is freed.
     * @return the instance of this {@link ThreadManagerConfig} so you can chain the config options.
     */
    public final ThreadManagerConfig withThreadCounts(int coreThreads, int maxThreads) {
        this.coreThreads = coreThreads;
        this.maxThreads = maxThreads;
        return this;
    }

    /**
     * Convenience method to tell the {@link ThreadManager} to pre load the core threads, if the manager is not
     * already initialized.
     *
     * @param preload
     * @return the instance of this {@link ThreadManagerConfig} so you can chain the config options.
     */
    public final ThreadManagerConfig withCorePreload(boolean preload) {
        preLoadCore = preload;
        return this;
    }

    /**
     * Convenience method to tell the {@link ThreadManager} if it should allow the core threads to rimeout
     * after a period of inactivity. Default is <code>false</code>. This method also allows you to set the
     * {@link #coreTimeoutPeriodMs} as the second argument.
     *
     * @param allowTimeout
     * @param timeoutPeriod
     * @return the instance of this {@link ThreadManagerConfig} so you can chain the config options.
     */
    public final ThreadManagerConfig withAllowingCoreTimeout(boolean allowTimeout, Long timeoutPeriod) {
        allowCoreTimeOut = allowTimeout;
        coreTimeoutPeriodMs = timeoutPeriod;
        return this;
    }

    /**
     * Convenience method to tell the {@link ThreadManager} that it's being used in a unit test environment.
     * This instantiates the Main threads as a background thread. This is done because using the Handler and
     * Looper in a unit test don't work very well, at least not without a lot of hacking. This is the alternative.
     * @param unitTesting
     * @return the instance of this {@link ThreadManagerConfig} so you can chain the config options.
     */
    public final ThreadManagerConfig withUnitTesting(boolean unitTesting) {
        unitTest = unitTesting;
        return this;
    }


    @Override
    public ThreadManagerConfig clone() {
        ThreadManagerConfig clone;
        try {
            clone = (ThreadManagerConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            clone = this;
        }
        return clone;
    }

    /**
     * Class used byt {@link ThreadManager} when it is running in a unit test
     * environment. It's been debated if this should be package private, but left
     * it visible in case it's useful to instantiate this in a unit test somwehere.
     */
    public static final class UnitTestUIHandler extends UIHandler {

        private final AalHandler handler;

        public UnitTestUIHandler() {
            handler = new AalHandler();
        }

        @Override
        public final void post(Runnable action) {
            handler.post(action);
        }

        @Override
        public final void postDelayed(Runnable action, long delay) {
            handler.postDelayed(action, delay);
        }

        @Override
        public final void removeCallbacks(Runnable action) {
            handler.removeCallbacks(action);
        }

        @Override
        public final void init(IThreadManager mgr) {
            handler.init(mgr);
        }

        public final void quit() {
            handler.quit();
        }
    }

    /**
     * Class used by {@link ThreadManager} when running in a non-unit test environment (in that,
     * the {@link Handler} and {@link Looper} classes work as they should when run). You shouldn't
     * need to use this class, as the {@link ThreadManager} instantiates it automatically, but it has
     * been left visible in the off-chance it may be useful elsewhere.
     */
    public static final class DefaultUIHandler extends UIHandler {

        private final Handler handler;

        public DefaultUIHandler() {
            handler = new Handler(Looper.getMainLooper());
        }

        @Override
        public final void post(Runnable action) {
            handler.post(action);
        }

        @Override
        public final void postDelayed(Runnable action, long delay) {
            handler.postDelayed(action, delay);
        }

        @Override
        public final void removeCallbacks(Runnable action) {
            handler.removeCallbacks(action);
        }

        @Override
        public void init(IThreadManager mgr) {
        }

        public final Handler getHandler() {
            return handler;
        }
    }

}
