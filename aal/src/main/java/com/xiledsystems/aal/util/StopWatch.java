package com.xiledsystems.aal.util;


public final class StopWatch {

    private long start;
    private long lastDifference;

    public final void start() {
        start = System.currentTimeMillis();
    }

    public final long stop() {
        lastDifference = System.currentTimeMillis() - start;
        return lastDifference;
    }

    public final long getDifference() {
        return lastDifference;
    }

}
