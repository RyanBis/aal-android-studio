package com.xiledsystems.aal.util;


import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Utility class for creating new LayoutParams.
 */
@State(DevelopmentState.PRODUCTION)
public final class LayoutUtil {

    private LayoutUtil() {}


    /**
     * Create a new {@link android.widget.LinearLayout.LayoutParams} with the specified {@link LayoutSize}
     * @param size
     * @return
     */
    public static LinearLayout.LayoutParams newLinear(LayoutSize size) {
        switch (size) {
            case MATCH_MATCH:
                return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            case MATCH_WRAP:
                return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            case WRAP_MATCH:
                return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            default:
                return new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }
    }

    /**
     * Create a new {@link android.widget.LinearLayout.LayoutParams} with the specified width and height
     * @param width
     * @param height
     * @return
     */
    public static LinearLayout.LayoutParams newLinear(int width, int height) {
        return new LinearLayout.LayoutParams(width, height);
    }

    /**
     * Create a new {@link android.widget.LinearLayout.LayoutParams} with the specified {@link LayoutSize}, and set it
     * to the specified {@link View}
     * @param view
     * @param size
     * @return
     */
    public static void newLinearOn(View view, LayoutSize size) {
        view.setLayoutParams(newLinear(size));
    }

    /**
     * Create a new {@link android.widget.LinearLayout.LayoutParams} with the specified width and height, and set it
     * to the specified {@link View}
     * @param view
     * @param width
     * @param height
     * @return
     */
    public static void newLinearOn(View view, int width, int height) {
        view.setLayoutParams(newLinear(width, height));
    }

    /**
     * Create a new {@link android.widget.RelativeLayout.LayoutParams} with the specified {@link LayoutSize}
     * @param size
     * @return
     */
    public static RelativeLayout.LayoutParams newRelative(LayoutSize size) {
        switch (size) {
            case MATCH_MATCH:
                return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            case MATCH_WRAP:
                return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            case WRAP_MATCH:
                return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            default:
                return new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        }
    }

    /**
     * Create a new {@link android.widget.RelativeLayout.LayoutParams} with the specified width and height
     * @param width
     * @param height
     * @return
     */
    public static RelativeLayout.LayoutParams newRelative(int width, int height) {
        return new RelativeLayout.LayoutParams(width, height);
    }

    /**
     * Create a new {@link android.widget.RelativeLayout.LayoutParams} with the specified {@link LayoutSize}, and set it
     * to the specified {@link View}
     * @param view
     * @param size
     * @return
     */
    public static void newRelativeOn(View view, LayoutSize size) {
        view.setLayoutParams(newRelative(size));
    }

    /**
     * Create a new {@link android.widget.RelativeLayout.LayoutParams} with the specified width and height, and set it
     * to the specified {@link View}
     * @param view
     * @param width
     * @param height
     * @return
     */
    public static void newRelativeOn(View view, int width, int height) {
        view.setLayoutParams(newRelative(width, height));
    }

    /**
     * Create a new {@link android.widget.FrameLayout.LayoutParams} with the specified {@link LayoutSize}
     * @param size
     * @return
     */
    public static FrameLayout.LayoutParams newFrame(LayoutSize size) {
        switch (size) {
            case MATCH_MATCH:
                return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            case MATCH_WRAP:
                return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            case WRAP_MATCH:
                return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.MATCH_PARENT);
            default:
                return new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        }
    }

    /**
     * Create a new {@link android.widget.FrameLayout.LayoutParams} with the specified width and height
     * @param width
     * @param height
     * @return
     */
    public static FrameLayout.LayoutParams newFrame(int width, int height) {
        return new FrameLayout.LayoutParams(width, height);
    }

    /**
     * Create a new {@link android.widget.FrameLayout.LayoutParams} with the specified {@link LayoutSize}, and set it
     * to the specified {@link View}
     * @param view
     * @param size
     * @return
     */
    public static void newFrameOn(View view, LayoutSize size) {
        view.setLayoutParams(newFrame(size));
    }

    /**
     * Create a new {@link android.widget.FrameLayout.LayoutParams} with the specified width and height, and set it
     * to the specified {@link View}
     * @param view
     * @param width
     * @param height
     * @return
     */
    public static void newFrameOn(View view, int width, int height) {
        view.setLayoutParams(newFrame(width, height));
    }

    /**
     * Create a new {@link ViewGroup.LayoutParams} with the specified {@link LayoutSize}
     * @param size
     * @return
     */
    public static ViewGroup.LayoutParams newViewGroup(LayoutSize size) {
        switch (size) {
            case MATCH_MATCH:
                return new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            case MATCH_WRAP:
                return new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            case WRAP_MATCH:
                return new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            default:
                return new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    /**
     * Create a new {@link ViewGroup.LayoutParams} with the specified width and height
     * @param width
     * @param height
     * @return
     */
    public static ViewGroup.LayoutParams newViewGroup(int width, int height) {
        return new ViewGroup.LayoutParams(width, height);
    }

    /**
     * Create a new {@link ViewGroup.LayoutParams} with the specified {@link LayoutSize}, and set it
     * to the specified {@link View}
     * @param view
     * @param size
     * @return
     */
    public static void newViewGroupOn(View view, LayoutSize size) {
        view.setLayoutParams(newViewGroup(size));
    }

    /**
     * Create a new {@link ViewGroup.LayoutParams} with the specified width and height, and set it
     * to the specified {@link View}
     * @param view
     * @param width
     * @param height
     * @return
     */
    public static void newViewGroupOn(View view, int width, int height) {
        view.setLayoutParams(newViewGroup(width, height));
    }

    /**
     * Create a new LayoutParam of the given {@link LayoutType} with the given
     * {@link LayoutSize}
     * @param type
     * @param size
     * @param <T>
     * @return
     */
    public static <T extends ViewGroup.LayoutParams> T newParams(LayoutType type, LayoutSize size) {
        switch (type) {
            case FRAME:
                return (T) newFrame(size);
            case LINEAR:
                return (T) newLinear(size);
            case RELATIVE:
                return (T) newRelative(size);
            default:
                return (T) newViewGroup(size);
        }
    }

    /**
     * Create a new LayoutParam of the given {@link LayoutType} with the given width, and height
     * @param type
     * @param width
     * @param height
     * @param <T>
     * @return
     */
    public static <T extends ViewGroup.LayoutParams> T newParams(LayoutType type, int width, int height) {
        switch (type) {
            case FRAME:
                return (T) newFrame(width, height);
            case LINEAR:
                return (T) newLinear(width, height);
            case RELATIVE:
                return (T) newRelative(width, height);
            default:
                return (T) newViewGroup(width, height);
        }
    }

}
