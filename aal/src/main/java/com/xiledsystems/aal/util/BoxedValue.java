package com.xiledsystems.aal.util;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Generic class used to "box" any value. This is handy when manipulating a value between threads, and final won't cut it.
 * This class is NOT thread-safe, so it is up to you to ensure safety when manipulating the value in multiple threads.
 */
@State(DevelopmentState.UNTESTED)
public class BoxedValue<T> {

    public T value;

    public BoxedValue() {
    }

    public BoxedValue(T val) {
        value = val;
    }


    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
