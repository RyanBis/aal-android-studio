package com.xiledsystems.aal.util;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;


@TargetApi(Build.VERSION_CODES.M)
public final class MarshmallowUtil {


    public static boolean hasPermission(Context context, String permission) {
        return (context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
    }

    public static void requestPermission(Activity context, String[] permissions, int requestCode) {
        context.requestPermissions(permissions, requestCode);
    }

}
