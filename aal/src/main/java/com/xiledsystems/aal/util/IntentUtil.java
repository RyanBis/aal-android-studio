package com.xiledsystems.aal.util;


import android.content.Intent;
import android.net.Uri;
import android.provider.AlarmClock;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;


public final class IntentUtil {

    private final static String DEFAULT_EMAIL_CHOOSER_MSG = "Send e-mail with...";

    private IntentUtil() {
    }

    public static Intent pickPicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        return intent;
    }

    public static Intent generalShare(String subject, String bodyText, String url) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (!TextUtil.isEmpty(url)) {
            intent.setData(Uri.parse(url));
        }
        intent.putExtra(Intent.EXTRA_TEXT, bodyText);
        intent.setType("text/plain");
        return intent;
    }

    /**
     * Requires {@link android.Manifest.permission#SET_ALARM} permission.
     *
     * @param message
     * @param hour
     * @param minutes
     * @return
     */
    public static Intent createAlarm(String message, int hour, int minutes) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        intent.putExtra(AlarmClock.EXTRA_MESSAGE, message);
        intent.putExtra(AlarmClock.EXTRA_HOUR, hour);
        intent.putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        return intent;
    }

    public static Intent addCalendarEvent(String title, String location, Calendar begin, Calendar end) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setData(Events.CONTENT_URI);
        intent.putExtra(Events.TITLE, title);
        intent.putExtra(Events.EVENT_LOCATION, location);
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, begin);
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, end);
        return intent;
    }

    public static final Intent newEmail(String email, String subject) {
        return newEmail(subject, null, DEFAULT_EMAIL_CHOOSER_MSG, null, email);
    }

    public static final Intent newEmail() {
        return newEmail(null, null, DEFAULT_EMAIL_CHOOSER_MSG, null);
    }

    public static final Intent newEmail(File file) {
        return newEmail(null, null, DEFAULT_EMAIL_CHOOSER_MSG, file);
    }

    public static final Intent newEmailMult(ArrayList<File> files) {
        return newEmailMult(null, null, DEFAULT_EMAIL_CHOOSER_MSG, files);
    }

    public static final Intent newEmailMult() {
        return newEmailMult(null, null, DEFAULT_EMAIL_CHOOSER_MSG, null);
    }

    /**
     * Create a new Intent for sending an email.
     *
     * @param subject
     * @param body
     * @param chooserMsg the message shown in the chooser dialog title
     * @param file       attachment
     * @param toEmails
     * @return
     */
    public static final Intent newEmail(String subject, String body, String chooserMsg, File file, String... toEmails) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        if (!TextUtil.isEmpty(subject)) {
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        if (!TextUtil.isEmpty(body)) {
            intent.putExtra(Intent.EXTRA_TEXT, body);
        }
        if (toEmails != null && toEmails.length > 0) {
            intent.putExtra(Intent.EXTRA_EMAIL, toEmails);
        }
        if (file != null && file.exists()) {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        }
        return Intent.createChooser(intent, chooserMsg);
    }

    /**
     * Create a new Intent for sending an email with multiple file attachments
     *
     * @param subject
     * @param body
     * @param chooserMsg the message shown in the chooser dialog title
     * @param files      attachments
     * @param toEmails
     * @return
     */
    public static final Intent newEmailMult(String subject, String body, String chooserMsg, ArrayList<File> files, String... toEmails) {
        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.setType("message/rfc822");
        if (!TextUtil.isEmpty(subject)) {
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        if (!TextUtil.isEmpty(body)) {
            intent.putExtra(Intent.EXTRA_TEXT, body);
        }
        if (toEmails != null && toEmails.length > 0) {
            intent.putExtra(Intent.EXTRA_EMAIL, toEmails);
        }
        if (files != null && files.size() > 0) {
            ArrayList<Uri> uris = new ArrayList<Uri>(files.size());
            for (File f : files) {
                uris.add(Uri.fromFile(f));
            }
            intent.putExtra(Intent.EXTRA_STREAM, uris);
        }
        return Intent.createChooser(intent, chooserMsg);
    }
}
