package com.xiledsystems.aal.util;


import android.annotation.TargetApi;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Build;

public final class LollipopUtil {

    private LollipopUtil() {}

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static SoundPool newPool(int maxStreams, int usage, int contentType) {
        AudioAttributes audioAttrs = new AudioAttributes.Builder().setUsage(usage)
                .setContentType(contentType).build();
        return new SoundPool.Builder().setMaxStreams(maxStreams).setAudioAttributes(audioAttrs).build();
    }

}
