package com.xiledsystems.aal.util;


import android.annotation.TargetApi;
import android.os.Build;
import java.util.Arrays;


@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public final class ByteBuff {

    private byte[] bytes;
    private int curSize = 0;
    private int requestedSize = 10;


    public ByteBuff(byte[] bytes) {
        this.bytes = bytes;
        curSize = bytes.length;
        requestedSize = curSize;
    }

    public ByteBuff(int size) {
        requestedSize = size;
        bytes = newBytes(requestedSize);
    }

    public ByteBuff() {
        bytes = newBytes(requestedSize);
    }

    public final ByteBuff append(byte[] data) {
        if (data.length + curSize >= bytes.length) {
            int diff = (data.length + curSize) - bytes.length;
            diff += 10;
            byte[] tmp = newBytes(bytes.length + diff);
            System.arraycopy(bytes, 0, tmp, 0, curSize);
            System.arraycopy(data, 0, tmp, curSize, data.length);
            bytes = tmp;
        } else {
            System.arraycopy(data, 0, bytes, curSize, data.length);
        }
        curSize += data.length;
        return this;
    }

    public final ByteBuff append(byte data) {
        curSize += 1;
        if (curSize >= bytes.length) {
            byte[] tmp = newBytes(bytes.length + 11);
            System.arraycopy(bytes, 0, tmp, 0, bytes.length);
            tmp[curSize] = data;
            bytes = tmp;
        } else {
            bytes[curSize] = data;
        }
        return this;
    }

    public final byte[] bytes() {
        return bytes;
    }

    public final byte[] pollBytes() {
        byte[] b = Arrays.copyOf(bytes, curSize);
        bytes = newBytes(requestedSize);
        curSize = 0;
        return b;
    }

    public final int length() {
        return curSize;
    }

    private static byte[] newBytes(int size) {
        return new byte[size];
    }

}
