package com.xiledsystems.aal.util;


import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class to manage a few different ways of thread processing.
 * <p/>
 * The most basic, is {@link ThreadManager#async(Runnable)}, which uses an
 * AsyncTask as the backing processor. This is perfect for one-off situations, where
 * it's not being run very often.
 * <p/>
 * Next, is using the class as a singleton, and it becomes a Thread pool manager. The
 * default instance sets the core thread count to the number of cores in the device + 1, and
 * the maximum thread count to twice that, plus 1. The core threads can be preloaded when you
 * get the first instance of this class.
 * <p/>
 * Another way to have fun with threads, is to use the {@link AalHandler} class, which this class
 * allows you to get new instances. {@link AalHandler} allows you to post {@link Runnable}s to the
 * backing thread dispensed from this {@link ThreadManager} using {@link #newAalHandler()}, or
 * {@link #newAalHandler(String)}. You can also retrieve an already created one from a name you
 * provided with {@link #newAalHandler(String)}. You can also get a Set of the names of the current
 * handlers with {@link #getHandlerNames()}.
 */
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@State(DevelopmentState.PRODUCTION)
public final class ThreadManager implements IThreadManager {


    private static final String DEFAULT_HANDLER_NAME = "AalHandler (%s)";

    private static ThreadManager manager;
    private static ThreadPoolExecutor executor;
    private static UIHandler uiHandler;
    private static final Set<PoolUIRunner> wrappedRunners = new HashSet<>();
    private static final AtomicBoolean initialized = new AtomicBoolean(false);
    private final ConcurrentHashMap<String, AalHandler> aalHandlers = new ConcurrentHashMap<>();
    private final AtomicInteger currentHandlers;
    private final AtomicInteger coreThreads;
    private final AtomicInteger maxThreads;
    private final Object MAP_LOCK = new Object();
    private ThreadManagerConfig config;


    private ThreadManager(ThreadManagerConfig managerConfig) {
        coreThreads = new AtomicInteger(1);
        maxThreads = new AtomicInteger(3);
        currentHandlers = new AtomicInteger(0);
        updateFromConfig(managerConfig);
        init();
    }

    /**
     * Returns the singleton instance of this {@link ThreadManager}. If the instance is null,
     * {@link #getInstance(ThreadManagerConfig)} is called with a new instance of
     * {@link ThreadManagerConfig}.
     */
    public static ThreadManager getInstance() {
        if (manager == null) {
            manager = new ThreadManager(new ThreadManagerConfig());
        }
        return manager;
    }

    /**
     *
     * @param preLoadCores - This is ignored now.
     * @return
     *
     * @deprecated - This is deprecated, and just calls {@link #getInstance()}.
     */
    @Deprecated
    public static ThreadManager getInstance(boolean preLoadCores) {
        return getInstance();
    }

    /**
     * Returns the instance of {@link ThreadManager}, and sets the coreThreads, and maxThreds.
     */
    @Deprecated
    public static ThreadManager getInstance(int coreThreads, int maxThreads) {
        if (manager != null) {
            manager.config.coreThreads = coreThreads;
            manager.config.maxThreads = maxThreads;
            manager.updateFromConfig(manager.config);
            return manager;
        } else {
            ThreadManagerConfig config = new ThreadManagerConfig();
            config.coreThreads = coreThreads;
            config.maxThreads = maxThreads;
            return getInstance(config);
        }
    }

    /**
     * @return singleton instance of {@link ThreadManager} with provided {@link ThreadManagerConfig}. This
     * will update the backing {@link ThreadManagerConfig} with what you pass here, and update it's internal
     * configuration options accordingly.
     *
     */
    public static ThreadManager getInstance(ThreadManagerConfig config) {
        if (config == null) {
            config = new ThreadManagerConfig();
        }
        if (manager == null) {
            manager = new ThreadManager(config);
            if (config.preLoadCore) {
                executor.prestartAllCoreThreads();
            }
        } else {
            manager.updateFromConfig(config);
        }
        return manager;
    }

    /**
     * Execute a Runnable action using the backing Thread pool.
     *
     * @param action
     */
    public final void execute(Runnable action) {
        checkInit();
        executor.execute(action);
    }

    /**
     * Execute Runnable action using the backing Thread pool. Then, execute
     * the Runnable finished on the UI thread.
     *
     * @param action
     * @param finished
     */
    public final void execute(final Runnable action, final Runnable finished) {
        checkInit();
        PoolUIRunner run = new PoolUIRunner(action, finished);
        wrappedRunners.add(run);
        execute(run);
    }

    /**
     * Execute a {@link Callable} with the backing thread pool. This returns
     * a {@link Future} object.
     *
     * @param action
     * @param <T>
     * @return
     */
    public final <T> Future<T> execute(final Callable<T> action) {
        checkInit();
        return executor.submit(action);
    }

    /**
     * Remove a Runnable action from the backing Thread pool.
     *
     * @param action
     */
    public final void remove(Runnable action) {
        if (!executor.remove(action)) {
            PoolUIRunner run = getWrappedRunner(action);
            if (run != null) {
                remove(run);
                executor.purge();
            }
        } else {
            executor.purge();
        }
    }

    /**
     *
     * @return whether the {@link ThreadManager} is initialized, and ready to execute actions
     * on background Threads.
     */
    public final boolean initialized() {
        return initialized.get();
    }

    public final void shutdown() {
        wrappedRunners.clear();
        synchronized (MAP_LOCK) {
            AalHandler h;
            List<String> keys = Collections.list(aalHandlers.keys());
            for (String key : keys) {
                h = aalHandlers.remove(key);
                h.quit();
            }
        }
        executor.shutdownNow();
        initialized.set(false);
        currentHandlers.set(0);
    }

    /**
     * Shuts down the backing Thread pool. This may take some time.
     *
     * @deprecated This is deprecated. Please use {@link #shutdown()} instead.
     */
    @Deprecated
    public final void dispose() {
        shutdown();
    }

    /**
     * Overload of {@link #disposeBlocking(long)}, which uses a
     * wait time of 1000ms.
     */
    public final boolean disposeBlocking() {
        boolean success = false;
        executor.shutdownNow();
        initialized.set(false);
        try {
            success = executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
            return success;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * This is a blocking method, in that it waits for a maximum of the provided time in milliseconds
     * for a shutdown signal from the backing Thread pool.
     *
     * @return true if received signal that the Thread pool shutdown
     */
    public final boolean disposeBlocking(long maxToWait) {
        boolean success = false;
        executor.shutdownNow();
        initialized.set(false);
        try {
            success = executor.awaitTermination(maxToWait, TimeUnit.MILLISECONDS);
            return success;
        } catch (InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Adjust the number of threads the backing ThreadPool is allowed to use. coreThreads is the amount of threads
     * that get spun up initially, with maxThreads being, well, the maximum number of threads this {@link ThreadManager}
     * is allowed to use.
     */
    public final void setThreadCount(int coreThreads, int maxThreads) {
        this.coreThreads.set(coreThreads);
        this.maxThreads.set(maxThreads);
        updateThreadCounts();
    }

    /**
     * Returns a new instance of {@link AalHandler} with a thread from the ThreadPool backing this {@link ThreadManager}.
     */
    public final AalHandler newAalHandler() {
        final String name = String.format(DEFAULT_HANDLER_NAME, aalHandlers.size() + 1);
        AalHandler h = new AalHandler(this);
        synchronized (MAP_LOCK) {
            aalHandlers.put(name, h);
        }
        return h;
    }

    /**
     * Same as {@link #newAalHandler()}, with an argument which allows you to specify the name of the backing Thread.
     */
    public final AalHandler newAalHandler(String threadName) {
        AalHandler h = new AalHandler(this, threadName);
        synchronized (MAP_LOCK) {
            aalHandlers.put(threadName, h);
        }
        return h;
    }

    /**
     * @return <code>true</code> if an {@link AalHandler} already exists with the given name.
     */
    public final boolean hasAalHandler(String name) {
        return aalHandlers.containsKey(name);
    }

    /**
     *
     * @return the {@link AalHandler} linked to the {@link ThreadManager} with the given name. If none exist with that name,
     * <code>null</code> will be returned.
     */
    public final AalHandler getAalHandler(String name) {
        return aalHandlers.get(name);
    }

    /**
     * @return a {@link Set} of the names of the {@link AalHandler}s that were created with this manager.
     */
    public final Set<String> getHandlerNames() {
        return new HashSet<>(Collections.list(aalHandlers.keys()));
    }

    /**
     * Internal method, please don't call this yourself.
     */
    public final void handlerOpened() {
        currentHandlers.incrementAndGet();
        updateThreadCounts();
    }

    /**
     * Internal method, please don't call this yourself.
     */
    public final void handlerClosed() {
        currentHandlers.decrementAndGet();
        updateThreadCounts();
    }

    /**
     * Initializes the backing thread pool. This is not necessary to call, as it's checked before executing anything,
     * but it's mainly here for unit testing.
     */
    public final void init() {
        if (!initialized.get()) {
            initexecutor(coreThreads.get(), maxThreads.get(), config.allowCoreTimeOut, config.coreTimeoutPeriodMs);
            if (uiHandler != null) {
                uiHandler.init(this);
            }
        }
    }

    private void initexecutor(int core, int max, boolean allowTimeOut, long timeoutPeriod) {
        executor = new ThreadPoolExecutor(core, max, timeoutPeriod, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<Runnable>());
        executor.allowCoreThreadTimeOut(allowTimeOut);
        initialized.set(true);
    }

    private void updateFromConfig(ThreadManagerConfig managerConfig) {
        this.config = managerConfig.clone();
        if (config.coreThreads == null || config.maxThreads == null) {
            int cores = DeviceUtil.getNumberOfCores();
            config.coreThreads = config.coreThreads == null ? cores + 1 : config.coreThreads;
            config.maxThreads = config.maxThreads == null ? (cores * 2) + 1 : config.maxThreads;
        }
        boolean updateThreads = false;
        if (coreThreads.get() != config.coreThreads) {
            updateThreads = true;
            coreThreads.set(config.coreThreads);
        }
        if (maxThreads.get() != config.maxThreads) {
            updateThreads = true;
            maxThreads.set(config.maxThreads);
        }
        if (config.unitTest) {
            if (uiHandler == null || uiHandler instanceof ThreadManagerConfig.DefaultUIHandler) {
                uiHandler = new ThreadManagerConfig.UnitTestUIHandler();
            }
        } else {
            if (uiHandler == null || uiHandler instanceof ThreadManagerConfig.UnitTestUIHandler) {
                uiHandler = new ThreadManagerConfig.DefaultUIHandler();
            }
        }
        if (initialized.get()) {
            uiHandler.init(this);
        }
        if (executor != null) {
            executor.allowCoreThreadTimeOut(config.allowCoreTimeOut);
            if (config.allowCoreTimeOut && config.coreTimeoutPeriodMs > 0) {
                executor.setKeepAliveTime(config.coreTimeoutPeriodMs, TimeUnit.MILLISECONDS);
            }
            if (updateThreads) {
                updateThreadCounts();
            }
        }
    }

    private PoolUIRunner getWrappedRunner(Runnable action) {
        for (PoolUIRunner run : wrappedRunners) {
            if (run.getRun().equals(action)) {
                return run;
            }
        }
        return null;
    }

    private void checkInit() {
        if (!initialized.get()) {
            init();
        }
    }

    private void updateThreadCounts() {
        executor.setCorePoolSize(coreThreads.get());
        executor.setMaximumPoolSize(maxThreads.get() + currentHandlers.get());
    }

    /**
     * Use an AsyncTask to run an action in a background
     * thread. This is good to use for one-off, smaller operations.
     *
     * @param action
     */
    public static void async(final Runnable action) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                action.run();
                return null;
            }
        }.execute();
    }

    /**
     * Use an AsyncTask to run an action in a background
     * thread. This is good to use for one-off, smaller operations.
     * The provided uiAciton Runnable is run on the UI thread right
     * after the threadAction is complete.
     *
     * @param threadAction
     * @param uiAction
     * @deprecated - Use {@link #async(AsyncRunnable)} instead.
     */
    @Deprecated
    public static void async(final Runnable threadAction, final Runnable uiAction) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                if (threadAction != null) {
                    threadAction.run();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (uiAction != null) {
                    uiAction.run();
                }
            }
        }.execute();
    }

    /**
     * Use an AsyncTask to run an action in a background
     * thread. This is good to use for one-off, smaller operations.
     * The provided {@link AsyncRunnable} allows you to run operations
     * in a background thread ({@link AsyncRunnable#runInThread()}), then on the
     * UI thread once done ({@link AsyncRunnable#runAfterInUI()}).
     *
     * @param actions
     */
    public static void async(final AsyncRunnable actions) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                if (actions != null) {
                    actions.runInThread();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (actions != null) {
                    actions.runAfterInUI();
                }
            }
        }.execute();
    }

    /**
     * @deprecated - You should use {@link #getUiHandler()} instead.
     * @return
     */
    @Deprecated
    public static Handler getHandler() {
        if (uiHandler == null || !(uiHandler instanceof ThreadManagerConfig.DefaultUIHandler)) {
            return new Handler(Looper.getMainLooper());
        }
        return ((ThreadManagerConfig.DefaultUIHandler)uiHandler).getHandler();
    }

    /**
     * @return the {@link UIHandler} for posting {@link Runnable}s to the UI thread. This replaces
     * {@link #getHandler()}, only because using this class makes it easier to unit test. When NOT testing,
     * the returned instance will contain a {@link Handler} which is linked to the UI thread.
     */
    public static UIHandler getUiHandler() {
        if (uiHandler == null) {
            return new ThreadManagerConfig.DefaultUIHandler();
        }
        return uiHandler;
    }

    /**
     * Interface used when executing an operation on a background thread, then posting something to the UI
     * thread when it's done.
     *
     * See {@link #async(AsyncRunnable)}.
     */
    public interface AsyncRunnable {
        void runInThread();

        void runAfterInUI();
    }



    private final static class PoolUIRunner implements Runnable {

        private final Runnable action;
        private final Runnable finished;

        public PoolUIRunner(Runnable action, Runnable finished) {
            this.action = action;
            this.finished = finished;
        }

        public final Runnable getRun() {
            return action;
        }

        @Override
        public final void run() {
            action.run();
            uiHandler.post(finished);
        }
    }

}