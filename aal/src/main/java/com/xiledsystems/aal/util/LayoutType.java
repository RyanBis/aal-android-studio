package com.xiledsystems.aal.util;

/**
 * An enumeration for declaring which type of LayoutParams you want to create
 */
public enum LayoutType {

    /**
     * {@link android.widget.RelativeLayout}
     */
    RELATIVE,

    /**
     * {@link android.widget.LinearLayout}
     */
    LINEAR,

    /**
     * {@link android.widget.FrameLayout}
     */
    FRAME,

    /**
     * {@link android.view.ViewGroup}
     */
    VIEWGROUP

}
