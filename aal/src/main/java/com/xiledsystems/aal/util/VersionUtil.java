package com.xiledsystems.aal.util;


import android.os.Build;


public final class VersionUtil {

    private VersionUtil() {}

    /**
     *
     * @return true if the device is running Marshmallow or higher.
     */
    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= 23;
    }

    /**
     *
     * @return true if the device is running Lollipop or higher
     */
    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= 21;
    }

    /**
     *
     * @return true if the device is running KitKat or above.
     */
    public static boolean isKitKat() {
        return Build.VERSION.SDK_INT >= 19;
    }
}
