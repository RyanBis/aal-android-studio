package com.xiledsystems.aal.util;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Alternate class to Android's {@link android.os.Handler} for background threads. This class was created so that it's easier
 * to unit test code -- Android's {@link android.os.Handler} does not unit test very easily without a lot of hacking. The backing Threads are
 * taken from the thread pool in {@link ThreadManager} (but don't take up slots, in that, when a new {@link AalHandler} is created, the core
 * and max thread counts are adjusted. To get an instance of this class, call {@link ThreadManager#newAalHandler()}.
 * <b>You should remember to call {@link #quit()} when you are done with this handler.</b>
 */
@State(DevelopmentState.UNTESTED)
public final class AalHandler {

    private final LinkedBlockingQueue<HandlerRunnable> runnables;
    private IThreadManager manager;

    private Thread thread;
    private AtomicBoolean running = new AtomicBoolean(false);
    private AtomicBoolean paused = new AtomicBoolean(false);


    AalHandler(IThreadManager manager) {
        runnables = new LinkedBlockingQueue<>();
        this.manager = manager;
        init();
    }

    AalHandler() {
        runnables = new LinkedBlockingQueue<>();
    }

    AalHandler(IThreadManager manager, final String threadName) {
        this(manager);
        post(new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName(threadName);
            }
        });
    }

    /**
     * Post a {@link Runnable} to execute on the thread backing this {@link AalHandler}.
     */
    public final void post(Runnable action) {
        runnables.add(new HandlerRunnable(action, System.currentTimeMillis(), 0));
    }

    /**
     * Post a {@link Runnable} to execute on the backing thread of this {@link AalHandler} after
     * te given delay.
     */
    public final void postDelayed(Runnable action, long delay) {
        runnables.add(new HandlerRunnable(action, System.currentTimeMillis(), delay));
    }

    /**
     * Removes any posted {@link Runnable}s posted to this {@link AalHandler}, if it has not
     * been executed yet.
     */
    public final void removeCallbacks(Runnable action) {
        Iterator<HandlerRunnable> it = runnables.iterator();
        while (it.hasNext()) {
            HandlerRunnable run = it.next();
            if (run.runnable == action) {
                run.cancel();
            }
        }
    }

    /**
     * @return if this {@link AalHandler} is still running, and can execute {@link Runnable}s posted to it.
     */
    public final boolean isAlive() {
        return running.get() && thread != null;
    }

    /**
     * @return the {@link Thread} backing this {@link AalHandler}.
     */
    public final Thread getThread() {
        return thread;
    }

    /**
     * Pause this {@link AalHandler} from executing any more {@link Runnable}s. Unlike {@link #stop()}, or {@link #quit()},
     * this keeps the backing Thread alive. You will have to call {@link #resume()} to continue processing any Runnables
     * in the queue.
     *
     * @see #stop()
     * @see #quit()
     */
    public final void pause() {
        if (running.get()) {
            paused.compareAndSet(false, true);
        }
    }

    /**
     * Only applicable if {@link #pause()} has been called. This resumes processing of any {@link Runnable}s in
     * the queue.
     *
     * @see #pause()
     */
    public final void resume() {
        if (running.get()) {
            paused.compareAndSet(true, false);
        }
    }

    /**
     * Stop this {@link AalHandler} from executing any more {@link Runnable}s posted. Un-executed Runnables will get cleared
     * from the queue. However, some Runnables may execute before the Thread exits. You will have to call {@link #start()} in
     * order for this {@link AalHandler} to run any further Runnables. This method does nothing if this
     * {@link AalHandler} is already stopped.
     *
     * @see #pause()
     * @see #quit()
     */
    public final void stop() {
        shutdown(false);
    }

    /**
     * Start executing {@link Runnable}s posted to this {@link AalHandler}, if there are any in the queue. This method only needs
     * to be called if {@link #stop()} was called at some point. It's also safe to call this after {@link #pause()}, as it will
     * call {@link #resume()}.
     *
     * @see #stop()
     * @see #pause()
     * @see #quit()
     */
    public final void start() {
        init();
    }

    /**
     * Clears the queue of this {@link AalHandler}, interrupts, and stops the backing Thread. If a Runnable is being executed at
     * the time of calling this method, it will complete, but no other Runnables will execute. If you want the instance to be
     * able to execute any more {@link Runnable}s after calling this method, you must call {@link #start()} first.
     *
     * @see #stop()
     * @see #pause()
     */
    public final void quit() {
        shutdown(true);
    }


    final void init(IThreadManager mgr) {
        if (!running.get()) {
            manager = mgr;
            init();
        }
    }


    private void shutdown(boolean interrupt) {
        if (running.compareAndSet(true, false)) {

            if (interrupt)
                thread.interrupt();

            runnables.clear();
            thread = null;

            // clearFromManager() is called when the Thread is interrupted, so only call this if it's NOT interrupted.
            if (!interrupt)
                clearFromManager();
        }
    }

    private void clearFromManager() {
        manager.handlerClosed();
    }

    private void init() {
        if (!running.get()) {
            manager.handlerOpened();
            manager.execute(new HandleRunnable());
        }
        // Just in case start() was called instead of resume() after calling pause().
        resume();
    }


    private final static class HandlerRunnable {

        private final Runnable runnable;
        private final long timePosted;
        private final long delay;
        private final AtomicBoolean canceled;

        HandlerRunnable(Runnable action, long posted, long delay) {
            runnable = action;
            timePosted = posted;
            this.delay = delay;
            canceled = new AtomicBoolean(false);
        }

        public final void run() {
            runnable.run();
        }

        final void cancel() {
            canceled.set(true);
        }

        final boolean canceled() {
            return canceled.get();
        }

        final boolean ready(long curTime) {
            return canceled() || delay == 0 || (curTime - timePosted) >= delay;
        }
    }

    private final class HandleRunnable implements Runnable {

        private long curTime;

        @Override
        public final void run() {
            thread = Thread.currentThread();
            running.set(true);
            while (running.get()) {
                // Bail out if the thread got interrupted
                if (doInterruptCheck())
                    return;

                // Don't process anything if paused, but we still want the sleep in here to keep the thread alive.
                if (!paused.get()) {
                    if (!runnables.isEmpty()) {
                        curTime = System.currentTimeMillis();
                        Iterator<HandlerRunnable> it = runnables.iterator();
                        while (it.hasNext()) {
                            // If the thread is interrupted, we want to bail out at the soonest point, so this is an
                            // additional check
                            if (doInterruptCheck())
                                return;

                            // Like with the interrupted check, we'll be just as aggressive with paused() calls
                            if (paused.get())
                                continue;

                            HandlerRunnable run = it.next();

                            // A ready check makes sure if it's a delayed runnable, the appropriate amount of time has
                            // passed, OR the runnable has been canceled. This is because this is the only area allowed
                            // to remove anything from the queue to ensure thread safety.
                            if (run.ready(curTime)) {
                                it.remove();
                                if (!run.canceled()) {
                                    run.run();
                                }
                            }
                        }
                    }
                }
                // Add a small sleep time to avoid being a battery drain.
                try {
                    Thread.sleep(5);
                } catch (Exception e) {
                }
            }
        }

        private boolean doInterruptCheck() {
            if (Thread.interrupted()) {
                running.set(false);
                clearFromManager();
                return true;
            }
            return false;
        }
    }

}
