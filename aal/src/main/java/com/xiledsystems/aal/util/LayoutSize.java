package com.xiledsystems.aal.util;

/**
 * Enumeration for declaring the automatic width/height parameters
 * when creating new LayoutParams.
 */
public enum LayoutSize {

    /**
     * match_parent width, match_parent height
     */
    MATCH_MATCH,

    /**
     * wrap_content width, match_parent height
     */
    WRAP_MATCH,

    /**
     * match_parent width, wrap_content height
     */
    MATCH_WRAP,

    /**
     * wrap_content width, wrap_content height
     */
    WRAP_WRAP

}
