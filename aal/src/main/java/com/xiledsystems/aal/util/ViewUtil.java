package com.xiledsystems.aal.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.lang.reflect.Field;


@State(DevelopmentState.PRODUCTION)
@TargetApi(Build.VERSION_CODES.CUPCAKE)
public final class ViewUtil {
	
	private ViewUtil() {		
	}
	
	/**
	 * This will hide the view from being drawn on screen.
	 *
	 * @deprecated Use {@link #hide(View)} instead
	 * 
	 * @param view
	 */
	@Deprecated
	public static void hideView(View view) {
		view.setVisibility(View.GONE);
	}

	/**
	 * This will hide the view from being drawn on screen.
	 *
	 * @param view
	 */
	public static void hide(View view) {
		view.setVisibility(View.GONE);
	}
	
	/**
	 * Show a view on screen, whether it is hidden, or invisible
	 * 
	 * @param view
	 *
	 * @deprecated Use {@link #show(View)} instead
	 */
	@Deprecated
	public static void unHideView(View view) {
		view.setVisibility(View.VISIBLE);
	}

	/**
	 * Show a view on screen, whether it is hidden, or invisible
	 *
	 * @param view
	 */
	public static void show(View view) {
		view.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Make a view invisible. The view will still take up space on
	 * screen.
	 * 
	 * @param view
	 *
	 * @deprecated Use {@link #invisible(View)} instead
	 */
	@Deprecated
	public static void makeInvisible(View view) {
		view.setVisibility(View.INVISIBLE);
	}

	/**
	 * Make a view invisible. The view will still take up space on
	 * screen.
	 *
	 * @param view
	 */
	public static void invisible(View view) {
		view.setVisibility(View.INVISIBLE);
	}

	/**
	 * Convenience method for finding a view from a specific view, without the need
	 * to cast (this method does it for you).
	 *
	 * @param resId the resource Id of the view to find
	 * @param view the containing view
	 * @param <T>
	 * @return
	 *
	 * @deprecated Use {@link #find(int, View)} instead
	 */
	@Deprecated
	public static <T extends View> T findView(int resId, View view) {
		return (T) view.findViewById(resId);
	}

	/**
	 * Convenience method for finding a view from a specific view, without the need
	 * to cast (this method does it for you).
	 *
	 * @param resId the resource Id of the view to find
	 * @param parent the containing view
	 * @param <T>
	 * @return
	 */
	public static <T extends View> T find(int resId, View parent) {
		return (T) parent.findViewById(resId);
	}

	/**
	 * Convenience method for finding a view within an Activity without the need
	 * to cast (it's handled by this method).
	 *
	 * @param resId the resource Id of the view to find
	 * @param context the containing Activity
	 * @param <T>
	 * @return
	 *
	 * @deprecated Use {@link #find(int, Activity)} instead
	 */
	@Deprecated
	public static <T extends View> T findView(int resId, Activity context) {
		return (T) context.findViewById(resId);
	}

	/**
	 * Convenience method for finding a view within an Activity without the need
	 * to cast (it's handled by this method).
	 *
	 * @param resId the resource Id of the view to find
	 * @param context the containing Activity
	 * @param <T>
	 * @return
	 */
	public static <T extends View> T find(int resId, Activity context) {
		return (T) context.findViewById(resId);
	}

	/**
	 * Convenience method to inflate a layout from xml. This automatically
	 * casts to whatever View you want.
	 *
	 * @param context
	 * @param layoutId
	 * @param <T>
	 * @return
	 */
	public static <T extends View> T create(Activity context, int layoutId) {
		return (T) LayoutInflater.from(context).inflate(layoutId, null);
	}

	/**
	 * Convenience method to set text on a {@link TextView} that you have a {@link TextWatcher} set on.
	 * This will remove the watcher, then set the text, then re-add the watcher. This is handy if you want
	 * to set the text without the {@link TextWatcher} firing.
	 * @param view
	 * @param watcher
	 * @param text
	 */
	public static void setText(TextView view, TextWatcher watcher, String text) {
		view.removeTextChangedListener(watcher);
		view.setText(text);
		view.addTextChangedListener(watcher);
	}

	/**
	 * Same as {@link #setText(TextView, TextWatcher, String)}, only with a {@link Spannable} instead
	 * of a String.
	 * @param view
	 * @param watcher
	 * @param spannable
	 */
	public static void setText(TextView view, TextWatcher watcher, Spannable spannable) {
		view.removeTextChangedListener(watcher);
		view.setText(spannable);
		view.addTextChangedListener(watcher);
	}

	/**
	 * Convenience method to set a {@link CompoundButton} (eg {@link android.widget.CheckBox}'s check, without
	 * firing the {@link android.widget.CompoundButton.OnCheckedChangeListener}.
	 * @param view
	 * @param checked
	 */
	public static void setChecked(CompoundButton view, boolean checked) {
		try {
			Field listener = view.getClass().getDeclaredField("mOnCheckedChangeListener");
			listener.setAccessible(true);
			CompoundButton.OnCheckedChangeListener mListener = (CompoundButton.OnCheckedChangeListener) listener.get(view);
			view.setOnCheckedChangeListener(null);
			view.setChecked(checked);
			view.setOnCheckedChangeListener(mListener);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Show the soft keyboard.
	 * 
	 * @param context
	 */
	public static void showKeyboard(Context context) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
	}
	
	/**
	 * Hide the soft keyboard.
	 * @param context
	 * @param view - Any view that's currently in the window
	 */
	public static void hideKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(),0);
	}

	public static Point getScreenSize(Activity context) {
		Point point = new Point();
		Display display = context.getWindowManager().getDefaultDisplay();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			display.getSize(point);
		} else {
			point.x = display.getWidth();
			point.y = display.getHeight();
		}
		return point;
	}

	public static Point getAvailableSize(Activity context) {
		Point point = new Point();
		View content = ViewUtil.find(android.R.id.content, context);
		point.x = content.getWidth();
		point.y = content.getHeight();
		return point;
	}

}