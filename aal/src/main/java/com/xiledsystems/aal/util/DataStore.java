package com.xiledsystems.aal.util;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


/**
 * This is a convenience class for storing/retrieving {@link java.io.Serializable} objects to disk.
 * <p/>
 * You must make sure to add the following to your AndroidManifest.xml if you are going to save to
 * external storage:
 * <p/>
 * <pre>
 *     &lt;manifest ...&gt;
 * &lt;uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" /&gt;
 * &lt;uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" /&gt;
 * &lt;/manifest&gt;
 * </pre>
 * <p/>
 * As long as this class is class within an Activity which implements {@link com.xiledsystems.aal.context.IAalActivity}, then
 * permission handling will be handled for you.
 */
@TargetApi(Build.VERSION_CODES.DONUT)
public final class DataStore extends NonDisplayComponent implements OnRequestPermissionsListener {


    private static final int WRITE_REQ = 1771;

    private PermissionListener permissionListener;
    private boolean hasPermissions = Build.VERSION.SDK_INT < Build.VERSION_CODES.M;

    /**
     * Use this constructor if you are only planning on writing to the internal app storage, or cache area (no permissions are required).
     *
     * @param context
     */
    public DataStore(Context context) {
        super(context);
        if (VersionUtil.isMarshmallow()) {
            hasPermissions = hasPermissions_private();
        }
    }

    /**
     * @return whether the app has been granted the requisite permissions to write to external memory. This is a cached
     * boolean, if you want to check permissions directly from the system, then call {@link #forceCheckPermissions()}, which will
     * also update the cached boolean.
     */
    public final boolean hasPermissions() {
        return hasPermissions;
    }

    /**
     * @return whether the app has been granted the requisite permissions to write to external memory. This will recheck on a system level
     * if permissions are granted, and update the cached boolean (see {@link #hasPermissions()}).
     */
    public final boolean forceCheckPermissions() {
        return hasPermissions = hasPermissions_private();
    }

    /**
     * Request the requisite permissions to write to external memory. The supplied {@link PermissionListener} gets called when the
     * permissions are granted by the end user (or if they're not).
     *
     * @param permissionListener
     */
    public final void requestPermission(PermissionListener permissionListener) {
        this.permissionListener = permissionListener;
        MarshmallowUtil.requestPermission((Activity) getContext(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_REQ);
    }

    /**
     * Store a Serializable object to the app's internal storage. (Files directory)
     *
     * @param context  - The app, activity, or service context
     * @param filename - The name of the file to create for this object
     * @param object   - The object to store
     * @return true if the object was stored successfully
     */
    public static <T extends Serializable> boolean put(Context context, String filename, T object) {
       return put_private(context, FileLocation.INTERNAL, filename, object);
    }

    /**
     * Overload of {@link #put(Context, String, Serializable)}.
     * @param filename
     * @param object
     * @param <T>
     * @return
     */
    public final <T extends Serializable> boolean put(String filename, T object) {
        return put_private(getContext(), FileLocation.INTERNAL, filename, object);
    }

    /**
     * Store a byte[] to the app's internal storage. (Files directory)
     *
     * @param context  - The app, activity, or service context
     * @param fileName - The name of the file to create for this object
     * @param data     - The byte[] to store
     * @return true if the byte[] was stored successfully
     */
    public static boolean put(Context context, String fileName, byte[] data) {
        try {
            FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fos.write(data);
            fos.flush();
            fos.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Overload of {@link #put(Context, String, byte[])}.
     *
     * @param fileName
     * @param data
     * @return
     */
    public final boolean put(String fileName, byte[] data) {
        return put(getContext(), fileName, data);
    }

    /**
     * Store a Serializable object to the app's cache directory.
     *
     * @param context  The app, activity, or service context
     * @param filename - The name of the file to create for this object
     * @param object   - The object to store
     * @return true if the object was stored successfully
     */
    public static <T extends Serializable> boolean putToCache(Context context, String filename, T object) {
        return put_private(context, FileLocation.CACHE, filename, object);
    }

    /**
     * Overload of {@link #putToCache(Context, String, Serializable)}.
     * @param filename
     * @param object
     * @param <T>
     * @return
     */
    public final <T extends Serializable> boolean putToCache(String filename, T object) {
        return put_private(getContext(), FileLocation.CACHE, filename, object);
    }

    /**
     * Store a Serializable object to the device's external storage
     *
     * @param filename - The name of the file to create for this object
     * @param object   - The object to store
     * @return true if the object was stored successfully
     */
    public final <T extends Serializable> boolean putToExternal(String filename, T object) {
        return put_private(FileLocation.EXTERNAL, filename, object);
    }


    /**
     * Returns a previously stored Object from the app's internal storage.
     *
     * @param context  The app, activity, or service context
     * @param filename - The name of the file previously created
     * @return The object that was stored to the specified filename. If none found, null is returned.
     */
    public static <T extends Serializable> T get(Context context, String filename) {
        return get_private(FileLocation.INTERNAL, context, filename);
    }

    /**
     * Overload of {@link #get(Context, String)}.
     * @param filename
     * @param <T>
     * @return
     */
    public final <T extends Serializable> T get(String filename) {
        return get_private(FileLocation.INTERNAL, getContext(), filename);
    }

    /**
     * Returns a previously stored byte[] from the app's internal storage.
     *
     * @param context  The app, activity, or service context
     * @param fileName - The name of the file previously created
     * @return The byte[] that was stored to the specified filename. If none found, null is returned.
     */
    public static byte[] getBytes(Context context, String fileName) {
        byte[] data = null;
        try {
            FileInputStream in = context.openFileInput(fileName);
            in.read(data);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Overload of {@link #getBytes(Context, String)}.
     * @param fileName
     * @return
     */
    public final byte[] getBytes(String fileName) {
        return getBytes(getContext(), fileName);
    }

    /**
     * Returns a previously stored Object from the app's internal storage.
     *
     * @param context      The app, activity, or service context
     * @param filename     The name of the file previously created
     * @param defaultValue If an error, or no file found, the default value will be returned
     * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
     */
    public static <T extends Serializable> T get(Context context, String filename, T defaultValue) {
        T value = get(context, filename);
        return value == null ? defaultValue : value;
    }

    /**
     * Overload of {@link #get(Context, String, Serializable)}.
     *
     * @param filename
     * @param defaultValue
     * @param <T>
     * @return
     */
    public final <T extends Serializable> T get(String filename, T defaultValue) {
        T value = get(getContext(), filename);
        return value == null ? defaultValue : value;
    }

    /**
     * Returns a previously stored Object from the app's cache directory.
     *
     * @param context  The app, activity, or service context
     * @param filename - The name of the file previously created
     * @return The object that was stored to the specified filename. If none found, null is returned.
     */
    public static <T extends Serializable> T getFromCache(Context context, String filename) {
        return get_private(FileLocation.CACHE, context, filename);
    }

    /**
     * Overload of {@link #getFromCache(Context, String)}.
     *
     * @param filename
     * @param <T>
     * @return
     */
    public final <T extends Serializable> T getFromCache(String filename) {
        return get_private(FileLocation.CACHE, getContext(), filename);
    }

    /**
     * Returns a previously stored Object from the app's cache directory.
     *
     * @param context      The app, activity, or service context
     * @param filename     The name of the file previously created
     * @param defaultValue If an error, or no file found, the default value will be returned
     * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
     */
    public static <T extends Serializable> T getFromCache(Context context, String filename, T defaultValue) {
        T value = get_private(FileLocation.CACHE, context, filename);
        return value == null ? defaultValue : value;
    }

    /**
     * Overload of {@link #getFromCache(Context, String, Serializable)}.
     *
     * @param filename
     * @param defaultValue
     * @param <T>
     * @return
     */
    public final <T extends Serializable> T getFromCache(String filename, T defaultValue) {
        T value = get_private(FileLocation.CACHE, getContext(), filename);
        return value == null ? defaultValue : value;
    }

    /**
     * Returns a previously stored Object from the device's external storage.
     *
     * @param filename - The name of the file previously created
     * @return The object that was stored to the specified filename. If none found, null is returned.
     */
    public final <T extends Serializable> T getFromExternal(String filename) {
        return get_private(FileLocation.EXTERNAL, null, filename);
    }

    /**
     * Returns a previously stored Object from the device's external storage.
     *
     * @param filename     The name of the file previously created
     * @param defaultValue If an error, or no file found, the default value will be returned
     * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
     */
    public final <T extends Serializable> T getFromExternal(String filename, T defaultValue) {
        T value = getFromExternal(filename);
        return value == null ? defaultValue : value;
    }

    private boolean hasPermissions_private() {
        return !VersionUtil.isMarshmallow() || MarshmallowUtil.hasPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private static <T extends Serializable> T get_private(FileLocation location, Context context, String filename) {
        T value = null;
        ObjectInputStream ois = null;
        try {
            switch (location) {
                case INTERNAL:
                    ois = new ObjectInputStream(context.openFileInput(filename));
                    break;
                case CACHE:
                    File file = new File(context.getCacheDir(), filename);
                    if (file.exists()) {
                        ois = new ObjectInputStream(new FileInputStream(file));
                    }
                    break;
                case EXTERNAL:
                    file = new File(Environment.getExternalStorageDirectory(), filename);
                    if (file.exists()) {
                        ois = new ObjectInputStream(new FileInputStream(file));
                    }
                    break;
            }
            if (ois != null) {
                value = (T) ois.readObject();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ois != null) {
            try {
                ois.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    private static <T extends Serializable> boolean put_private(Context context, FileLocation location, String filename, T object) {
        boolean success = false;
        ObjectOutputStream out = null;
        try {
            switch (location) {
                case INTERNAL:
                    out = new ObjectOutputStream(context.openFileOutput(filename, Context.MODE_PRIVATE));
                    break;
                case CACHE:
                    File file = new File(context.getCacheDir(), filename);
                    if (file.exists()) {
                        file.delete();
                    }
                    out = new ObjectOutputStream(new FileOutputStream(file));
                    break;
                case EXTERNAL:
                    file = new File(Environment.getExternalStorageDirectory(), filename);
                    if (file.exists()) {
                        file.delete();
                    }
                    out = new ObjectOutputStream(new FileOutputStream(file));
                    break;
            }
            out.writeObject(object);
            out.flush();
            out.close();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    private <T extends Serializable> boolean put_private(FileLocation location, String filename, T object) {
        return !hasPermissions || put_private(getContext(), location, filename, object);
    }

    @Override
    protected final OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected final OnCreateDestroyListener getCreateDestroyListener() {
        return null;
    }

    @Override
    protected final OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected final OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected final OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected final OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected final CoreServiceLifecycleListener getServiceLifecycleListener() {
        return null;
    }

    @Override
    protected final OnRequestPermissionsListener getPermissionsListener() {
        return this;
    }

    @Override
    public final void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == WRITE_REQ) {
            if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    hasPermissions = true;
                    if (permissionListener != null) {
                        permissionListener.permissionsResult(true);
                    }
                } else {
                    permissionListener.permissionsResult(false);
                }
            }
        }
    }

    /**
     * Callback which is called when permissions are granted (or not) after calling {@link #requestPermission(PermissionListener)}. Also
     * see {@link #hasPermissions()}.
     */
    public interface PermissionListener {
        void permissionsResult(boolean gotPermissions);
    }

    private enum FileLocation {
        INTERNAL,
        EXTERNAL,
        CACHE,
    }
}
