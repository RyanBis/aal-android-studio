package com.xiledsystems.aal.util;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@State(DevelopmentState.PRODUCTION)
public final class FragUtil {

    private FragUtil() {
    }

    public static void replaceFragment(Activity context, int fragContainerId, Fragment frag) {
        FragmentManager man = context.getFragmentManager();
        FragmentTransaction ft = man.beginTransaction();
        ft.replace(fragContainerId, frag);
        ft.commit();
    }

    public static void replaceFragment(Activity context, int fragContainerId, Fragment frag, int enterAnim, int exitAnim) {
        FragmentManager man = context.getFragmentManager();
        FragmentTransaction ft = man.beginTransaction();
        ft.setCustomAnimations(enterAnim, exitAnim);
        ft.replace(fragContainerId, frag);
        ft.commit();
    }
}
