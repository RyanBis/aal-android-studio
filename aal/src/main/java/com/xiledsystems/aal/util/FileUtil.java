package com.xiledsystems.aal.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import android.content.Context;
import android.os.Environment;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public final class FileUtil {

    private FileUtil() {
    }

    /**
     * Copies a file from one location to another. The new path is destructive. If you
     * copy a file to an existing file's path, that file with be overwritten with the
     * new content.
     * Paths expect an absolute file path.
     * @param srcPath
     * @param newPath
     * @return
     */
    public static boolean copyFile(String srcPath, String newPath) {
        File old = new File(srcPath);
        File newf = new File(newPath);
        if (newf.exists()) {
            newf.delete();
        }
        InputStream is;
        try {
            is = new FileInputStream(old);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        BufferedInputStream bin = new BufferedInputStream(is);
        OutputStream out;
        try {
            out = new FileOutputStream(newf);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            try {
                bin.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        BufferedOutputStream bout = new BufferedOutputStream(out);
        byte[] buf = new byte[1024];
        try {
            while (bin.read(buf) >= 0) {
                bout.write(buf);
            }
            bin.close();
            if (bout != null) {
                bout.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Moves a file from one location to another. This will delete any existing file
     * at the new path's location, and delete the original file.
     * Paths expect an absolute file path.
     *
     * @param oldPath
     * @param newPath
     * @return
     */
    public static boolean moveFile(String oldPath, String newPath) {
        File old = new File(oldPath);
        File newf = new File(newPath);
        if (newf.exists()) {
            newf.delete();
        }
        InputStream is;
        try {
            is = new FileInputStream(old);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        BufferedInputStream bin = new BufferedInputStream(is);
        OutputStream out;
        try {
            out = new FileOutputStream(newf);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            try {
                bin.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        BufferedOutputStream bout = new BufferedOutputStream(out);
        byte[] buf = new byte[1024];
        try {
            while (bin.read(buf) >= 0) {
                bout.write(buf);
            }
            bin.close();
            if (bout != null) {
                bout.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        old.delete();
        return true;
    }

    /**
     * Store a Serializable object to the app's internal storage. (Files directory)
     *
     * @param context  - The app, activity, or service context
     * @param filename - The name of the file to create for this object
     * @param object   - The object to store
     * @return true if the object was stored successfully
     */
    public static boolean storeObject(Context context, String filename, Object object) {
        try {
            FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(object);
            os.flush();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Store a byte[] to the app's internal storage. (Files directory)
     *
     * @param context  - The app, activity, or service context
     * @param fileName - The name of the file to create for this object
     * @param data   - The byte[] to store
     * @return true if the byte[] was stored successfully
     */
    public static boolean storeBytes(Context context, String fileName, byte[] data) {
        try {
            FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            fos.write(data);
            fos.flush();
            fos.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Store a Serializable object to the app's cache directory.
     *
     * @param context  The app, activity, or service context
     * @param filename - The name of the file to create for this object
     * @param object   - The object to store
     * @return true if the object was stored successfully
     */
    public static boolean storeObjectToCache(Context context, String filename, Object object) {
        File file = new File(context.getCacheDir(), filename);
        if (file.exists()) {
            file.delete();
        }
        OutputStream out;
        try {
            out = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(object);
            os.flush();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Store a Serializable object to the device's external storage
     *
     * @param context  The app, activity, or service context
     * @param filename - The name of the file to create for this object
     * @param object   - The object to store
     * @return true if the object was stored successfully
     */
    public static boolean storeObjectToExternal(Context context, String filename, Object object) {
        File file = new File(Environment.getExternalStorageDirectory(), filename);
        if (file.exists()) {
            file.delete();
        }
        OutputStream out;
        try {
            out = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(object);
            os.flush();
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Returns a previously stored Object from the app's internal storage.
     *
     * @param context  The app, activity, or service context
     * @param filename - The name of the file previously created
     * @return The object that was stored to the specified filename. If none found, null is returned.
     */
    public static Object getObject(Context context, String filename) {
        Object value = null;
        try {
            FileInputStream filestream = context.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(filestream);
            value = ois.readObject();
            ois.close();
        } catch (FileNotFoundException e) {
        } catch (StreamCorruptedException e) {
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
        }
        return value;
    }

    /**
     * Returns a previously stored byte[] from the app's internal storage.
     *
     * @param context  The app, activity, or service context
     * @param fileName - The name of the file previously created
     * @return The byte[] that was stored to the specified filename. If none found, null is returned.
     */
    public static byte[] getBytes(Context context, String fileName) {
        byte[] data = null;
        try {
            FileInputStream in = context.openFileInput(fileName);
            in.read(data);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * Returns a previously stored Object from the app's internal storage.
     *
     * @param context      The app, activity, or service context
     * @param filename     The name of the file previously created
     * @param defaultValue If an error, or no file found, the default value will be returned
     * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
     */
    public static Object getObject(Context context, String filename, Object defaultValue) {
        Object value = defaultValue;
        try {
            FileInputStream filestream = context.openFileInput(filename);
            ObjectInputStream ois = new ObjectInputStream(filestream);
            value = ois.readObject();
            ois.close();
        } catch (FileNotFoundException e) {
        } catch (StreamCorruptedException e) {
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
        }
        return value;
    }

    /**
     * Convenience method which casts {@link #getObject(Context, String)} for you.
     *
     * @param context
     * @param fileName
     * @param <T>
     * @return
     */
    public static <T> T getObject_cast(Context context, String fileName) {
        return (T) getObject(context, fileName);
    }

    /**
     * Convenience method which casts {@link #getObject(Context, String, Object)} for you.
     *
     * @param context
     * @param fileName
     * @param defValue
     * @param <T>
     * @return
     */
    public static <T> T getObject_cast(Context context, String fileName, T defValue) {
        return (T) getObject(context, fileName, defValue);
    }

    /**
     * Returns a previously stored Object from the app's cache directory.
     *
     * @param context  The app, activity, or service context
     * @param filename - The name of the file previously created
     * @return The object that was stored to the specified filename. If none found, null is returned.
     */
    public static Object getObjectFromCache(Context context, String filename) {
        Object value = null;
        try {
            File file = new File(context.getCacheDir(), filename);
            if (file.exists()) {
                FileInputStream filestream = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(filestream);
                value = ois.readObject();
                ois.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Returns a previously stored Object from the app's cache directory.
     *
     * @param context      The app, activity, or service context
     * @param filename     The name of the file previously created
     * @param defaultValue If an error, or no file found, the default value will be returned
     * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
     */
    public static Object getObjectFromCache(Context context, String filename, Object defaultValue) {
        Object value = new Object();
        try {
            File file = new File(context.getCacheDir(), filename);
            if (!file.exists()) {
                return defaultValue;
            }
            FileInputStream filestream = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(filestream);
            value = ois.readObject();
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
            return defaultValue;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return defaultValue;
        }
        return value;
    }

    /**
     * Convenience method which casts {@link #getObjectFromCache(Context, String)} for you.
     *
     * @param context
     * @param filename
     * @param <T>
     * @return
     */
    public static <T> T getObjectFromCache_cast(Context context, String filename) {
        return (T) getObjectFromCache(context, filename);
    }

    /**
     * Convenience method which casts {@link #getObjectFromCache(Context, String, Object)} for you.
     *
     * @param context
     * @param filename
     * @param defValue
     * @param <T>
     * @return
     */
    public static <T> T getObjectFromCache_cast(Context context, String filename, T defValue) {
        return (T) getObjectFromCache(context, filename, defValue);
    }

    /**
     * Returns a previously stored Object from the device's external storage.
     *
     * @param filename - The name of the file previously created
     * @return The object that was stored to the specified filename. If none found, null is returned.
     */
    public static Object getObjectFromExternal(String filename) {
        Object value = null;
        try {
            File file = new File(Environment.getExternalStorageDirectory(), filename);
            if (file.exists()) {
                FileInputStream filestream = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(filestream);
                value = ois.readObject();
                ois.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * Returns a previously stored Object from the device's external storage.
     *
     * @param filename     The name of the file previously created
     * @param defaultValue If an error, or no file found, the default value will be returned
     * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
     */
    public static Object getObjectFromExternal(String filename, Object defaultValue) {
        Object value = new Object();
        try {
            File file = new File(Environment.getExternalStorageDirectory(), filename);
            if (!file.exists()) {
                return defaultValue;
            }
            FileInputStream filestream = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(filestream);
            value = ois.readObject();
            ois.close();
        } catch (IOException e) {
            e.printStackTrace();
            return defaultValue;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return defaultValue;
        }
        return value;
    }

    /**
     * Convenience method which casts {@link #getObjectFromExternal(String)} for you.
     *
     * @param filename
     * @param <T>
     * @return
     */
    public static <T> T getObjectFromExternal_cast(String filename) {
        return (T) getObjectFromExternal(filename);
    }

    /**
     * Convenience method which casts {@link #getObjectFromExternal(String, Object)} for you.
     *
     * @param filename
     * @param defValue
     * @param <T>
     * @return
     */
    public static <T> T getObjectFromExternal_cast(String filename, T defValue) {
        return (T) getObjectFromExternal(filename, defValue);
    }

    /**
     * Like {@link #deleteFile(File)}, only any directories with the name provided will NOT be
     * deleted.
     *
     * @param dir
     * @param exemptDirName
     * @return
     */
    public static boolean deleteFile(File dir, String exemptDirName) {
        boolean success = true;
        if (dir != null)
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                File f = files[i];
                if (f.isDirectory()) {
                    if (!f.getName().equals(exemptDirName)) {
                        success = deleteFile(f);
                    } else {
                        success = true;
                    }
                } else {
                    success = f.delete();
                }
                if (!success) {
                    return false;
                }
            }
        } else {
            success = dir.delete();
        }
        return success;
    }

    /**
     * Delete the file/directory recursively. If you give this method a file, it will be deleted, and if you
     * give it a directory, the directory's contents will be deleted, and the dir removed.
     *
     * @param dir
     * @return true if everything was deleted successfully
     */
    public static boolean deleteFile(File dir) {
        boolean success = true;
        if (dir != null) {
            if (dir.isDirectory()) {
                File[] files = dir.listFiles();
                for (int i = 0; i < files.length; i++) {
                    File f = files[i];
                    if (f.isDirectory()) {
                        success = deleteFile(f);
                    } else {
                        success = f.delete();
                    }
                    if (!success) {
                        return false;
                    }
                }
            } else {
                success = dir.delete();
            }
        }
        return success;
    }

}