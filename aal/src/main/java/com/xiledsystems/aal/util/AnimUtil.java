package com.xiledsystems.aal.util;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ProgressBar;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@State(DevelopmentState.PRODUCTION)
public final class AnimUtil {

    private AnimUtil() {
    }

	private static AnimationListener animListener = null;
    private static Interpolator interpolator;


	public static void setListener(AnimationListener listener) {
		animListener = listener;
	}

	public static void FadeIn(View view, int duration, AnimationListener listener) {
		AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(duration);
		anim.setAnimationListener(listener);
		view.startAnimation(anim);
	}

	public static void FadeIn_hw(final View view, int duration, final AnimationListener listener) {
		view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(duration);
		anim.setAnimationListener(wrapAnimationListener(view, listener));
		view.startAnimation(anim);
	}

	public static void FadeOut(View view, int duration, AnimationListener listener) {
		AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(duration);
		anim.setAnimationListener(listener);
		view.startAnimation(anim);
	}

	public static void FadeOut_hw(View view, int duration, AnimationListener listener) {
		view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(duration);
		anim.setAnimationListener(wrapAnimationListener(view, listener));
		view.startAnimation(anim);
	}

	public static void Animate(Context context, View view, int animationRes, AnimationListener listener) {
		Animation anim = AnimationUtils.loadAnimation(context, animationRes);
		if (listener != null) {
			anim.setAnimationListener(listener);
		}
		view.startAnimation(anim);
	}

	public static void Animate_hw(Context context, View view, int animationRes, AnimationListener listener) {
		view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		Animation anim = AnimationUtils.loadAnimation(context, animationRes);
		anim.setAnimationListener(wrapAnimationListener(view, listener));
		view.startAnimation(anim);
	}

	public static void animate_hw(View view, Animation animation, AnimationListener listener) {
		view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		animation.setAnimationListener(wrapAnimationListener(view, listener));
		view.startAnimation(animation);
	}

    public static void animateProgressBar(ProgressBar bar, int from, int to, int animLength, Integer delay) {
        ObjectAnimator anim = ObjectAnimator.ofInt(bar, "progress", from, to);
        anim.setDuration(animLength);
        if (delay != null) {
            anim.setStartDelay(delay);
        }
        if (interpolator != null) {
            anim.setInterpolator(interpolator);
        }
        anim.start();
    }

	public static void animateProgressBar_hw(ProgressBar bar, int from, int to, int animLength, Integer delay) {
		ObjectAnimator anim = ObjectAnimator.ofInt(bar, "progress", from, to);
		anim.setDuration(animLength);
		anim.addListener(wrapAnimatorListener(bar, null));
		if (delay != null) {
			anim.setStartDelay(delay);
		}
		if (interpolator != null) {
			anim.setInterpolator(interpolator);
		}
		anim.start();
	}

	/**
	 * Create a new {@link TranslateAnimation} object which is {@link Animation#RELATIVE_TO_PARENT}
	 * @param fromX
	 * @param toX
	 * @param fromY
	 * @param toY
	 * @return
	 */
	public static TranslateAnimation newTranslateRelativeToParent(float fromX, float toX, float fromY, float toY) {
		return new TranslateAnimation(Animation.RELATIVE_TO_PARENT, fromX, Animation.RELATIVE_TO_PARENT, toX, Animation.RELATIVE_TO_PARENT, fromY,
				Animation.RELATIVE_TO_PARENT, toY);
	}

	/**
	 * Convenience method which creates a {@link TranslateAnimation} object with the values provided, which
	 * is {@link Animation#RELATIVE_TO_PARENT}. This also calls {@link TranslateAnimation#setFillAfter(boolean)}, setting
	 * it to <code>true</code>.
	 * @param duration
	 * @param startOffset
	 * @param fromX
	 * @param toX
	 * @param fromY
	 * @param toY
	 * @return
	 */
	public static TranslateAnimation newTranslateRelativeToParent(long duration, long startOffset, float fromX, float toX, float fromY, float toY) {
		TranslateAnimation t = newTranslateRelativeToParent(fromX, toX, fromY, toY);
		t.setDuration(duration);
		t.setStartOffset(startOffset);
		t.setFillAfter(true);
		return t;
	}

	/**
	 * Create a new {@link TranslateAnimation} object which is {@link Animation#RELATIVE_TO_SELF}
	 * @param fromX
	 * @param toX
	 * @param fromY
	 * @param toY
	 * @return
	 */
	public static TranslateAnimation newTranslateRelativeToSelf(float fromX, float toX, float fromY, float toY) {
		return new TranslateAnimation(Animation.RELATIVE_TO_SELF, fromX, Animation.RELATIVE_TO_SELF, toX, Animation.RELATIVE_TO_SELF, fromY,
				Animation.RELATIVE_TO_SELF, toY);
	}

	/**
	 * Convenience method which creates a {@link TranslateAnimation} object with the values provided, which
	 * is {@link Animation#RELATIVE_TO_SELF}. This also calls {@link TranslateAnimation#setFillAfter(boolean)}, setting
	 * it to <code>true</code>.
	 * @param duration
	 * @param startOffset
	 * @param fromX
	 * @param toX
	 * @param fromY
	 * @param toY
	 * @return
	 */
	public static TranslateAnimation newTranslateRelativeToSelf(long duration, long startOffset, float fromX, float toX, float fromY, float toY) {
		TranslateAnimation t = newTranslateRelativeToSelf(fromX, toX, fromY, toY);
		t.setDuration(duration);
		t.setStartOffset(startOffset);
		t.setFillAfter(true);
		return t;
	}

	private static AnimationListener wrapAnimationListener(final View view, final AnimationListener listener) {
		return new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				if (listener != null) {
					listener.onAnimationStart(animation);
				}
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				view.setLayerType(View.LAYER_TYPE_NONE, null);
				if (listener != null) {
					listener.onAnimationEnd(animation);
				}
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				if (listener != null) {
					listener.onAnimationRepeat(animation);
				}
			}
		};
	}

	private static AnimatorListener wrapAnimatorListener(final View view, final AnimatorListener listener) {
		return new AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
				if (listener != null) {
					listener.onAnimationStart(animation);
				}
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				view.setLayerType(View.LAYER_TYPE_NONE, null);
				if (listener != null) {
					listener.onAnimationEnd(animation);
				}
			}

			@Override
			public void onAnimationCancel(Animator animation) {
				view.setLayerType(View.LAYER_TYPE_NONE, null);
				if (listener != null) {
					listener.onAnimationCancel(animation);
				}
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
				if (listener != null) {
					listener.onAnimationRepeat(animation);
				}
			}
		};
	}

}