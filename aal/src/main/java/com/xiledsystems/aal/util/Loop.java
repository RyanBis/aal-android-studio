package com.xiledsystems.aal.util;


import android.content.Context;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.context.AalService;
import com.xiledsystems.aal.internal.NonDisplayComponent;
import java.util.LinkedList;


/**
 * Use this class if you want to loop something in it's own thread.
 * If you are not using this class within an {@link AalActivity}, or {@link AalService},
 * then remember to call {@link Loop#dispose()} on this object when you are done (usually in onDestroy()).
 *
 */
@State(DevelopmentState.UNTESTED)
public class Loop extends NonDisplayComponent {

    private /*finalish*/ Runnable action;
    private boolean running = false;
    private int delayTime = 0;
    private int interval = 1000;
    private boolean autoStop = true;
    private boolean wasRunning = false;
    private final Runner tRunner = new Runner();
    private static ThreadManager manager;
    private final Object queueLock = new Object();
    private final LinkedList<Runnable> actionQueue;

    /**
     * Recommended constructor when using within an {@link AalActivity} or {@link AalService}.
     * This passes the processing off to {@link ThreadManager} for the actual processing.
     *
     * @param context
     * @param action
     */
    public Loop(Context context, Runnable action) {
        super(context);
        this.action = action;
        manager = ThreadManager.getInstance(true);
        actionQueue = new LinkedList<>();
    }

    /**
     * Constructor for using outside of {@link AalActivity}/{@link AalService}. Remember to
     * call {@link Loop#dispose()} when you are done with this object (usually in onDestroy()).
     *
     * @param action
     */
    public Loop(Runnable action) {
        super(null);
        this.action = action;
        manager = ThreadManager.getInstance(true);
        actionQueue = new LinkedList<>();
    }

    /**
     * This shouldn't be used often. The idea is, if you have too many threads running,
     * it can be worse for performance. The backing {@link ThreadManager} class will allow
     * a max of 2 x cpu cores, so if you absolutely need to run more loops, and be able to
     * run other Thread operations, then this convenience method is here for that.
     * @param coreThreads
     * @param maxThreads
     */
    public void updateThreadCounts(int coreThreads, int maxThreads) {
        manager.setThreadCount(coreThreads, maxThreads);
    }

    /**
     * This needs to be called before calling {@link Loop#start()}. It is not needed to
     * call this method manually, as it is called automatically by the
     * {@link Loop#start()} method.
     */
    public void initialize() {
        manager.execute(tRunner);
    }

    /**
     * Starts the thread, if it isn't already, and begins processing
     * the supplied Runnable action given in the constructor.
     */
    public void start() {
        initialize();
        running = true;
    }

    public boolean isRunning() {
        return running;
    }

    /**
     * Stops processing the Runnable action.
     */
    public void stop() {
        running = false;
    }

    /**
     * Stops the thread, and explicitly clears the Runnable from the Thread.
     */
    public void dispose() {
        stop();
        manager.remove(tRunner);
    }

    /**
     * @return if this Loop is set tp AutoStop (only applies when this object is
     * called from an {@link AalActivity})
     */
    public boolean isAutoStop() {
        return autoStop;
    }

    /**
     * Set this Loop to automatically stop, and restart itself when the Activity loses/regains
     * focus. Default = true
     * @param autoStop
     */
    public void setAutoStop(boolean autoStop) {
        this.autoStop = autoStop;
    }

    /**
     * @return the interval this Loop is running at
     */
    public int getInterval() {
        return interval;
    }

    /**
     * Set the interval time in ms between loops
     *
     * @param interval
     */
    public void setInterval(int interval) {
        this.interval = interval;
    }

    /**
     * @return the time this Loop will delay before running for the first time.
     */
    public int getDelayTime() {
        return delayTime;
    }

    /**
     * Set a time to delay before running for the first time. The delay will not happen
     * again, unless {@link Loop#dispose()} is called.
     * @param delayTime
     */
    public void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

    /**
     * Post an action to this {@link Loop}, on it's backing thread.
     * @deprecated - if you need this kind of functionality, consider using {@link AalHandler} instead.
     */
    @Deprecated
    public void postAction(Runnable action) {
        synchronized (queueLock) {
            actionQueue.add(action);
        }
    }

    private void dispatchLoopEvent() {
        if (action != null) {
            action.run();
        }
    }

    private void dispatchPostedActions() {
        synchronized (queueLock) {
            int size = actionQueue.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    actionQueue.poll().run();
                }
            }
        }
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return new LifeServiceListener();
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return new OnStartStopListener() {
            @Override
            public void onStart() {
                if (autoStop && wasRunning && !running) {
                    running = true;
                    start();
                }
            }

            @Override
            public void onStop() {
                if (autoStop && running) {
                    wasRunning = true;
                    running = false;
                }
            }
        };
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new OnCreateDestroyListener() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
            }

            @Override
            public void onDestroy() {
                dispose();
            }
        };
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    private final class Runner implements Runnable {

        private Thread runnerThread;

        @Override
        public final void run() {
            int sleepTime;
            long beginTime;
            long timeDiff;
            boolean firstrun=true;
            runnerThread = Thread.currentThread();
            while (running) {
                if (runnerThread.interrupted()) {
                    running = false;
                    return;
                }
                if (firstrun) {
                    try {
                        Thread.sleep(delayTime);
                    } catch (InterruptedException e) {
                    }
                    firstrun = false;
                }
                // Here we setup a loop to keep running the dispatched event.
                beginTime = System.currentTimeMillis();
                dispatchLoopEvent();
                dispatchPostedActions();
                timeDiff = System.currentTimeMillis() - beginTime;
                sleepTime = (int) (interval - timeDiff);
                if (sleepTime > 0) {
                    try {
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    private final class LifeServiceListener extends ServiceLifecycleListener {
        @Override
        public void onDestroy() {
            dispose();
        }
    }

}