package com.xiledsystems.aal.util;

import java.io.File;
import java.io.FileFilter;
import java.util.List;
import java.util.regex.Pattern;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ConfigurationInfo;
import android.content.res.Configuration;
import android.os.BatteryManager;
import android.os.Build;
import android.util.DisplayMetrics;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@TargetApi(Build.VERSION_CODES.CUPCAKE)
@State(DevelopmentState.PRODUCTION)
public final class DeviceUtil {

    private DeviceUtil() {
    }

    public enum ScreenSize {
        SMALL,
        NORMAL,
        LARGE,
        XLARGE,
        UNKNOWN
    }

    public enum Density {
        LOW(0.75),
        MEDIUM(1.0),
        HIGH(1.5),
        XHIGH(2.0),
        XXHIGH(3.0),
        XXXHIGH(4.0);

        private final double multiplier;

        Density(double mult) {
            this.multiplier = mult;
        }

        public double getMultiplier() {
            return multiplier;
        }
    }

    /**
     * Displays a toast message stating the screen size. One of five options:
     * small, normal, large, xlarge, and unknown
     *
     * @param context
     */
    public static void showScreenSize(Context context) {
        Toast.makeText(context, "Screen Size: " + getScreenSizeString(context), Toast.LENGTH_LONG).show();
    }

    /**
     * @param context
     * @return a String stating the screen size (small, normal, large, xlarge, unknown)
     */
    public static String getScreenSizeString(Context context) {
        return getScreenSize(context).toString();
    }

    /**
     * @param context
     * @return a {@link com.xiledsystems.aal.util.DeviceUtil.ScreenSize} stating the screen size
     */
    public static ScreenSize getScreenSize(Context context) {
        Configuration config = context.getResources().getConfiguration();
        int layout = config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
        ScreenSize size;
        switch (layout) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                size = ScreenSize.SMALL;
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                size = ScreenSize.NORMAL;
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                size = ScreenSize.LARGE;
                break;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                size = ScreenSize.XLARGE;
                break;
            default:
                size = ScreenSize.UNKNOWN;
        }
        return size;
    }

    /**
     * Displays a toast message stating the density of this device. One of
     * four options:
     * low, medium, high, xhigh
     *
     * @param context
     */
    public static void showDensity(Activity context) {
        Toast.makeText(context, "Screen density: " + getDensityString(context), Toast.LENGTH_LONG).show();
    }

    /**
     * @param context
     * @return a String stating the density (low, medium, high, xhigh)
     */
    public static String getDensityString(Activity context) {
        return getDensity(context).toString();
    }

    /**
     * @param context
     * @return a {@link com.xiledsystems.aal.util.DeviceUtil.Density} stating the density (low, medium, high, xhigh, xxhigh, xxxhigh)
     */
    public static Density getDensity(Activity context) {
        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;
        Density dense = Density.LOW;
        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
                dense = Density.LOW;
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                dense = Density.MEDIUM;
                break;
            case DisplayMetrics.DENSITY_HIGH:
                dense = Density.HIGH;
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                dense = Density.XHIGH;
                break;
            // DisplayMetrics.DENSITY_XXHIGH == 480. This is to support APIs less than
            // 16
            case 480:
                dense = Density.XXHIGH;
                break;
            // DisplayMetrics,DENSITY_XXXHIGH = 640. This is to support APIs less than
            // 18
            case 640:
                dense = Density.XXXHIGH;
                break;
        }
        return dense;
    }

    /**
     * Use this method to check if the device supports OpenGL ES 2.0.
     *
     * @param context
     * @return
     */
    public static boolean supportsOpenGLES20(Context context) {
        final ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo info = manager.getDeviceConfigurationInfo();
        return info.reqGlEsVersion >= 0x20000;
    }

    /**
     * Checks to see if a Service is running. This will only check the current app's Services.
     *
     * @param context
     * @param className The name of the service you want to check.
     * @return
     */
    public static boolean isServiceRunning(Context context, String className) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        String serviceFullName = context.getPackageName() + className;
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceFullName.equalsIgnoreCase(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Clears the application's cache directory.
     *
     * @param context
     * @return
     */
    public static boolean clearAppCache(Context context) {
        boolean success;
        try {
            success = FileUtil.deleteFile(context.getCacheDir(), "lib");
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    /**
     * Clear the application's app data (this also clears the cache directory). If sstemWipe
     * is true, then the app will exit aftre calling this method (it calls
     * {@link ActivityManager#clearApplicationUserData()} ). This is only available on KitKat
     * and above. Otherwise, the systemWipe argument is ignored.
     *
     * @param context
     * @param systemWipe
     * @return
     */
    public static boolean clearAppData(Context context, boolean systemWipe) {
        boolean success;
        try {
            if (systemWipe && VersionUtil.isKitKat()) {
                success = KitKatUtil.clearAppData(context);
            } else {
                success = FileUtil.deleteFile(context.getCacheDir().getParentFile(),"lib");
            }
        } catch (Exception e) {
            e.printStackTrace();
            success = false;
        }
        return success;
    }

    /**
     * Checks to see if an Activity is running. This only checks to see if it is the "top Activity".
     *
     * @param context
     * @param className The name of the Form to check
     * @return
     */
    public static boolean isActivityRunning(Context context, String className) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        StringBuilder b = new StringBuilder();
        b.append("ComponentInfo{");
        b.append(context.getPackageName());
        b.append("/");
        b.append(context.getPackageName());
        b.append(".");
        b.append(className);
        b.append("}");
        String actName = b.toString();
        List<RunningTaskInfo> activities = manager.getRunningTasks(Integer.MAX_VALUE);
        int size = activities.size();
        for (int i = 0; i < size; i++) {
            if (activities.get(i).topActivity.toString().equalsIgnoreCase(actName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if the device is plugged in to AC, or USB. On devices with API 17 or higher,
     * this will return true if the device is being charging wirelessly as well.
     *
     * @param context
     * @return
     */
    public static boolean isPluggedIn(Context context) {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        // 4 = Wireless charging added in API 17
        return plugged == BatteryManager.BATTERY_PLUGGED_AC || plugged == BatteryManager.BATTERY_PLUGGED_USB || plugged == 4;
    }

    /**
     * @return The number of CPU cores in the device
     */
    public static int getNumberOfCores() {
        class CPUFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                return Pattern.matches("cpu[0-9]", pathname.getName());
            }
        }
        try {
            File dir = new File("/sys/devices/system/cpu");
            File[] files = dir.listFiles(new CPUFilter());
            if (files == null) {
                return 3;
            }
            return files.length;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

}
