package com.xiledsystems.aal.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaScannerConnection;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public final class MediaUtil {
	
	private MediaUtil() {		
	}

	/**
	 * Calling this method makes it so that when the user pressed the volume up/down on their device, the
	 * media volume is the volume that is affected (by default it's the ringer volume).
	 *
	 * @param context
     */
	public static void setVolumeButtonsToMedia(Activity context) {
		context.setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	/**
	 * This will inform the device that there is a new image in the gallery, and to add it to it's
	 * internal list. The same thing happens when you reboot the device, but this removes the need
	 * to do that.
	 *
	 * @param context
	 * @param fullPath
     */
	public static void addImageToGallery(Context context, String fullPath) {
		ImageConnectionClient client = new ImageConnectionClient(fullPath);
		MediaScannerConnection connection = new MediaScannerConnection(context, client);
		client.setScanner(connection);
		connection.connect();
	}

	/**
	 * Loads a bitmap from resources, and resamples the bitmap to the requested width, and height. Note that this
	 * will NOT change the aspect ratio of the image.
	 *
	 * This is just taken from Android's site here {@link "http://developer.android.com/training/displaying-bitmaps/load-bitmap.html"}
	 * @param context
	 * @param resourceId
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static Bitmap getResampledBitmap(Context context, int resourceId, int reqWidth, int reqHeight) {
		return getResampledBitmap(context.getResources(), resourceId, reqWidth, reqHeight);
	}

	/**
	 * This is just taken from Android's site here {@link "http://developer.android.com/training/displaying-bitmaps/load-bitmap.html"}
	 * @param res
	 * @param resourceId
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static Bitmap getResampledBitmap(Resources res, int resourceId, int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resourceId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resourceId, options);
	}

	/**
	 * This is just taken from Android's site here {@link "http://developer.android.com/training/displaying-bitmaps/load-bitmap.html"}
	 * @param options
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

}