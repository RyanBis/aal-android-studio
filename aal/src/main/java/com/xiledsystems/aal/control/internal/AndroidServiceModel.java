package com.xiledsystems.aal.control.internal;

import android.app.Service;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.events.EventListener;


@State(DevelopmentState.UNTESTED)
public abstract class AndroidServiceModel extends ServiceModel {

    protected AndroidServiceModel(Service service) {
        super(new AndroidServiceController(service));
    }

    public abstract Class<?> getServiceClass();

    protected AndroidServiceModel(Service service, Class<? extends Enum> category) {
        this(service);
        setCategory(category);
    }

    protected AndroidServiceModel(Service service, Class<? extends Enum> category, EventListener listener) {
        this(service, category);
        registerAllEventListener(listener);
    }

    @Override
    protected AndroidServiceController getServiceController() {
        return super.getServiceController();
    }
}
