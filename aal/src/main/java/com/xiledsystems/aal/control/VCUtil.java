package com.xiledsystems.aal.control;


import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventManager;


/**
 * Utility class for hooking up Events on common View operations (such as onClick, onLongClick, etc).
 */
@State(DevelopmentState.PRODUCTION)
public class VCUtil {

    private VCUtil() {}

    /**
     * Sets the view's onClick listener to fire an Event to the Event system, with the
     * given {@link Enum} category.
     * @param view
     * @param category
     * @param <E>
     */
    public static <E extends Enum> void setClickEvent(View view, final E category) {
        setClickEvent(view, category, (Object[]) null);
    }

    /**
     * Sets the view's onClick listener to fire an Event to the Event system, with the
     * given {@link Enum} category, and data.
     * @param view
     * @param category
     * @param args
     * @param <E>
     */
    public static <E extends Enum> void setClickEvent(View view, final E category, final Object... args) {
        setClickEvent(view, new Event.Builder(category, args));
    }

    /**
     * Sets the view's onClick listener to fire an Event to the Event system, with
     * the given {@link Event}.
     * @param view
     * @param builder
     */
    public static void setClickEvent(View view, final Event.Builder builder) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postEvent(builder);
            }
        });
    }

    /**
     * Sets the view's onLongClick listener to fire an Event to the Event system, with the
     * given {@link Enum} category. The returnTrue argument specifies if you want to return
     * true in the onLongClick listener. If true, a short vibration occurs on long click.
     * @param view
     * @param category
     * @param returnTrue
     * @param <E>
     */
    public static <E extends Enum> void setLongClickEvent(View view, final E category, final boolean returnTrue) {
        setLongClickEvent(view, category, returnTrue, (Object[]) null);
    }

    /**
     * Sets the view's onLongClick listener to fire an Event to the Event system, with the
     * given {@link Enum} category, and data. The returnTrue argument specifies if you want to return
     * true in the onLongClick listener. If true, a short vibration occurs on long click.
     * @param view
     * @param category
     * @param returnTrue
     * @param args
     * @param <E>
     */
    public static <E extends Enum> void setLongClickEvent(View view, final E category, final boolean returnTrue, final Object... args) {
        setLongClickEvent(view, new Event.Builder(category, args), returnTrue);
    }

    /**
     * Sets the view's onLongClick listener to fire an Event to the Event system, with
     * the given {@link Event}. The returnTrue argument specifies if you want to return
     * true in the onLongClick listener. If true, a short vibration occurs on long click.
     * @param view
     * @param builder
     * @param returnTrue
     */
    public static void setLongClickEvent(View view, final Event.Builder builder, final boolean returnTrue) {
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                postEvent(builder);
                return returnTrue;
            }
        });
    }

    /**
     * Sets the {@link CompoundButton}'s onCheckedChanged listener to fire an Event to the Event system, with the
     * given {@link Enum} category. In the event, the first item is a boolean stating if it's checked or not.
     * @param view
     * @param category
     * @param <E>
     */
    public static <E extends Enum> void setCheckedEvent(CompoundButton view, final E category) {
        setCheckedEvent(view, category, (Object[]) null);
    }

    /**
     * Sets the {@link CompoundButton}'s onCheckedChanged listener to fire an Event to the Event system, with the
     * given {@link Event}. In the event, the first item is a boolean stating if it's checked or not. (Any data passed
     * by this method moves back 1 spot to make room for the isChecked boolean)
     * @param view
     * @param builder
     */
    public static void setCheckedEvent(CompoundButton view, final Event.Builder builder) {
        view.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                postEvent(builder.push(isChecked));
            }
        });
    }

    /**
     * Sets the {@link CompoundButton}'s onCheckedChanged listener to fire an Event to the Event system, with the
     * given {@link Enum} category, and data. In the event, the first item is a boolean stating if it's checked or not. (Any data passed
     * by this method moves back 1 spot to make room for the isChecked boolean)
     * @param view
     * @param category
     * @param args
     * @param <E>
     */
    public static <E extends Enum> void setCheckedEvent(CompoundButton view, final E category, final Object... args) {
        setCheckedEvent(view, new Event.Builder(category, args));
    }

    /**
     * Sets the {@link TextView}'s text changed listener to fire an Event on the Event system, with the given
     * {@link Enum} category. The full text of the {@link TextView} is returned in the first position of the
     * {@link Event}.
     * @param view
     * @param category
     * @param <E>
     */
    public static <E extends Enum> void setTextChangedEvent(TextView view, final E category) {
        setTextChangedEvent(view, category, (Object[]) null);
    }

    public static <E extends Enum> void setTextChangedEvent(TextView view, final E category, final Object... args) {
        setTextChangedEvent(view, new Event.Builder(category, args));
    }

    public static <E extends Enum> void setTextChangedEvent(TextView view, final Event.Builder builder) {
        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str;
                if (s == null) {
                    str = "";
                } else {
                    str = s.toString();
                }
                builder.push(str);
                postEvent(builder);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public static void setProgressChangedEvent(SeekBar view, final Enum category) {
        setProgressChangedEvent(view, category, (Object[]) null);
    }

    public static void setProgressChangedEvent(SeekBar view, final Enum category, final Object... args) {
        setProgressChangedEvent(view, new Event.Builder(category, args));
    }

    public static void setProgressChangedEvent(SeekBar view, final Event.Builder builder) {
        view.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    builder.push(progress);
                    postEvent(builder);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    private static void postEvent(Event.Builder builder) {
        EventManager.getInstance().postEvent(builder);
    }

}