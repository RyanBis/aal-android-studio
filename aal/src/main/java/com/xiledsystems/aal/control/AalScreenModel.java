package com.xiledsystems.aal.control;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.AalActivity;
import com.xiledsystems.aal.context.ActivityManager;
import com.xiledsystems.aal.context.IAalActivity;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.control.internal.AndroidScreenModel;
import com.xiledsystems.aal.events.EventListener;

/**
 * Class to extend when using an Activity which either extends {@link com.xiledsystems.aal.context.AalActivity}, or
 * implements {@link IAalActivity}. This overlays some convenient methods on top of {@link AndroidScreenModel}.
 */
@TargetApi(Build.VERSION_CODES.ECLAIR)
@State(DevelopmentState.PRODUCTION)
public abstract class AalScreenModel extends AndroidScreenModel {


    protected AalScreenModel(IAalActivity context) {
        this(context, null, null);
    }

    protected AalScreenModel(IAalActivity context, Class<? extends Enum> category) {
        this(context, category, null);
    }

    protected AalScreenModel(IAalActivity context, Class<? extends Enum> category, EventListener eventListener) {
        super(context.getManager().getContext(), category, eventListener);
        context.getManager().registerOnActivityResultListener(listener);
        context.getManager().registerOnCreateDestroyListener(listener);
        context.getManager().registerOnStartStopListener(listener);
        context.getManager().setOnBackPressed(new Runnable() {
            @Override
            public void run() {
                backPressed();
            }
        });
    }

    protected AalScreenModel() {
        super();
    }

    /**
     * Override this method to perform actions when this screen is created, but not yet
     * shown. The more that's done here, the more of a lag there will be before the
     * screen shows up on screen.
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
    }

    /**
     * Override this method to perform actions when the screen enters (is visible on screen).
     */
    public void onEnter() {
    }

    /**
     * Override this method to intercept when the back button has been pressed
     */
    public void backPressed() {
        if (getContext() instanceof AalActivity) {
            ((AalActivity) getContext()).doNativeBackPressed();
        } else {
            getContext().finish();
        }
    }

    /**
     * Override this method to perform actions when you start another screen for
     * a result (like when using {@link #gotoScreenForResult(Class, int)}.
     *
     * @param requestCode
     * @param success
     * @param data
     */
    public void onScreenResult(int requestCode, boolean success, Intent data) {
    }

    /**
     * Override this method to perform actions when this screen is no longer
     * displayed on screen.
     */
    public void onExit() {
    }

    /**
     * Override this method to perform any last-second actions when this screen is
     * being killed, or destroyed.
     */
    public void onKill() {
    }

    private LifecycleListener listener = new LifecycleListener() {

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            onScreenResult(requestCode, resultCode == Activity.RESULT_OK, data);
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            AalScreenModel.this.onCreate(savedInstanceState);
        }

        @Override
        public void onStart() {
            onEnter();
        }

        @Override
        public void onStop() {
            onExit();
        }

        @Override
        public void onDestroy() {
            onKill();
            destroy();
        }
    };

    private ActivityManager getManager() {
        return ((IAalActivity) getContext()).getManager();
    }

}
