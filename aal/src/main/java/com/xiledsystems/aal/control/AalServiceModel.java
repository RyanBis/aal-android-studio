package com.xiledsystems.aal.control;


import android.app.Service;
import android.content.Intent;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.IAalService;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.context.ServiceManager;
import com.xiledsystems.aal.control.internal.AndroidServiceModel;
import com.xiledsystems.aal.events.EventListener;


@State(DevelopmentState.UNTESTED)
public abstract class AalServiceModel extends AndroidServiceModel {

    private Listener listener = new Listener();

    protected AalServiceModel(IAalService service) {
        this(service, null, null);
    }

    protected AalServiceModel(IAalService service, Class<? extends Enum> category) {
        this(service, category, null);
    }

    protected AalServiceModel(IAalService service, Class<? extends Enum> category, EventListener listener) {
        super(service.getManager().getService(), category, listener);
        service.getManager().registerLifecycleListener(this.listener);
    }

    public void onCreate() {
    }

    public int onStart(Intent intent, int flags, int startId) {
        return Service.START_NOT_STICKY;
    }

    public void onKill() {
    }

    private class Listener extends ServiceLifecycleListener {

        @Override
        public void onCreateService() {
            onCreate();
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            int flag = onStart(intent, flags, startId);
            getManager().setStartFlag(flag);
            return flag;
        }

        @Override
        public void onDestroy() {
            onKill();
            destroy();
        }
    }

    private ServiceManager getManager() {
        return ((IAalService) getServiceController().getContext()).getManager();
    }

}
