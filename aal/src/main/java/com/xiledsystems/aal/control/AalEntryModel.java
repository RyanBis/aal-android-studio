package com.xiledsystems.aal.control;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.IAalActivity;

/**
 * There's really no difference using this class, or {@link AalScreenModel} anymore.
 * You can use it to simply have it be your initial controller in your app, and only
 * have this one call {@link #destroy()}
 */
@State(DevelopmentState.PRODUCTION)
public abstract class AalEntryModel extends AalScreenModel {

    protected AalEntryModel(IAalActivity context) {
        this(context, null);
    }

    protected AalEntryModel(IAalActivity context, Class<? extends Enum> category) {
        super(context, category);
        manager.init();
    }

    protected AalEntryModel() {
        super();
    }

    @Override
    public void destroy() {
        if (killEventManagerOnDestroy()){
            manager.shutdown();
        }
        super.destroy();
    }

    public abstract boolean killEventManagerOnDestroy();
}
