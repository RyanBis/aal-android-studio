package com.xiledsystems.aal.control.internal;


import android.app.Activity;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.events.EventManager;

/**
 * This is the same thing as {@link AndroidScreenModel}, only it calls
 * {@link EventManager#init()} in it's constructor. This is meant to be the
 * entry point for your app.
 */
@State(DevelopmentState.PRODUCTION)
public abstract class EntryModel extends AndroidScreenModel {

    public EntryModel(Activity context) {
        super(context);
        manager.init();
    }

    @Override
    public void destroy() {
        if (killEventManagerOnDestroy()){
            manager.shutdown();
        }
        super.destroy();
    }

    public abstract boolean killEventManagerOnDestroy();
}
