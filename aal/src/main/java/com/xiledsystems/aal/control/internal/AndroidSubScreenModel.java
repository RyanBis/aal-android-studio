package com.xiledsystems.aal.control.internal;


import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.ActivityUtil;


@State(DevelopmentState.ALPHA)
public abstract class AndroidSubScreenModel extends SubScreenModel {

    @Override
    protected AndroidSubViewController getSubViewController() {
        return super.getSubViewController();
    }

    protected AndroidSubScreenModel(Fragment frag) {
        this(frag, null);
    }

    protected AndroidSubScreenModel(Fragment frag, Class<? extends Enum> category) {
        super(new AndroidSubViewController(frag));
        setCategory(category);
    }

    public void gotoScreen(Class<?> classToGoTo, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getSubViewController().gotoScreen(clazz, flags);
    }

    public void gotoScreen(Class<?> classToGoTo, Bundle data) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getSubViewController().gotoScreen(clazz, data);
    }

    public void gotoScreen(Class<?> classToGoTo, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getSubViewController().gotoScreen(clazz, data, flags);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getSubViewController().gotoScreenForResult(clazz, requestCode);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getSubViewController().gotoScreenForResult(clazz, requestCode, flags);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode, Bundle data) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getSubViewController().gotoScreenForResult(clazz, requestCode, data);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getSubViewController().gotoScreenForResult(clazz, requestCode, data, flags);
    }

    public void openOtherApp(String action) {
        getSubViewController().openOtherApp(action);
    }

    public void openOtherapp(String action, Uri data) {
        getSubViewController().openOtherApp(action, data);
    }

    public void openOtherApp(Intent intent) {
        getSubViewController().openOtherApp(intent);
    }

    public void openOtherAppForResult(String action, int requestCode) {
        getSubViewController().openOtherAppForResult(action, requestCode);
    }

    public void openOtherAppForResult(String action, Uri data, int requestCode) {
        getSubViewController().openOtherAppForResult(action, data, requestCode);
    }

    public void openOtherAppForResult(Intent intent, int requestCode) {
        getSubViewController().openOtherAppForResult(intent, requestCode);
    }

    public void startService(Class<?> serviceToStart) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getSubViewController().startService(clazz);
    }

    public void startService(Class<?> serviceToStart, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getSubViewController().startService(clazz, flags);
    }

    public void startService(Class<?> serviceToStart, Bundle data) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getSubViewController().startService(clazz, data);
    }

    public void startService(Class<?> serviceToStart, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getSubViewController().startService(clazz, data, flags);
    }

    public void stopService(Class<?> serviceToStop) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getSubViewController().stopService(clazz);
    }

    public void stopService(Class<?> serviceToStop, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getSubViewController().stopService(clazz, flags);
    }

    public void stopService(Class<?> serviceToStop, Bundle data) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getSubViewController().stopService(clazz, data);
    }

    public void stopService(Class<?> serviceToStop, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getSubViewController().stopService(clazz, data, flags);
    }

    /**
     * Override the backing Activity transition animations when using any of the gotoScreen methods.
     */
    public void overrideTransition(int enterAnim, int exitAnim) {
        getContext().overridePendingTransition(enterAnim, exitAnim);
    }

    public Activity getContext() {
        return getSubViewController().getActivity();
    }

}
