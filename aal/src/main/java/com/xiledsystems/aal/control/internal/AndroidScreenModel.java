package com.xiledsystems.aal.control.internal;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.util.ActivityUtil;


/**
 * Simple Controller class for using in an MVC like pattern using AAL. This class
 * uses the {@link EventManager} class to handle events. Extend this class for controllers
 * for your app. Before events get handled, one of two things must first happen.
 * 1. call {@link EventManager#init()} once when your app starts.
 * 2. Have your first Screen extend {@link EntryModel} instead, which will
 * init the {@link EventManager} for you.
 *
 * Remember to instantiate the controller in the Activity's creation, and to run
 * {@link #destroy()} when done with the controller (usually in onDestroy()).
 *
 */
@State(DevelopmentState.PRODUCTION)
@TargetApi(Build.VERSION_CODES.ECLAIR)
public abstract class AndroidScreenModel extends ScreenModel<AndroidViewController> {

    private static final String TAG = "ScreenController_%s";

    protected AndroidScreenModel(Activity context) {
        this(context, null);
    }

    protected AndroidScreenModel(Activity context, Class<? extends Enum> category) {
        super(new AndroidViewController(context));
        setCategory(category);
    }

    protected AndroidScreenModel(Activity context, Class<? extends Enum> category, EventListener listener) {
        super(new AndroidViewController(context));
        setCategory(category);
        registerAllEventListener(listener);
    }

    protected AndroidScreenModel() {
        super();
    }

    @Override
    public AndroidViewController getViewController() {
        return super.getViewController();
    }

    /**
     * Override the backing Activity transition animations when using any of the gotoScreen methods.
     */
    public void overrideTransition(int enterAnim, int exitAnim) {
        getContext().overridePendingTransition(enterAnim, exitAnim);
    }

    @Override
    public void gotoScreen(Class<?> classToGoTo) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreen(clazz);
    }

    public void gotoScreen(Class<?> classToGoTo, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreen(clazz, flags);
    }

    public void gotoScreen(Class<?> classToGoTo, Bundle data) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreen(clazz, data);
    }

    public void gotoScreen(Class<?> classToGoTo, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreen(clazz, data, flags);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreenForResult(clazz, requestCode);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreenForResult(clazz, requestCode, flags);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode, Bundle data) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreenForResult(clazz, requestCode, data);
    }

    public void gotoScreenForResult(Class<?> classToGoTo, int requestCode, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getActivityClass(classToGoTo, logger);
        getViewController().gotoScreenForResult(clazz, requestCode, data, flags);
    }

    public void openOtherApp(String action) {
        getViewController().openOtherApp(action);
    }

    public void openOtherapp(String action, Uri data) {
        getViewController().openOtherApp(action, data);
    }

    public void openOtherApp(Intent intent) {
        getViewController().openOtherApp(intent);
    }

    public void openOtherAppForResult(String action, int requestCode) {
        getViewController().openOtherAppForResult(action, requestCode);
    }

    public void openOtherAppForResult(String action, Uri data, int requestCode) {
        getViewController().openOtherAppForResult(action, data, requestCode);
    }

    public void openOtherAppForResult(Intent intent, int requestCode) {
        getViewController().openOtherAppForResult(intent, requestCode);
    }

    public void startService(Class<?> serviceToStart) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getViewController().startService(clazz);
    }

    public void startService(Class<?> serviceToStart, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getViewController().startService(clazz, flags);
    }

    public void startService(Class<?> serviceToStart, Bundle data) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getViewController().startService(clazz, data);
    }

    public void startService(Class<?> serviceToStart, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStart);
        getViewController().startService(clazz, data, flags);
    }

    public void stopService(Class<?> serviceToStop) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getViewController().stopService(clazz);
    }

    public void stopService(Class<?> serviceToStop, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getViewController().stopService(clazz, flags);
    }

    public void stopService(Class<?> serviceToStop, Bundle data) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getViewController().stopService(clazz, data);
    }

    public void stopService(Class<?> serviceToStop, Bundle data, int flags) {
        Class<?> clazz = ActivityUtil.getServiceClass(serviceToStop);
        getViewController().stopService(clazz, data, flags);
    }

    public Activity getContext() {
        return getViewController().getContext();
    }

    public Intent getIntent() {
        return getContext().getIntent();
    }

    public abstract Class<?> getActivityClass();

}
