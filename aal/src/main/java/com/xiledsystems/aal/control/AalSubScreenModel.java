package com.xiledsystems.aal.control;


import android.app.Activity;
import android.content.Intent;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.FragmentManager;
import com.xiledsystems.aal.context.IAalFragment;
import com.xiledsystems.aal.context.LifecycleListener;
import com.xiledsystems.aal.control.internal.AndroidSubScreenModel;


@State(DevelopmentState.ALPHA)
public abstract class AalSubScreenModel extends AndroidSubScreenModel {

    protected AalSubScreenModel(IAalFragment frag) {
        super(((FragmentManager)frag.getManager()).getFragment());
    }

    protected AalSubScreenModel(IAalFragment frag, Class<? extends  Enum> category) {
        super(((FragmentManager)frag.getManager()).getFragment(), category);
    }

    /**
     * Override this method to perform actions when the screen enters (is visible on screen).
     */
    public void onEnter() {
    }

    /**
     * Override this method to perform actions when this screen is no longer
     * displayed on screen.
     */
    public void onExit() {
    }

    /**
     * Override this method to perform any last-second actions when this screen is
     * being killed, or destroyed.
     */
    public void onKill() {
    }

    /**
     * Override this method to perform actions when you start another screen for
     * a result (like when using {@link #gotoScreenForResult(Class, int)}.
     *
     * @param requestCode
     * @param success
     * @param data
     */
    public void onScreenResult(int requestCode, boolean success, Intent data) {
    }

    private LifecycleListener listener = new LifecycleListener() {

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            onScreenResult(requestCode, resultCode == Activity.RESULT_OK, data);
        }

        @Override
        public void onStart() {
            onEnter();
        }

        @Override
        public void onStop() {
            onExit();
        }

        @Override
        public void onDestroy() {
            onKill();
            destroy();
        }
    };

}
