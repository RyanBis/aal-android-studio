package com.xiledsystems.aal.control.internal;


import android.util.Log;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.util.TextUtil;

import java.lang.reflect.Method;


@State(DevelopmentState.ALPHA)
public abstract class SubScreenModel extends Model {

    private static final String TAG = "SubScreenController_%s";

    private SubViewController controller;


    protected SubScreenModel(SubViewController controller) {
        super(true);
        this.controller = controller;
    }

    protected SubScreenModel() {
        super(false);
    }

    protected <T extends SubViewController> T getSubViewController() {
        return (T) controller;
    }

    public void gotoScreen(Class<?> classToGoTo) {
        Class<?> actClas = getOtherActivityClass(classToGoTo);
        if (actClas != null) {
            getSubViewController().gotoScreen(actClas);
        } else {
            getSubViewController().gotoScreen(classToGoTo);
        }
    }

    Class<?> getOtherActivityClass(Class<?> screenClass) {
        AndroidScreenModel screen = null;
        try {
            screen = (AndroidScreenModel) screenClass.newInstance();
        } catch (Exception e) {
            logger.logE(TextUtil.string("Unable to find no-argument constructor in ", screenClass.getSimpleName(), "! Starting another activity from this controller will not work!"));
        }
        if (screen != null) {
            try {
                Method getActClass = screenClass.getDeclaredMethod("getActivityClass");
                Class<?> actClass = (Class<?>) getActClass.invoke(screen, new Object[0]);
                return actClass;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
