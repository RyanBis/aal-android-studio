package com.xiledsystems.aal.control.internal;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.ALPHA)
public interface SubViewController {
    void gotoScreen(Class<?> controller);
}
