package com.xiledsystems.aal.control.internal;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.events.Event;


@State(DevelopmentState.UNTESTED)
public abstract class ServiceModel extends Model {

    private NonViewController serviceController;

    protected ServiceModel(NonViewController serviceController) {
        super(true);
        this.serviceController = serviceController;
    }

    protected <T extends NonViewController> T getServiceController() {
        return (T) serviceController;
    }

    public void stop() {
        serviceController.stop();
    }

    @Override
    public boolean processEvent(Event args) {
        return false;
    }
}
