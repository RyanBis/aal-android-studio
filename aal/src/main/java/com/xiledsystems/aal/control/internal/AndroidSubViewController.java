package com.xiledsystems.aal.control.internal;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@State(DevelopmentState.BETA)
public class AndroidSubViewController implements SubViewController {


    private Fragment fragment;

    public AndroidSubViewController(Fragment frag) {
        fragment = frag;
    }

    Activity getActivity() {
        return fragment.getActivity();
    }

    public void gotoScreen(Class<?> clazz) {
        Intent intent = new Intent(getActivity(), clazz);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, int flags) {
        Intent intent = new Intent(getActivity(), clazz).setFlags(flags);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz);
        intent.putExtras(bundle);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, Bundle bundle, int flags) {
        Intent intent = new Intent(getActivity(), clazz).putExtras(bundle).setFlags(flags);
        gotoActivity(intent);
    }

    public void openOtherApp(String action) {
        openOtherApp(new Intent(action));
    }

    public void openOtherApp(String action, Uri data) {
        openOtherApp(new Intent(action).setData(data));
    }

    public void openOtherApp(Intent intent) {
        gotoActivity(intent);
    }

    public void openOtherAppForResult(String action, int requestCode) {
        openOtherAppForResult(new Intent(action), requestCode);
    }

    public void openOtherAppForResult(String action, Uri data, int requestCode) {
        openOtherAppForResult(new Intent(action).setData(data), requestCode);
    }

    public void openOtherAppForResult(Intent intent, int requestCode) {
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode) {
        Intent intent = new Intent(getActivity(), clazz);
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode, int flags) {
        Intent intent = new Intent(getActivity(), clazz).setFlags(flags);
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz).putExtras(bundle);
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode, Bundle bundle, int flags) {
        Intent intent = new Intent(getActivity(), clazz).putExtras(bundle).setFlags(flags);
        gotoActivityForResult(intent, requestCode);
    }

    protected void gotoActivityForResult(Intent intent, int requestCode) {
        if (getActivity() != null) {
            getActivity().startActivityForResult(intent, requestCode);
        }
    }

    protected void gotoActivity(Intent intent) {
        if (getActivity() != null) {
            getActivity().startActivity(intent);
        }
    }

    protected void startService(Class<?> clazz) {
        if (getActivity() != null) {
            getActivity().startService(new Intent(getActivity(), clazz));
        }
    }

    protected void startService(Class<?> clazz, int flags) {
        if (getActivity() != null) {
            getActivity().startService(new Intent(getActivity(), clazz).setFlags(flags));
        }
    }

    protected void startService(Class<?> clazz, Bundle data) {
        if (getActivity() != null) {
            getActivity().startService(new Intent(getActivity(), clazz).putExtras(data));
        }
    }

    protected void startService(Class<?> clazz, Bundle data, int flags) {
        if (getActivity() != null) {
            getActivity().startService(new Intent(getActivity(), clazz).putExtras(data).setFlags(flags));
        }
    }

    protected void stopService(Class<?> clazz) {
        if (getActivity() != null) {
            getActivity().stopService(new Intent(getActivity(), clazz));
        }
    }

    protected void stopService(Class<?> clazz, int flags) {
        if (getActivity() != null) {
            getActivity().stopService(new Intent(getActivity(), clazz).setFlags(flags));
        }
    }

    protected void stopService(Class<?> clazz, Bundle data) {
        if (getActivity() != null) {
            getActivity().stopService(new Intent(getActivity(), clazz).putExtras(data));
        }
    }

    protected void stopService(Class<?> clazz, Bundle data, int flags) {
        if (getActivity() != null) {
            getActivity().stopService(new Intent(getActivity(), clazz).putExtras(data).setFlags(flags));
        }
    }

    public void onDestroy() {
        fragment = null;
    }
}
