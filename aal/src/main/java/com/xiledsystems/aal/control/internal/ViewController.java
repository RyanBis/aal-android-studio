package com.xiledsystems.aal.control.internal;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

@State(DevelopmentState.PRODUCTION)
public abstract class ViewController {

    abstract void gotoScreen(Class<?> controller);
    @Deprecated
    abstract void goBack();
    abstract void kill();
    public abstract <T> T getContext();
    abstract void destroy();

}
