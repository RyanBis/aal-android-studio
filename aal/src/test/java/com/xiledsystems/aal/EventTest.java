package com.xiledsystems.aal;


import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.events.EventManagerConfig;
import com.xiledsystems.aal.events.EventThreadManagerConfig;
import com.xiledsystems.aal.events.PostType;
import com.xiledsystems.aal.util.ByteUtil;
import com.xiledsystems.aal.util.TextUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class EventTest {

    private EventManager eventManager;


    private enum TestEvent {
        ONE,
        TWO,
        THREE
    }

    @Before
    public void setup() {
        reset();
    }

    private void reset() {
        if (eventManager != null) {
            eventManager.shutdown();
        }
        EventThreadManagerConfig tConfig = new EventThreadManagerConfig().withUnitTesting(true);
        EventManagerConfig config = new EventManagerConfig().withLogging(true).withThreadConfig(tConfig);
        eventManager = EventManager.getInstance(config);
        eventManager.clearAllListeners();
        eventManager.setDefaultPostType(PostType.onMain());
        eventManager.init();
    }

    @After
    public void tearDown() {
        eventManager.shutdown();
    }

    @Test(timeout = 1000)
    public void nullConfigTest() {
        reset();
        eventManager.shutdown();
        EventManagerConfig config = new EventManagerConfig();
        EventThreadManagerConfig tConfig = new EventThreadManagerConfig();
        tConfig.coreThreads = null;
        tConfig.maxThreads = null;
        tConfig.unitTest = true;
        config.threadConfig = tConfig;
        config.postType = null;
        config.defaultDelay = null;
        eventManager = EventManager.getInstance(config);
        eventManager.init();
        assertTrue(eventManager.initialized());
    }


    @Test(timeout = 5000)
    public void allEventListenerTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);
        eventManager = EventManager.getInstance();

        eventManager.registerAllEventListener(event -> {
            TestEvent tEvent = event.getEventKey(TestEvent.class);
            if (tEvent != null) {
                switch (tEvent) {
                    case ONE:
                        eventManager.registerAllEventListener(event1 -> false);
                        eventManager.postEvent(TestEvent.TWO);
                        return true;
                    case TWO:
                        eventManager.postEvent(TestEvent.THREE);
                        eventManager.registerAllEventListener(event12 -> false);
                        return true;
                    case THREE:
                        release(s);
                        return true;
                }
            }
            return false;
        });
        for (int i = 0; i < 1000; i++) {
            eventManager.registerAllEventListener(event -> false);
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 1000);
        s.acquire();
    }

    @Test(timeout = 2000)
    public void allEventListenerThreadTest() throws Exception {
        final Semaphore s = new Semaphore(0);

        reset();
        eventManager.registerAllEventListener(event -> {
            TestEvent tEvent = event.getEventKey(TestEvent.class);
            if (tEvent != null) {
                switch (tEvent) {
                    case ONE:
                        eventManager.registerAllEventListener(event1 -> false);
                        eventManager.postEvent(TestEvent.TWO);
                        return true;
                    case TWO:
                        eventManager.postEvent(TestEvent.THREE);
                        eventManager.registerAllEventListener(event12 -> false);
                        return true;
                    case THREE:
                        release(s);
                        return true;
                }
            }
            return false;
        });
        for (int i = 0; i < 1000; i++) {
            eventManager.registerAllEventListener(event -> false);
        }
        eventManager.setDefaultPostType(PostType.onThread());
        eventManager.postDelayedEvent(TestEvent.ONE, 1000);
        s.acquire();
    }

    @Test(timeout = 4000)
    public void allEventListenerCurrentTest() {
        reset();
        eventManager.registerAllEventListener(event -> {
            TestEvent tEvent = event.getEventKey(TestEvent.class);
            if (tEvent != null) {
                switch (tEvent) {
                    case ONE:
                        eventManager.registerAllEventListener(event1 -> false);
                        eventManager.postEvent(TestEvent.TWO);
                        return true;
                    case TWO:
                        eventManager.registerAllEventListener(event12 -> false);
                        eventManager.postEvent(TestEvent.THREE);
                        return true;
                    case THREE:
                        return true;
                }
            }
            return false;
        });
        for (int i = 0; i < 1000; i++) {
            eventManager.registerAllEventListener(event -> false);
        }
        eventManager.setDefaultPostType(PostType.onCurrent());
        eventManager.postEvent(TestEvent.ONE);
    }

    @Test(timeout = 15000)
    public void categoryListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        eventManager = EventManager.getInstance();
        reset();
        eventManager.registerCategoryListener(TestEvent.class, event -> {
            TestEvent tEvent = event.getEventKey(TestEvent.class);
            if (tEvent != null) {
                switch (tEvent) {
                    case ONE:
                        eventManager.registerCategoryListener(TestEvent.class, event1 -> {
                            List<byte[]> byteList = new ArrayList<>();
                            for (int i = 0; i < 50000; i++) {
                                byteList.add(ByteUtil.random(1024));
                            }
                            return false;
                        });
                        eventManager.postEvent(TestEvent.TWO);
                        return true;
                    case TWO:
                        eventManager.postEvent(TestEvent.THREE);
                        eventManager.registerCategoryListener(TestEvent.class, event12 -> false);
                        return true;
                    case THREE:
                        release(s);
                        return true;
                }
            }
            return false;
        });
        for (int i = 0; i < 5000; i++) {
            eventManager.registerCategoryListener(TestEvent.class, event -> false);
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 5000);
        s.acquire();
    }

    @Test(timeout = 18000)
    public void eventListenerTest() throws Exception {
        final Semaphore s = new Semaphore(0);
        reset();
        eventManager = EventManager.getInstance();
        eventManager.registerListener(TestEvent.ONE, event -> {
            eventManager.registerListener(TestEvent.TWO, event1 -> {
                final List<byte[]> byteList = new ArrayList<>();
                for (int i = 0; i < 50000; i++) {
                    byteList.add(ByteUtil.random(1024));
                }
                return false;
            });
            eventManager.postEvent(TestEvent.TWO);
            return true;
        });
        eventManager.registerListener(TestEvent.TWO, event -> {

            eventManager.registerListener(TestEvent.THREE, event12 -> {
                final List<byte[]> byteList = new ArrayList<>();
                for (int i = 0; i < 10000; i++) {
                    byteList.add(ByteUtil.random(1024));
                }
                return false;
            });
            eventManager.postEvent(TestEvent.THREE);
            return true;
        });
        eventManager.registerListener(TestEvent.THREE, event -> {
            eventManager.registerListener(TestEvent.ONE, event13 -> false);
            release(s);
            return true;
        });
        for (int i = 0; i < 5000; i++) {
            eventManager.registerListener(TestEvent.ONE, event -> true);
        }
        eventManager.postDelayedEvent(TestEvent.ONE, 5000);
        s.acquire();
    }

    @Test(timeout = 5000)
    public void delayedEventTest() throws Exception {
        final Semaphore s = new Semaphore(0);

        reset();
        final AtomicInteger delayLength = new AtomicInteger(100);
        final AtomicLong start = new AtomicLong(0);
        eventManager.registerCategoryListener(TestEvent.class, event -> {
            long diff = System.currentTimeMillis() - start.get();
            assertTrue(TextUtil.string("Diff: ", diff, " Target: ", delayLength.get()), diff >= delayLength.get());
            // Allow for as much as one cycle + once cycle - 1 ms of buffer, as it depends on when the event is posted
            // relative to the event cycle
            int max = delayLength.get() + (((1000 / eventManager.getCurrentEPS()) * 2) - 1);
            assertTrue(TextUtil.string("Diff: ", diff, " Max: ", max), diff <= max);
            if (delayLength.get() < 600) {
                delayLength.set(delayLength.get() + 100);
                start.set(System.currentTimeMillis());
                eventManager.postDelayedEvent(TestEvent.ONE, delayLength.get());
            } else {
                release(s);
            }
            return true;
        });
        start.set(System.currentTimeMillis());
        eventManager.postDelayedEvent(TestEvent.ONE, delayLength.get());
        s.acquire();
    }

    @Test(timeout = 4000)
    public void delayedEventConfigTest() throws Exception {
        final Semaphore s = new Semaphore(0);

        reset();
        final EventManagerConfig config = new EventManagerConfig();
        config.defaultDelay = 100L;
        eventManager.setConfig(config);
        final AtomicLong start = new AtomicLong(0);
        eventManager.registerCategoryListener(TestEvent.class, event -> {
            long diff = System.currentTimeMillis() - start.get();
            assertTrue("Diff: " + diff + " Target: " + config.defaultDelay, diff >= config.defaultDelay);
            // Allow for as much as one cycle + once cycle - 1 ms of buffer, as it depends on when the event is posted
            // relative to the event cycle
            int max = (int) (config.defaultDelay + (((1000 / eventManager.getCurrentEPS()) * 2) - 1));
            assertTrue(TextUtil.string("Diff: ", diff, " Max: ", max), diff < max);
            if (config.defaultDelay < 600) {
                config.defaultDelay = config.defaultDelay + 100;
                eventManager.setConfig(config);
                start.set(System.currentTimeMillis());
                eventManager.postEvent(TestEvent.ONE);
            } else {
                release(s);
            }
            return true;
        });
        start.set(System.currentTimeMillis());
        eventManager.postEvent(TestEvent.ONE);
        s.acquire();
    }

    @Test(timeout = 1000)
    public void maxExecutionTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);
        eventManager = EventManager.getInstance();
        final int max = 3;
        eventManager.registerListener(TestEvent.ONE, event -> {
            int i = 0;
            i++;
            return true;
        });
        eventManager.registerListener(TestEvent.ONE, event -> true);
        eventManager.registerListener(TestEvent.ONE, event -> {
            assertTrue(event.currentExecutions() + 1 == max);
            release(s);
            return true;
        });
        eventManager.postEventMax(max, TestEvent.ONE);
        s.acquire();
    }

    @Test(timeout = 1000)
    public void testCancelEvent() throws Exception {
        reset();

        final Semaphore s = new Semaphore(0);

        eventManager.registerListener(TestEvent.ONE, event -> {
            assertFalse(eventManager.hasEvent(TestEvent.TWO));
            release(s);
            return true;
        });

        eventManager.postDelayedEvent(TestEvent.ONE, 250);
        eventManager.postDelayedEvent(TestEvent.TWO, 500);
        eventManager.cancelEvent(TestEvent.TWO);
//        ThreadManager.getInstance(new ThreadManagerConfig().withUnitTesting(true)).execute(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(50);
//                } catch (Exception e) {
//                }
//                eventManager.cancelEvent(TestEvent.TWO);
//            }
//        });

        s.acquire();
    }


    private void release(Semaphore s) {
        if (s != null) {
            try {
                s.release();
            } catch (Exception e) {
            }
        }
    }

}
