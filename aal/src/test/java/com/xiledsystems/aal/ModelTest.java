package com.xiledsystems.aal;


import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.events.EventManagerConfig;
import com.xiledsystems.aal.events.EventThreadManagerConfig;
import org.junit.Test;


public class ModelTest {


    private EventManager eventManager;


    private void reset() {
        if (eventManager != null && eventManager.initialized()) {
            eventManager.shutdown();
        }
        EventManagerConfig config = new EventManagerConfig().withThreadConfig(new EventThreadManagerConfig().withUnitTesting(true));
        eventManager = EventManager.getInstance(config);
        eventManager.init();
    }

    @Test
    public void test() throws Exception {
    }

}
