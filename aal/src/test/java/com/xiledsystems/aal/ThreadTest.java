package com.xiledsystems.aal;


import android.drm.DrmStore;

import com.xiledsystems.aal.util.AalHandler;
import com.xiledsystems.aal.util.Loop;
import com.xiledsystems.aal.util.Rand;
import com.xiledsystems.aal.util.ThreadManager;
import com.xiledsystems.aal.util.ThreadManagerConfig;
import com.xiledsystems.aal.util.UIHandler;
import org.junit.After;
import org.junit.Test;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;


public final class ThreadTest {

    private ThreadManager mgr;


    @Test(timeout = 200)
    public void executeTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        mgr.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(150);
                } catch (Exception e) {
                }
                release(s);
            }
        });
        s.acquire();
    }

    @Test(timeout = 175)
    public void executeFutureCallableTest() throws Exception {
        reset();
        Future<Integer> future = mgr.execute(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                try {
                    Thread.sleep(150);
                } catch (Exception e) {
                }
                return 1;
            }
        });
        int num = future.get();
        assertEquals(1, num);
    }

    @Test(timeout = 50)
    public void executeBackAndForeGroundTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        final AtomicReference<Thread> bThread = new AtomicReference<>();
        final AtomicReference<Thread> uiThread = new AtomicReference<>();

        final UIHandler uiHandler = ThreadManager.getUiHandler();

        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                uiThread.set(Thread.currentThread());
                mgr.execute(new Runnable() {
                    @Override
                    public void run() {
                        bThread.set(Thread.currentThread());
                        assertNotEquals(bThread, uiThread);
                        release(s);
                    }
                });
            }
        });
        s.acquire();
    }

    @Test(timeout = 50)
    public void shutdownInitTest() throws Exception {
        reset();
        mgr.shutdown();
        Future<Integer> future = mgr.execute(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 1;
            }
        });
        int num = future.get();
        assertEquals(1, num);
    }

    @Test(timeout = 1150)
    public void loopTest1() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        final Loop loop = new Loop(new Runnable() {

            int i = 0;

            @Override
            public void run() {
                if (i == 10) {
                    release(s);
                } else {
                    i++;
                }
            }
        });
        loop.setInterval(100);
        loop.start();
        s.acquire();
    }

    @Test(timeout = 1150)
    public void loopTest2() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        final Loop loop = new Loop(new Runnable() {

            int i = 0;

            @Override
            public void run() {
                if (i == 10) {
                    release(s);
                } else {
                    i++;
                }
            }
        });
        loop.setDelayTime(100);
        loop.setInterval(100);
        loop.start();
        s.acquire();
    }

    @Test(timeout = 350)
    public void aalHandlerTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        final AalHandler handler = mgr.newAalHandler("Tester");
        handler.start();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        release(s);
                    }
                });
            }
        }, 250);
        s.acquire();
    }

    @Test
    public void aalHandlerStopTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        final AtomicInteger lastRunnable = new AtomicInteger(0);

        final AalHandler handler = mgr.newAalHandler("Tester");
        for (int i = 0; i < 1000; i++) {
            final int current = i + 1;
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    lastRunnable.set(current);
                    System.out.println("Starting runnable " + current + ".");
                    for (int i = 0; i < 10; i++) {
                        String junk = Rand.getRandomString(100);
                    }
                }
            };
            handler.postDelayed(run, 25);
        }
        handler.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(275);
                } catch (Exception e) {}
                int start = lastRunnable.get();
                System.out.println("Stopping handler at " + start + "...");
                handler.stop();
                try {
                    Thread.sleep(100);
                } catch (Exception e) {}
                int end = lastRunnable.get();
                System.out.println("Handler stopped at " + end + ".");
                assertTrue(start != end);
                s.release();
            }
        }).start();
        s.acquire();
    }

    @Test
    public void aalHandlerQuitTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        final AtomicInteger lastRunnable = new AtomicInteger(0);

        final AalHandler handler = mgr.newAalHandler("Tester");
        for (int i = 0; i < 1000; i++) {
            final int current = i + 1;
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    lastRunnable.set(current);
                    System.out.println("Starting runnable " + current + ".");
                    for (int i = 0; i < 10; i++) {
                        String junk = Rand.getRandomString(100);
                    }
                }
            };
            handler.postDelayed(run, 25);
        }
        handler.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(275);
                } catch (Exception e) {}
                int start = lastRunnable.get();
                System.out.println("Quitting handler at " + start + "...");
                handler.quit();
                try {
                    Thread.sleep(100);
                } catch (Exception e) {}
                int end = lastRunnable.get();
                System.out.println("Handler stopped at " + end + ".");
                assertTrue(end - start < 2);
                s.release();
            }
        }).start();
        s.acquire();
    }

    @Test
    public void aalHandlerPauseResumeTest() throws Exception {
        reset();
        final Semaphore s = new Semaphore(0);

        final AtomicInteger lastRunnable = new AtomicInteger(0);

        final AalHandler handler = mgr.newAalHandler("Tester");

        for (int i = 0; i < 1000; i++) {
            final int current = i + 1;
            Runnable run = new Runnable() {
                @Override
                public void run() {
                    lastRunnable.set(current);
                    System.out.println("Starting runnable " + current + ".");
                    for (int i = 0; i < 10; i++) {
                        String junk = Rand.getRandomString(100);
                    }
                }
            };
            handler.postDelayed(run, 25);
        }
        handler.start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(275);
                } catch (Exception e) {}
                int start;
                handler.pause();
                try {
                    Thread.sleep(10);
                } catch (Exception e) {}
                start = lastRunnable.get();
                System.out.println("Pausing handler at " + start + "...");
                try {
                    Thread.sleep(250);
                } catch (Exception e) {}
                int paused = lastRunnable.get();
                assertTrue(start == paused);
                System.out.println("Resuming handler at " + paused);
                handler.resume();

                try {
                    Thread.sleep(150);
                } catch (Exception e) {}

                int end = lastRunnable.get();
                System.out.println("Last runnable checked was " + end + ".");
                handler.quit();
                assertTrue(start != end);
                s.release();
            }
        }).start();
        s.acquire();
    }


    private void reset() {
        if (mgr != null && mgr.initialized()) {
            mgr.shutdown();
        }

        mgr = ThreadManager.getInstance(new ThreadManagerConfig().withThreadCounts(3, 6).withUnitTesting(true));
        mgr.init();
    }

    private void release(Semaphore s) {
        if (s != null) {
            try {
                s.release();
            } catch (Exception e) {
            }
        }
    }

    @After
    public void teardown() {
        mgr.shutdown();
    }
}
