package com.xiledsystems.aal;


import com.xiledsystems.aal.util.ByteBuff;
import com.xiledsystems.aal.util.ByteUtil;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public final class BytesTest {


    @Test
    public void hexTest() throws Exception {
        byte[] bytes = { 0x00, 0x01, (byte) 0xFF, 0x3D, (byte) 0x7C };
        String hexed = ByteUtil.bytesToHex(bytes);
        assertEquals("0001FF3D7C", hexed);
        byte[] dehex = ByteUtil.hexToBytes("4B0010F4C2");
        byte[] check = new byte[] {(byte) 0x4B, 0x00, 0x10, (byte) 0xF4, (byte) 0xC2 };
        assertTrue(Arrays.equals(dehex, check));
    }

    @Test
    public void reverseTest() throws Exception {
        byte[] bytes = { 0x1, 0x2, 0x3, 0x4, 0x5 };
        byte[] rBytes = { 0x5, 0x4, 0x3, 0x2, 0x1 };
        byte[] reversed = ByteUtil.reverse(bytes);
        assertTrue(Arrays.equals(rBytes, reversed));
    }

    @Test
    public void valueTest() throws Exception {
        boolean bool = true;
        byte[] sByte = new byte[] { 0x61, (byte) 0xA8 };
        byte[] nsByte = new byte[] { (byte) 0x9E, (byte) 0x58 };
        byte[] iByte = new byte[] { 0x00, (byte) 0xBC, 0x61, 0x4E };
        byte[] niByte = new byte[] { (byte) 0xFF, 0x43, (byte) 0x9E, (byte) 0xB2 };
        byte[] lByte = new byte[] { 0x00, 0x00, 0x00, 0x01, 0x31, 0x61, (byte) 0xBF, 0x15 };
        byte[] nlByte = new byte[] { (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFE, (byte) 0xCE, (byte) 0x9E, 0x40, (byte) 0xEB };
        assertTrue(ByteUtil.boolToByte(bool) == 1);
        bool = false;
        assertTrue(ByteUtil.boolToByte(bool) == 0);
        short s = 25000;
        assertTrue(Arrays.equals(ByteUtil.shortToBytes(s), sByte));
        s = -25000;
        assertTrue(Arrays.equals(ByteUtil.shortToBytes(s), nsByte));
        int i = 12345678;
        assertTrue(Arrays.equals(ByteUtil.intToBytes(i), iByte));
        i = -12345678;
        assertTrue(Arrays.equals(ByteUtil.intToBytes(i), niByte));
        long l = 5123456789L;
        assertTrue(Arrays.equals(ByteUtil.longToBytes(l), lByte));
        l = -5123456789L;
        assertTrue(Arrays.equals(ByteUtil.longToBytes(l), nlByte));
        assertEquals(ByteUtil.byteToBool((byte) 0), false);
        assertEquals(ByteUtil.byteToBool((byte) 1), true);
        assertEquals(ByteUtil.bytesToShort(sByte), 25000);
        assertEquals(ByteUtil.bytesToShort(nsByte), -25000);
        assertEquals(ByteUtil.bytesToInt(iByte), 12345678);
        assertEquals(ByteUtil.bytesToInt(niByte), -12345678);
        assertEquals(ByteUtil.bytesToLong(lByte), 5123456789L);
        assertEquals(ByteUtil.bytesToLong(nlByte), -5123456789L);
    }

    @Test
    public void byteArrayModTest() throws Exception {
        byte[] bytesA = { 0x1, 0x2, 0x3, 0x4 };
        byte[] bytesB = { 0x5, 0x6, 0x7, 0x8 };
        byte[] b = new byte[0];
        b = ByteUtil.append(b, bytesA);
        assertTrue(Arrays.equals(b, bytesA));
        b = ByteUtil.append(b, bytesB);
        assertTrue(Arrays.equals(b, new byte[] { 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8 }));
        b = ByteUtil.padStart(b, 10);
        assertTrue(Arrays.equals(b, new byte[] { 0x0, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8 }));
        b = ByteUtil.append(b, (byte) 0, 2);
        assertTrue(Arrays.equals(b, new byte[] { 0x0, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x0, 0x0 }));
    }

    @Test
    public void byteBufferTest() throws Exception {
        ByteBuff buff = new ByteBuff(5);
        assertTrue(buff.length() == 0);
        buff.append(new byte[] { 0x22, 0x33, 0x44, 0x55 });
        assertTrue(buff.length() == 4);
        buff.pollBytes();
        assertTrue(buff.length() == 0);
        buff.append(new byte[] { 0x44, 0x71 });
        assertTrue(Arrays.equals(buff.pollBytes(), new byte[] { 0x44, 0x71 }));
        buff = new ByteBuff(new byte[] { 0x66, 0x77, 0x55 });
        assertTrue(buff.length() == 3);
    }

}
