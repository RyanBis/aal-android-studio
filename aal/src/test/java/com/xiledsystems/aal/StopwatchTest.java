package com.xiledsystems.aal;


import org.junit.Test;
import static org.junit.Assert.assertTrue;
import com.xiledsystems.aal.util.StopWatch;


public final class StopwatchTest {


    @Test
    public void stopWatchTest() throws Exception {
        StopWatch watch = new StopWatch();
        watch.start();
        Thread.sleep(50);
        watch.stop();
        assertTrue(watch.getDifference() >= 50 && !(watch.getDifference() < 50));
    }

}
