package com.xiledsystems.aal;


import com.xiledsystems.aal.encrypt.AES;
import com.xiledsystems.aal.encrypt.AESDecryptedData;
import com.xiledsystems.aal.encrypt.AESEncryptedData;
import com.xiledsystems.aal.encrypt.AESStrength;
import com.xiledsystems.aal.encrypt.Key;
import com.xiledsystems.aal.encrypt.RSA;
import com.xiledsystems.aal.encrypt.RSAData;
import com.xiledsystems.aal.encrypt.RSAStrength;
import com.xiledsystems.aal.util.TextUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Test;
import java.security.Security;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public final class EncryptionTest {


    private void reset() {
        // For straight up java, we need to add the BouncyCastle provider.
        BouncyCastleProvider prov = new BouncyCastleProvider();
        Security.addProvider(prov);
    }

    @Test
    public void aes128Test() throws Exception {
        reset();
        doAesTest(AESStrength.LOW);
    }

    @Test
    public void aes192Test() throws Exception {
        reset();
        doAesTest(AESStrength.MEDIUM);
    }

    @Test
    public void aes256Test() throws Exception {
        reset();
        doAesTest(AESStrength.HIGH);
    }

    @Test
    public void rsa256Test() throws Exception {
        reset();
        doRsaTest(RSAStrength.LOW);
    }

    @Test
    public void rsa512Test() throws Exception {
        reset();
        doRsaTest(RSAStrength.MEDIUM);
    }

    @Test
    public void rsa1024Test() throws Exception {
        reset();
        doRsaTest(RSAStrength.HIGH);
    }

    @Test
    public void rsa2048Test() throws Exception {
        reset();
        doRsaTest(RSAStrength.VERY_HIGH);
    }

    @Test
    public void rsa4096Test() throws Exception {
        reset();
        doRsaTest(RSAStrength.ULTRA_HIGH);
    }


    private static void doAesTest(AESStrength strength) {
        final String string = "Our string to test";
        AES a = new AES(strength);
        Key key = a.generateKey();
        AESEncryptedData enc = a.encryptData(key, TextUtil.stringToBytes(string));
        assertTrue(enc.getFailure().name(), enc.success());
        AESDecryptedData dec = a.decryptData(key, enc);
        assertTrue(enc.getFailure().name(), dec.success());
        assertEquals(TextUtil.bytesToString(dec.getBytes()), string);
    }

    private static void doRsaTest(RSAStrength strength) {
        final String string = "Some other string to test.";
        RSA r = new RSA(strength);
        Key key = r.generateKey();
        RSAData enc = r.encryptData(key, TextUtil.stringToBytes(string));
        assertTrue(enc.success());
        RSAData dec = r.decryptData(key, enc);
        assertTrue(dec.success());
        assertEquals(TextUtil.bytesToString(dec.getBytes()), string);
    }

}
