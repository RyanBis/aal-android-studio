package com.xiledsystems.aal;


import com.xiledsystems.aal.util.TextUtil;
import org.junit.Test;
import java.util.Arrays;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


public final class TextTest {


    @Test
    public void emptyTest() throws Exception {
        assertTrue(TextUtil.isEmpty(""));
        assertTrue(TextUtil.isEmpty(null));
    }

    @Test
    public void notEmptyTest() throws Exception {
        assertTrue(TextUtil.notEmpty("hi!"));
        assertFalse(TextUtil.notEmpty(""));
    }

    @Test
    public void stringConcatTest() throws Exception {
        String s = TextUtil.string("a", "b", "c");
        assertEquals(s, "abc");
        s = TextUtil.string(s, 1, 2, 3);
        assertEquals(s, "abc123");
    }

    @Test
    public void bytesTest() throws Exception {
        byte[] bytes = new byte[] { (byte) 0x74, 0x65, 0x73, 0x74, 0x69, 0x6E, 0x67 };
        assertTrue(Arrays.equals(bytes, TextUtil.stringToBytes("testing", "UTF-8")));
        assertEquals(TextUtil.byteToString(bytes, "UTF-8"), "testing");
        assertTrue(Arrays.equals(bytes, TextUtil.stringToBytes("testing")));
        assertEquals(TextUtil.bytesToString(bytes), "testing");
    }

}
