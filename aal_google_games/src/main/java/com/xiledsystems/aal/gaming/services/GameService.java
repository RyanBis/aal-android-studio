package com.xiledsystems.aal.gaming.services;


import android.app.Activity;
import android.content.Intent;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.gaming.google.GameHelper;
import java.util.ArrayList;
import java.util.List;

@State(DevelopmentState.PRODUCTION)
public class GameService extends BaseGameService {


    private GameHelper helper;
    private static int boardRequestNum = 0;


    public GameService(Activity context, GameServiceListener gameListener) {
        super(context, gameListener);
    }

    public GameHelper getGameHelper() {
        return helper;
    }

    private static int nextRequestNumber() {
        boardRequestNum++;
        return boardRequestNum;
    }

    @Override
    public void setup() {
        helper = new GameHelper(getActivity(), GameHelper.CLIENT_GAMES);
        helper.setup(listener);
    }

    @Override
    public void signIn() {
        setShouldBeSignedIn(true);
        if (helper == null) {
            setup();
        }
        helper.beginUserInitiatedSignIn();
    }

    @Override
    public void signOut() {
        setShouldBeSignedIn(false);
        if (helper != null) {
            helper.signOut();
        }
    }

    @Override
    public boolean isAmazon() {
        return false;
    }

    @Override
    public boolean isSignedIn() {
        if (helper != null) {
            return helper.isSignedIn();
        }
        return false;
    }

    @Override
    public void showLeaderBoard(String leaderBoard) {
        if (helper != null) {
            getActivity().startActivityForResult(Games.Leaderboards.getLeaderboardIntent(helper.getApiClient(), leaderBoard), nextRequestNumber());
        }
    }

    @Override
    public void showLeaderBoards() {
        if (helper != null) {
            getActivity().startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(helper.getApiClient()), nextRequestNumber());
        }
    }

    @Override
    public void sendScore(String leaderboardId, long score) {
        if (helper != null) {
            Games.Leaderboards.submitScore(helper.getApiClient(), leaderboardId, score);
        }
    }

    @Override
    public void getPlayerScore(final String leaderboardId, int timeType) {
        if (helper != null) {
            int span;
            switch (timeType) {
                case DAY:
                    span = LeaderboardVariant.TIME_SPAN_DAILY;
                    break;
                case WEEK:
                    span = LeaderboardVariant.TIME_SPAN_WEEKLY;
                    break;
                default:
                    span = LeaderboardVariant.TIME_SPAN_ALL_TIME;
            }
            PendingResult<Leaderboards.LoadPlayerScoreResult> resp = Games.Leaderboards.loadCurrentPlayerLeaderboardScore(helper.getApiClient(),
                    leaderboardId, span, LeaderboardVariant.COLLECTION_PUBLIC);
            resp.setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
                @Override
                public void onResult(Leaderboards.LoadPlayerScoreResult result) {
                    if (!result.getStatus().isSuccess()) {
                        if (listener != null) {
                            listener.onError(result.getStatus().getStatusMessage());
                        }
                    }
                    if (listener != null) {
                        listener.onScoreReceived(leaderboardId, (int) result.getScore().getRank(), result.getScore().getRawScore());
                    }
                }
            });
        }
    }

//    /**
//     * Returns an instance of {@link GameHelper}.
//     * @param <T>
//     * @return
//     */
//    @Override
//    protected Object getBackingService() {
//        return helper;
//    }


//    @Override
//    public GameHelper getNativeGameService() {
//        return helper;
//    }


    /**
     *
     * @return {@link GameHelper}
     */
    public GameHelper getNativeGameService() {
        return helper;
    }

    @Override
    public void showAchievements() {
        if (helper != null) {
            getActivity().startActivityForResult(Games.Achievements.getAchievementsIntent(helper.getApiClient()), nextRequestNumber());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (helper != null) {
            helper.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onStart() {
        if (helper != null) {
            helper.onStart(getActivity());
        }
    }

    @Override
    public void onStop() {
        if (helper != null) {
            helper.onStop();
        }
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void incrementAchievement(String achievementId, int amount) {
        if (helper != null) {
            Games.Achievements.increment(helper.getApiClient(), achievementId, amount);
        }
    }

    @Override
    public void getPlayersAchievements() {
        final List<com.xiledsystems.aal.gaming.services.Achievement> achievements = new ArrayList<>();
        if (helper != null) {
            PendingResult<Achievements.LoadAchievementsResult> res = Games.Achievements.load(helper.getApiClient(), true);
            res.setResultCallback(new ResultCallback<Achievements.LoadAchievementsResult>() {
                @Override
                public void onResult(Achievements.LoadAchievementsResult result) {
                    if (result.getStatus().isSuccess()) {
                        AchievementBuffer buff = result.getAchievements();
                        int size = buff.getCount();
                        for (int i = 0; i < size; i++) {
                            com.xiledsystems.aal.gaming.services.Achievement a = new com.xiledsystems.aal.gaming.services.Achievement();
                            Achievement aga = buff.get(i);
                            a.setTitle(aga.getName());
                            a.setPointValue((int) aga.getXpValue());
                            a.setIncremental(aga.getType() == Achievement.TYPE_INCREMENTAL);
                            a.setDescription(aga.getDescription());
                            a.setId(aga.getAchievementId());
                            a.setImageUrl(aga.getUnlockedImageUrl());
                            a.setDateUnlocked(aga.getLastUpdatedTimestamp());
                            a.setHidden(aga.getState() == Achievement.STATE_HIDDEN);
                            a.setUnlocked(aga.getState() == Achievement.STATE_UNLOCKED);
                            int steps = 1;
                            try {
                                steps = aga.getCurrentSteps();
                            } catch (Exception e) {
                            }
                            a.setProgress(steps);
                            achievements.add(a);
                        }
                        if (listener != null) {
                            listener.onAchievementsReceived(achievements);
                        }
                    } else {
                        if (listener != null) {
                            listener.onError(result.getStatus().getStatusMessage());
                        }
                    }
                }
            });
        }
    }

    @Override
    public void unlockAchievement(String achievementId) {
        if (helper != null) {
            Games.Achievements.unlock(helper.getApiClient(), achievementId);
        }
    }
}