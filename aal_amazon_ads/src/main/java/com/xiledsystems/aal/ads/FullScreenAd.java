package com.xiledsystems.aal.ads;


import android.app.Activity;

import com.amazon.device.ads.Ad;
import com.amazon.device.ads.AdError;
import com.amazon.device.ads.AdProperties;
import com.amazon.device.ads.AdRegistration;
import com.amazon.device.ads.DefaultAdListener;
import com.amazon.device.ads.InterstitialAd;
import com.xiledsystems.aal.ads.amazon.BuildConfig;
import com.xiledsystems.aal.ads.base.BaseFullScreenAd;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public class FullScreenAd extends BaseFullScreenAd {

    private InterstitialAd fullAd;
    private boolean adLoaded = false;


    public FullScreenAd(Activity context, String appKey, AdInterface adHandler) {
        super(context, appKey, adHandler);
    }

    @Override
    public void onCreate(Activity context, String appKey) {
        AdRegistration.setAppKey(appKey);
    }

    @Override
    public void loadAd() {
        fullAd = new InterstitialAd(getContext());
        fullAd.setListener(new Listener());
        fullAd.loadAd();
    }

    @Override
    public void showAd() {
        if (adLoaded) {
            try {
                fullAd.showAd();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
                if (getAdHandler() != null) {
                    getAdHandler().adError("General Exception Caught: " + e.getMessage());
                }
            }
        } else {
            if (getAdHandler() != null) {
                getAdHandler().adError("Ad is not loaded yet.");
            }
        }
    }

    @Override
    public void onDestroy() {
        fullAd = null;
    }

    @Override
    public void addTestDevice(String device) {
    }

    @Override
    public boolean isAdLoaded() {
        return adLoaded;
    }

    private class Listener extends DefaultAdListener {
        @Override
        public void onAdLoaded(Ad ad, AdProperties adProperties) {
            adLoaded = true;
            if (getAdHandler() != null) {
                getAdHandler().adLoaded();
            }
        }

        @Override
        public void onAdFailedToLoad(Ad ad, AdError error) {
            if (FullScreenAd.this.getAdHandler() != null) {
                FullScreenAd.this.getAdHandler().adLoadingError(error.getMessage());
            }
        }

        @Override
        public void onAdDismissed(Ad ad) {
            if (FullScreenAd.this.getAdHandler() != null) {
                FullScreenAd.this.getAdHandler().adClosed();
            }
        }
    }

}
