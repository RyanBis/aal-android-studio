[id]: https://img.shields.io/badge/Javadocs-2.4.05-green.svg "Javadoc link image"

![Version Badge](https://img.shields.io/badge/Current%20Version-2.4.05-green.svg)

[![Javadoc Badge](https://img.shields.io/badge/Javadocs-2.4.05-green.svg)](https://javadoc.io/doc/com.xiledsystems/aal/2.4.05)


# Another Android Library (AAL) #

This is just what the name suggests, another android library. AAL is designed to be a general purpose library for making common android operations easier.
Some of the things included are:

* Utility classes (file utilities, device utilities, preferences wrapper, Rand (random generator))
* Sqlite Wrapper classes. Makes dealing with sqlite databases a breeze.
* AES/RSA Encryption convenience classes for encrypting/decrypting data easily
* SecurePrefs class which encrypts SharedPreferences automatically.
* SecureStore class which encrypts and persists lists of things to a sql database
* SoundPlayer class for playing short sounds ( <=5 seconds ) [Multi-stream]
* SongPlayer class for playing songs, or sounds longer than 5 seconds [Single stream]
* Basic 2D game engine using SurfaceView, with sprites, and collision detection, and spritesheet support (only Texture Packer right now).
* AAL Control - A light MVC framework for Android
* Custom dialogs, custom Dropdown class, and some utility classes for working with views.
* TCP System - A module which sets up boiler plate code for communicating over TCP (discovery, hosting a server, send/receiving, etc).

## Support Forum ##

If you need more help, or have any general questions, you can post a message on the support forum at [https://groups.google.com/forum/#!forum/aal-support](https://groups.google.com/forum/#!forum/aal-support)

### Multi-Market Support ###

AAL has an added goal of making multi-market builds easier to deal with. Currently, there are four separate modules for this that you can add, along with the core AAL library.

* [Amazon Ads] (aal_amazon_ads)
* [Amazon Game Circle] (aal_amazon_game_circle)
* [Google (Admob) Ads] (aal_google_ads)
* [Google Play Game Services] (aal_google_games)

[Multi market setup](https://bitbucket.org/RyanBis/aal-android-studio/wiki/Multi%20Market%20Setup)


### Modules ###

Besides the four modules listed above, there are some other modules as well. The idea is to keep the size down, and only add what you need.

* [Game Engine] (aal_game_engine) - This module contains the 2D game engine - while usable, it's not yet in a finished state.
* [Control] (aal_control) - This module is unique in that it's the only module which does NOT have a dependency on the core AAL module. This is meant to be
pretty bare-bones, and small. It contains the annotations, events, control, and util packages only. This module adds nothing you don't get in the core AAL
module, again, it's just designed to be for the MVC framework, without any of the other things from the core library.
* [TCP System] (aal_tcp) - This is only in it's own module because it requires API 16 (whereas the core AAL library only requires API 14). The module provides
classes for communicating with other "targets" over TCP. (Phones, printers, servers, etc).



### Stand-Alone AAL Setup ###

```
#!java

dependencies {
    compile 'com.xiledsystems:aal:2.4.05'
}
```


For more detailed information, please visit the [wiki.](https://bitbucket.org/RyanBis/aal-android-studio/wiki/browse/)

For feature requests, bugs, etc, please submit an issue in the [issue tracker.](https://bitbucket.org/RyanBis/aal-android-studio/issues?status=new&status=open)