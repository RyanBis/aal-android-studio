package com.xiledsystems.aal.gaming.services;


import android.app.Activity;
import android.content.Intent;
import com.amazon.ags.api.AGResponseCallback;
import com.amazon.ags.api.AGResponseHandle;
import com.amazon.ags.api.AmazonGamesCallback;
import com.amazon.ags.api.AmazonGamesClient;
import com.amazon.ags.api.AmazonGamesFeature;
import com.amazon.ags.api.AmazonGamesStatus;
import com.amazon.ags.api.achievements.Achievement;
import com.amazon.ags.api.achievements.GetAchievementsResponse;
import com.amazon.ags.api.leaderboards.GetPlayerScoreResponse;
import com.amazon.ags.api.leaderboards.SubmitScoreResponse;
import com.amazon.ags.api.player.AGSignedInListener;
import com.amazon.ags.api.player.Player;
import com.amazon.ags.api.player.PlayerClient;
import com.amazon.ags.api.player.RequestPlayerResponse;
import com.amazon.ags.constants.LeaderboardFilter;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;


@State(DevelopmentState.PRODUCTION)
public class GameService extends BaseGameService {

    private AmazonGamesClient client;
    private EnumSet<AmazonGamesFeature> serviceTypes = EnumSet.of(AmazonGamesFeature.Achievements, AmazonGamesFeature.Leaderboards, AmazonGamesFeature.Whispersync);
    private AmazonCallBack aCallback;


    public GameService(Activity context, GameServiceListener listener) {
        super(context, listener);
        aCallback = new AmazonCallBack();
    }

    @Override
    public void setup() {
        //client =  (AmazonGamesClient) AmazonGamesClient.getInstance();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void onStart() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onResume() {
        AmazonGamesClient.initialize(getActivity(), aCallback, serviceTypes);
    }

    @Override
    public void onPause() {
        if (client != null) {
            client.release();
        }
    }

    @Override
    public void signIn() {
        setShouldBeSignedIn(true);
        if (client != null) {
            client.getPlayerClient().setSignedInListener(new AGSignedInListener() {
                @Override
                public void onSignedInStateChange(boolean b) {
                    if (listener != null) {
                        if (b) {
                            listener.onSignInSuccess();
                        } else {
                            listener.onSignInFail();
                        }
                    }
                }
            });
            client.showSignInPage();
        }
    }

    @Override
    public void signOut() {
        setShouldBeSignedIn(false);
    }

    @Override
    public boolean isAmazon() {
        return true;
    }

    @Override
    public boolean isSignedIn() {
        if (client != null) {
            return client.getPlayerClient().isSignedIn();
        } else {
            return false;
        }
    }

    @Override
    public void showLeaderBoard(String leaderBoard) {
        if (client != null) {
            client.getLeaderboardsClient().showLeaderboardOverlay(leaderBoard);
        }
    }

    @Override
    public void showLeaderBoards() {
        if (client != null) {
            client.getLeaderboardsClient().showLeaderboardsOverlay();
        }
    }

    @Override
    public void sendScore(String leaderboardId, long score) {
        if (client != null) {
            AGResponseHandle<SubmitScoreResponse> resp = client.getLeaderboardsClient().submitScore(leaderboardId, score);
            resp.setCallback(new AGResponseCallback<SubmitScoreResponse>() {
                @Override
                public void onComplete(SubmitScoreResponse result) {
                    if (result.isError()) {
                        if (listener != null) {
                            listener.onError("Error submitting score. Amazon errorcode: " + result.getError());
                        }
                    }
                }
            });
        }
    }

    @Override
    public void getPlayerScore(final String leaderboardId, int timeType) {
        LeaderboardFilter filter;
        switch (timeType) {
            case DAY:
                filter = LeaderboardFilter.GLOBAL_DAY;
                break;
            case WEEK:
                filter = LeaderboardFilter.GLOBAL_WEEK;
                break;
            default:
                filter = LeaderboardFilter.GLOBAL_ALL_TIME;
        }
        if (client != null) {
            AGResponseHandle<GetPlayerScoreResponse> resp = client.getLeaderboardsClient().getLocalPlayerScore(leaderboardId, filter, (Object[]) null);
            resp.setCallback(new AGResponseCallback<GetPlayerScoreResponse>() {
                @Override
                public void onComplete(GetPlayerScoreResponse result) {
                    if (result.isError()) {
                        if (listener != null) {
                            listener.onError("Error trying to retrieve score. Amazon Error code: " + result.getError());
                        }
                    } else {
                        if (listener != null) {
                            listener.onScoreReceived(leaderboardId, result.getRank(), result.getScoreValue());
                        }
                    }
                }
            });
        }
    }

    @Override
    public void showAchievements() {
        if (client != null) {
            client.getAchievementsClient().showAchievementsOverlay();
        }
    }

    @Override
    public void unlockAchievement(String achievementId) {
        if (client != null) {
            client.getAchievementsClient().updateProgress(achievementId, 100.0f);
        }
    }

    @Override
    public void incrementAchievement(String achievementId, int amount) {
        if (client != null) {
            client.getAchievementsClient().updateProgress(achievementId, (float) amount);
        }
    }

    @Override
    public void getPlayersAchievements() {
        final List<com.xiledsystems.aal.gaming.services.Achievement> achs = new ArrayList<com.xiledsystems.aal.gaming.services.Achievement>();
        if (client != null) {
            PlayerClient pc = client.getPlayerClient();
            AGResponseHandle<RequestPlayerResponse> localplayer = pc.getLocalPlayer();
            localplayer.setCallback(new AGResponseCallback<RequestPlayerResponse>() {
                @Override
                public void onComplete(RequestPlayerResponse requestPlayerResponse) {
                    if (requestPlayerResponse.isError()) {
                        if (listener != null) {
                            listener.onError("Unable to retrieve Player. ErrorCode: " + requestPlayerResponse.getError());
                        }
                    } else {
                        Player p = requestPlayerResponse.getPlayer();
                        String playerId = p.getPlayerId();
                        AGResponseHandle<GetAchievementsResponse> aresp = client.getAchievementsClient().getAchievementsForPlayer(playerId);
                        aresp.setCallback(new AGResponseCallback<GetAchievementsResponse>() {
                            @Override
                            public void onComplete(GetAchievementsResponse getAchievementsResponse) {
                                if (getAchievementsResponse.isError()) {
                                    if (listener != null) {
                                        listener.onError("Unable to retrieve achievements. Error code: " + getAchievementsResponse.getError());
                                    }
                                } else {
                                    List<Achievement> agachs = getAchievementsResponse.getAchievementsList();
                                    int size = agachs.size();
                                    for (int i = 0; i < size; i++) {
                                        com.xiledsystems.aal.gaming.services.Achievement a = new com.xiledsystems.aal.gaming.services.Achievement();
                                        Achievement aga = agachs.get(i);
                                        if (aga.getDateUnlocked() != null) {
                                            a.setDateUnlocked(aga.getDateUnlocked().getTime());
                                        }
                                        a.setDescription(aga.getDescription());
                                        a.setHidden(aga.isHidden());
                                        a.setId(aga.getId());
                                        a.setImageUrl(aga.getImageURL());
                                        a.setIncremental(true);
                                        a.setPointValue(aga.getPointValue());
                                        a.setPosition(aga.getPosition());
                                        a.setProgress((int) aga.getProgress());
                                        a.setTitle(aga.getTitle());
                                        a.setUnlocked(aga.isUnlocked());
                                        achs.add(a);
                                    }
                                    if (listener != null) {
                                        listener.onAchievementsReceived(achs);
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    }

//    /**
//     * Returns an instance of {@link AmazonGamesClient}.
//     *
//     * @return
//     */
//    @Override
//    protected Object getBackingService() {
//        return client;
//    }


//    @Override
//    public AmazonGamesClient getNativeGameService() {
//        return client;
//    }


    /**
     *
     * @return {@link AmazonGamesClient}
     */
    public AmazonGamesClient getNativeGameService() {
        return client;
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    private class AmazonCallBack implements AmazonGamesCallback {
        @Override
        public void onServiceReady(AmazonGamesClient amazonGamesClient) {
            client = amazonGamesClient;
            if (listener != null && isSignedIn()) {
                listener.onSignInSuccess();
            }
        }

        @Override
        public void onServiceNotReady(AmazonGamesStatus amazonGamesStatus) {
            if (listener != null) {
                listener.onError(amazonGamesStatus.name());
            }
        }
    }
}
