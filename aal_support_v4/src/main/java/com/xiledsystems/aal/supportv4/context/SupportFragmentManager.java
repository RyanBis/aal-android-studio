package com.xiledsystems.aal.supportv4.context;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import com.xiledsystems.aal.context.BaseFragmentManager;
import com.xiledsystems.aal.context.IAalFragment;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SupportFragmentManager extends BaseFragmentManager
{

    private Fragment fragment;


    public SupportFragmentManager(Fragment fragment) {
        this.fragment = fragment;
        if (fragment instanceof IAalFragment) {

        } else {
            throw new IllegalAccessError("The context instantiating FragmentManager must implement the IAalFragment interface!");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment = null;
    }

    public Fragment getFragment() {
        return fragment;
    }

    @Override
    public Context getContext() {
        return fragment.getActivity();
    }

    @Override
    public Activity getActivity() {
        return fragment.getActivity();
    }
}
