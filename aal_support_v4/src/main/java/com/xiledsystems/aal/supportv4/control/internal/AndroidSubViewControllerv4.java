package com.xiledsystems.aal.supportv4.control.internal;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.xiledsystems.aal.control.internal.SubViewController;


public class AndroidSubViewControllerv4 implements SubViewController {


    private Fragment fragment;

    public AndroidSubViewControllerv4(Fragment frag) {
        fragment = frag;
    }

    Activity getActivity() {
        return fragment.getActivity();
    }

    public void gotoScreen(Class<?> clazz) {
        Intent intent = new Intent(getActivity(), clazz);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz);
        intent.putExtras(bundle);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, int requestCode) {
        Intent intent = new Intent(getActivity(), clazz);
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreen(Class<?> clazz, int requestCode, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz);
        intent.putExtras(bundle);
        gotoActivityForResult(intent, requestCode);
    }

    protected void gotoActivityForResult(Intent intent, int requestCode) {
        if (getActivity() != null) {
            getActivity().startActivityForResult(intent, requestCode);
        }
    }

    protected void gotoActivity(Intent intent) {
        if (getActivity() != null) {
            getActivity().startActivity(intent);
        }
    }
}
