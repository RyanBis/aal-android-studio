package com.xiledsystems.aal.control.internal;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public abstract class NonViewController {

    public abstract <T> T getContext();
    public abstract void destroy();
    public abstract void stop();
}
