package com.xiledsystems.aal.control.internal;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public abstract class ScreenModel<T extends ViewController> extends Model {

    private final T viewController;


    protected ScreenModel(T viewController) {
        super(true);
        this.viewController = viewController;
    }

    /**
     * Constructor used to determine the Activity class to open when using a {@link Model} class
     * in the {@link #gotoScreen(Class)} methods.
     */
    protected ScreenModel() {
        super(false);
        viewController = null;
    }

    protected T getViewController() {
        return viewController;
    }

    /**
     * Open up another screen.
     */
    public void gotoScreen(Class<?> classToGoTo) {
        viewController.gotoScreen(classToGoTo);
    }

    /**
     * @deprecated use {@link #kill()} instead.
     */
    @Deprecated
    public void goBack() {
        viewController.goBack();
    }

    /**
     * Close, or kill, this screen.
     */
    public void kill() {
        viewController.kill();
    }

    /**
     * This is called automatically by the system, you should never have to call this yourself, or override it.
     */
    @Override
    public void destroy() {
        viewController.destroy();
        super.destroy();
    }

}
