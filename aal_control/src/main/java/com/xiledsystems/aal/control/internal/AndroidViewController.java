package com.xiledsystems.aal.control.internal;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public class AndroidViewController extends ViewController {

    private Activity context;

    public AndroidViewController(Activity context) {
        this.context = context;
    }

    @Override
    public void gotoScreen(Class<?> clazz) {
        Intent intent = new Intent(context, clazz);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, int flags) {
        Intent intent = new Intent(context, clazz).setFlags(flags);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(context, clazz);
        intent.putExtras(bundle);
        gotoActivity(intent);
    }

    public void gotoScreen(Class<?> clazz, Bundle bundle, int flags) {
        Intent intent = new Intent(context, clazz).putExtras(bundle).setFlags(flags);
        gotoActivity(intent);
    }

    public void openOtherApp(String action) {
        openOtherApp(new Intent(action));
    }

    public void openOtherApp(String action, Uri data) {
        openOtherApp(new Intent(action).setData(data));
    }

    public void openOtherApp(Intent intent) {
        gotoActivity(intent);
    }

    public void openOtherAppForResult(String action, int requestCode) {
        openOtherAppForResult(new Intent(action), requestCode);
    }

    public void openOtherAppForResult(String action, Uri data, int requestCode) {
        openOtherAppForResult(new Intent(action).setData(data), requestCode);
    }

    public void openOtherAppForResult(Intent intent, int requestCode) {
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode) {
        Intent intent = new Intent(context, clazz);
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode, int flags) {
        Intent intent = new Intent(context, clazz).setFlags(flags);
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode, Bundle bundle) {
        Intent intent = new Intent(context, clazz).putExtras(bundle);
        gotoActivityForResult(intent, requestCode);
    }

    public void gotoScreenForResult(Class<?> clazz, int requestCode, Bundle bundle, int flags) {
        Intent intent = new Intent(context, clazz).putExtras(bundle).setFlags(flags);
        gotoActivityForResult(intent, requestCode);
    }

    protected void gotoActivityForResult(Intent intent, int requestCode) {
        if (context != null) {
            context.startActivityForResult(intent, requestCode);
        }
    }

    protected void gotoActivity(Intent intent) {
        if (context != null) {
            context.startActivity(intent);
        }
    }

    protected void startService(Class<?> clazz) {
        if (context != null) {
            context.startService(new Intent(context, clazz));
        }
    }

    protected void startService(Class<?> clazz, int flags) {
        if (context != null) {
            context.startService(new Intent(context, clazz).setFlags(flags));
        }
    }

    protected void startService(Class<?> clazz, Bundle data) {
        if (context != null) {
            context.startService(new Intent(context, clazz).putExtras(data));
        }
    }

    protected void startService(Class<?> clazz, Bundle data, int flags) {
        if (context != null) {
            context.startService(new Intent(context, clazz).putExtras(data).setFlags(flags));
        }
    }

    protected void stopService(Class<?> clazz) {
        if (context != null) {
            context.stopService(new Intent(context, clazz));
        }
    }

    protected void stopService(Class<?> clazz, int flags) {
        if (context != null) {
            context.stopService(new Intent(context, clazz).setFlags(flags));
        }
    }

    protected void stopService(Class<?> clazz, Bundle data) {
        if (context != null) {
            context.stopService(new Intent(context, clazz).putExtras(data));
        }
    }

    protected void stopService(Class<?> clazz, Bundle data, int flags) {
        if (context != null) {
            context.stopService(new Intent(context, clazz).putExtras(data).setFlags(flags));
        }
    }

    @Override
    public void goBack() {
        kill();
    }

    @Override
    public void kill() {
        if (context != null) {
            context.finish();
        }
    }

    protected Class<? extends Activity> getActivityClass() {
        if (context != null) {
            return context.getClass();
        } else {
            return null;
        }
    }

    public Activity getContext() {
        return context;
    }

    @Override
    void destroy() {
        context = null;
    }
}