package com.xiledsystems.aal.control.internal;


import android.support.annotation.CallSuper;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.events.Event;
import com.xiledsystems.aal.events.EventListener;
import com.xiledsystems.aal.events.EventManager;
import com.xiledsystems.aal.events.PostType;
import com.xiledsystems.aal.util.AalLogger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@State(DevelopmentState.PRODUCTION)
public abstract class Model implements EventListener {

    protected EventManager manager;
    private EventListener listener;
    private ConcurrentHashMap<Enum, List<EventListener>> eventListeners;
    private ConcurrentHashMap<Class<? extends Enum>, List<EventListener>> categoryListeners;
    protected AalLogger logger;
    private Class<? extends Enum> categoryClass;


    protected Model(boolean registerEvents) {
        if (manager == null) {
            manager = EventManager.getInstance();
        }
        if (registerEvents) {
            manager.registerAllEventListener(this);
        }
        eventListeners = new ConcurrentHashMap<>();
        categoryListeners = new ConcurrentHashMap<>();
    }

    /**
     * Override this method if you wish to capture ALL events passing through
     * the event system. Most of the time, {@link #processCategoryEvent(Event)} will cover
     * most cases.
     * @param args
     * @return
     */
    public boolean processEvent(Event args) {
        return false;
    }

    /**
     * Event dispatched for the category set with {@link #setCategory(Class)}. Return true if you responded
     * to a particular {@link Event}.
     */
    public abstract boolean processCategoryEvent(Event args);


    /**
     * Register an {@link EventListener} to listen for all {@link Event}s. You can only register one All Event Listener
     * per Model.
     */
    public void registerAllEventListener(EventListener listener) {
        this.listener = listener;
    }

    /**
     * Forwards {@link EventManager#registerCategoryListener(Class, EventListener, byte)}.
     */
    public void registerCategoryListener(Class<? extends Enum> category, EventListener listener) {
        addCateogryListener(category, listener);
        manager.registerCategoryListener(category, listener);
    }

    /**
     * Forwards {@link EventManager#registerCategoryListener(Class, EventListener, byte)}.
     */
    public void registerCategoryListener(Class<? extends Enum> category, EventListener listener, byte priority) {
        addCateogryListener(category, listener);
        manager.registerCategoryListener(category, listener, priority);
    }

    /**
     * Forwards {@link EventManager#registerListener(Enum, EventListener)}.
     */
    public void registerEventListener(Enum event, EventListener listener) {
        addEventListener(event, listener);
        manager.registerListener(event, listener);
    }

    /**
     * Forwards {@link EventManager#registerListener(Enum, EventListener, byte)}.
     */
    public void registerEventListener(Enum event, EventListener listener, byte priority) {
        addEventListener(event, listener);
        manager.registerListener(event, listener, priority);
    }

    /**
     * Set the category this {@link Model} will register with in the Event system. What you set here
     * is the event category that is dispatched to {@link #processCategoryEvent(Event)}.
     * @param categoryClass
     */
    public void setCategory(Class<? extends Enum> categoryClass) {
        this.categoryClass = categoryClass;
    }

    /**
     * @deprecated - use {@link #unregisterEventListener(Enum, EventListener)} instead.
     * @param event
     * @param listener
     */
    @Deprecated
    public void unregisterListener(Enum event, EventListener listener) {
        removeEventListener(event, listener);
        manager.unregisterListener(event, listener);
    }

    /**
     * Forwards {@link EventManager#unregisterListener(Enum, EventListener)}
     */
    public void unregisterEventListener(Enum event, EventListener listener) {
        removeEventListener(event, listener);
        manager.unregisterListener(event, listener);
    }

    /**
     * Forwards {@link EventManager#unregisterCategoryListener(Class, EventListener)}
     */
    public void unregisterCategoryListener(Class<? extends Enum> category, EventListener listener) {
        removeCategoryListener(category, listener);
        manager.unregisterCategoryListener(category, listener);
    }

    /**
     * Unregister the listener set with {@link #registerAllEventListener(EventListener)}.
     */
    public void unregisterAllEventListener() {
        listener = null;
    }

    /**
     * @deprecated Use {@link #unregisterAllEventListener()} instead.
     */
    @Deprecated
    public void unregisterListener() {
        listener = null;
    }

    /**
     * This unregisters every {@link EventListener} registered with this {@link Model}.
     */
    public void unregisterEveryListener() {
        unregisterAllEventListener();
        unregCategoryListeners();
        unregEventListeners();
    }

    /**
     * Forwards {@link #postOnCurrent(Enum)}
     */
    public <E extends Enum> void postEvent(E category) {
        postOnMain(category);
    }

    /**
     * Forwards {@link #postOnMain(Enum, Object...)}
     */
    public <E extends Enum> void postEvent(E category, Object... args) {
        postOnMain(category, args);
    }

    /**
     * Forwards {@link EventManager#postEvent(Event.Builder)}
     */
    public <E extends Enum> void postEvent(Event.Builder builder) {
        manager.postEvent(builder);
    }

    /**
     * Forwards {@link #postOnMain(Event.Builder)}
     */
    public <E extends Enum> void postOnMain(E category, Object... args) {
        Event.Builder b;
        if (args != null) {
            b = new Event.Builder(category, args);
        } else {
            b = new Event.Builder(category);
        }
        postOnMain(b);
    }

    /**
     * Forwards {@link #postOnMain(Enum, Object...)}
     */
    public <E extends Enum> void postOnMain(E category) {
        postOnMain(category, (Object[]) null);
    }

    /**
     * Forwards {@link EventManager#postEvent(Event.Builder)}, with a {@link PostType} of {@link PostType#onMain()}.
     */
    public <E extends Enum> void postOnMain(Event.Builder builder) {
        builder.setPostType(PostType.onMain());
        manager.postEvent(builder);
    }

    /**
     * Forwards {@link #postOnThread(Enum, Object...)}
     */
    public <E extends Enum> void postOnThread(E category) {
        postOnThread(category, (Object[]) null);
    }

    /**
     * Forwards {@link EventManager#postEvent(Event.Builder)} with a {@link PostType} of {@link PostType#onThread()}.
     */
    public <E extends Enum> void postOnThread(E category, Object... args) {
        Event.Builder b;
        if (args == null) {
            b = new Event.Builder(category);
        } else {
            b = new Event.Builder(category, args);
        }
        b.setPostType(PostType.onThread());
        manager.postEvent(b);
    }

    /**
     * Forwards {@link #postOnCurrent(Enum, Object...)}
     */
    public <E extends Enum> void postOnCurrent(E category) {
        postOnCurrent(category, (Object[]) null);
    }

    /**
     * Forwards {@link EventManager#postEvent(Event.Builder)} with a {@link PostType} of {@link PostType#onCurrent()}
     */
    public <E extends Enum> void postOnCurrent(E category, Object... args) {
        Event.Builder b;
        if (args == null) {
            b = new Event.Builder(category);
        } else {
            b = new Event.Builder(category, args);
        }
        b.setPostType(PostType.onCurrent());
        manager.postEvent(b);
    }

    /**
     * You should never override this method. You should instead override {@link #processEvent(Event)} if you wish
     * to have access to all Event categories. If you SO override this method, you should make sure to call
     * super, and call this method, so the held listeners are handled properly.
     */
    @CallSuper
    @Override
    public boolean onEvent(Event data) {
        if (!processEvent(data) && listener != null) {
            if (listener.onEvent(data))
                return true;
        }
        if (categoryClass != null && categoryClass.isInstance(data.getEventKey())) {
            if (processCategoryEvent(data))
                return true;
        }
        return false;
    }

    /**
     * You shouldn't have to call this yourself. If you decide to override this method, you should make sure to
     * call super to properly unregister listeners.
     */
    @CallSuper
    public void destroy() {
        unregisterEveryListener();
        listener = null;
        manager = null;
    }

    private void unregCategoryListeners() {
        final List<Class<? extends Enum>> keys = Collections.list(categoryListeners.keys());
        List<EventListener> list;
        for (Class<? extends Enum> cat : keys) {
            list = categoryListeners.get(cat);
            for (EventListener listener : list) {
                manager.unregisterCategoryListener(cat, listener);
            }
        }
        categoryListeners.clear();
    }

    private void unregEventListeners() {
        final List<Enum> keys = Collections.list(eventListeners.keys());
        List<EventListener> list;
        for (Enum event : keys) {
            list = eventListeners.get(event);
            for (EventListener listener : list) {
                manager.unregisterListener(event, listener);
            }
        }
        eventListeners.clear();
    }

    private void removeEventListener(Enum event, EventListener listener) {
        List<EventListener> list = eventListeners.get(event);
        if (list != null) {
            list.remove(listener);
            if (list.size() == 0) {
                eventListeners.remove(event);
            } else {
                eventListeners.put(event, list);
            }
        }
    }

    private void removeCategoryListener(Class<? extends Enum> category, EventListener listener) {
        List<EventListener> list = categoryListeners.get(category);
        if (list != null) {
            list.remove(listener);
            if (list.size() == 0) {
                categoryListeners.remove(category);
            } else {
                categoryListeners.put(category, list);
            }
        }
    }

    private void addCateogryListener(Class<? extends Enum> category, EventListener listener) {
        List<EventListener> list = categoryListeners.get(category);
        if (list == null) {
            list = (List) Collections.synchronizedList(new ArrayList<>(1));
        }
        if (!list.contains(listener)) {
            list.add(listener);
            categoryListeners.put(category, list);
        }
    }

    private void addEventListener(Enum event, EventListener listener) {
        List<EventListener> list = eventListeners.get(event);
        if (list == null) {
            list = (List) Collections.synchronizedList(new ArrayList<>(1));
        }
        if (!list.contains(listener)) {
            list.add(listener);
            eventListeners.put(event, list);
        }
    }

}
