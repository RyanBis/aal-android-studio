package com.xiledsystems.aal.control.internal;


import android.app.Service;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.UNTESTED)
public class AndroidServiceController extends NonViewController {

    private Service context;

    public AndroidServiceController(Service service) {
        context = service;
    }

    @Override
    public void stop() {
        if (context != null) {
            context.stopSelf();
        }
    }

    @Override
    public Service getContext() {
        return context;
    }

    @Override
    public void destroy() {
        context = null;
    }
}