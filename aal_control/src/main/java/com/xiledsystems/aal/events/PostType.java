package com.xiledsystems.aal.events;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Base abstract class for telling the {@link EventManager} how to post the resulting
 * {@link EventListener#onEvent(Event)} method after calling
 * any of the postEvent() methods in {@link EventManager}
 */
@State(DevelopmentState.PRODUCTION)
public abstract class PostType {

    enum Type {
        MAIN,
        THREAD,
        CURRENT
    }

    private static PostOnMain main;
    private static PostOnThread thread;
    private static PostOnCurrent current;


    abstract Type getType();

    public static PostOnMain onMain() {
        if (main == null) {
            main = new PostOnMain();
        }
        return main;
    }

    public static PostOnThread onThread() {
        if (thread == null) {
            thread = new PostOnThread();
        }
        return thread;
    }

    public static PostOnCurrent onCurrent() {
        if (current == null) {
            current = new PostOnCurrent();
        }
        return current;
    }

}
