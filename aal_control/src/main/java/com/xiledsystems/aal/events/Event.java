package com.xiledsystems.aal.events;


import android.annotation.TargetApi;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * Class used to signify an "event" that can be posted, and listeners can react to. This class holds information about the
 * particular event, such as any data, and how many times it's been executed.
 */
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@State(DevelopmentState.PRODUCTION)
public final class Event {

    private Object[] args;
    private int curIndex = 0;
    private final Enum eventKey;
    private final Set<Class<? extends EventListener>> processedSet;
    private final int maxExecutions;
    private final PostType type;
    private int curExecutions;
    public Long addTime = null;
    private Long delay = null;
    boolean canceled;


    /**
     * It's recommended to use the {@link Builder} class to create your {@link Event}.
     * @param maxExecutions
     * @param postType
     * @param eventKey
     *
     * @deprecated Use the {@link Builder} class to create an instance.
     */
    @Deprecated
    public Event(int maxExecutions, PostType postType, Enum eventKey) {
        this.eventKey = eventKey;
        this.type = postType;
        this.maxExecutions = maxExecutions;
        processedSet = new HashSet<>(0);
        args = new Object[0];
    }

    /**
     * Add data to this event. Best practice is to add all data for the event before posting it, then leave it alone.
     * This is why it's best to make use of the {@link Builder} class instead.
     * @param arg
     * @return
     *
     * @deprecated Use the {@link Builder} class to create an instance.
     */
    @Deprecated
    public final Event add(Object arg) {
        if (arg != null) {
            if (curIndex >= args.length) {
                args = Arrays.copyOf(args, args.length + 10);
            }
            args[curIndex] = arg;
            curIndex++;
        }
        return this;
    }

    /**
     * Add all data from another {@link Event} instance into this event instance. Best practice is to add all data for the event before posting it, then leave it alone.
     * This is why it's best to make use of the {@link Builder} class instead.
     * @param event
     *
     * @deprecated Use the {@link Builder} class to create an instance.
     */
    @Deprecated
    public final Event addAll(Event event) {
        if (event != null) {
            if (curIndex >= args.length) {
                args = Arrays.copyOf(args, args.length + event.size());
            }
            for (int i = 0; i < event.size(); i++) {
                args[curIndex] = event.get(i);
                curIndex++;
            }
        }
        return this;
    }

    /**
     *
     * @param oargs
     * @return
     *
     * @deprecated Use the {@link Builder} class to create an instance.
     */
    @Deprecated
    public final Event addAll(Object... oargs) {
        if (oargs != null) {
            if (curIndex >= args.length) {
                args = Arrays.copyOf(args, args.length + oargs.length);
            }
            for (int i = 0; i < oargs.length; i++) {
                args[curIndex] = oargs[i];
                curIndex++;
            }
        }
        return this;
    }

    /**
     * This will automatically cast the argument for you. This will
     * also not throw any Exceptions. If there's an issue (index out
     * of bounds, or class cast exception, etc), then null is returned.
     * @param index
     * @param <T>
     * @return
     */
    public final <T> T get(int index) {
        if (args != null && args.length > 0 && index > -1 && index < args.length) {
            try {
                return (T) args[index];
            } catch (Exception e) {
            }
        }
        return null;
    }

    /**
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public final <T> T get(Class<?> clazz) {
        if (args != null && args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (clazz.isInstance(args[i])) {
                    return (T) clazz.cast(args[i]);
                }
            }
        }
        return null;
    }

    /**
     *
     * @param clazz
     * @param offSet
     * @param <T>
     * @return
     */
    public final <T> T get(Class<?> clazz, int offSet) {
        int curCount = 0;
        if (args != null && args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (clazz.isInstance(args[i])) {
                    curCount++;
                    if (curCount >= offSet) {
                        return (T) clazz.cast(args[i]);
                    }
                }
            }
        }
        return null;
    }

    /**
     * Returns the amount of arguments contained in this event instance.
     *@return
     */
    public final int size() {
        if (args != null) {
            return args.length;
        } else {
            return 0;
        }
    }

    /**
     * Forwards {@link #getEventKey()}.
     * @param <T>
     * @return
     *
     * @deprecated Use {@link #getEventKey()} instead.
     */
    @Deprecated
    public final <T extends Enum> T getCategory() {
        return getEventKey();
    }

    /**
     * Returns the event key used to identify this particular {@link Event}. This used to be called
     * category, but was renamed to lesson confusion (a category would be the Enum class, rather than
     * a value within it).
     * @param <T>
     * @return
     */
    public final <T extends Enum> T getEventKey() {
        return (T) eventKey;
    }

    /**
     * Forwards {@link #getEventKey(Class)}.
     *
     * @deprecated Use {@link #getEventKey(Class<E>)} instead.
     */
    @Deprecated
    public final <E extends Enum> E getCategory(Class<E> enumClass) {
        return getEventKey(enumClass);
    }

    /**
     * Attempts to cast enum from the captured class. If it fails, null will
     * be returned. This method first checks if the event Enum is an instance
     * of the supplied class, then casts.
     *
     * @param enumClass
     * @return
     *
     */
    public final <E extends Enum> E getEventKey(Class<E> enumClass) {
        E action = null;
        if (enumClass.isInstance(eventKey)) {
            action = enumClass.cast(eventKey);
        }
        return action;
    }

    /**
     * Returns the raw {@link Object} array holding any data associated with this event instance.
     * @return
     */
    public final Object[] getArgs() {
        return args;
    }

    /**
     * Returns <code>true</code> if the {@param listenerClass} has processed this Event. Note, this does not check a particular instance,
     * just if a particular class has handled this event.
     */
    public final boolean handledBy(Class<? extends EventListener> listenerClass) {
        return processedSet.contains(listenerClass);
    }

    @Override
    public final String toString() {
        StringBuilder b = new StringBuilder();
        b.append("[").append(eventKey.name()).append("] ");
        if (args != null && args.length > 0) {
            b.append("[");
            for (int i = 0; i < args.length; i++) {
                b.append(args[i]);
                if (i < (args.length - 1)) {
                    b.append(", ");
                }
            }
            b.append("]");
        }
        return b.toString();
    }

    /**
     * Returns how many times this event instance has been executed.
     * @return
     */
    public final int currentExecutions() {
        return curExecutions;
    }

    /**
     * Returns the maximum number of executions this event instance allows.
     * @return
     */
    public final int maxExecutions() {
        return maxExecutions;
    }

    final void setAddTime(long time) {
        addTime = time;
    }

    final long delay() {
        return delay;
    }

    final boolean isLimited() {
        return maxExecutions > -1;
    }

    final PostType postType() {
        return type;
    }

    final Object[] data() {
        return args;
    }

    final boolean isDelayed() {
        return delay != null;
    }

    final boolean ready(long curTime) {
        return (curTime - addTime) >= delay;
    }

    final boolean hasMore() {
        return curExecutions < maxExecutions;
    }

    final void increaseExecution(EventListener listener) {
        curExecutions++;
        addProcessedListener(listener.getClass());
    }

    private void addProcessedListener(Class<? extends EventListener> listener) {
        processedSet.add(listener);
    }


    /**
     * Builder class used to create an instance of {@link Event}.
     */
    public final static class Builder {

        private PostType postType;
        private Integer maxExecutions;
        private final Enum eventKey;
        private final ArrayList<Object> data;
        private Long delay;


        /**
         * Create a new instance with the given event key.
         * @param eventKey
         */
        public Builder(Enum eventKey) {
            this.eventKey = eventKey;
            data = new ArrayList<>(0);
        }

        /**
         * Create a new instance with the given event key, and data
         * @param eventKey
         * @param args
         */
        public Builder(Enum eventKey, Object... args) {
            this(eventKey);
            if (args != null) {
                this.data.addAll(Arrays.asList(args));
            }
        }

        /**
         * Use the event key and data from the given {@link Event} instance, and create a new one.
         * @param event
         */
        public Builder(Event event) {
            this(event.getEventKey(), Arrays.copyOf(event.args, event.args.length));
        }

        /**
         * Set the {@link PostType} for the event.
         * @param type
         * @return
         */
        public final Builder setPostType(PostType type) {
            postType = type;
            return this;
        }

        /**
         * Set the maximum number of executions for this instance. If left unset, it's unlimited.
         * @param maxExecutions
         * @return
         */
        public final Builder setMaxExecutions(int maxExecutions) {
            this.maxExecutions = maxExecutions;
            return this;
        }

        /**
         * Set a delay in ms before attempting to execute this instance from the moment it's posted.
         * @param delay
         * @return
         */
        public final Builder setDelay(Long delay) {
            this.delay = delay;
            return this;
        }

        /**
         * Add data to this instance. The data will get added at the end of the backing array.
         * @param arg
         * @return
         */
        public final Builder add(Object arg) {
            data.add(arg);
            return this;
        }

        /**
         * Add any number of items to the backing array. This will insert as a single index.
         * @param args
         * @return
         */
        public final Builder add(Object... args) {
            data.add(args);
            return this;
        }

        /**
         * Add all the items given into the array, at the end of the array.
         * @param args
         * @return
         */
        public final Builder addAll(Object... args) {
            data.addAll(Arrays.asList(args));
            return this;
        }

        /**
         * Push data to the first index of the backing array. Everything else will get pushed back an index.
         * @param arg
         * @return
         */
        public final Builder push(Object arg) {
            data.add(0, arg);
            return this;
        }

        /**
         * Build the {@link Event} instance from the values in this {@link Builder} instance.         *
         * @param config Use this {@link EventManagerConfig} for any default values. If <code>null</code>, a new instance
         *               will be created and used.
         * @return
         */
        public final Event build(EventManagerConfig config) {
            config = config != null ? config : new EventManagerConfig();
            int max = maxExecutions == null ? config.defaultMaxExecutions : maxExecutions;
            PostType type = postType == null ? config.postType : postType;
            Long d = delay != null ? delay : config.defaultDelay;
            Event e = new Event(max, type, eventKey);
            if (data.size() > 0) {
                e.addAll(data.toArray());
            }
            if (d != null) {
                e.delay = delay;
            }
            return e;
        }
    }

}
