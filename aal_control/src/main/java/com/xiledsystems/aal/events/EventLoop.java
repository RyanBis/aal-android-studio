package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Class used by te Event system internally.
 */
@State(DevelopmentState.PRODUCTION)
class EventLoop {

    private final Runnable action;
    private boolean running = false;
    private int delayTime = 0;
    private int interval = 1000;
    private boolean autoStop = true;
    private boolean wasRunning = false;
    private final Runner tRunner = new Runner();
    private static EventThreadManager manager;


    /**
     * Remember to call {@link EventLoop#dispose()} when you are done with this object (usually in onDestroy()).
     *
     * @param action
     */
    public EventLoop(Runnable action) {
        this.action = action;
        manager = EventThreadManager.getInstance(true);
    }

    /**
     * This shouldn't be used often. The idea is, if you have too many threads running,
     * it can be worse for performance. The backing {@link EventThreadManager} class will allow
     * a max of 2 x cpu cores, so if you absolutely need to run more loops, and be able to
     * run other Thread operations, then this convenience method is here for that.
     * @param coreThreads
     * @param maxThreads
     */
    public final void updateThreadCounts(int coreThreads, int maxThreads) {
        manager.setThreadCount(coreThreads, maxThreads);
    }

    /**
     * This needs to be called before calling {@link EventLoop#start()}. It is not needed to
     * call this method manually, as it is called automatically by the
     * {@link EventLoop#start()} method.
     * @deprecated Don't use this method anymore. Just use {@link #start()} instead. This method
     * will be removed for the next major version
     */
    public final void initialize() {
        //manager.execute(tRunner);
        start();
    }


    /**
     * Starts the thread, if it isn't already, and begins processing
     * the supplied Runnable action given in the constructor.
     */
    public final void start() {
        running = true;
        manager.execute(tRunner);
    }

    public final boolean isRunning() {
        return running;
    }

    /**
     * Stops processing the Runnable action.
     */
    public final void stop() {
        running = false;
    }

    /**
     * Stops the thread, and explicitly clears the Runnable from the Thread.
     */
    public final void dispose() {
        stop();
        manager.remove(tRunner);
    }

    /**
     * Set this Loop to automatically stop, and restart itself when the Activity loses/regains
     * focus. Default = true
     * @param autoStop
     */
    public final void setAutoStop(boolean autoStop) {
        this.autoStop = autoStop;
    }

    /**
     * @return the interval this Loop is running at
     */
    public final int getInterval() {
        return interval;
    }

    /**
     * Set the interval time in ms between loops
     *
     * @param interval
     */
    public final void setInterval(int interval) {
        this.interval = interval;
    }

    /**
     * @return the time this Loop will delay before running for the first time.
     */
    public final int getDelayTime() {
        return delayTime;
    }

    /**
     * Set a time to delay before running for the first time. The delay will not happen
     * again, unless {@link EventLoop#dispose()} is called.
     * @param delayTime
     */
    public final void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

    private void dispatchLoopEvent() {
        if (action != null) {
            action.run();
        }
    }

    private final class Runner implements Runnable {
        @Override
        public void run() {
            int sleepTime;
            long beginTime;
            long timeDiff;
            boolean firstrun=true;
            while (running) {
                if (Thread.currentThread().interrupted()) {
                    running = false;
                    return;
                }
                if (firstrun) {
                    try {
                        Thread.sleep(delayTime);
                    } catch (InterruptedException e) {
                    }
                    firstrun = false;
                }
                // Here we setup a loop to keep running the dispatched event.
                beginTime = System.currentTimeMillis();
                dispatchLoopEvent();
                timeDiff = System.currentTimeMillis() - beginTime;
                sleepTime = (int) (interval - timeDiff);
                if (sleepTime > 0) {
                    try {
                        Thread.sleep(sleepTime);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

}