package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Convenience utility class for creating an {@link Event}.
 */
@State(DevelopmentState.PRODUCTION)
final class EventFactory {


    private EventFactory() {}


    static Event newEvent(Enum category, EventManagerConfig config) {
        Event.Builder b = new Event.Builder(category).setDelay(config.defaultDelay);
        return b.build(config);
    }

    static Event newEvent(Enum category, EventManagerConfig config, Object... data) {
        Event.Builder b = new Event.Builder(category, data).setDelay(config.defaultDelay);
        return b.build(config);
    }

    static Event.Builder newEventMax(int max, Enum category, EventManagerConfig config) {
        Event.Builder b = new Event.Builder(category).setMaxExecutions(max).setDelay(config.defaultDelay);
        return b;
    }

    static Event.Builder newEventMax(int max, Enum category, EventManagerConfig config, Object... data) {
        Event.Builder b = new Event.Builder(category, data).setMaxExecutions(max).setDelay(config.defaultDelay);
        return b;
    }

    static Event.Builder newDelayedEvent(Enum category, long delay, EventManagerConfig config, Object... data) {
        Event.Builder b = new Event.Builder(category, data).setDelay(delay);
        return b;
    }

    static Event.Builder newDelayedEventMax(int max, Enum category, long delay, EventManagerConfig config, Object... data) {
        Event.Builder b = new Event.Builder(category, data).setDelay(delay).setMaxExecutions(max).setDelay(config.defaultDelay);
        return b;
    }
}
