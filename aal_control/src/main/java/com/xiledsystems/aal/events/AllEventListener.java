package com.xiledsystems.aal.events;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Interface for listening to all Events posted with {@link EventManager}.
 */
@State(DevelopmentState.UNTESTED)
public interface AllEventListener {

    void onEvent(Enum category, EventArgs data);

}
