package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Post the {@link EventListener#onEvent(Event)} method on the main (UI)
 * thread regardless of the thread {@link EventManager#postEvent(Event.Builder)} is called from.
 * This is the default behavior of {@link EventManager#postEvent(Event)}, unless you
 * set another default using {@link EventManager#setDefaultPostType(PostType)}.
 */

@State(DevelopmentState.PRODUCTION)
public final class PostOnMain extends PostType {

    @Override
    final Type getType() {
        return Type.MAIN;
    }
}
