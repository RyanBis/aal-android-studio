package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

import java.util.Arrays;

/**
 * Class used to pass data in events posted with {@link EventManager}, returning
 * in {@link EventListener#onEvent(EventArgs)}.
 */
@State(DevelopmentState.UNTESTED)
public class EventArgs {

    private Object[] args;
    private int curIndex = 0;

    public EventArgs(Object... args) {
        this.args = args;
    }

    public EventArgs(int size) {
        args = new Object[size];
    }

    public EventArgs add(Object arg) {
        if (curIndex >= args.length) {
            args = Arrays.copyOf(args, args.length + 10);
        }
        args[curIndex] = arg;
        curIndex++;
        return this;
    }

    public EventArgs addAll(EventArgs eargs) {
        if (curIndex >= args.length) {
            args = Arrays.copyOf(args, args.length + eargs.size());
        }
        for (int i = 0; i < eargs.size(); i++) {
            args[curIndex] = eargs.get(i);
            curIndex++;
        }
        return this;
    }

    public EventArgs addAll(Object... oargs) {
        if (curIndex >= args.length) {
            args = Arrays.copyOf(args, args.length + oargs.length);
        }
        for (int i = 0; i < oargs.length; i++) {
            args[curIndex] = oargs[i];
            curIndex++;
        }
        return this;
    }

    /**
     * This will automatically cast the argument for you. This will
     * also not throw any Exceptions. If there's an issue (index out
     * of bounds, or class cast exception, etc), then null is returned.
     * @param index
     * @param <T>
     * @return
     */
    public <T> T get(int index) {
        if (args != null && args.length > 0 && index > -1 && index < args.length) {
            try {
                return (T) args[index];
            } catch (Exception e) {
            }
        }
        return null;
    }

    public int size() {
        if (args != null) {
            return args.length;
        } else {
            return 0;
        }
    }

}
