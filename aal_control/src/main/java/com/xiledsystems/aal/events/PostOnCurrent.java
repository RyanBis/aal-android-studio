package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Post the {@link EventListener#onEvent(Event)} method on the current
 * thread that {@link EventManager#postEvent(Event.Builder)} is called from.
 * This is NOT the recommended PostType to use. This will bypass the EventQueue entirely, and
 * just post the Event in the current thread. This would mainly be used for debugging, and special
 * circumstances.
 */

@State(DevelopmentState.PRODUCTION)
public final class PostOnCurrent extends PostType {

    @Override
    final Type getType() {
        return Type.CURRENT;
    }
}
