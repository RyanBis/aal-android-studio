package com.xiledsystems.aal.events;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Post the {@link EventListener#onEvent(Event)} method on a background
 * thread.
 */

@State(DevelopmentState.PRODUCTION)
public final class PostOnThread extends PostType {

    @Override
    final Type getType() {
        return Type.THREAD;
    }
}
