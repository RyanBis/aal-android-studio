package com.xiledsystems.aal.events;


import android.os.Handler;
import android.os.Looper;
import com.xiledsystems.aal.util.IThreadManager;
import com.xiledsystems.aal.util.UIHandler;


public class EventThreadManagerConfig implements Cloneable {


    public Integer coreThreads              = null;
    public Integer maxThreads               = null;
    public boolean preLoadCore              = true;
    public boolean allowCoreTimeOut         = false;
    public long coreTimeoutPeriodMs         = 3000;
    public boolean unitTest                 = false;


    public EventThreadManagerConfig withThreadCounts(int coreThreads, int maxThreads) {
        this.coreThreads = coreThreads;
        this.maxThreads = maxThreads;
        return this;
    }

    public EventThreadManagerConfig withCorePreload(boolean preload) {
        preLoadCore = preload;
        return this;
    }

    public EventThreadManagerConfig withCoreTimeoutPeriod(long timeOutPeriod) {
        coreTimeoutPeriodMs = timeOutPeriod;
        return this;
    }

    public EventThreadManagerConfig withUnitTesting(boolean unitTesting) {
        unitTest = unitTesting;
        return this;
    }



    @Override
    public EventThreadManagerConfig clone() {
        EventThreadManagerConfig clone;
        try {
            clone = (EventThreadManagerConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            clone = this;
        }
        return clone;
    }

    public static final class UnitTestUIHandler extends UIHandler {

        private final EventHandler handler;

        public UnitTestUIHandler() {
            handler = new EventHandler();
            handler.id = 666;
        }

        @Override
        public void post(Runnable action) {
            handler.post(action);
        }

        @Override
        public void postDelayed(Runnable action, long delay) {
            handler.postDelayed(action, delay);
        }

        @Override
        public void removeCallbacks(Runnable action) {
            handler.removeCallbacks(action);
        }

        @Override
        public void init(IThreadManager mgr) {
            handler.init(mgr);
        }

        public void quit() {
            handler.quit();
        }
    }

    public static final class DefaultUIHandler extends UIHandler {

        private final Handler handler;

        public DefaultUIHandler() {
            handler = new Handler(Looper.getMainLooper());
        }

        @Override
        public final void post(Runnable action) {
            handler.post(action);
        }

        @Override
        public final void postDelayed(Runnable action, long delay) {
            handler.postDelayed(action, delay);
        }

        @Override
        public final void removeCallbacks(Runnable action) {
            handler.removeCallbacks(action);
        }

        @Override
        public void init(IThreadManager mgr) {
        }

        public Handler getHandler() {
            return handler;
        }
    }

}
