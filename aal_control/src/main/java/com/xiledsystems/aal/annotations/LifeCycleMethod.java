package com.xiledsystems.aal.annotations;


/**
 * Enumeration to dictate which {@link android.app.Activity} lifecycle methods
 * a {@link com.xiledsystems.aal.internal.Component} uses. This is used so it's
 * easy to tell which methods you need to run for a {@link com.xiledsystems.aal.internal.Component}
 * if you are not using it in an {@link com.xiledsystems.aal.context.AalActivity}, or
 * {@link com.xiledsystems.aal.context.AalService}.
 */
public enum LifeCycleMethod {
    ON_CREATE,
    ON_ACTIVITY_RESULT,
    ON_NEW_INTENT,
    ON_RESUME,
    ON_START,
    ON_PAUSE,
    ON_STOP,
    ON_LOW_MEMORY,
    ON_DESTROY
}
