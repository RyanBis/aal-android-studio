package com.xiledsystems.aal.util;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.UNTESTED)
public abstract class UIHandler<T extends IThreadManager> {

    public abstract void post(Runnable action);
    public abstract void postDelayed(Runnable action, long delay);
    public abstract void removeCallbacks(Runnable action);
    public abstract void init(T mgr);

}
