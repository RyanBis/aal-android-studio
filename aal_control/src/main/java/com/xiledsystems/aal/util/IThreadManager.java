package com.xiledsystems.aal.util;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.PRODUCTION)
public interface IThreadManager {
    void execute(Runnable action);
    void handlerOpened();
    void handlerClosed();
}
