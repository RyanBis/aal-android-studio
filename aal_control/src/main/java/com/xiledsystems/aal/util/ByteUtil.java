package com.xiledsystems.aal.util;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.Base64;


@TargetApi(Build.VERSION_CODES.FROYO)
public final class ByteUtil {

    private ByteUtil() {}

    public static final byte[] EMPTY_ARRAY = new byte[0];

    public static int BASE64_FLAGS = Base64.NO_WRAP;



    private final static char[] HEX_ARRAY = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    private final static int SHORT_SIZE = Short.SIZE / 8;
    private final static int INT_SIZE = Integer.SIZE / 8;
    private final static int LONG_SIZE = Long.SIZE / 8;




    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int i = 0; i < bytes.length; i++ ) {
            v = bytes[i] & 0xFF;
            hexChars[i * 2] = HEX_ARRAY[v >>> 4];
            hexChars[i * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static byte[] hexToBytes(String hexString) {
        final int length = hexString.length();
        byte[] bytes = new byte[length / 2];
        for (int i = 0; i < length; i+=2) {
            bytes[i/2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(hexString.charAt(i + 1), 16));
        }
        return bytes;
    }

    public static String bytesToBase64(byte[] bytes) {
        return Base64.encodeToString(bytes, BASE64_FLAGS);
    }

    public static byte[] base64ToBytes(String base64String) {
        return Base64.decode(base64String, BASE64_FLAGS);
    }

    public static byte[] reverse(byte[] bytes) {
        byte temp;
        for (int i = 0; i < bytes.length / 2; i++) {
            temp = bytes[i];
            bytes[i] = bytes[bytes.length - i - 1];
            bytes[bytes.length - i - 1] = temp;
        }
        return bytes;
    }

    public static byte boolToByte(boolean value) {
        return value ? (byte) 0x1 : 0x0;
    }

    public static byte[] boolToBytes(boolean value) {
        return new byte[] { boolToByte(value) };
    }

    public static boolean bytesToBool(byte[] value) {
        return byteToBool(value[0]);
    }

    public static boolean byteToBool(byte value) {
        return value == 0x1;
    }

    public static short bytesToShort(byte[] value) {
        return bytesToShort(value, false);
    }

    public static short bytesToShort(byte[] value, boolean reverse) {
        return (short) doBytesToLong(SHORT_SIZE, value, reverse);
    }

    public static byte[] shortToBytes(short value) {
        return shortToBytes(value, false);
    }

    public static byte[] shortToBytes(short value, boolean reverse) {
        return doLongToBytes(SHORT_SIZE, value, reverse);
    }

    public static byte[] intToBytes(int value) {
        return intToBytes(value, false);
    }

    public static byte[] intToBytes(int value, boolean reverse) {
        return doLongToBytes(INT_SIZE, value, reverse);
    }

    public static int bytesToInt(byte[] value) {
        return bytesToInt(value, false);
    }

    public static int bytesToInt(byte[] value, boolean reverse) {
        return (int) doBytesToLong(INT_SIZE, value, reverse);
    }

    public static byte[] longToBytes(long value) {
        return longToBytes(value, false);
    }

    public static byte[] longToBytes(long value, boolean reverse) {
        return doLongToBytes(LONG_SIZE, value, reverse);
    }

    public static long bytesToLong(byte[] value) {
        return bytesToLong(value, false);
    }

    public static long bytesToLong(byte[] value, boolean reverse) {
        return doBytesToLong(LONG_SIZE, value, reverse);
    }

    public static byte[] doLongToBytes(int size, long value, boolean reverse) {
        byte[] bytes = new byte[size];
        for (int i = size - 1; i >= 0; i--) {
            bytes[i] = (byte) (value & 0xFF);
            value >>= 8;
        }
        if (reverse) {
            return reverse(bytes);
        } else {
            return bytes;
        }
    }

    public static long doBytesToLong(int size, byte[] value, boolean reverse) {
        if (reverse) {
            value = reverse(value);
        }
        long val = 0;
        if (value.length < size) {
            value = padStart(value, size);
        }
        for (int i = 0; i < size; i++) {
            val <<= 8;
            val |= (int) value[i] & 0xFF;
        }
        return val;
    }

    public static byte[] append(byte[] original, byte[] append) {
        byte[] bytes = new byte[original.length + append.length];
        for (int i = 0; i < original.length; i++) {
            bytes[i] = original[i];
        }
        for (int i = 0; i < append.length; i++) {
            bytes[i + original.length] = append[i];
        }
        return bytes;
    }

    public static byte[] append(byte[] original, byte value, int count) {
        byte[] bytes = new byte[original.length + count];
        for (int i = 0; i < original.length; i++) {
            bytes[i] = original[i];
        }
        for (int i = 0; i < count; i++) {
            bytes[i + original.length] = value;
        }
        return bytes;
    }

    public static byte[] padStart(byte[] array, int desiredLength) {
        if (array.length >= desiredLength) {
            return array;
        }
        byte[] pad = new byte[Math.min(desiredLength, desiredLength - array.length)];
        for (int i = 0; i < pad.length; i++) {
            pad[i] = 0x0;
        }
        return append(pad, array);
    }

    public static byte[] copyOfRange(byte[] from, int start, int end) {
        final int length = end - start;
        final byte[] result = new byte[length];
        System.arraycopy(from, start, result, 0, length);
        return result;
    }

    public static byte[] random(int size) {
        byte[] bytes = new byte[size];
        Rand.getRandom().nextBytes(bytes);
        return bytes;
    }

}
