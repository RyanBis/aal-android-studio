package com.xiledsystems.aal.util;


import android.util.Log;


public class AalLogger {


    private boolean unitTest;


    public AalLogger() {
        try {
            Log.e("AalLogger", "Logger Initialized.");
        } catch (RuntimeException e) {
            unitTest = e.getMessage().contains("not mocked");
        }
    }

    @Deprecated
    public void logE(IAalLogger component, String message) {
        logE(component, () -> message);
    }

    public void logE(IAalLogger component, LogFunction msgFunc) {
        if (component.isLoggingEnabled()) {
            final String tag = component.getClass().getSimpleName();
            final String msg = msgFunc.getMessage();
            doLogE(tag, msg);
        }
    }

    @Deprecated
    public void logW(IAalLogger component, String message) {
        logW(component, () -> message);
    }

    public void logW(IAalLogger component, LogFunction msgFunc) {
        if (component.isLoggingEnabled()) {
            final String tag = component.getClass().getSimpleName();
            final String msg = msgFunc.getMessage();
            doLogW(tag, msg);
        }
    }

    @Deprecated
    public void logD(IAalLogger component, String message) {
        logD(component, () -> message);
    }

    public void logD(IAalLogger component, LogFunction msgFunc) {
        if (component.isLoggingEnabled()) {
            final String tag = component.getClass().getSimpleName();
            final String msg = msgFunc.getMessage();
            doLogD(tag, msg);
        }
    }

    @Deprecated
    public void logI(IAalLogger component, String message) {
        logI(component, () -> message);
    }

    public void logI(IAalLogger component, LogFunction msgFunc) {
        if (component.isLoggingEnabled()) {
            final String tag = component.getClass().getSimpleName();
            final String msg = msgFunc.getMessage();
            doLogI(tag, msg);
        }
    }

    @Deprecated
    public void logEMethod(IAalLogger component, String message) {
        logEMethod(component, () -> message);
    }

    public void logEMethod(IAalLogger component, LogFunction msgFunc) {
        final LogFunction f = msgFunc;
        if (component.isLoggingEnabled() && f != null) {
            final String tag = TextUtil.string(component.getClass().getSimpleName(), ".", latestTrace(this).getMethodName(), "()");
            final String msg = f.getMessage();
            doLogE(tag, msg);
        }
    }

    @Deprecated
    public void logWMethod(IAalLogger component, String message) {
        logWMethod(component, () -> message);
    }

    public void logWMethod(IAalLogger component, LogFunction msgFunc) {
        final LogFunction f = msgFunc;
        if (component.isLoggingEnabled() && f != null) {
            final String tag = TextUtil.string(component.getClass().getSimpleName(), ".", latestTrace(this).getMethodName(), "()");
            final String msg = f.getMessage();
            doLogW(tag, msg);
        }
    }

    @Deprecated
    public void logDMethod(IAalLogger component, String message) {
        logDMethod(component, () -> message);
    }

    public void logDMethod(IAalLogger component, LogFunction msgFunc) {
        final LogFunction f = msgFunc;
        if (component.isLoggingEnabled() && f != null) {
            final String tag = TextUtil.string(component.getClass().getSimpleName(), ".", latestTrace(this).getMethodName(), "()");
            final String msg = f.getMessage();
            doLogD(tag, msg);
        }
    }

    @Deprecated
    public void logIMethod(IAalLogger component, String message) {
        logIMethod(component, () -> message);
    }

    public void logIMethod(IAalLogger component, LogFunction msgFunc) {
        final LogFunction f = msgFunc;
        if (component.isLoggingEnabled() && f != null) {
            final String tag = TextUtil.string(component.getClass().getSimpleName(), ".", latestTrace(this).getMethodName(), "()");
            final String msg = f.getMessage();
            doLogI(tag, msg);
        }
    }

    public void logE(String message) {
        final String tag = TextUtil.string(latestTrace(this).getMethodName(), "()");
        doLogE(tag, message);
    }

    public void logW(String message) {
        final String tag = TextUtil.string(latestTrace(this).getMethodName(), "()");
        doLogW(tag, message);
    }

    public void logD(String message) {
        final String tag = TextUtil.string(latestTrace(this).getMethodName(), "()");
        doLogD(tag, message);
    }

    public void logI(String message) {
        final String tag = TextUtil.string(latestTrace(this).getMethodName(), "()");
        doLogI(tag, message);
    }





    private static StackTraceElement latestTrace(AalLogger logger) {
        StackTraceElement[] elements = new Exception().getStackTrace();
        return latestTrace(logger, elements);
    }

    private void doLogE(String tag, String message) {
        if (unitTest) {
            System.out.println(TextUtil.string("E/", tag, ": ", message));
        } else {
            Log.e(tag, message);
        }
    }

    private void doLogW(String tag, String message) {
        if (unitTest) {
            System.out.println(TextUtil.string("W/", tag, ": ", message));
        } else {
            Log.w(tag, message);
        }
    }

    private void doLogD(String tag, String message) {
        if (unitTest) {
            System.out.println(TextUtil.string("D/", tag, ": ", message));
        } else {
            Log.d(tag, message);
        }
    }

    private void doLogI(String tag, String message) {
        if (unitTest) {
            System.out.println(TextUtil.string("I/", tag, ": ", message));
        } else {
            Log.i(tag, message);
        }
    }

    private static StackTraceElement latestTrace(AalLogger logger, StackTraceElement[] elements) {
        for (int i = 0; i < elements.length; i++) {
            if (!elements[i].getClassName().equals(logger.getClass().getName())) {
                return elements[i];
            }
        }
        return null;
    }

}
