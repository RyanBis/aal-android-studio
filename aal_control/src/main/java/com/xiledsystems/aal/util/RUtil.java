package com.xiledsystems.aal.util;


import android.content.Context;
import android.graphics.drawable.Drawable;

public final class RUtil {

    private RUtil() {}

    /**
     * Get an integer defined in the res directory. This gets the id from the
     * name provided, then gets the integer.
     * @param context
     * @param name
     * @return the integer from resources, -1 if the resource was not found
     */
    public static int getInteger(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "integer", context.getPackageName());
        if (id != 0) {
            return context.getResources().getInteger(id);
        } else {
            return -1;
        }
    }

    /**
     *
     * @param context
     * @param name
     * @return integer array from resources with the given name. <code>null</code> is returned if the resource is not found.
     */
    public static int[] getIntArray(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "array", context.getPackageName());
        if (id != 0) {
            return context.getResources().getIntArray(id);
        } else {
            return null;
        }
    }

    /**
     *
     * @param context
     * @param name
     * @return string resource with the given name. <code>null</code> is returned if the resource was not found.
     */
    public static String getString(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "string", context.getPackageName());
        if (id != 0) {
            return context.getResources().getString(id);
        } else {
            return null;
        }
    }

    /**
     *
     * @param context
     * @param name
     * @return the id of the view with the id name provided. This will return 0 if the id was not found.
     */
    public static int getViewId(Context context, String name) {
        return context.getResources().getIdentifier(name, "id", context.getPackageName());
    }

    /**
     *
     * @param context
     * @param name
     * @param <T>
     * @return Drawable with the given name. This will automatically cast the Drawable to whichever type you use. However, null
     * will be returned if the resource was not found.
     */
    public static <T extends Drawable> T getDrawable(Context context, String name) {
        int id = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        if (id != 0) {
            return (T) context.getResources().getDrawable(id);
        } else {
            return null;
        }
    }

    /**
     * Convenience method to automatically cast a drawable from resources.
     * @param context
     * @param resId
     * @param <T>
     * @return
     */
    public static <T extends Drawable> T getDrawable(Context context, int resId) {
        return (T) context.getResources().getDrawable(resId);
    }

}
