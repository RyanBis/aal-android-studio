package com.xiledsystems.aal.util;


public interface IAalLogger {
    boolean isLoggingEnabled();
}
