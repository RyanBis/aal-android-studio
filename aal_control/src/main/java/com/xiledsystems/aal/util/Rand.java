package com.xiledsystems.aal.util;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Utility class for generating random numbers and strings.
 *
 * This holds a static instance of {@link SecureRandom}. You don't have to instantiate this yourself, it will be done
 * automatically the first time you call any of the methods that generate a random object. You can clear the instance
 * using {@link #dispose()}.
 *
 * Note that other classes may call methods in this class (such as some of the encryption classes), so the instance
 * can likely be recreated. If you don't want this class to <i>ever</i> cache an instance in memory, then set
 * {@link #cacheRandomInstance} to <code>false</code>.
 *
 * The seed held when calling {@link #setRandomSeed(long)} isn't cleared until {@link #dispose()} is called. If {@link #setRandomSeed(long)}
 * is never called, then a new seed will be generated when a new {@link SecureRandom} instance is created.
 */
@State(DevelopmentState.PRODUCTION)
public final class Rand {
	
	private static Random random;

    public static final char[] LOW_CHARS = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    public static final char[] UPPER_CHARS = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    public static final char[] NUMS = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
    public static final char[] SPECIAL_CHARS = { '/', '@', '#', '!', '$', '%', '^', '&', '*' };

    public static boolean cacheRandomInstance = true;

    private static final int TYPE_LOWERCASE = 0;
    private static final int TYPE_UPPERCASE = 1;
    private static final int TYPE_NUMBER = 2;
    private static final int TYPE_SPECIAL = 3;

    private static Long seed;


    /**
     * Set the seed that will be used by the backing {@link SecureRandom} instance. This will update the instance if it
     * already is instantiated.
     */
    public static void setRandomSeed(long randomSeed) {
        seed = randomSeed;
        if (random != null) {
            if (cacheRandomInstance)
                random.setSeed(seed);
            else
                random = null;
        }
    }

    /**
     * Generates a random {@link String} of letters of the given length.
     *
     * @see #LOW_CHARS
     * @see #UPPER_CHARS
     */
    public static String randomCharString(int length) {
        StringBuilder b = new StringBuilder();
        checkRandom();
        for (int i = 0; i < length; i++) {
            int type = random.nextInt(TYPE_NUMBER);
            b = getRandomCharacter(type, b, SPECIAL_CHARS);
        }
        if (!cacheRandomInstance)
            random = null;
        return b.toString();
    }

    /**
     * Generates a random {@link String} of letters and numbers of the given length.
     *
     * @see #LOW_CHARS
     * @see #UPPER_CHARS
     * @see #NUMS
     */
    public static String randomNumCharString(int length) {
        StringBuilder b = new StringBuilder();
        checkRandom();
        for (int i = 0; i < length; i++) {
            int type = random.nextInt(TYPE_SPECIAL);
            b = getRandomCharacter(type, b, SPECIAL_CHARS);
        }
        if (!cacheRandomInstance)
            random = null;
        return b.toString();
    }

    /**
     * Generates a random {@link String} of letters, numbers, and special characters of the given length.
     *
     * @see #LOW_CHARS
     * @see #UPPER_CHARS
     * @see #NUMS
     * @see #SPECIAL_CHARS
     */
    public static String getRandomString(int length) {
        return randomString(length, SPECIAL_CHARS);
    }

    /**
     * Generates a random {@link String} of letters, numbers, and special characters of the given length.
     *
     * @see #LOW_CHARS
     * @see #UPPER_CHARS
     * @see #NUMS
     */
    public static String randomString(int length, char[] specialChars) {
        checkRandom();
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int types;
            if (specialChars == null || specialChars.length == 0) {
                types = 3;
            } else {
                types = 4;
            }
            int type = random.nextInt(types);
            b = getRandomCharacter(type, b, specialChars);
        }
        if (!cacheRandomInstance)
            random = null;
        return b.toString();
    }

    /**
     * Get a random integer in the given range. Both arguments are inclusive.
     */
	public static int rndInt(int from, int to) {
		checkRandom();
		int c = random.nextInt(20) + 1;
		int i = 0;
		int r = random.nextInt((to - from) + 1) + from;
		while (i < c) {
			i++;
			r = random.nextInt((to - from) + 1) + from;
		}
        if (!cacheRandomInstance)
            random = null;
		return r;
	}

    /**
     * Generate a randomly filled byte array of the given size.
     */
    public static byte[] randomBytes(int size) {
        return ByteUtil.random(size);
    }

    /**
     * Get the instance of {@link SecureRandom} backing this class. If it is not instantiated, it will be before being
     * returned. In other words, this will never return <code>null</code>.
     */
    public static Random getRandom() {
        checkRandom();
        final Random newRandom = random;
        random = null;
        return newRandom;
    }

    /**
     * Clears the static instance of {@link SecureRandom}, and the seed set via {@link #setRandomSeed(long)}.
     * You generally shouldn't need to call this, but if you want to be strict about memory, this method allows
     * you to do so. Although as soon as any other method gets called in this class, the instance will be
     * recreated, thus resulting in chewing more CPU.
     */
    public static void dispose() {
        random = null;
        seed = null;
    }




    private static StringBuilder getRandomCharacter(int type, StringBuilder b, char[] specialChars) {
        int index;
        switch (type) {
            case TYPE_SPECIAL:
                index = random.nextInt(specialChars.length);
                b.append(specialChars[index]);
                return b;
            case TYPE_NUMBER:
                index = random.nextInt(NUMS.length);
                b.append(NUMS[index]);
                return b;
            case TYPE_UPPERCASE:
                index = random.nextInt(UPPER_CHARS.length);
                b.append(UPPER_CHARS[index]);
                return b;
            default:/*TYPE_LOWERCASE*/
                index = random.nextInt(LOW_CHARS.length);
                b.append(LOW_CHARS[index]);
                return b;
        }
    }

    private static long generateNewSeed() {
        int val = 1;
        long number = 0;
        for (int i = 1; i < 65; i++) {
            if (random.nextBoolean()) {
                number += val;
            }
            val *= 2;
        }
        if (random.nextBoolean()) {
            return ~number;
        }
        return number;
    }

    private static void checkRandom() {
        if (random == null) {
            random = new SecureRandom();
            if (seed != null) {
                random.setSeed(seed);
            } else {
                random.setSeed(generateNewSeed());
            }
        }
    }
}
