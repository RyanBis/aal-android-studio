package com.xiledsystems.aal.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.List;
import java.util.Set;


/**
 * Wrapper class for storing basic things to
 * {@link SharedPreferences}.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@State(DevelopmentState.PRODUCTION)
public class Prefs {
	

	private final static String LIST = "jsonlist";
	private final static String MAP = "jsonmap";
	private final Context context;
	private String prefsFileName = "MyPrefs";
	private SharedPreferences prefs;
	

	public Prefs(Context context) {
		this(context, "MyPrefs");
	}

	public Prefs(Context context, String fileName) {
		this.context = context;
		prefsFileName = fileName;
		loadPrefsInstance();
	}

	/**
	 * Sets the filename of this prefs instance.
	 * 
	 * @param filename
	 * @deprecated - This will be removed at some point in the future. You should instantiate this class with
	 * the preferred filename in the constructor instead.
	 */
	@Deprecated
	public void setFileName(String filename) {
		prefsFileName = filename;
		loadPrefsInstance();
	}

	/**
	 * 
	 * @return the filename this prefs instance is storing to
	 */
	public String getFileName() {
		return prefsFileName;
	}

	/**
	 * Store a boolean to shared prefs
	 * 
	 * @param tag The name to store this boolean under
	 * @param valueToStore
	 */
	public void storeBoolean(String tag, boolean valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putBoolean(tag, valueToStore);
		edit.commit();
	}

	/**
	 * Store an integer to prefs
	 * 
	 * @param tag The name to store this integer under
	 * @param valueToStore
	 */
	public void storeInt(String tag, int valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putInt(tag, valueToStore);
		edit.commit();
	}

	/**
	 * Store a Long to prefs
	 * 
	 * @param tag  The name to store this long under
	 * @param valueToStore
	 */
	public void storeLong(String tag, long valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putLong(tag, valueToStore);
		edit.commit();
	}

	/**
	 * Store a String to prefs
	 * 
	 * @param tag  The name to store this String under
	 * @param valueToStore
	 */
	public void storeString(String tag, String valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putString(tag, valueToStore);
		edit.commit();
	}

    /**
     * Store a String Set to prefs.
     *
     * @param tag
     * @param strings
     */
    public void storeStringSet(String tag, Set<String> strings) {
        prefs.edit().putStringSet(tag, strings).commit();
    }

    /**
     * Retrieves a string Set saved at the tag location
     * This will return null if the tag doesn't exist.
     * @param tag
     * @return
     */
	public Set<String> getStringSet(String tag) {
        return prefs.getStringSet(tag, null);
    }

	/**
	 * 
	 * @param tag The name of the boolean to retrieve from prefs
	 * @return false if none found
	 */
	public boolean getBoolean(String tag) {
		return prefs.getBoolean(tag, false);
	}

	/**
	 * 
	 * @param tag The name of the integer to retrieve from prefs
	 * @return 0 if none found
	 */
	public int getInt(String tag) {
		return prefs.getInt(tag, 0);
	}

	/**
	 * 
	 * @param tag The name of the long to retrieve from prefs
	 * @return 0 if none found
	 */
	public long getLong(String tag) {
		return prefs.getLong(tag, 0);
	}

	/**
	 * 
	 * @param tag The name of the String to retrieve from prefs
	 * @return "" if none found
	 */
	public String getString(String tag) {
		return prefs.getString(tag, "");
	}

	/**
	 * 
	 * @param tag The name of the boolean to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public boolean getBoolean(String tag, boolean defValue) {
		return prefs.getBoolean(tag, defValue);
	}

	/**
	 * 
	 * @param tag The name of the integer to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public int getInt(String tag, int defValue) {
		return prefs.getInt(tag, defValue);
	}

	/**
	 * 
	 * @param tag The name of the long to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public long getLong(String tag, long defValue) {
		return prefs.getLong(tag, defValue);
	}

	/**
	 * 
	 * @param tag The name of the String to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public String getString(String tag, String defValue) {
		return prefs.getString(tag, defValue);
	}
	
	private void loadPrefsInstance() {
		prefs = context.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
	}

}