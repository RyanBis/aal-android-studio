package com.xiledsystems.aal.util;


import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;


@TargetApi(Build.VERSION_CODES.KITKAT)
public final class KitKatUtil {

    public static boolean clearAppData(Context context) {
        return ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).clearApplicationUserData();
    }

}
