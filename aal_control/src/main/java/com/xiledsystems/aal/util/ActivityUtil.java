package com.xiledsystems.aal.util;


import android.util.Log;
import com.xiledsystems.aal.control.internal.AndroidScreenModel;
import com.xiledsystems.aal.control.internal.AndroidServiceModel;
import com.xiledsystems.aal.util.AalLogger;
import com.xiledsystems.aal.util.TextUtil;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;


public final class ActivityUtil {

    private ActivityUtil() {}

    public static Class<?> getOtherActivityClass(Class<?> screenClass, AalLogger logger) {
        if (AndroidScreenModel.class.isAssignableFrom(screenClass)) {
            AndroidScreenModel screen = null;
            try {
                Constructor[] cons = screenClass.getDeclaredConstructors();
                for (Constructor c : cons) {
                    if (c.getParameterTypes().length == 0) {
                        c.setAccessible(true);
                        screen = (AndroidScreenModel) c.newInstance();
                        break;
                    }
                }
            } catch (Exception e) {
                logger.logE(TextUtil.string("Unable to find no-argument constructor in ", screenClass.getSimpleName(), "! Starting another activity from this controller will not work!"));
            }
            if (screen != null) {
                try {
                    Method getActClass = screenClass.getDeclaredMethod("getActivityClass");
                    Class<?> actClass = (Class<?>) getActClass.invoke(screen, new Object[0]);
                    return actClass;
                } catch (Exception e) {
                    e.printStackTrace();
                    logger.logE(TextUtil.string("Error trying to get Activity class. Error msg: ", e.getMessage()));
                }
            }
        }
        return null;
    }

    public static Class<?> getActivityClass(Class<?> classToGoTo, AalLogger logger) {
        Class<?> clazz;
        Class<?> actClas = getOtherActivityClass(classToGoTo, logger);
        if (actClas != null) {
            clazz = actClas;
        } else {
            clazz = classToGoTo;
        }
        return clazz;
    }

    public static Class<?> getServiceClass(Class<?> serviceToStart) {
        Class<?> clazz;
        Class<?> actClas = getOtherServiceClass(serviceToStart);
        if (actClas != null) {
            clazz = actClas;
        } else {
            clazz = serviceToStart;
        }
        return clazz;
    }

    public static Class<?> getOtherServiceClass(Class<?> serviceModelClass) {
        if (AndroidServiceModel.class.isAssignableFrom(serviceModelClass)) {
            AndroidServiceModel model = null;
            try {
                Constructor[] cons = serviceModelClass.getDeclaredConstructors();
                for (Constructor c : cons) {
                    if (c.getParameterTypes().length == 0) {
                        c.setAccessible(true);
                        model = (AndroidServiceModel) c.newInstance();
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e(String.format("ActivityUtil", serviceModelClass.getSimpleName()), "Unable to find no-argument constructor! Starting another service from this controller will not work!");
            }
            if (model != null) {
                try {
                    Method getServiceClass = serviceModelClass.getDeclaredMethod("getServiceClass");
                    Class<?> servClass = (Class<?>) getServiceClass.invoke(model, new Object[0]);
                    return servClass;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

}
