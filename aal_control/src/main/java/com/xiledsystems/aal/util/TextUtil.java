package com.xiledsystems.aal.util;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import java.io.UnsupportedEncodingException;


@State(DevelopmentState.PRODUCTION)
public final class TextUtil {

	private TextUtil() {		
	}

	/**
	 * @param string
	 * @return <code>true</code> if the given String is either null, or empty
	 */
	public static boolean isEmpty(String string) {
		return string == null || string.length() < 1;
	}

	/**
	 * The opposite of {@link #isEmpty(String)}. Simple makes sure the string is NOT null, and is
	 * a length of at least 1.
	 * @param string
	 * @return
     */
	public static boolean notEmpty(String string) {
		return string != null && string.length() > 0;
	}

	/**
	 * Builds a string from the given objects. This is an order of magnitude faster than simply
	 * adding strings together like this [ "current count: " + myCount + " something else: " + otherVar ]. This
	 * uses a StringBuilder to build out the string.
	 * @param objects
	 * @return
	 */
	public static String string(Object... objects) {
		StringBuilder b = new StringBuilder();
		if (objects != null) {
			for (int i = 0; i < objects.length; i++) {
				b.append(objects[i]);
			}
		}
		return b.toString();
	}

	public static String byteToString(byte[] data, String encoding) {
		String s = null;
		try {
			s = new String(data, encoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return s;
	}

	public static String bytesToString(byte[] data) {
		return byteToString(data, "UTF-8");
	}

	public static byte[] stringToBytes(String string, String stringEncoding) {
		byte[] b = null;
		try {
			b = string.getBytes(stringEncoding);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			b = string.getBytes();
		}
		return b;
	}

	public static byte[] stringToBytes(String string) {
		return stringToBytes(string, "UTF-8");
	}
}
