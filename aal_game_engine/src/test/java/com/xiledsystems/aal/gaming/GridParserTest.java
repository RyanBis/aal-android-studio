package com.xiledsystems.aal.gaming;


import com.xiledsystems.aal.gaming.spritesheet.GridParser;
import com.xiledsystems.aal.gaming.spritesheet.SpriteInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import java.util.List;


@RunWith(RobolectricTestRunner.class)
public class GridParserTest {

    @Test
    public void parserTest() throws Exception {
        GridParser parser = new GridParser(10, 10, 320, 640);
        List<SpriteInfo> info = parser.parseSheet("");
        int i = 0;
        i++;

        parser = new GridParser(10, 10, 320, 640, 1, 6);
        info = parser.parseSheet("");
        i++;
    }

}
