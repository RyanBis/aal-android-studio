package com.xiledsystems.aal.gaming;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


public class BaseGamePanel extends SurfaceView implements SurfaceHolder.Callback {

    private MainGameThread mainThread;
    private OnTouchDown downTouch;
    private OnTouchUp upTouch;
    private OnDragStop onDragStop;
    private OnDragged onDrag;
    private EdgeReached edgeReached;
    private int backColor;
    private Paint paint;
    private String avgFPS;
    private boolean showFPS;
    private List<VisibleGameObject> objects;
    private List<NonVisibleGameObject> nObjects;
    private PanelInputBuffer touchHandler = new PanelInputBuffer();
    private TouchParser touchParser = new TouchParser();
    private static Rect srcRec;
    private static Bitmap backgroundBitmap;
    private static Bitmap movingBackgroundBitmap;
    private static Canvas backgroundCanvas;
    private boolean useAverage = true;
    private final List<VisibleGameObject> addQueue = new ArrayList<>();
    private final List<NonVisibleGameObject> addNQueue = new ArrayList<>();
    private boolean objectAdded = false;
    private Paint fpsPaint;
    private int fpsColor = Color.WHITE;
    private LayerSorter objectSorter = new LayerSorter();
    private LayerSorter2 nObjectSorter = new LayerSorter2();
    private AtomicBoolean objectsNeedSorting = new AtomicBoolean(true);
    private AtomicBoolean nObjectsNeedSorting = new AtomicBoolean(true);
    private boolean movingBackground = false;
    private int oWidth;
    private int oHeight;
    private static int srcWidth;
    private static int srcHeight;


    public BaseGamePanel(Context context) {
        this(context, null, 0);
    }

    public BaseGamePanel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseGamePanel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        backgroundBitmap = Bitmap.createBitmap(400, 800, Bitmap.Config.ARGB_8888);
        backgroundCanvas = new Canvas(backgroundBitmap);
        getHolder().addCallback(this);
        setFocusable(true);
        objects = new ArrayList<>();
        nObjects = new ArrayList<>();
    }

    public void addObject(VisibleGameObject obj) {
        synchronized (addQueue) {
            addQueue.add(obj);
            objectAdded = true;
        }
    }

    public void addObject(NonVisibleGameObject obj) {
        synchronized (addNQueue) {
            addNQueue.add(obj);
            objectAdded = true;
        }
    }

    public void removeObject(VisibleGameObject obj) {
        synchronized (objects) {
            objects.remove(obj);
        }
    }

    public void removeObject(NonVisibleGameObject obj) {
        synchronized (nObjects) {
            nObjects.remove(obj);
        }
    }

    public void removeAllObjects() {
        synchronized (objects) {
            objects.clear();
        }
        synchronized (nObjects) {
            nObjects.clear();
        }
    }

    public void setBackgroundResource(int resourceId) {
        backgroundBitmap = BitmapFactory.decodeResource(getResources(), resourceId);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        backgroundBitmap = Bitmap.createScaledBitmap(backgroundBitmap, width, height, true);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mainThread = new MainGameThread(getHolder(), this);
        mainThread.setRunning(true);
        mainThread.start();
        mainThread.useStats(showFPS);
        mainThread.showAverageFPS(useAverage);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        mainThread.setRunning(false);
        while (retry) {
            try {
                mainThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return touchHandler.onTouchEvent(event);
    }

    @Override
    protected void onDraw(Canvas canvas) {
    }

    public void setBackgroundColor(int color) {
        backColor = color;
        paint = new Paint(backColor);
        if (backgroundCanvas != null) {
            backgroundCanvas.drawPaint(paint);
        }
    }

    public void setOnTouchDownListener(OnTouchDown down) {
        downTouch = down;
    }

    public void setOnTouchUpListener(OnTouchUp up) {
        upTouch = up;
    }

    public void setOnDraggedStop(OnDragStop ondragStop) {
        this.onDragStop = ondragStop;
    }

    public void setOnDraggedListener(OnDragged dragged) {
        this.onDrag = dragged;
    }

    public void setEdgeReachedListener(EdgeReached edgeReached) {
        this.edgeReached = edgeReached;
    }

    public void useAverageFPS(boolean useAverage) {
        this.useAverage = useAverage;
        if (mainThread != null) {
            mainThread.showAverageFPS(useAverage);
        }
    }

    public void setAverageFPS(String fps) {
        avgFPS = fps;
    }

    public void showFPS(boolean show) {
        showFPS = show;
        if (mainThread != null) {
            mainThread.useStats(show);
        }
    }

    public void setFPSTextColor(int color) {
        fpsColor = color;
        if (fpsPaint == null) {
            fpsPaint = new Paint();
            fpsPaint.setTextSize(30);
        }
        fpsPaint.setColor(color);
    }

    public void displayFPS(Canvas canvas, String fps) {
        if (canvas != null && fps != null) {
            if (fpsPaint == null) {
                fpsPaint = new Paint();
                fpsPaint.setTextSize(30);
                fpsPaint.setColor(fpsColor);
            }
            canvas.drawText(fps, this.getWidth() - 150, 30, fpsPaint);
        }
    }

    public void edgeReached(VisibleGameObject object, int edge) {
        if (edgeReached != null) {
            edgeReached.edgeReached(object, edge);
        }
    }

    private static List<VisibleGameObject> addQueueObjects(List<VisibleGameObject> queue, List<VisibleGameObject> objects) {
        for (int j = 0; j < queue.size(); j++) {
            VisibleGameObject obj = queue.get(j);
            for (int i = 0; i < objects.size(); i++) {
                if (objects.get(i).getZLayer() > obj.getZLayer()) {
                    objects.add(i, obj);
                    i = objects.size();
                }
            }
            objects.add(obj);
        }
        return objects;
    }

    private static List<NonVisibleGameObject> addNQueueObjects(List<NonVisibleGameObject> queue, List<NonVisibleGameObject> objects) {
        for (int j = 0; j < queue.size(); j++) {
            NonVisibleGameObject obj = queue.get(j);
            for (int i = 0; i < objects.size(); i++) {
                if (objects.get(i).getZLayer() > obj.getZLayer()) {
                    objects.add(i, obj);
                    i = objects.size();
                }
            }
            objects.add(obj);
        }
        return objects;
    }

    public void update() {
        // Update all objects
        long now = System.currentTimeMillis();
        processTouchEvents();
        if (objectAdded) {
            synchronized (addQueue) {
                objects = addQueueObjects(addQueue, objects);
                addQueue.clear();
                objectAdded = false;
            }
            synchronized (addNQueue) {
                nObjects = addNQueueObjects(addNQueue, nObjects);
                addNQueue.clear();
            }
            Collections.sort(objects, objectSorter);
            Collections.sort(nObjects, nObjectSorter);
        }
        if (objectsNeedSorting.compareAndSet(true, false)) {
            for (NonVisibleGameObject obj : nObjects) {
                obj.update(now);
            }
        }
        if (nObjectsNeedSorting.compareAndSet(true, false)) {
            for (VisibleGameObject obj : objects) {
                obj.updateObject(now);
            }
        }
    }

    private final static class LayerSorter implements Comparator<VisibleGameObject> {
        @Override
        public final int compare(VisibleGameObject o1, VisibleGameObject o2) {
            return Integer.valueOf(o1.getZLayer()).compareTo(o2.getZLayer());
        }
    }

    private final static class LayerSorter2 implements Comparator<NonVisibleGameObject> {
        @Override
        public final int compare(NonVisibleGameObject o1, NonVisibleGameObject o2) {
            return Integer.valueOf(o1.getZLayer()).compareTo(o2.getZLayer());
        }
    }

    private void processTouchEvents() {
        synchronized (touchHandler.getLock()) {
            touchHandler.iterate();
            while (touchHandler.hasNext()) {
                touchHandler.processAndRemove(touchParser);
            }
        }
    }

    public void render(Canvas canvas) {
        // Render all objects on screen
        if (canvas != null) {
            canvas.drawBitmap(backgroundBitmap, 0, 0, null);
            for (VisibleGameObject obj : objects) {
                obj.draw(canvas);
            }
            if (showFPS) {
                displayFPS(canvas, avgFPS);
            }
        }
    }

    public void findCollisions(VisibleGameObject movedObject) {
        findCollisions(objects, movedObject);
    }

    public static void findCollisions(List<VisibleGameObject> objects, VisibleGameObject movedObject) {
        for (VisibleGameObject obj : objects) {
            if (obj != movedObject) {
                if (movedObject.collidingWith(obj)) {
                    if (!movedObject.isVisible() || !movedObject.isEnabled() || !obj.isVisible() || !obj.isEnabled() ||
                            !movedObject.getCollisionListener().colliding(obj, movedObject)) {
                        movedObject.getCollisionListener().noLongerCollidingWith(obj);
                    }
                } else {
                    if (movedObject.isVisible() && movedObject.isEnabled() && obj.isVisible() && obj.isEnabled() &&
                            movedObject.getCollisionListener().colliding(obj, movedObject)) {
                        movedObject.getCollisionListener().collidedWith(obj);
                    }
                }
            }
        }
    }

    public void cleanUpTheDead() {
        // Clean up any dead objects
        Iterator<VisibleGameObject> it = objects.iterator();
        while (it.hasNext()) {
            if (it.next().isDead() && objects.size() > 0) {
                it.remove();
            }
        }
    }

    /**
     * Run this to release all memory objects held by the canvas, and it's children. This should be called when the
     * panel is no longer needed (usually in onDestroy).
     */
    public void release() {
        for (VisibleGameObject o : objects) {
            o.release();
        }
        objects.clear();
        touchParser.draggedObjects.clear();
        mainThread.setRunning(false);
    }

    public class TouchParser {
        /**
         * The number of pixels right, left, up, or down, a sequence of drags
         * must move from the starting point to be considered a drag (instead of
         * a touch).
         */
        public static final int TAP_THRESHOLD = 30;

        /**
         * The width of a finger. This is used in determining whether a sprite
         * is touched. Specifically, this is used to determine the horizontal
         * extent of a bounding box that is tested for collision with each
         * sprite. The vertical extent is determined by {@link #FINGER_HEIGHT}.
         */
        public static final int FINGER_WIDTH = 24;

        /**
         * The width of a finger. This is used in determining whether a sprite
         * is touched. Specifically, this is used to determine the vertical
         * extent of a bounding box that is tested for collision with each
         * sprite. The horizontal extent is determined by {@link #FINGER_WIDTH}.
         */
        public static final int FINGER_HEIGHT = 24;

        private static final int HALF_FINGER_WIDTH = FINGER_WIDTH / 2;
        private static final int HALF_FINGER_HEIGHT = FINGER_HEIGHT / 2;
        protected final List<VisibleGameObject> draggedObjects = new ArrayList<>();
        // startX and startY hold the coordinates of where a touch/drag started
        private static final int UNSET = -1;
        private float startX = UNSET;
        private float startY = UNSET;

        // lastX and lastY hold the coordinates of the previous step of a drag
        private float lastX = UNSET;
        private float lastY = UNSET;

        private int width;
        private int height;

        private float x, y;

        private boolean drag = false;
        private Rect touchBox;

        public void parse(MotionEvent event) {
            width = getWidth();
            height = getHeight();
            x = Math.max(0, (int) event.getX());
            y = Math.max(0, (int) event.getY());

            touchBox = newRect(x, y, width, height);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    draggedObjects.clear();
                    startX = x;
                    startY = y;
                    lastX = x;
                    lastY = y;
                    drag = false;
                    throwObjectTouchDownEvents(objects, draggedObjects, touchBox, startX, startY);
                    throwTouchDown(x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    boolean handled = false;
                    // Ensure that this was preceded by an ACTION_DOWN
                    if (startX == UNSET || startY == UNSET || lastX == UNSET || lastY == UNSET) {
                        Log.w("GamePanel", "In Canvas.MotionEventParser.parse(), " + "an ACTION_MOVE was passed without a " +
                                "preceding ACTION_DOWN: " + event);
                    }
                    // If the new point is near the start point, it may just be
                    // a tap
                    if (isTap(startX, startY, x, y)) {
                        break;
                    }
                    // Otherwise, it's a drag.
                    drag = true;
                    // Update draggedSprites by adding any that are currently
                    // being touched.
                    updateDraggedObjects(objects, draggedObjects, touchBox);

                    // Raise a Dragged event for any affected sprites
                    handled = throwObjectDraggedEvents(draggedObjects, startX, startY, lastX, lastY, x, y);

                    // Last argument indicates whether a sprite handled the drag
                    throwDragged(startX, startY, lastX, lastY, x, y, handled);
                    lastX = x;
                    lastY = y;
                    break;
                case MotionEvent.ACTION_UP:
                    // If we never strayed far from the start point, it's a tap.
                    // (If we did stray far, we've already handled the movements in the
                    // ACTION_MOVE case.)
                    if (!drag) {
                        // It's a tap
                        handled = processObjectUpEvent(draggedObjects, startX, startY);
                        // Last argument indicates that one or more sprites
                        // handled the tap
                        throwTouchUp(startX, startY, handled);
                    } else {
                        // Throw the done dragging event
                        throwStoppedDragging();
                    }
                    // Prepare for next drag
                    drag = false;
                    startX = UNSET;
                    startY = UNSET;
                    lastX = UNSET;
                    lastY = UNSET;
                    break;
            }
        }

    }

    private static boolean isTap(float startX, float startY, float x, float y) {
        return (Math.abs(x - startX) < TouchParser.TAP_THRESHOLD && Math.abs(y - startY) < TouchParser.TAP_THRESHOLD);
    }

    private static boolean processObjectUpEvent(List<VisibleGameObject> draggedObjects, float startX, float startY) {
        for (int i = 0; i < draggedObjects.size(); i++) {
            VisibleGameObject object = draggedObjects.get(i);
            if (object.isEnabled() && object.isVisible()) {
                object.throwTouchUp(startX, startY);
                return true;
            }
        }
        return false;
    }

    private void throwStoppedDragging() {
        if (onDragStop != null) {
            onDragStop.onDragStop();
        }
    }

    public void throwTouchDown(float x, float y) {
        if (downTouch != null) {
            downTouch.onTouchDown(x, y);
        }
    }

    public void throwTouchUp(float x, float y, boolean spriteTouched) {
        if (upTouch != null) {
            upTouch.onTouchUp(x, y, spriteTouched);
        }
    }

    public void throwDragged(float startX, float startY, float lastX, float lastY, float curX, float curY, boolean draggedSprite) {
        if (onDrag != null) {
            onDrag.onDragged(startX, startY, lastX, lastY, curX, curY, draggedSprite);
        }
    }

    public void setFPS(int fps) {
        mainThread.setMaxFps(fps);
    }


    void flagVisibleObjectSort() {
        objectsNeedSorting.compareAndSet(false, true);
    }

    void flagNonVisibleObjectSort() {
        nObjectsNeedSorting.compareAndSet(false, true);
    }


    private boolean throwObjectDraggedEvents(List<VisibleGameObject> objects, float startX, float startY, float lastX, float lastY, float curX, float curY) {
        boolean handled = false;
        int size = objects.size();
        for (int i = 0; i < size; i++) {
            VisibleGameObject object = objects.get(i);
            if (object.isEnabled() && object.isVisible()) {
                object.throwDragged(startX, startY, lastX, lastY, curX, curY);
                handled = true;
            }
        }
        return handled;
    }

    private void updateDraggedObjects(List<VisibleGameObject> objects, List<VisibleGameObject> draggedSprites, Rect rect) {
        int size = objects.size();
        for (int i = 0; i < size; i++) {
            VisibleGameObject object = objects.get(i);
            if (!draggedSprites.contains(object) && object.isEnabled() && object.isVisible() && object.intersectsWith(rect)) {
                draggedSprites.add(object);
            }
        }
    }

    private void throwObjectTouchDownEvents(List<VisibleGameObject> objects, List<VisibleGameObject> draggedObjects, Rect boundBox, float x, float y) {
        int size = objects.size();
        for (int i = 0; i < size; i++) {
            VisibleGameObject obj = objects.get(i);
            if (obj.isEnabled() && obj.isVisible() && obj.intersectsWith(boundBox)) {
                draggedObjects.add(obj);
                obj.throwTouchDown(x, y);
            }
        }
    }

    private static Rect newRect(Float x, Float y, int width, int height) {
        return new Rect(Math.max(0, x.intValue() - TouchParser.HALF_FINGER_HEIGHT),
                Math.max(0, y.intValue() - TouchParser.HALF_FINGER_WIDTH), Math.min(width - 1, x.intValue() + TouchParser.HALF_FINGER_WIDTH),
                Math.min(height - 1, y.intValue() + TouchParser.HALF_FINGER_HEIGHT));
    }

}
