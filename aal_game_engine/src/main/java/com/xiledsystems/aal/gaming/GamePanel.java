package com.xiledsystems.aal.gaming;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.internal.Component;

/**
 * Class to use for creating 2D games. This uses a SurfaceView to draw on, and all
 * game operations are processed in the main game thread (update(), checking collisions,
 * etc). The only thing done in the UI thread is actually drawing the objects on screen.
 * The default FPS is set to 60fps (though not all devices will be able to keep up).
 * This is a wrapper class to hide all unnecessary methods. If you want the view backed up
 * by this class, you can call getPanelClass() after instantiating, and it will return
 * an instance of {@link BaseGamePanel}.
 */
@State(value = DevelopmentState.BETA, note = "This class is somewhere between alpha and beta. Some of" +
        "the methods and logic in here are from a production library, but have been changed." +
        "Things may change further, and there are still some things to add in.")
public class GamePanel extends Component {

    private final BaseGamePanel panel;


    /**
     * Use this constructor if you want to place the panel in xml. Make sure you
     * use {@link BaseGamePanel} view in xml.
     * @param context
     * @param panelResId
     */
    public GamePanel(Activity context, int panelResId) {
        super(context);
        panel = context.findViewById(panelResId);
    }

    /**
     * Use this constructor if you want to place the panel in xml, and the panel is in another layout. Make sure you
     * use {@link BaseGamePanel} view in xml.
     * @param context
     * @param parentLayout
     * @param panelResId
     */
    public GamePanel(Activity context, View parentLayout, int panelResId) {
        super(context);
        panel = parentLayout.findViewById(panelResId);
    }

    /**
     * Use this constructor if you are programmatically adding the panel
     * to your Activity's layout.
     * @param context
     */
    public GamePanel(Context context) {
        super(context);
        panel = new BaseGamePanel(context);
    }

    public GamePanel(Context context, AttributeSet attrs) {
        super(context);
        panel = new BaseGamePanel(context, attrs);
    }

    public GamePanel(Context context, AttributeSet attrs, int defStyle) {
        super(context);
        panel = new BaseGamePanel(context, attrs, defStyle);
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new OnCreateDestroyListener() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
            }

            @Override
            public void onDestroy() {
                release();
            }
        };
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return null;
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    public void addObject(VisibleGameObject obj) {
        panel.addObject(obj);
    }

    public void addObject(NonVisibleGameObject obj) {
        panel.addObject(obj);
    }

    public void removeObject(VisibleGameObject obj) {
        panel.removeObject(obj);
    }

    public void removeObject(NonVisibleGameObject obj) {
        panel.removeObject(obj);
    }

    public void removeAllObjects() {
        panel.removeAllObjects();
    }

    public void setBackgroundResource(int resourceId) {
        panel.setBackgroundResource(resourceId);
    }

    public void setBackgroundColor(int color) {
        panel.setBackgroundColor(color);
    }

    public void setOnTouchDownListener(OnTouchDown down) {
        panel.setOnTouchDownListener(down);
    }

    public void setOnTouchUpListener(OnTouchUp up) {
        panel.setOnTouchUpListener(up);
    }

    public void setOnDraggedStop(OnDragStop ondragStop) {
        panel.setOnDraggedStop(ondragStop);
    }

    public void setOnDraggedListener(OnDragged dragged) {
        panel.setOnDraggedListener(dragged);
    }

    public void setEdgeReachedListener(EdgeReached edgeReached) {
        panel.setEdgeReachedListener(edgeReached);
    }

    /**
     * This will display the FPS in the upper right corner of the GamePanel
     * @param showfps
     */
    public void showFPS(boolean showfps) {
        panel.showFPS(showfps);
    }

    /**
     * Run this to release all memory objects held by the canvas, and it's children. This should be called when the
     * panel is no longer needed (usually in onDestroy). This is handled automatically when using in an {@link com.xiledsystems.aal.context.AalActivity}.
     */
    public void release() {
        panel.release();
    }

    /**
     * This sets the attempted FPS rate for the gamepanel. Default is 60.
     * @param fps
     */
    public void setPanelFPS(int fps) {
        panel.setFPS(fps);
    }

    /**
     *
     * @return The view backing this gamepanel
     */
    public BaseGamePanel getPanelView() {
        return panel;
    }

    public int getWidth() {
        return panel.getWidth();
    }

    public int getHeight() {
        return panel.getHeight();
    }

}