package com.xiledsystems.aal.gaming.collision;


import com.xiledsystems.aal.gaming.VisibleGameObject;

public interface BaseCollisionDetector {

    public boolean colliding(VisibleGameObject object1, VisibleGameObject object2);

}
