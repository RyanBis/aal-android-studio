package com.xiledsystems.aal.gaming;


public interface OnTouchDown {
    void onTouchDown(float x, float y);
}
