package com.xiledsystems.aal.gaming.spritesheet;


import org.json.JSONException;

import java.util.List;

public interface SheetParser {
    List<SpriteInfo> parseSheet(String jsonData) throws JSONException;
}
