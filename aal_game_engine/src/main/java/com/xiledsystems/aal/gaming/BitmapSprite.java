package com.xiledsystems.aal.gaming;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.gaming.spritesheet.SpriteSheet;


@State(DevelopmentState.BETA)
public class BitmapSprite extends BaseSprite {

    private Bitmap bitmap;
    // This is used when rotating, so we have the original bitmap to work from
    private Bitmap origBitmap;
    private Rect srcRect;
    private RectF destRec;
    private SpriteSheet spriteSheet;
    private int animTicks = 1;
    private int curTick = 1;
    private int maxFrames;
    private int curFrame = 0;
    private float wDiff = 0;
    private float hDiff = 0;
    private boolean animRepeat = false;
    private boolean dieAfterAnim = false;
    private boolean useSheet;
    private boolean useBitmapSize = true;
    private Matrix matrix;
    private float px, py;


    public BitmapSprite(GamePanel panel) {
        super(panel);
        srcRect = new Rect();
        destRec = new RectF();
        matrix = new Matrix();
    }

    public BitmapSprite(GamePanel panel, Bitmap bitmap) {
        super(panel);
        this.bitmap = bitmap;
        srcRect = new Rect();
        destRec = new RectF();
        setNewSrcRect(srcRect, bitmap);
        setDefWidth(bitmap.getWidth());
        setDefHeight(bitmap.getHeight());
        matrix = new Matrix();
        origBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
    }

    public void setDieAfterAnimation(boolean die) {
        dieAfterAnim = die;
    }

    public boolean willDieAfterAnimation() {
        return dieAfterAnim;
    }

    public void setRepeatAnimation(boolean repeat) {
        animRepeat = repeat;
    }

    public boolean isRepeatAnimation() {
        return animRepeat;
    }

    public void setSpriteSheet(SpriteSheet sheet, int ticksPerFrame) {
        useSheet = true;
        spriteSheet = sheet;
        bitmap = spriteSheet.getSheet();
        if (getWidth() == -1) {
            setDefWidth(spriteSheet.getSpriteInfo(0).getWidth());
        }
        if (getHeight() == -1) {
            setDefHeight(spriteSheet.getSpriteInfo(0).getHeight());
        }
        animTicks = ticksPerFrame;
        maxFrames = spriteSheet.getSpriteCount() - 1;
    }

    public SpriteSheet getSpriteSheet() {
        return spriteSheet;
    }

    public void setBitmap(int resourceId) {
        if (bitmap != null && !bitmap.isRecycled()) {
            try {
                this.bitmap.recycle();
            } catch (Exception e) {
            }
        }
        if (resourceId < 1) {
            Log.e(getClass().getSimpleName(), "Called setBitmap() with non-valid resource id!");
            return;
        }
        bitmap = BitmapFactory.decodeResource(getGamePanel().getPanelView().getResources(), resourceId);
        origBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        setNewSrcRect(srcRect, bitmap);
        if (getWidth() == -1) {
            setDefWidth(bitmap.getWidth());
        }
        if (getHeight() == -1) {
            setDefHeight(bitmap.getHeight());
        }
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        if (this.bitmap != null && !this.bitmap.isRecycled()) {
            try {
                this.bitmap.recycle();
            } catch (Exception e) {
            }
        }
        if (bitmap == null) {
            Log.e(getClass().getSimpleName(), "Called setBitmap() with a null bitmap!");
            return;
        }
        this.bitmap = bitmap;
        origBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        setNewSrcRect(srcRect, bitmap);
        if (getWidth() == -1) {
            setDefWidth(bitmap.getWidth());
        }
        if (getHeight() == -1) {
            setDefHeight(bitmap.getHeight());
        }
    }

    /**
     * This method is the same as setBitmap(), only it does NOT
     * recycle the old bitmap before setting the new one provided.
     *
     * @param bitmap
     */
    public void setBitmapNoRecycle(Bitmap bitmap) {
        this.bitmap = bitmap;
        setNewSrcRect(srcRect, bitmap);
        origBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        if (getWidth() == -1) {
            setDefWidth(bitmap.getWidth());
        }
        if (getHeight() == -1) {
            setDefHeight(bitmap.getHeight());
        }
    }

    public Rect getSrcRect() {
        return srcRect;
    }

    @Override
    public void update(long gameTime) {
        super.update(gameTime);
        if (!isDead()) {
            if (useSheet) {
                if (curTick >= animTicks) {
                    curTick = -1;
                    if (curFrame > maxFrames) {
                        if (!animRepeat) {
                            if (dieAfterAnim) {
                                kill();
                                return;
                            }
                        } else {
                            curFrame = 0;
                        }
                    }
                    bitmap = spriteSheet.getBitmap(curFrame);
                    setNewSrcRect(srcRect, bitmap);
                    wDiff = getCurWidth() - bitmap.getWidth();
                    hDiff = getCurHeight() - bitmap.getHeight();
                    if (wDiff != 0) {
                        adjustWidthSize();
                    }
                    if (hDiff != 0) {
                        adjustHeightSize();
                    }
                    setDefWidth(bitmap.getWidth());
                    setDefHeight(bitmap.getHeight());
                    curFrame++;
                }
                // TODO: Looks like we're adding two ticks here. I'm not sure that was the desired action. Leaving this here
                // in case there's a good reason for it to have been here.
//                else if (curTick < animTicks) {
//                    curTick++;
//                }
                curTick++;
            }
            setDestRect();
            if (isRotating()) {
                checkAndSetBitmapAngle();
            }
        }
    }

    private void adjustWidthSize() {
        setX(getX() + wDiff / 2);
    }

    private void adjustHeightSize() {
        setY(getY() + hDiff / 2);
    }

    private static void setNewSrcRect(Rect srcRect, Bitmap bitmap) {
        srcRect.right = bitmap.getWidth();
        srcRect.bottom = bitmap.getHeight();
    }

    @Override
    public void setRotates(boolean rotate) {
        super.setRotates(rotate);
        if (rotate) {
            checkAndSetBitmapAngle();
        }
    }

    private void checkAndSetBitmapAngle() {
        if (getCachedRotationAngle() != getAngle()) {
            matrix.setRotate(-getAngle().floatValue(), origBitmap.getWidth() / 2, origBitmap.getHeight() / 2);
            bitmap = Bitmap.createBitmap(origBitmap, 0, 0, origBitmap.getWidth(), origBitmap.getHeight(), matrix, true);
            setNewSrcRect(srcRect, bitmap);
            setWidth(bitmap.getWidth());
            setHeight(bitmap.getHeight());
            setRotatedDestRect(destRec, getX(), getY(), halfCurWidth(), halfCurHeight(), bitmap);
            setCachedRotationAngle(getAngle());
        }
    }

    private static void setRotatedDestRect(RectF destRec, float x, float y, float halfCurWidth, float halfCurHeight, Bitmap bitmap) {
        destRec.left = x + halfCurWidth - bitmap.getWidth() / 2;
        destRec.top = y + halfCurHeight - bitmap.getHeight() / 2;
        destRec.right = x + halfCurWidth + bitmap.getWidth() / 2;
        destRec.bottom = y + halfCurHeight + bitmap.getHeight() / 2;
    }

    private float halfCurWidth() {
        return getCurWidth() / 2;
    }

    private float halfCurHeight() {
        return getCurHeight() / 2;
    }

    private float halfSrcWidth() {
        return (srcRect.right - srcRect.left) / 2;
    }

    private float halfSrcHeight() {
        return (srcRect.bottom - srcRect.top) / 2;
    }

    private void setDestRect() {
        destRec.left = getX();
        destRec.right = getX() + getCurWidth();
        destRec.top = getY();
        destRec.bottom = getY() + getCurHeight();
    }

    @Override
    public void draw(Canvas canvas) {
        if (isVisible()) {
            if (bitmap != null) {
                canvas.drawBitmap(bitmap, srcRect, destRec, null);
            }
        }
    }

    @Override
    public void release() {
        if (bitmap != null) {
            if (!bitmap.isRecycled()) {
                try {
                    bitmap.recycle();
                } catch (Exception e) {
                }
            }
            bitmap = null;
        }

    }

}