package com.xiledsystems.aal.gaming.spritesheet;

import android.content.Context;
import android.graphics.Rect;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Class used to read a sprite sheet's JSON data mapping file. This is only tested
 * with sprite sheets mapped with Texture Packer.
 *
 */
@State(DevelopmentState.BETA)
public class SheetReader {

    private static SheetReader reader;

    private SheetReader() {
    }

    @Deprecated
    public static SheetReader getInstance() {
        if (reader == null) {
            reader = new SheetReader();
        }
        return reader;
    }

    @Deprecated
    public List<SpriteInfo> getSpriteLocations(String jsonData) throws JSONException {
        return readSpriteLocations(jsonData);
    }

    public static String loadAssetsFile(Context context, String fileName) throws IOException {
        final InputStream in = context.getAssets().open(fileName);
        final BufferedReader r = new BufferedReader(new InputStreamReader(in));
        final StringBuilder string = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            string.append(line);
        }
        return string.toString();
    }

    public static List<SpriteInfo> readSpriteLocations(SheetParser parser, String jsonData) throws JSONException {
        if (parser != null) {
            return parser.parseSheet(jsonData);
        }
        return new ArrayList<>(0);
    }

    public static List<SpriteInfo> readSpriteLocations(String jsonData) throws JSONException {
        return readSpriteLocations(new TexturePackerParser(), jsonData);
    }

}