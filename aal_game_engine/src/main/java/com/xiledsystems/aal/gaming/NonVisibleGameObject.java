package com.xiledsystems.aal.gaming;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.BETA)
public abstract class NonVisibleGameObject extends GameObject {

    protected NonVisibleGameObject(GamePanel panel) {
        super(panel, false);
        panel.addObject(this);
    }

}
