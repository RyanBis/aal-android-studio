package com.xiledsystems.aal.gaming.collision;


import com.xiledsystems.aal.gaming.VisibleGameObject;


public abstract class SimpleRectangle extends CollisionListener {


    @Override
    public CollisionListener.CollisionType getCollisionType() {
        return CollisionType.SIMPLE_RECT;
    }


    @Override
    public boolean colliding(VisibleGameObject object1, VisibleGameObject object2) {
        if (object2.getCollisionListener() == null || object2.getCollisionListener() instanceof SimpleRectangle) {
            return CollisionProcessor.simpleRectangleToSimpleRect(object1.getRect(1), object2.getRect(1));
        } else if (object2.getCollisionListener() instanceof CircleCollision) {
            return CollisionProcessor.circleToRectangle(object2, object1);
        }
        return false;
    }


}
