package com.xiledsystems.aal.gaming.spritesheet;


import android.graphics.Rect;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;


public class GridParser implements SheetParser {

    private final int verticalSize;
    private final int horizontalSize;
    private final int sheetWidth;
    private final int sheetHeight;
    private final int mStopAtIndex;
    private final int mOffSet;


    /**
     * Overload of {@link GridParser#GridParser(int, int, int, int)}, which uses the same number of
     * columns as rows.
     */
    public GridParser(int size, int width, int height) {
        this(size, size, width, height);
    }

    /**
     * Overload of {@link GridParser#GridParser(int, int, int, int, int)}, which will build a list
     * for every sector of the grid.
     */
    public GridParser(int columns, int rows, int width, int height) {
        this(columns, rows, width, height, Integer.MAX_VALUE);
    }

    /**
     * Build a List of {@link SpriteInfo} from the information given.
     *
     *
     * @param columns The number of columns across
     * @param rows The number of rows down
     * @param width The total width of the sprite sheet
     * @param height The total height of the sprite sheet
     * @param stopAtIndex Stop at this index, and don't make anymore SpriteInfo instances
     */
    public GridParser(int columns, int rows, int width, int height, int stopAtIndex) {
        this(columns, rows, width, height, 0, stopAtIndex);
    }

    /**
     * Build a List of {@link SpriteInfo} from the information given.
     *
     *
     * @param columns The number of columns across
     * @param rows The number of rows down
     * @param width The total width of the sprite sheet
     * @param height The total height of the sprite sheet
     * @param stopAtIndex Stop at this index, and don't make anymore SpriteInfo instances
     */
    public GridParser(int columns, int rows, int width, int height, int offSet, int stopAtIndex) {
        horizontalSize = columns;
        verticalSize = rows;
        sheetHeight = height;
        sheetWidth = width;
        mStopAtIndex = stopAtIndex;
        mOffSet = offSet;
    }

    public int getStopAtIndex() {
        return mStopAtIndex;
    }

    public int getOffSet() {
        return mOffSet;
    }

    @Override
    public List<SpriteInfo> parseSheet(String jsonData) throws JSONException {
        List<SpriteInfo> list = new ArrayList<>();
        int width = sheetWidth / horizontalSize;
        int height = sheetHeight / verticalSize;
        int index = -1;
        int l, t, r, b;
        Rect rec;
        SpriteInfo info;
        for (int y = 0; y < verticalSize; y++) {
            for (int x = 0; x < horizontalSize; x++) {
                index++;

                if (index < mOffSet) continue;

                if (index > mStopAtIndex) {
                    return list;
                }
                info = new SpriteInfo();
                info.setIndex(index);
                info.setFileName(String.valueOf(index));
                info.setHeight(height);
                info.setWidth(width);
                l = x * width;
                t = y * height;
                r = l + width;
                b = t + height;
                rec = new Rect(l, t, r, b);
                info.setRect(rec);
                list.add(info);
            }
        }
        return list;
    }
}
