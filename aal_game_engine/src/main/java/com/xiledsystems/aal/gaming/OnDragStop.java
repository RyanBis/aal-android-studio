package com.xiledsystems.aal.gaming;


public interface OnDragStop {
    void onDragStop();
}
