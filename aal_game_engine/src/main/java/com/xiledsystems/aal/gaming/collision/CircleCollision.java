package com.xiledsystems.aal.gaming.collision;


import com.xiledsystems.aal.gaming.VisibleGameObject;

public abstract class CircleCollision extends CollisionListener {

    @Override
    public CollisionType getCollisionType() {
        return CollisionType.CIRCLE;
    }


    @Override
    public boolean colliding(VisibleGameObject object1, VisibleGameObject object2) {
        if (object2.getCollisionListener() == null || object2.getCollisionListener() instanceof SimpleRectangle) {
            return CollisionProcessor.circleToRectangle(object1, object2);
        } else if (object2.getCollisionListener() instanceof CircleCollision) {
            return CollisionProcessor.circleToCircle(object1, object2);
        }
        return false;
    }
}
