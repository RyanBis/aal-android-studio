package com.xiledsystems.aal.gaming;


public interface OnDragged {
    void onDragged(float startX, float startY, float lastX, float lastY, float curX, float curY, boolean draggedSprite);
}
