package com.xiledsystems.aal.gaming.spritesheet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.util.LruCache;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.context.CoreServiceLifecycleListener;
import com.xiledsystems.aal.context.OnActivityResultListener;
import com.xiledsystems.aal.context.OnCreateDestroyListener;
import com.xiledsystems.aal.context.OnLowMemoryListener;
import com.xiledsystems.aal.context.OnNewIntentListener;
import com.xiledsystems.aal.context.OnRequestPermissionsListener;
import com.xiledsystems.aal.context.OnResumePauseListener;
import com.xiledsystems.aal.context.OnStartStopListener;
import com.xiledsystems.aal.context.ServiceLifecycleListener;
import com.xiledsystems.aal.internal.Component;
import org.json.JSONException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to help with using spritesheets. Use this class to read in a spritesheet, and it's JSON data file
 * generated with Texture Packer. This class holds each sprite in a bitmap cache for better performance. If
 * you want the cache to take up less memory, use {@link #setMemoryDivisor(int)}, or {@link #setCacheMaxSize(int)}.
 * <br><br>
 * Make sure you run the release() method when you are finished using this class (usually it's best
 * in onDestroy to make sure all resources are released when shutting down). This method is automatically
 * run if this SpriteSheet is instantiated in an Activity which extends AalActivity.
 * <br><br>
 * The json file should reside in the project's assets folder.
 *
 */
@State(DevelopmentState.BETA)
public class SpriteSheet extends Component {

    private final static String TAG = "SpriteSheetHelper";

    private Bitmap spriteSheet;
    private List<SpriteInfo> sheetInfo;
    private int spriteCount;
    private final Context context;

    private BitmapCache bitmapCache;
    private final int maxMemory;
    private int cacheSize;
    private int memDivisor = 8;


    public SpriteSheet(Context context, String jsonMapName, int sheetResId) {
        super(context);
        this.context = context;
        sheetInfo = new ArrayList<>();
        spriteSheet = BitmapFactory.decodeResource(context.getResources(), sheetResId);
        loadSheetData(jsonMapName);
        maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        cacheSize = maxMemory / memDivisor;
        bitmapCache = new BitmapCache(cacheSize);
    }

    @Override
    protected OnCreateDestroyListener getCreateDestroyListener() {
        return new OnCreateDestroyListener() {
            @Override
            public void onCreate(Bundle savedInstanceState) {
            }

            @Override
            public void onDestroy() {
                release();
            }
        };
    }

    @Override
    protected OnNewIntentListener getNewIntentListener() {
        return null;
    }

    @Override
    protected OnStartStopListener getStartStopListener() {
        return null;
    }

    @Override
    protected OnLowMemoryListener getLowMemoryListener() {
        return null;
    }

    @Override
    protected OnActivityResultListener getActivityResultListener() {
        return null;
    }

    @Override
    protected OnResumePauseListener getPauseResumeListener() {
        return null;
    }

    @Override
    protected CoreServiceLifecycleListener getServiceLifecycleListener() {
        return new ServiceLifecycleListener() {
            @Override
            public void onDestroy() {
                release();
            }
        };
    }

    @Override
    protected OnRequestPermissionsListener getPermissionsListener() {
        return null;
    }

    /**
     * This sets the maximum memory the cache can use to (maxMemory / memDivisor).
     * The default is 8 (so the cache can use 1/8th the total memory allocated to
     * the app).
     * NOTE: calling this method resets the cache.
     * @param divisor
     */
    public void setMemoryDivisor(int divisor) {
        memDivisor = divisor;
        cacheSize = maxMemory / memDivisor;
        bitmapCache.evictAll();
        bitmapCache = new BitmapCache(cacheSize);
    }

    /**
     * This sets the maximum memory the cache can use to the number you specify.
     * You're best to use the divisor instead, so you know you will always fall under
     * the available memory. This method is here in case you are tracking that manually.
     *
     * NOTE: calling this method resets the cache.
     * @param size
     */
    public void setCacheMaxSize(int size) {
        cacheSize = size;
        bitmapCache.evictAll();
        bitmapCache = new BitmapCache(cacheSize);
    }

    /**
     * This method loads the JSON data file located in the
     * assets directory. Just pass the name of the file.
     *
     * @param fileName - Name of the file in assets to load (must be in json format)
     *
     * @return true if the load was successful, false otherwise
     */
    public boolean loadSheetData(String fileName) {
        String jsonData;
        try {
            jsonData = SheetReader.loadAssetsFile(context, fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        try {
            sheetInfo = SheetReader.readSpriteLocations(jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean loadSheetData(SheetParser parser, String fileName) {
        if (parser == null) return false;

        String jsonData = "";
        if (!(parser instanceof GridParser)) {
            try {
                jsonData = SheetReader.loadAssetsFile(context, fileName);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        try {
            sheetInfo = SheetReader.readSpriteLocations(parser, jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Returns the bitmap located at the frame provided. null is returned if an invalid
     * frame is provided.
     *
     * @param frame
     * @return
     */
    public Bitmap getBitmap(int frame) {
        if (frame < 0 || frame > (sheetInfo.size() - 1)) {
            Log.e(TAG, "Invalid frame number! Frame: " + frame + " Max frame: " + (sheetInfo.size() - 1));
            return null;
        }
        final Bitmap b = bitmapCache.get(frame);
        if (b != null) {
            return b;
        } else {
            SpriteInfo info = sheetInfo.get(frame);
            final Bitmap nb = Bitmap.createBitmap(spriteSheet, info.getRect().left, info.getRect().top, info.getWidth(), info.getHeight());
            bitmapCache.put(frame, nb);
            return nb;
        }
    }

    public Rect getSrcRect(int frame, Rect srcRect) {
        if (frame < 0 || frame > (sheetInfo.size() - 1)) {
            Log.e(TAG, "Invalid frame number! Frame: " + frame + " Max frame: " + (sheetInfo.size() - 1));
            return null;
        }
        srcRect.set(sheetInfo.get(frame).getRect());
        return srcRect;
    }

    /**
     * Returns a bitmap which had the original filename provided. This filename represents the
     * name of the file BEFORE it got added to the sprite sheet. This is helpful if you don't
     * know the frame number of a particular bitmap within the sprite sheet.
     *
     * @param fileName
     * @return
     */
    public Bitmap getBitmap(String fileName) {
        for (SpriteInfo info : sheetInfo) {
            if (info.getName().equals(fileName)) {
                final Bitmap b = bitmapCache.get(info.getIndex());
                if (b != null && !b.isRecycled()) {
                    return b;
                } else {
                    final Bitmap nb = Bitmap.createBitmap(spriteSheet, info.getRect().left, info.getRect().top, info.getWidth(), info.getHeight());
                    bitmapCache.put(info.getIndex(), nb);
                    return nb;
                }
            }
        }
        return null;
    }

    /**
     * This will return the order of frames. (First frame, is the first
     * item in the list -- index 0). You can then reorder this list,
     * and pass it back to reorder the frame list.
     *
     * @return arraylist of the filenames in the sprite sheet. This is in order of frames.
     */
    public ArrayList<String> frameOrder() {
        ArrayList<String> list = new ArrayList<String>();
        int size = sheetInfo.size();
        for ( int i = 0; i < size; i++) {
            list.add(sheetInfo.get(i).getName());
        }
        return list;
    }

    /**
     * Use this method to reorder the frame numbers. Pass an arraylist
     * of the filenames in the order you'd like them to be in. This will
     * then reorder the frame numbers to the order of the list you pass
     * in. The backing cache will be reset when calling this method.
     *
     * @param names Arraylist of filenames in order
     */
    public void setFrameOrder(ArrayList<String> names) {
        if (names.size() != sheetInfo.size()) {
            Log.e(TAG, "The names list is not the same length as the length of frames!");
            return;
        }
        // Copy the current order into a new temporary list
        ArrayList<SpriteInfo> tmplist = new ArrayList<SpriteInfo>(sheetInfo);
        int size = sheetInfo.size();

        ArrayList<String> oldOrder = new ArrayList<String>();
        for (int i = 0; i < size; i++) {
            oldOrder.add(tmplist.get(i).getName());
        }

        // Clear the list before we start adding to it.
        sheetInfo.clear();
        for ( int i = 0; i < size; i++) {

            int oldindex=-2;

            // We have to iterate through the tmp list to find the index
            // of the name given
            for (int x = 0; x < size; x++) {
                if (oldOrder.get(x).equalsIgnoreCase(names.get(i))) {
                    oldindex = x;
                    break;
                }
            }
            if (oldindex == -2) {
                Log.e(TAG, "Name was not found. Make sure your list is accurate.");
                return;
            }
            sheetInfo.add(tmplist.get(oldindex).setIndex(i));
        }
        spriteCount = sheetInfo.size();
        bitmapCache.evictAll();
        bitmapCache = new BitmapCache(cacheSize);
    }

    public int getSpriteCount() {
        return spriteCount;
    }

    public SpriteInfo getSpriteInfo(int frame) {
        return sheetInfo.get(frame);
    }

    public Bitmap getSheet() {
        return spriteSheet;
    }

    /**
     * Run this to release resources held by the SpriteSheetHelper. Run this when you
     * no longer need the helper (usually in onDestroy).
     */
    public void release() {
        if (spriteSheet != null && !spriteSheet.isRecycled()) {
            spriteSheet.recycle();
        }
        spriteCount = 0;
        sheetInfo.clear();
    }

    private static final class BitmapCache extends LruCache<Integer, Bitmap> {

        BitmapCache(int maxSize) {
            super(maxSize);
        }

        @Override
        protected final int sizeOf(Integer frame, Bitmap bitmap) {
            return bitmap.getByteCount() / 1024;
        }
    }

}
