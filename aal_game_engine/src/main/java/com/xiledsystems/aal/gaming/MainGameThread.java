package com.xiledsystems.aal.gaming;

import java.text.DecimalFormat;
import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceHolder;


public class MainGameThread extends Thread {
	
	
	private final static String LOG = "MainGameThread";
	
	private static int MAX_FPS = 60;
	private static int MAX_FRAME_SKIPS = 5;
	private static int FRAME_PERIOD = 1000/MAX_FPS;
	
	private DecimalFormat df = new DecimalFormat("#");
	private boolean running;
	private final SurfaceHolder surfaceHolder;
	private final BaseGamePanel gamePanel;
	private boolean logging = false;
	private boolean stats = false;
	private long lastStore = 0;
	private long statusIntervalCounter = 0;
	private long framesSkippedTotal = 0;
	private long framesSkippedPerCycle = 0;
	private int frameCountPerCycle = 0;
	private long totalFrameCount = 0;
	private int statInterval = 1000;
	private int statHistoryLength = 10;
	private double[] fpsArray;
	private long statCount;
	private double averageFPS;
    private boolean showAverage;
	
	
	public MainGameThread(SurfaceHolder sHolder, BaseGamePanel panel) {
		surfaceHolder = sHolder;
		gamePanel = panel;
	}
			
	public int getStatInterval() {
		return statInterval;
	}

	public void setStatInterval(int statInterval) {
		this.statInterval = statInterval;
	}

	public int getStatHistoryLength() {
		return statHistoryLength;
	}

	public void setStatHistoryLength(int statHistoryLength) {
		this.statHistoryLength = statHistoryLength;
	}

	public boolean isLogging() {
		return logging;
	}

	public void setLogging(boolean logging) {
		this.logging = logging;
	}

	public boolean useStats() {
		return stats;
	}

	public void useStats(boolean stats) {
		this.stats = stats;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public boolean isRunning() {
		return running;
	}	

	public long getTotalFramesSkipped() {
		return framesSkippedTotal;
	}

	public long getTotalFrames() {
		return totalFrameCount;
	}





	public void showAverageFPS(boolean showAverage) {
		this.showAverage = showAverage;
	}

	@Override
	public void run() {
		Canvas canvas;
		if (logging) {
			Log.d(LOG, "Starting main game thread");
		}
		long begin;
		int sleep;
		int skipped;
		initArray();
		while(running) {
			canvas = null;
			try {
                begin = System.currentTimeMillis();
                skipped = 0;
                gamePanel.update();
				canvas = surfaceHolder.lockCanvas();
				synchronized (surfaceHolder) {
					gamePanel.render(canvas);
				}
			} finally {
				if (canvas != null) {
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
            gamePanel.cleanUpTheDead();
            sleep = (int) (FRAME_PERIOD - (System.currentTimeMillis() - begin));
            if (sleep > 0) {
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                }
            }
            while (sleep < 0 && skipped < MAX_FRAME_SKIPS) {
                gamePanel.update();
                sleep += FRAME_PERIOD;
                skipped++;
            }
            if (logging && skipped > 0) {
                Log.d(LOG, "Frames skipped: " + String.valueOf(skipped));
            }
            if (stats) {
                framesSkippedPerCycle += skipped;
                checkStats();
            }
        }
	}




	private void checkStats() {
		frameCountPerCycle++;
		totalFrameCount++;
        long now = System.currentTimeMillis();
		statusIntervalCounter += (now - statusIntervalCounter);
		if (statusIntervalCounter >= lastStore + statInterval) {
			statCount++;
			framesSkippedTotal += framesSkippedPerCycle;
			framesSkippedPerCycle = 0;
			statusIntervalCounter = 0;
			frameCountPerCycle = 0;
			statusIntervalCounter = now;
			lastStore = statusIntervalCounter;
            if (showAverage) {
                fpsArray[(int) statCount % statHistoryLength] = getcurFPS(frameCountPerCycle, statInterval);
                averageFPS = getAverageFPS(0d, fpsArray, statHistoryLength, statCount);
            } else {
                averageFPS = getcurFPS(frameCountPerCycle, statInterval);
            }
			gamePanel.setAverageFPS("FPS: " + df.format(averageFPS));
		}
	}



	public static int getMaxFps() {
		return MAX_FPS;
	}

	public static void setMaxFps(int mAX_FPS) {
		MAX_FPS = mAX_FPS;
	}

	public static int getMaxFrameSkips() {
		return MAX_FRAME_SKIPS;
	}

	public static void setMaxFrameSkips(int mAX_FRAME_SKIPS) {
		MAX_FRAME_SKIPS = mAX_FRAME_SKIPS;
	}




    private static double getAverageFPS(double tFps, double[] fpsArray, int statHistoryLength, long statCount) {
        for (int i = 0; i < statHistoryLength; i++) {
            tFps += fpsArray[i];
        }
        if (statCount < statHistoryLength) {
            return tFps / statCount;
        } else {
            return tFps / statHistoryLength;
        }
    }

    private static double getcurFPS(int frameCountPerCycle, int statInterval) {
        return (double) (frameCountPerCycle / (statInterval / 1000));
    }
	
	private void initArray() {
		fpsArray = new double[statHistoryLength];
		for (int i = 0; i < statHistoryLength; i++) {
			fpsArray[i] = 0.0;
		}
	}

}