package com.xiledsystems.aal.gaming.spritesheet;


import android.graphics.Rect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TexturePackerParser implements SheetParser {

    @Override
    public List<SpriteInfo> parseSheet(String jsonData) throws JSONException {
        List<SpriteInfo> spriteLocs = new ArrayList<>();
        JSONObject mainObj = new JSONObject(jsonData);
        JSONArray frames = mainObj.getJSONArray("frames");
        int size = frames.length();
        for (int i = 0; i < size; i++) {
            JSONObject frame = frames.getJSONObject(i);
            SpriteInfo info = new SpriteInfo();
            String file = frame.getString("filename");
            info.setFileName(file);
            JSONObject file_frame = frame.getJSONObject("frame");
            int width = file_frame.getInt("w");
            int height = file_frame.getInt("h");
            Rect r = new Rect();
            r.left = file_frame.getInt("x");
            r.top = file_frame.getInt("y");
            r.bottom = r.top + height;
            r.right = r.left + width;
            info.setRect(r);
            info.setWidth(width);
            info.setHeight(height);
            info.setIndex(i);
            spriteLocs.add(info);
        }
        return spriteLocs;
    }
}
