package com.xiledsystems.aal.gaming;

import java.util.HashSet;
import java.util.Set;
import android.graphics.Canvas;
import android.graphics.Rect;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;
import com.xiledsystems.aal.gaming.collision.CollisionListener;


/**
 * Base visible object class. This class holds the coordinates of the instance, it's radius (if applicable), and it's width, and height.
 * It also holds an instance of {@link CollisionListener}, for detecting collisions with other {@link VisibleGameObject}s.
 * You also get a touch handler, with {@link TouchListener} to listen for all touch events.
 */
@State(DevelopmentState.BETA)
public abstract class VisibleGameObject extends GameObject {

    private float width = -1;
    private float height = -1;
    private float defWidth, defHeight;
    private float x, y;
    private boolean enabled;
    private TouchListener touchListener;
    private boolean visible = true;
    private float radius;

    private CollisionListener collisionListener;
    private Set<VisibleGameObject> registeredCollisions = new HashSet<>();


    public abstract void draw(Canvas canvas);


    public VisibleGameObject(GamePanel panel) {
        super(panel, true);
        panel.addObject(this);
    }

    public void setDefWidth(float width) {
        defWidth = width;
    }

    public float getDefWidth() {
        return defWidth;
    }

    public void setDefHeight(float height) {
        defHeight = height;
    }

    /**
     * This is used when you want to use circle collision detection
     * for this object.
     * @param radius
     */
    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getRadius() {
        return radius;
    }

    public float getDefHeight() {
        return defHeight;
    }

    public float getCurWidth() {
        if (getWidth() == -1) {
            return defWidth;
        } else {
            return getWidth();
        }
    }

    public float getCurHeight() {
        if (getHeight() == -1) {
            return defHeight;
        } else {
            return getHeight();
        }
    }

    public void setCollisionListener(CollisionListener listener) {
        collisionListener = listener;
    }

    public CollisionListener getCollisionListener() {
        return collisionListener;
    }

    public void setTouchListener(TouchListener listener) {
        touchListener = listener;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getX() {
        return x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getY() {
        return y;
    }

    public void setLocation(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getWidth() {
        return width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getHeight() {
        return height;
    }

    public void updateObject(long gameTime) {
        if (enabled) {
            update(gameTime);
        }
    }

    public void throwDragged(float startX, float startY, float lastX, float lastY, float curX, float curY) {
        if (touchListener != null) {
            touchListener.onDragged(this, startX, startY, lastX, lastY, curX, curY);
        }
    }

    public void throwTouchDown(float x, float y) {
        if (touchListener != null) {
            touchListener.onTouchDown(this, x, y);
        }
    }

    public void throwTouchUp(float x, float y) {
        if (touchListener != null) {
            touchListener.onTouchUp(this, x, y);
        }
    }


    public boolean collidingWith(VisibleGameObject object) {
        return registeredCollisions.contains(object);
    }

    /**
     * Determines whether this object intersects with the given rectangle.
     *
     * @param rect the rectangle
     * @return {@code true} if they intersect, {@code false} otherwise
     */
    public boolean intersectsWith(Rect rect) {
        return getRect(0).intersect(rect);
    }

    /**
     * Indicates whether the specified point is contained by this sprite.
     * Subclasses of Sprite that are not rectangular should override this method.
     *
     * @param qx the x-coordinate
     * @param qy the y-coordinate
     * @return whether (qx, qy) falls within this object
     */
    public static boolean containsPoint(double qx, double qy, float curX, float curY, float curWidth, float curHeight) {
        return qx >= curX && qx < curX + curWidth &&
                qy >= curY && qy < curY + curHeight;
    }

    public Rect getRect(int border) {
        return new Rect((int) getX() - border, (int) getY() - border,
                (int) (getX() + getCurWidth() - 1 + border), (int) (getY() + getCurHeight() - 1 + border));
    }



    public interface TouchListener {
        void onTouchDown(VisibleGameObject object, float x, float y);
        void onTouchUp(VisibleGameObject object, float x, float y);
        void onDragged(VisibleGameObject object, float startX, float startY, float lastX, float lastY, float curX, float curY);
    }
	
}
