package com.xiledsystems.aal.gaming;


public interface EdgeReached {
    void edgeReached(VisibleGameObject object, int edge);
}
