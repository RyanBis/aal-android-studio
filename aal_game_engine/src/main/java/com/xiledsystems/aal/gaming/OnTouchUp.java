package com.xiledsystems.aal.gaming;


public interface OnTouchUp {
    void onTouchUp(float x, float y, boolean spriteTouched);
}
