package com.xiledsystems.aal.gaming;


import android.graphics.Canvas;
import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Base object class for all objects to be added into a {@link GamePanel} instance. This can include visible, and non-visible objects.
 * Usually, you'll want to use {@link BitmapSprite} for visible objects -- though if you want to implement your own logic in the
 * draw method, you can subclass {@link BaseSprite} instead, and override {@link BaseSprite#draw(Canvas)}.
 * {@link NonVisibleGameObject}s can be used to run logic in the game's update thread loop. Each object will have it's {@link #update(long)}
 * method called on each tick of the game loop. You can use {@link #setZLayer(int)} to control the order in which these objects are called. All
 * visible objects have their update methods called, <i>then</i> the non-visible ones.
 */
@State(DevelopmentState.BETA)
public abstract class GameObject {

    private boolean dead = false;
    private final GamePanel panel;
    private int z;
    // Boolean which represents if this object can ever be visible in the game panel or not
    private final boolean visibleObject;


    protected abstract void update(long gameTime);
    public abstract void release();


    protected GameObject(GamePanel panel, boolean visibleObject) {
        this.panel = panel;
        this.visibleObject = visibleObject;
    }

    /**
     * Get the {@link BaseGamePanel} instance this object is a part of.
     */
    public BaseGamePanel getGamePanelView() {
        return panel.getPanelView();
    }

    /**
     * Get the {@link GamePanel} instance this object is a part of.
     */
    public GamePanel getGamePanel() {
        return panel;
    }

    /**
     * Returns the z layer of this object. This allows ordering of game objects' {@link #update(long)}, and
     * {@link VisibleGameObject#draw(Canvas)} (if applicable) method calls.
     */
    public final int getZLayer() {
        return z;
    }

    /**
     * Set the z layer of this object.
     */
    public final void setZLayer(int zLayer) {
        z = zLayer;
        if (visibleObject)
            getGamePanelView().flagVisibleObjectSort();
        else
            getGamePanelView().flagNonVisibleObjectSort();
    }

    /**
     * Kill this object. This will remove the object from the {@link GamePanel}'s update loop, so it won't call
     * {@link #update(long)}, or any other method.
     */
    public void kill() {
        dead = true;
    }

    /**
     * Returns whether this object is dead or not.
     */
    public boolean isDead() {
        return dead;
    }
}
