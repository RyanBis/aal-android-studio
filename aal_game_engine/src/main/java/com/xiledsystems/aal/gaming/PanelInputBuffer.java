package com.xiledsystems.aal.gaming;

import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;


public class PanelInputBuffer {

    private List<MotionEvent> touchEvents = new ArrayList<>();
    private final static Object lock = new Object();
    private Iterator<MotionEvent> iterator;



    public boolean onTouchEvent(MotionEvent motionEvent) {
        synchronized (lock) {
            touchEvents.add(MotionEvent.obtain(motionEvent));
        }
        return true;
    }

    public void iterate() {
        iterator = touchEvents.iterator();
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public MotionEvent processAndRemove(BaseGamePanel.TouchParser parser) {
        MotionEvent e = iterator.next();
        parser.parse(e);
        iterator.remove();
        return e;
    }

    public synchronized Object getLock() {
        return lock;
    }

    public List<MotionEvent> getTouchEvents() {
        return touchEvents;
    }
}
