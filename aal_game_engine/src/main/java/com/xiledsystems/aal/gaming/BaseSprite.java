package com.xiledsystems.aal.gaming;


import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;


@State(DevelopmentState.BETA)
public abstract class BaseSprite extends VisibleGameObject {

    static final int DIRECTION_NORTH = 1;
    static final int DIRECTION_NORTHEAST = 2;
    static final int DIRECTION_EAST = 3;
    static final int DIRECTION_SOUTHEAST = 4;
    static final int DIRECTION_SOUTH = -1;
    static final int DIRECTION_SOUTHWEST = -2;
    static final int DIRECTION_WEST = -3;
    static final int DIRECTION_NORTHWEST = -4;
    // Special values
    static final int DIRECTION_NONE = 0;
    static final int DIRECTION_MIN = -4;
    static final int DIRECTION_MAX = 4;

    private Integer canvasWidth;
    private Integer canvasHeight;

    private float speed;
    private double angle = 0;
    private double cachedRotationAngle;
    private double heading;
    private double headingRadians;  // heading in radians
    private double headingCos;      // cosine(heading)
    private double headingSin;

    private boolean keepInBounds = true;
    private boolean checkForEdges = false;

    private boolean west, east, north, south;
    private boolean rotates = false;
    private int edgeDirection = DIRECTION_NONE;


    public BaseSprite(GamePanel panel) {
        super(panel);
        setAngle(angle);
    }

    public void setAngle(double degrees) {
        angle = degrees;
        heading = -degrees;
        headingRadians = Math.toRadians(heading);
        headingCos = Math.cos(headingRadians);
        headingSin = Math.sin(headingRadians);
    }

    public void setRotates(boolean rotate) {
        rotates = rotate;
    }

    public boolean isRotating() {
        return rotates;
    }

    public double getCachedRotationAngle() {
        return cachedRotationAngle;
    }

    public void setCachedRotationAngle(double rotationAngle) {
        cachedRotationAngle = rotationAngle;
    }

    public Double getAngle() {
        return angle;
    }

    public boolean checkForEdges() {
        return checkForEdges;
    }

    public void checkForEdges(boolean check) {
        checkForEdges = check;
    }

    public void pointAt(VisibleGameObject object) {
        setAngle(calcAngleToPointAt(object, this));
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }

    protected final int hitEdge(int canvasWidth, int canvasHeight) {
        // Determine in which direction(s) we are out of bounds, if any.
        // Note that more than one boolean value can be true.  For example, if
        // the sprite is past the northwest boundary, north and west will be true.
        west = overWestEdge();
        north = overNorthEdge();
        east = overEastEdge(canvasWidth);
        south = overSouthEdge(canvasHeight);

        // If no edge was hit, return.
        if (!(north || south || east || west)) {
            return DIRECTION_NONE;
        }

        // Move the sprite back into bounds.  Note that we don't just reverse the
        // last move, since that might have been multiple pixels, and we'd only need
        // to undo part of it.
        if (keepInBounds) {
            moveIntoBounds(canvasWidth, canvasHeight);
        }

        // Determine the appropriate return value.
        if (west) {
            if (north)
                return DIRECTION_NORTHWEST;
            else if (south)
                return DIRECTION_SOUTHWEST;
            else
                return DIRECTION_WEST;
        }
        if (east) {
            if (north)
                return DIRECTION_NORTHEAST;
            else if (south)
                return DIRECTION_SOUTHEAST;
            else
                return DIRECTION_EAST;
        }
        if (north)
            return DIRECTION_NORTH;
        if (south)
            return DIRECTION_SOUTH;

        throw new AssertionError("Unreachable code hit in SpriteComponent.hitEdge()");
    }

    protected final void moveIntoBounds(int canvasWidth, int canvasHeight) {
        // Check if the sprite is too wide to fit on the canvas.
        if (getCurWidth() > canvasWidth) {
            // Sprite is too wide to fit. If it isn't already at the left edge, move it there.
            if (getX() != 0) {
                setX(0);
            }
        } else if (overWestEdge()) {
            setX(0);
        } else if (overEastEdge(canvasWidth)) {
            setX(canvasWidth - getCurWidth());
        }

        // Check if the sprite is too tall to fit on the canvas. We don't want to cause a stack
        // overflow by moving the sprite to the top edge and then to the bottom edge, repeatedly.
        if (getCurHeight() > canvasHeight) {
            // Sprite is too tall to fit. If it isn't already at the top edge, move it there.
            if (getY() != 0) {
                setY(0);
            }
        } else if (overNorthEdge()) {
            setY(0);
        } else if (overSouthEdge(canvasHeight)) {
            setY(canvasHeight - getCurHeight());
        }
    }

    protected final boolean overWestEdge() {
        return getX() < 0;
    }

    protected final boolean overEastEdge(int canvasWidth) {
        return getX() + getCurWidth() > canvasWidth;
    }

    protected final boolean overNorthEdge() {
        return getY() < 0;
    }

    protected final boolean overSouthEdge(int canvasHeight) {
        return getY() + getCurHeight() > canvasHeight;
    }

    protected void updateCoordinates() {
        setX(calcNewCoordinate(getX(), speed, headingCos));
        setY(calcNewCoordinate(getY(), speed, headingSin));
    }

    public void bounce(int direction) {
        moveIntoBounds(canvasWidth, canvasHeight);

        double normalizedAngle = normalize(angle);
        // Only transform heading if sprite was moving in that direction.
        // This avoids oscillations.
        if ((direction == DIRECTION_EAST
                && (normalizedAngle < 90 || normalizedAngle > 270))
                || (direction == DIRECTION_WEST
                && (normalizedAngle > 90 && normalizedAngle < 270))) {
            setAngle(180 - normalizedAngle);
        } else if ((direction == DIRECTION_NORTH
                && normalizedAngle > 0 && normalizedAngle < 180)
                || (direction == DIRECTION_SOUTH && normalizedAngle > 180)) {
            setAngle(360 - normalizedAngle);
        } else if ((direction == DIRECTION_NORTHEAST
                && normalizedAngle > 0 && normalizedAngle < 90)
                || (direction == DIRECTION_NORTHWEST
                && normalizedAngle > 90 && normalizedAngle < 180)
                || (direction == DIRECTION_SOUTHWEST
                && normalizedAngle > 180 && normalizedAngle < 270)
                || (direction == DIRECTION_SOUTHEAST && normalizedAngle > 270)) {
            setAngle(180 + normalizedAngle);
        }
    }

    public void keepInBounds(boolean keepIn) {
        keepInBounds = keepIn;
    }

    public boolean isKeptInbounds() {
        return keepInBounds;
    }

    @Override
    public void update(long gameTime) {
        if (canvasWidth == null || canvasHeight == null) {
            canvasWidth = getGamePanel().getWidth();
            canvasHeight = getGamePanel().getHeight();
        }
        updateCoordinates();
        if (getCollisionListener() != null) {
            getGamePanelView().findCollisions(this);
        }
        if (checkForEdges) {
            edgeDirection = hitEdge(canvasWidth, canvasHeight);
            if (edgeDirection != DIRECTION_NONE) {
                getGamePanelView().edgeReached(this, hitEdge(canvasWidth, canvasHeight));
            }
        }
        if (keepInBounds) {
            moveIntoBounds(canvasWidth, canvasHeight);
        }

    }



    private static double calcAngleToPointAt(VisibleGameObject object, VisibleGameObject thisObject) {
        return -Math.toDegrees(Math.atan2(
                object.getY() - thisObject.getY() + (object.getCurHeight() - thisObject.getCurHeight()) / 2,
                object.getX() - thisObject.getX() + (object.getCurWidth() - thisObject.getCurWidth()) / 2));
    }

    private static float calcNewCoordinate(float val, float speed, Double heading) {
        return val + speed * heading.floatValue();
    }

    private static double normalize(double angle) {
        angle = angle % 360;
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }
}
