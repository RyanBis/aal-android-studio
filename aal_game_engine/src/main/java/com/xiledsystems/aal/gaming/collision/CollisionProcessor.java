package com.xiledsystems.aal.gaming.collision;


import android.graphics.Rect;

import com.xiledsystems.aal.gaming.VisibleGameObject;


public class CollisionProcessor {

    private CollisionProcessor() {
    }

    private static double tmpX, tmpY, tmpSq;

    public static boolean simpleRectangleToSimpleRect(Rect rect1, Rect rect2) {
        return rect1.intersect(rect2);
    }

    public static boolean circleToCircle(VisibleGameObject obj, VisibleGameObject obj2) {
        return (obj2.getX() - obj.getX()) * (obj2.getX() - obj.getX()) +
                (obj2.getY() - obj.getY()) * (obj2.getY() - obj.getY()) <=
                (obj.getRadius() + obj2.getRadius()) * (obj.getRadius() + obj2.getRadius());
    }

    public static boolean circleToRectangle(VisibleGameObject circle, VisibleGameObject rectangle) {
        tmpX = Math.abs(circle.getX() - rectangle.getX());
        tmpY = Math.abs(circle.getY() - rectangle.getY());
        if (tmpX > (rectangle.getWidth() / 2 + circle.getRadius()) || tmpY > (rectangle.getHeight() / 2 + circle.getRadius())) {
            return false;
        }
        if (tmpX < (rectangle.getWidth() / 2) || tmpY < (rectangle.getHeight() / 2)) {
            return true;
        }
        tmpSq = Math.pow(tmpX - rectangle.getWidth() / 2, 2) + Math.pow(tmpY - rectangle.getHeight() / 2, 2);
        return tmpSq <= Math.pow(circle.getRadius(), 2);
    }

}
