package com.xiledsystems.aal.gaming;


public interface OnMove {
    void onMove(float x, float y);
}
