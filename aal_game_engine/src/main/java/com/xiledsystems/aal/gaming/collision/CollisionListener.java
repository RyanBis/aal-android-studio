package com.xiledsystems.aal.gaming.collision;


import com.xiledsystems.aal.gaming.VisibleGameObject;

public abstract class CollisionListener implements BaseCollisionDetector {

    public enum CollisionType {
        SIMPLE_RECT, CIRCLE, RECT
    }

    private CollisionType cType;

    public void setCollistionType() {
        cType = getCollisionType();
    }

    public abstract CollisionType getCollisionType();
    public abstract void collidedWith(VisibleGameObject object);
    public abstract void noLongerCollidingWith(VisibleGameObject object);

    public CollisionListener() {
        setCollistionType();
    }

}
