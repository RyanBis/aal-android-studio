package com.xiledsystems.aal.gaming.spritesheet;

import android.graphics.Rect;

import com.xiledsystems.aal.annotations.DevelopmentState;
import com.xiledsystems.aal.annotations.State;

/**
 * Class for storing info on a sprite within a spritesheet.
 */
@State(DevelopmentState.BETA)
public class SpriteInfo {

    private Rect srcRec;
    private int width;
    private int height;
    private int index;
    private String fileName;

    public void setRect(Rect rect) {
        this.srcRec = rect;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setFileName(String filename) {
        this.fileName = filename;
    }

    public Rect getRect() {
        return srcRec;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getName() {
        return fileName;
    }

    public int getIndex() {
        return index;
    }

    public SpriteInfo setIndex(int index) {
        this.index = index;
        return this;
    }
}
